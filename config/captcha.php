<?php
/*
 * Secret key and Site key get on https://www.google.com/recaptcha
 * */
return [
    'enable'    => env('CAPTCHA_ENABLE', true),
//    'secret' => env('CAPTCHA_SECRET', 'default_secret'),
//    'sitekey' => env('CAPTCHA_SITEKEY', 'default_sitekey'),

    // https://github.com/mewebstudio/captcha
    'default'   => [
        'length'    => 5,
        'width'     => 200,
        'height'    => 96,
        'quality'   => 10,
    ],
];