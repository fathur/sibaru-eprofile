var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
    return gulp.src([
        './resources/assets/sass/app.scss',
        './resources/assets/sass/bootstrap.scss',
        './resources/assets/sass/login.scss'
    ])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
            'resources/assets/js/metronic/app.js',
            'resources/assets/js/metronic/layout.js',
            'resources/assets/js/metronic/quick-sidebar.js',
            'resources/assets/js/metronic/quick-nav.js',
            'resources/assets/js/helper.js'
    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('default', function () {
    //gulp.watch('./resource/assets/sass/**/*.scss', function () {
    gulp.run('sass');
    //});
});
