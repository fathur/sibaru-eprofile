<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 31/07/18
 * Time: 04.03
 */

namespace App\Http\Controllers;


use App\Notifications\Proposal\ProposalDraftToNew;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = \Auth::user()->unreadNotifications;
        return view('notification.index', compact('notifications'));
    }

    public function show($id)
    {
        $notification = \Auth::user()->unreadNotifications()->find($id);

        $notification->markAsRead();


        return redirect()->route('proposals.edit', $notification->data['proposal']['id']);
    }

    public function history()
    {
        $notifications = \Auth::user()->notifications->take(100);
        return view('notification.history', compact('notifications'));
    }
}