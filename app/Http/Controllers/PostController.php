<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 8/20/17
 * Time: 22:05
 */

namespace App\Http\Controllers;


use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
        return view('post.create');

    }

    public function store(Request $request)
    {
        $post = new Post();

        $post->title = $request->get('title');
        $post->slug = str_slug($request->get('title'));
        $post->content = $request->get('content');
        $post->save();

        return redirect()->route('posts.edit', $post->id);
    }

    /**
     * @param integer|string $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (is_integer($id)) {
            $post = Post::find($id);
        } elseif (is_string($id)) {
            $post = Post::whereSlug($id)->first();
        }

        if (is_null($post)) {
            return redirect()->route('posts.create');
        }

        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $post = Post::find($id);
        $post->title = $request->get('title');
//        $post->slug = str_slug($request->get('title'));
        $post->content = $request->get('content');
        $post->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        return Post::destroy($id);
    }
}