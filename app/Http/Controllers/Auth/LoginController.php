<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/usulan';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('public.login');
    }

    protected function validateLogin(Request $request)
    {
        if (config('captcha.enable')) {
            $this->validate($request, [
                $this->username() => 'required|string',
                'password' => 'required|string',
                'captcha' => 'required|captcha'
//            'g-recaptcha-response' => 'required|captcha'
            ], [
//            'g-recaptcha-response.required' => 'Captcha harus diisi.',
//            'g-recaptcha-response.captcha' => 'Captcha yang dimasukkan tidak valid.',
                'captcha.required' => 'Captcha harus diisi.',
                'captcha.captcha' => 'Captcha belum tepat.'
            ]);
        } else {
            $this->validate($request, [
                $this->username() => 'required|string',
                'password' => 'required|string',
            ]);
        }

    }

    public function username()
    {
        return 'username';
    }

    public function redirectTo()
    {
        $user = \Auth::user();

        if ($user->role->slug == 'admin-central' || $user->role->slug == 'admin-technical') {
            return '/dashboard';
        }

        return $this->redirectTo;

    }
}
