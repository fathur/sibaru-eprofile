<?php

namespace App\Http\Controllers;

use App\Models\DwellingComponent;
use App\Models\DwellingPlan;
use App\Models\DwellingTypes;
use App\Models\Proposal;
use App\Models\UnitSize;
use App\Models\UnitTypes;
use Illuminate\Http\Request;

class DwellingPlanController extends Controller
{
    protected $identifier = 'dwelling-plan';

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $unitSlug = $request->get('unit_slug');
        $proposalId = $request->get('proposal_id');

        $proposal = Proposal::with(['proposer' => function ($q) {
            $q->with(['region']);
        }])->find($proposalId);

        $unitSizes = UnitSize::whereType($unitSlug)->get();

        $dwellingTypes = DwellingTypes::all();

        $unitTypes = UnitTypes::whereRecipientId($proposal->recipient_id)->get();

        $components = DwellingComponent::all();

        return response()->json([
            'title' => 'Tambah data',

            'body' => view("dwelling.{$unitSlug}.create")
                ->with([
                    'proposal'  => $proposal,
                    'identifier'     => $this->identifier,
                    'dwellingTypes'  => $dwellingTypes,
                    'unitSizes'      => $unitSizes,
                    'unitSizesRusun' => $this->generateUnitSizeForRusun(),
                    'unitTypes'      => $unitTypes,
                    'proposalId'     => $proposalId,
                    'components'     => $components
                ])
                ->render(),

            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proposal = Proposal::with(['unit'])->find($request->get('proposal_id'));
        $functionName = "store" . title_case($proposal->unit->slug);

        return $this->{$functionName}($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function storeRusun(Request $request)
    {
        $validator = validator($request->all(), [
            'type'        => 'required',
            'tower_block' => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return DwellingPlan::create([
            'proposal_id' => $request->get('proposal_id'),
//            'unit_type_id' => $request->get('unit_type'),
//            'unit_size_id' => $request->get('unit_size_rusun'),
            'type'        => $request->get('type'),
            'tower_block'  => $request->get('tower_block'),
            'amount'      => $request->get('amount'),
            'budget_plan' => $request->get('budget_plan')
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function storeRusus(Request $request)
    {
        $validator = validator($request->all(), [
            'type'        => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return DwellingPlan::create([
            'proposal_id'      => $request->get('proposal_id'),
            'dwelling_type_id' => $request->get('dwelling_type'),
//            'unit_size_id'     => $request->get('unit_size'),

            'type'        => $request->get('type'),
            'amount'           => $request->get('amount'),
            'budget_plan'      => $request->get('budget_plan')
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function storeRuk(Request $request)
    {
        $validator = validator($request->all(), [
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return DwellingPlan::create([
            'proposal_id'  => $request->get('proposal_id'),
//            'component_id' => $request->get('component'),
            'amount'       => $request->get('amount'),
            'budget_plan'  => $request->get('budget_plan')
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function storeSwadaya(Request $request)
    {
        $validator = validator($request->all(), [
            'proposal_id' => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if ($request->has('coordinate')) {

            $coordinate = $request->get('coordinate');

            $xCoordinate = explode(',', $coordinate);

            $latitude = trim($xCoordinate[0]);
            $longitude = trim($xCoordinate[1]);
        } else {
            $latitude = null;
            $longitude = null;
        }

        return DwellingPlan::create([
            'proposal_id' => $request->get('proposal_id'),
            'location_id' => $request->get('location'),
            'latitude'    => $latitude,
            'longitude'   => $longitude,
            'amount'      => $request->get('amount'),
            'budget_plan' => $request->get('budget_plan')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dwellingPlan = DwellingPlan::with([
            'proposal' => function ($q) {
                $q->with('unit');
            },
            // 'region'
        ])->find($id);

        $unitSizes = UnitSize::whereType($dwellingPlan->proposal->unit->slug)->get();

        $dwellingTypes = DwellingTypes::all();

        $unitTypes = UnitTypes::whereRecipientId($dwellingPlan->proposal->recipient_id)->get();
//        $unitTypes = UnitTypes::all();
        $components = DwellingComponent::all();

        return response()->json([
            'title'  => "Edit",
            'body'   => view("dwelling.{$dwellingPlan->proposal->unit->slug}.edit")
                ->with([
                    'dwellingPlan'   => $dwellingPlan,
                    'dwellingTypes'  => $dwellingTypes,
                    'unitSizes'      => $unitSizes,
                    'unitTypes'      => $unitTypes,
                    'unitSizesRusun' => $this->generateUnitSizeForRusun(),
                    'components'     => $components

                ])->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, $id)
    {
        $dwellingPlan = DwellingPlan::with(['proposal' => function ($q) {
            $q->with('unit');
        }])->find($id);

        $functionName = "update" . title_case($dwellingPlan->proposal->unit->slug);

        return $this->{$functionName}($request, $dwellingPlan);
    }

    /**
     * @param Request      $request
     * @param DwellingPlan $dwellingPlan
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function updateRusun(Request $request, DwellingPlan $dwellingPlan)
    {
        $validator = validator($request->all(), [
            'type'        => 'required',
            'tower_block' => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }
//
//        $dwellingPlan->unit_type_id = $request->get('unit_type');
//        $dwellingPlan->unit_size_id = $request->get('unit_size_rusun');
        $dwellingPlan->type = $request->get('type');
        $dwellingPlan->tower_block = $request->get('tower_block');
        $dwellingPlan->amount = $request->get('amount');
        $dwellingPlan->budget_plan = $request->get('budget_plan');

        $dwellingPlan->save();

        return response()->json([]);
    }

    /**
     * @param Request      $request
     * @param DwellingPlan $dwellingPlan
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function updateRusus(Request $request, DwellingPlan $dwellingPlan)
    {
        $validator = validator($request->all(), [
            'type'        => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $dwellingPlan->dwelling_type_id = $request->get('dwelling_type');
//        $dwellingPlan->unit_size_id = $request->get('unit_size');
        $dwellingPlan->type = $request->get('type');
        $dwellingPlan->amount = $request->get('amount');
        $dwellingPlan->budget_plan = $request->get('budget_plan');

        $dwellingPlan->save();

        return response()->json([]);

    }

    /**
     * @param Request      $request
     * @param DwellingPlan $dwellingPlan
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function updateRuk(Request $request, DwellingPlan $dwellingPlan)
    {
        $validator = validator($request->all(), [

            'amount'      => 'required',
            'budget_plan' => 'required'

        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

//        $dwellingPlan->component_id = $request->get('component');
        $dwellingPlan->amount = $request->get('amount');
        $dwellingPlan->budget_plan = $request->get('budget_plan');

        $dwellingPlan->save();

        return response()->json([]);

    }

    /**
     * @param Request      $request
     * @param DwellingPlan $dwellingPlan
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function updateSwadaya(Request $request, DwellingPlan $dwellingPlan)
    {
        $validator = validator($request->all(), [
            'proposal_id' => 'required',
            'amount'      => 'required',
            'budget_plan' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if ($request->has('coordinate')) {

            $coordinate = $request->get('coordinate');

            $xCoordinate = explode(',', $coordinate);

            $latitude = trim($xCoordinate[0]);
            $longitude = trim($xCoordinate[1]);
        } else {
            $latitude = null;
            $longitude = null;
        }

        $dwellingPlan->location_id = $request->get('location');
        $dwellingPlan->latitude = $latitude;
        $dwellingPlan->longitude = $longitude;
        $dwellingPlan->amount = $request->get('amount');
        $dwellingPlan->budget_plan = $request->get('budget_plan');

        $dwellingPlan->save();

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DwellingPlan::find($id)->delete();

        return response()->json([]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function data(Request $request)
    {
        $proposal_id = $request->get('proposal_id', 0);

        $builder = DwellingPlan::select([
            'dwelling_plans.*',
            \DB::raw('unit_types.name as unit_type_name'),
            \DB::raw('dwelling_types.name as dwelling_type_name'),
            \DB::raw('unit_sizes.text as unit_size_text'),
            \DB::raw('unit_sizes.total_floor as unit_size_floor'),
            \DB::raw('dwelling_components.name as component_name'),
            \DB::raw('regions.name as location'),
        ])
            ->whereProposalId($proposal_id)
            ->leftJoin('unit_types', 'unit_types.id', '=', 'dwelling_plans.unit_type_id')
            ->leftJoin('dwelling_types', 'dwelling_types.id', '=', 'dwelling_plans.dwelling_type_id')
            ->leftJoin('dwelling_components', 'dwelling_components.id', '=', 'dwelling_plans.component_id')
            ->leftJoin('regions', 'regions.id', '=', 'dwelling_plans.location_id')
            ->leftJoin('unit_sizes', 'unit_sizes.id', '=', 'dwelling_plans.unit_size_id');

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {
                return view('layout.action')
                    ->with([
                        'edit_link'   => route('dwellings.plans.edit', [$data->id]),
                        'delete_link' => route('dwellings.plans.destroy', [$data->id]),
                        'identifier'  => $this->identifier
                    ])
                    ->render();
            })
            ->addColumn('rusun_unit_size', function ($data) {
                $text = $data->unit_size_text;
                $floor = $data->unit_size_floor;

                return "T. {$text} / {$floor} lantai";
            })
            ->editColumn('budget_plan', function ($data) {
                return 'Rp. ' . number_format($data->budget_plan, 0, ',', '.');
            })
            ->editColumn('location', function ($data) {
                return \Cache::get('region-' . $data->location_id);
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    /**
     * @return array
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    private function generateUnitSizeForRusun()
    {
        $unitSizes = UnitSize::whereType('rusun')->get();

        // todo: classical method, change to new method with `each`
        $r = [];
        foreach ($unitSizes as $unitSize) {
            array_push($r, [
                'id'   => $unitSize->id,
                'name' => "T. {$unitSize->text} / {$unitSize->total_floor} lantai"
            ]);
        }

        return $r;
    }
}
