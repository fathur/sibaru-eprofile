<?php

namespace App\Http\Controllers;

use App\Models\Proposal;
use App\Models\Region;
use App\Models\RegionProfile;
use App\Models\EprofileAsset;
use App\Models\Unit;
use App\Repositories\RegionProfileExport;
use App\Transformers\BacklogTransformer;
use App\Transformers\BasicInfoProvinceTransformer;
use App\Transformers\RecapTransformer;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

class RegionProfileController extends Controller
{
    protected $identifier = 'region-profile';

    protected $rules = [
        'year' => 'required|integer|digits:4',
        /* 'head_name',
        'deputy_name',
        'head_photo' => 'url',
        'deputy_photo' => 'url',
        'head_party',
        'deputy_party',
        'period_start' => 'integer|digits:4',
        'period_end' =>'integer|digits:4',
        'perkim',
        'perkim_pic_name',
        'perkim_phone',
        'perkim_fax',
        'perkim_email',
        'sea_large',
        'land_large',
        'latitude',
        'longitude',
        'total_man' => 'numeric',
        'total_woman' => 'numeric',
        'population_growth' => 'numeric',
        'population_density' => 'numeric',
        'total_city_poor_people' => 'numeric',
        'total_village_poor_people' => 'numeric',
        'total_district' => 'numeric',
        'total_city' => 'numeric',
        'apbd_amount',
        'pad_pajak',
        'pad_retribusi',
        'pad_sda',
        'pad_others',
        'backlog_house',
        'household',
        'image' => 'url',
        'pupr_budget' => 'numeric',*/
    ];

    protected $ruleMessages = [
        'year.required' => 'Tahun tidak boleh kosong',
        'year.integer'  => 'Tahun harus 4 digit angka',
        'year.digits'   => 'Tahun harus 4 digit angka',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        if (\Gate::denies('do-admin')) {
            abort(404);
        }

        if ($request->has('year')) {
            $profile = RegionProfile::whereRegionId($id)
                ->where('year', $request->get('year'))
                ->first();

            return redirect()->route('regions.profiles.edit', [$id, $profile->id]);

        }

        $region = Region::find($id);

        $title = 'Profile ' . $region->name;

        return view('region.profile.index', compact('title', 'region'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, Request $request)
    {
        $region = Region::find($id);
        $title = 'Profile ' . $region->name;

        return view('region.profile.create', compact('region', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param                           $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $validator = validator($request->all(), $this->rules, $this->ruleMessages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator);
        }

        $profile = RegionProfile::create([
            'region_id'                 => $id,
            'year'                      => $request->get('year'),
            'head_name'                 => $request->get('head_name'),
            'deputy_name'               => $request->get('deputy_name'),
            'head_photo'                => $request->get('head_photo'),
            'deputy_photo'              => $request->get('deputy_photo'),
            'head_party'                => $request->get('head_party'),
            'deputy_party'              => $request->get('deputy_party'),
//            'period_start'              => $request->get('period_start'),
//            'period_end'                => $request->get('period_end'),
            'perkim'                    => $request->get('perkim'),
            'perkim_pic_name'           => $request->get('perkim_pic_name'),
            'perkim_phone'              => $request->get('perkim_phone'),
            'perkim_fax'                => $request->get('perkim_fax'),
            'perkim_email'              => $request->get('perkim_email'),
            'sea_large'                 => $request->get('sea_large'),
            'land_large'                => $request->get('land_large'),
            'latitude'                  => $request->get('latitude'),
            'longitude'                 => $request->get('longitude'),
            'total_man'                 => $request->get('total_man'),
            'total_woman'               => $request->get('total_woman'),
            'population_growth'         => $request->get('population_growth'),
            'population_density'        => $request->get('population_density'),
            'total_city_poor_people'    => $request->get('total_city_poor_people'),
            'total_village_poor_people' => $request->get('total_village_poor_people'),
            'total_district'            => $request->get('total_district'),
            'total_city'                => $request->get('total_city'),
            'apbd_amount'               => $request->get('apbd_amount'),
            'pad_pajak'                 => $request->get('pad_pajak'),
            'pad_retribusi'             => $request->get('pad_retribusi'),
            'pad_sda'                   => $request->get('pad_sda'),
            'pad_others'                => $request->get('pad_others'),
            'backlog_house'             => $request->get('backlog_house'),
            'household'                 => $request->get('household'),
            'image'                     => $request->get('logo'),
            'pupr_budget'               => $request->get('pupr_budget'),
            'koordinat'                 => $request->get('koordinat'),
            'masa_bakti'                => $request->get('masa_bakti'),
            'area_large'                => $request->get('area_large'),
            'perkim_kelembagaan'        => $request->get('perkim_kelembagaan'),
            'rtlh'                      => $request->get('rtlh'),
            'backlog_kepemilikan'       => $request->get('backlog_kepemilikan'),
            'total_people'              => $request->get('total_people'),
        ]);

        // Save image
        for ($i = 1; $i <= 4; $i++) {
            if ($request->has('location_asset_' . $i)) {
                $location{$i} = $request->get('location_asset_' . $i);
                $filename{$i} = $request->get('filename_asset_' . $i);

                $asset = new EprofileAsset();
                $asset->region_profile_id = $profile->id;
                $asset->type = 'image';
                $asset->filename = $filename{$i};
                $asset->url = $location{$i};
                $asset->save();
            }
        }

        if ($profile) {

            $this->createAllRelatedKabupaten($profile);

            return redirect()->route('regions.profiles.edit', [$id, $profile->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $regionId
     * @param $profileId
     * @return \Illuminate\Http\Response
     *
     */
    public function show($regionId, $profileId, Request $request)
    {
        if (\Gate::denies('do-admin')) {
            abort(404);
        }

        $show = (new RegionProfileExport())
            ->setProfile(RegionProfile::with('region')->find($profileId))
            ->inRegion(Region::find($regionId))
            ->show();

        return $show;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $regionId
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($regionId, $id)
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $profile = RegionProfile::with(['region'])->find($id);


        $title = 'Edit profile ' . $profile->region->name;


        return view('region.profile.edit', compact('title', 'profile'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $regionId
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $regionId, $id)
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $validator = validator($request->all(), $this->rules, $this->ruleMessages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator);
        }

        $profile = RegionProfile::find($id);

        $profile->year = $request->get('year');
        $profile->head_name = $request->get('head_name');
        $profile->deputy_name = $request->get('deputy_name');
        $profile->head_photo = $request->get('head_photo');
        $profile->deputy_photo = $request->get('deputy_photo');
        $profile->head_party = $request->get('head_party');
        $profile->deputy_party = $request->get('deputy_party');
        $profile->period_start = $request->get('period_start');
        $profile->period_end = $request->get('period_end');
        $profile->perkim = $request->get('perkim');
        $profile->perkim_pic_name = $request->get('perkim_pic_name');
        $profile->perkim_phone = $request->get('perkim_phone');
        $profile->perkim_fax = $request->get('perkim_fax');
        $profile->perkim_email = $request->get('perkim_email');
        $profile->sea_large = $request->get('sea_large');
        $profile->land_large = $request->get('land_large');
        $profile->latitude = $request->get('latitude');
        $profile->longitude = $request->get('longitude');
        $profile->total_man = $request->get('total_man');
        $profile->total_woman = $request->get('total_woman');
        $profile->population_growth = $request->get('population_growth');
        $profile->population_density = $request->get('population_density');
        $profile->total_city_poor_people = $request->get('total_city_poor_people');
        $profile->total_village_poor_people = $request->get('total_village_poor_people');
        $profile->total_district = $request->get('total_district');
        $profile->total_city = $request->get('total_city');
        $profile->apbd_amount = $request->get('apbd_amount');
        $profile->pad_pajak = $request->get('pad_pajak');
        $profile->pad_retribusi = $request->get('pad_retribusi');
        $profile->pad_sda = $request->get('pad_sda');
        $profile->pad_others = $request->get('pad_others');
        $profile->backlog_house = $request->get('backlog_house');
        $profile->household = $request->get('household');
        $profile->image = $request->get('logo');
        $profile->pupr_budget = $request->get('pupr_budget');
        $profile->koordinat = $request->get('koordinat');
        $profile->masa_bakti = $request->get('masa_bakti');
        $profile->area_large = $request->get('area_large');
        $profile->perkim_kelembagaan = $request->get('perkim_kelembagaan');
        $profile->rtlh = $request->get('rtlh');
        $profile->backlog_kepemilikan = $request->get('backlog_kepemilikan');
        $profile->total_people = $request->get('total_people');

        if ($profile->save()) {

            $this->createAllRelatedKabupaten($profile);

            return redirect()->back()
                ->with('status', 'Success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $regionId
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($regionId, $id)
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $profile = RegionProfile::find($id);
        $profile->delete();

        return response()->json([]);

    }

    public function data(Request $request)
    {
        if ($request->has('sub')) {
            return $this->treeGrid($request);
        } else {

            return $this->dataTable($request);
        }

    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function viewAsset($id, Request $request)
    {
//        dd(base_path('public/img/no_photo_available.jpg'));
        $type = $request->get('type');

        $profile = RegionProfile::find($id);

        switch ($type) {
            case 'logo':
                if (is_file(base_path($profile->image))) {

                    $image = Image::make(file_get_contents(base_path($profile->image)));

                } else {
                    $image = Image::make(file_get_contents(base_path('public/img/no_photo_available.jpg')));
                }
                break;
            case 'head':
                if (is_file(base_path($profile->head_photo))) {

                    $image = Image::make(file_get_contents(base_path($profile->head_photo)));

                } else {
                    $image = Image::make(file_get_contents(base_path('public/img/no_photo_available.jpg')));
                }
                break;
            case 'deputy':
                if (is_file(base_path($profile->deputy_photo))) {

                    $image = Image::make(file_get_contents(base_path($profile->deputy_photo)));

                } else {
                    $image = Image::make(file_get_contents(base_path('public/img/no_photo_available.jpg')));
                }
                break;
            case 'image':
                $asset = EprofileAsset::find($id);

                if (is_file(base_path($asset->url))) {

                    $image = Image::make(file_get_contents(base_path($asset->url)));

                } else {
                    $image = Image::make(file_get_contents(base_path('public/img/no_photo_available.jpg')));
                }
                break;
            default:
                $image = Image::make(file_get_contents(base_path('public/img/no_photo_available.jpg')));
        }

        $response = \Response::make($image->encode('png'));
        // set content-type
        $response->header('Content-Type', 'image/png');

        return $response;
    }

    private function createAllRelatedKabupaten($profile)
    {
        $year = $profile->year;
        Region::whereParentId($profile->region_id)
            ->whereLevel(2)
            //->get()
            ->chunk(10, function ($regencies) use ($year) {

                foreach ($regencies as $regency) {
                    $check = RegionProfile::where('region_id', $regency->id)
                        ->where('year', $year)
                        ->count();

                    if ($check == 0) {
                        RegionProfile::create([
                            'region_id' => $regency->id,
                            'year'      => $year
                        ]);
                    }
                }
            });
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function dataTable(Request $request)
    {
        if (\Gate::denies('do-admin')) {
            abort(404);
        }

        $regionId = $request->get('region');
        $region = Region::find($regionId);

        $builder = RegionProfile::with(['region'])->where('region_id', $regionId);

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) use ($region) {
                return view('region.profile.action', compact('region'))
                    ->with([
                        'edit_link'   => route('regions.profiles.edit', [$region->id, $data->id]),
                        'delete_link' => route('regions.profiles.destroy', [$region->id, $data->id]),
                        'identifier'  => $this->identifier,
                        'data'        => $data
                    ])
                    ->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function treeGrid(Request $request)
    {
        $results = [];

        $profiles = RegionProfile::leftJoin('regions', 'regions.id', '=', 'region_profiles.region_id')
            ->select([
                'regions.name',
                'region_profiles.*'
            ])->where('regions.parent_id', $request->get('parent'))
            ->where('year', $request->get('year'))
            ->where('regions.level', 2)
            ->orderBy('regions.name')
            ->get();

        foreach ($profiles as $profile) {
            $profile = $profile->toArray();
            $profile['state'] = 'open'; //$this->hasChild($region, $level) ? 'closed' : 'open';
//            $profile['level'] = $this->namedLevel($profile['level']);
            $profile['action'] = view('region.profile.action-kabupaten', compact('profile'))
                ->with([
                    'identifier' => $this->identifier,
                    'year'       => $request->has('year') ? $request->get('year') : null,
                ])
                ->render();
            array_push($results, $profile);
        }

        return $results;
    }

    /**
     * @param $regionId
     * @param $profileId
     * @param Request $request
     * @return mixed
     */
    public function export($regionId, $profileId, Request $request)
    {
        $export = (new RegionProfileExport())
            ->setProfile(RegionProfile::with('region')->find($profileId))
            ->inRegion(Region::find($regionId))
            ->setType($request->get('type'))
            ->export();

        return $export;
    }

    /**
     * @param integer $provinceId
     * @param integer $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiBasicInfo($provinceId, $year)
    {
        try {


            $data = (new RegionProfileExport())
                ->whenYear($year)
                ->inRegion(Region::find($provinceId))
                ->getRegionProfile();

            $fractal = new Manager;
            $resource = new Item($data, new BasicInfoProvinceTransformer());
            $response = $fractal->createData($resource)->toArray();
            return response()->json($response);

        } catch (\ErrorException $e) {
            return response()->json([
                'data' => []
            ]);
        }
    }

    public function apiBacklog($provinceId, $year)
    {
        try {

            $data = (new RegionProfileExport())
                ->whenYear($year)
                ->inRegion(Region::find($provinceId))
                ->getRegencyProfiles();

            $fractal = new Manager;
            $resource = new Collection($data, new BacklogTransformer());
            $response = $fractal->createData($resource)->toArray();
            return response()->json($response);
        } catch (\ErrorException $e) {
            return response()->json([
                'data' => []
            ]);
        }
    }

    public function apiRecapitulation($provinceId, $year)
    {
        try {
            $regionProfile = (new RegionProfileExport())
                ->whenYear($year)
                ->inRegion(Region::find($provinceId))
                ->getRegionProfile();

            // kemenpera
            $kemenpera = [];
            foreach ((new RegionProfileExport)->recapKemenpera($regionProfile) as $item) {
                array_push($kemenpera, [
                    'kegiatan' => $item->program,
                    'volume'   => (double)$item->volume,
                    'satuan'   => $item->unit,
                ]);
            }

            // tahun sebelumnya
            $previous = [];
            foreach ((new RegionProfileExport)->recapPreviousYears($regionProfile) as $unitId => $programs) {

                foreach ($programs as $programId => $value) {
                    array_push($previous, [
                        'direktorat'  => $value['unit_name'],
                        'kegiatan'    => $value['program_name'],
                        'jumlah_unit' => $value['sum_unit'],
                    ]);
                }
            }

            // tahun sekarang
            $current = [];
            foreach ((new RegionProfileExport)->recapCurrentYear($regionProfile) as $unitId => $programs) {

                foreach ($programs as $programId => $value) {
                    array_push($current, [
                        'direktorat'  => $value['unit_name'],
                        'kegiatan'    => $value['program_name'],
                        'jumlah_unit' => $value['sum_unit'],
                    ]);
                }
            }

            $data = [
                'kemenpera' => $kemenpera,
                'previous'  => $previous,
                'current'   => $current
            ];

            return response()->json(['data' => $data]);
        } catch (\ErrorException $e) {
            return response()->json([
                'data' => []
            ]);
        }
//        $regionProfile = (new RegionProfileExport())
//            ->whenYear($year)
//            ->inRegion(Region::find($provinceId))
//            ->getRegionProfile();
//
//        $data = (object)[
//            'kemenpera' => (new RegionProfileExport)->recapKemenpera($regionProfile),
//            'previousYears' => (new RegionProfileExport())->recapPreviousYears($regionProfile),
//            'currentYear'   => (new RegionProfileExport())->recapCurrentYear($regionProfile)
//        ];
//
//        dd($data->toArray());
//
//        $fractal = new Manager;
//        $fractal->parseIncludes(['kemenpera','previous','current']);
//        $resource = new Item($data, new RecapTransformer());
//
//        $response = $fractal->createData($resource)->toArray();
//        return response()->json($response);

    }


    public function apiDetail($provinceId, $year)
    {
        try {
            $regencyProfiles = (new RegionProfileExport())
                ->whenYear($year)
                ->inRegion(Region::find($provinceId))
                ->getRegencyProfiles();

            $units = Unit::whereNotIn('slug', ['dirjen'])->get();

            $data = [];
            foreach ($regencyProfiles as $regencyProfile) {

                $directorates = [];

                foreach ($units as $unit) {


                    $programsWithRecipients = (new RegionProfileExport())->getProgramsWithRecipients($regencyProfile, $unit);

                    if (count($programsWithRecipients) > 0) {

                        $programs = [];
                        foreach ($programsWithRecipients as $uniques) {

                            $detail = [];
                            foreach ($uniques['eprofiles'] as $eprofile) {
                                array_push($detail, [
                                    'deskripsi'   => $eprofile->proposer_remark,
                                    'jumlah_unit' => $eprofile->amount_unit
                                ]);
                            }

                            array_push($programs, [
                                'kegiatan'    => $uniques['program']->name . ', ' . $uniques['recipient']->name,
                                'jumlah_unit' => $uniques['sum_unit'],
                                'detail'      => $detail
                            ]);
                        }

                        array_push($directorates, [
                            'direktorat'  => $unit->name,
                            'jumlah_unit' => (new RegionProfileExport())->sumUnitInDirectorate($regencyProfile, $unit),
                            'programs'    => $programs
                        ]);
                    }

                }

                array_push($data, [
                    'region'       => $regencyProfile->name,
                    'directorates' => $directorates
                ]);
            }

            $fake = [
                [
                    'region'       => 'Kabupaten Aceh Besar',
                    'directorates' => [
                        [
                            'direktorat'  => 'Rumah Susun',
                            'jumlah_unit' => 45,
                            'programs'    => [
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ],
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'direktorat'  => 'Rumah Khusus',
                            'jumlah_unit' => 45,
                            'programs'    => [
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ],
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'direktorat'  => 'Swadaya',
                            'jumlah_unit' => 45,
                            'programs'    => [
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ],
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'direktorat'  => 'RUK',
                            'jumlah_unit' => 45,
                            'programs'    => [
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ],
                                [
                                    'kegiatan'    => 'Pembangunan Baru',
                                    'jumlah_unit' => 343,
                                    'detail'      => [
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ],
                                        [
                                            'deskripsi'   => 'Desa Bulak Baru',
                                            'jumlah_unit' => 34
                                        ]
                                    ]
                                ]
                            ]
                        ],

                    ]
                ]
            ];


            return response()->json(['data' => $data]);
        } catch (\ErrorException $e) {
            return response()->json([
                'data' => []
            ]);
        }
    }
}
