<?php

namespace App\Http\Controllers;

use App\DataTables\FaqDataTable;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    protected $identifier = 'faq';

    public function index(FaqDataTable $dataTable)
    {
        $title = 'FAQ';


        return $dataTable->render('faq.index', [
            'title' => $title,
            'identifier' => $this->identifier,
        ]);
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'title'  => 'FAQ baru',
                'body'   => view('faq.modal.create')
                    ->with([
                        'identifier' => $this->identifier
                    ])
                    ->render(),
                'footer' => view('faq.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('faq.create');
        }
    }

    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'question'     => 'required',
            'answer' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return Faq::create([
            'question'   => $request->get('question'),
            'answer' => $request->get('answer'),

        ]);
    }

    public function edit($id, Request $request)
    {

        $faq = Faq::find($id);

//        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit faq",
                'body'   => view('faq.modal.edit')
                    ->with([
                        'faq' => $faq,
                        'identifier' => $this->identifier

                    ])
                    ->render(),
                'footer' => view('faq.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
//        } else {
//            return view('faq.edit');
//        }
    }

    public function update(Request $request, $id)
    {
        $faq = Faq::find($id);

        $validator = validator($request->all(), [
            'question'     => 'required',
            'answer' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $faq->question = $request->get('question');
        $faq->answer = $request->get('answer');
        $faq->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('faq.index');

    }

    public function destroy($id)
    {
        return Faq::destroy($id);
    }
}