<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $identifier = 'user';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Users <small>List of authorized person</small>';

        return view('user.index', compact('title'))->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'title'  => 'User baru',
                'body'   => view('user.modal.create')
                    ->with([
                        'identifier' => $this->identifier,
                        'roles' => Role::all(),
                        'units' => Unit::all()
                    ])
                    ->render(),
                'footer' => view('user.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('user.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'name'     => 'required',
            'username' => 'required|unique:users,username',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:7'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return User::create([
            'role_id'   => $request->get('role'),
            'agency_id' => $request->get('agency'),
            'unit_id' => $request->has('unit') ? $request->get('unit') : null,
            'nip'       => $request->get('nip'),
            'name'      => $request->get('name'),
            'username'  => $request->get('username'),
            'email'     => $request->get('email'),
            'phone'     => $request->get('phone'),
            'password'  => \Hash::make($request->get('password')),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     *
     */
    public function edit($id, Request $request)
    {
        $user = User::with(['agency'])->find($id);

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit user {$user->name}",
                'body'   => view('user.modal.edit')
                    ->with([
                        'user' => $user,
                        'roles' => Role::all(),
                        'units' => Unit::all()
                    ])
                    ->render(),
                'footer' => view('user.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('user.edit');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User                      $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = validator($request->all(), [
            'name'     => 'required',
            'username' => 'required',
            'email'    => 'required|email',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $user->role_id =  $request->get('role');
        $user->agency_id =  $request->get('agency');
        $user->unit_id =  $request->has('unit') ? $request->get('unit') : null;
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->nip =  $request->get('nip');
        $user->phone = $request->get('phone');
        $user->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function data(Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        return \Datatables::of(User::query())
            ->editColumn('email', function ($data) {
                return "<a href='mailto:{$data->email}'>{$data->email}</a>";

            })
            ->addColumn('action', function ($data) {
                return view('user.action')
                    ->with([
                        'user'  => $data,
                        'edit_link'   => route('users.edit', [$data->id]),
                        'delete_link' => route('users.destroy', [$data->id]),
                        'password_link' => route('users.password.update', [$data->id]),
                        'identifier'     => $this->identifier
                    ])
                    ->render();
            })
            ->rawColumns(['email', 'action'])
            ->make(true);
    }

    public function getFormPassword(Request $request)
    {
        $id = $request->get('userId');
        $user = User::find($id);

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit password <strong>{$user->name}</strong>",
                'body'   => view('user.modal.password')
                    ->with([
                        'user'  => $user
                    ])
                    ->render()
            ]);
        }
    }

    public function updatePassword($userId, Request $request)
    {

        $validator = validator($request->all(), [
            'password1'     => 'required|min:7',
            'password2'     => 'required|same:password1'
        ], [
            'password1.required' => 'Password tidak boleh kosong',
            'password1.min' => 'Password minimal 7 karakter',
            'password2.required' => 'Konfirmasi password tidak boleh kosong',
            'password2.same' => 'Konfirmasi password tidak cocok',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if($request->get('password1') !== $request->get('password2')) {
            return response()->json([
                'password2' => ['Password not match']
            ], 400);

        }


        $password = $request->get('password2');

        $user = User::find($userId);
        $user->password = \Hash::make($password);
        $user->save();

        if ($request->ajax()) {
            return response()->json([]);
        }
    }

    public function showProfile()
    {
        $user = \Auth::user();
        return view('user.profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $me = \Auth::user();
        $me->nip = $request->get('nip');
        $me->name = $request->get('name');
        $me->email = $request->get('email');
        $me->phone = $request->get('phone');
        $me->save();

        return redirect()->to('profile')->with('status-profile', 'Profile berhasil di perbarui.');
    }

    public function updateProfilePassword(Request $request)
    {
        $this->validate($request, [
            'password_old'  => 'required',
            'password_new'  => 'required|min:7',
            'password_new_confirm' => 'required|same:password_new'
        ], [
            'password_old.required' => 'Isian password lama harus diisi.',
            'password_new.required' => 'Isian password baru harus diisi.',
            'password_new_confirm.required' => 'Isian konfirmasi password baru harus diisi.',
            'password_new.min'  => 'Password baru minimal 7 karakter',
            'password_new_confirm.same' => 'Konfirmasi password baru tidak sama.'
        ]);

        $me = \Auth::user();

        if (!\Hash::check($request->get('password_old'), $me->password)) {
            return redirect()->to('profile')->with('status-password-error', 'Password lama tidak sama.');
        }

        $me->password = bcrypt($request->get('password_new'));
        $me->save();

        return redirect()->to('profile')->with('status-password', 'Password berhasil di rubah.');

    }
}
