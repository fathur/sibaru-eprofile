<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use App\Models\Region;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    protected $identifier = 'instansi';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Instansi';

        return view('agency.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $parents = Agency::whereNull('parent_id')->get();

        if ($request->ajax()) {
            return response()->json([
                'title'  => 'Instansi baru',
                'body'   => view('agency.modal.create')
                    ->with([
                        'parents'    => $parents,
                        'identifier' => $this->identifier
                    ])
                    ->render(),
                'footer' => view('agency.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('agency.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if ((int)$request->get('parent') == Agency::where('name', 'Pemda')->first()->id) {
            $regionId = $request->get('region');
            $region = Region::find($regionId);
            $regionParent = $region->parent;
            $type = 'pemda';

            if($regionParent) {
                $agencyParent = Agency::whereRegionId($regionParent->id)->first();
                $agencyParentId = $agencyParent->id;
            } else {
                $agencyParentId = (int)$request->get('parent');
            }
//            $parent = Agency::whereRegionId($region)->first();
        } else {
            $regionId = null;
            $type = null;
            $agencyParentId = (int)$request->get('parent');

            if($agencyParentId === 0) {
                $agencyParentId = null;
            }
        }

        # $parent = (int)$request->get('parent') == 0 ? null : $request->get('parent');
        return Agency::create([
            'region_id' => $regionId,
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'fax' => $request->get('fax'),
            'address' => $request->get('address'),
            'logo' => $request->get('logo_url'),
            'parent_id' => $agencyParentId,
            'type' => $type
//            'type'      => $request->get('type')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agency $agency
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     * @internal param Agency $agency
     *
     */
    public function edit($id)
    {
        $agency = Agency::find($id);
        $parents = Agency::whereNull('parent_id')->get();

        return response()->json([
            'title'  => "Ubah instansi",
            'body'   => view('agency.modal.edit')
                ->with([
                    'agency'     => $agency,
                    'parents'    => $parents,
                    'identifier' => $this->identifier
                ])
                ->render(),
            'footer' => view('agency.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     * @internal param Agency $agency
     */
    public function update(Request $request, $id)
    {
        $validator = validator($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if ((int)$request->get('parent') == Agency::where('name', 'Pemda')->first()->id) {
            $regionId = $request->get('region');
            $region = Region::find($regionId);
            $regionParent = $region->parent;
            $type = 'pemda';

            if($regionParent) {
                $agencyParent = Agency::whereRegionId($regionParent->id)->first();
                $agencyParentId = $agencyParent->id;
            } else {
                $agencyParentId = (int)$request->get('parent');
            }
        } else {
            $regionId = null;
            $type = null;
            $agencyParentId = (int)$request->get('parent');

            if($agencyParentId === 0) {
                $agencyParentId = null;
            }
        }

//        $parent = (int)$request->get('parent') == 0 ? null : $request->get('parent');

        $agency = Agency::find($id);
        $agency->region_id = $regionId;
        $agency->name      = $request->get('name');
        $agency->phone     = $request->get('phone');
        $agency->fax       = $request->get('fax');
        $agency->address   = $request->get('address');
        $agency->logo      = $request->get('logo_url');
        $agency->parent_id = $agencyParentId;
        $agency->type   = $type;
        $agency->save();

        return $agency;
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Agency::destroy($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Throwable
     */
    public function data(Request $request)
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $id = $request->get('id');
        $results = [];

        if ($request->has('id')) {

            $parentAgency = Agency::find($id);

            if (strtolower($parentAgency->name) == strtolower('pemda')) {

                $agencies = Agency::select(['agencies.*'])
                    ->leftJoin('regions', 'regions.id', '=', 'agencies.region_id')
                    ->where('agencies.parent_id', $id)
                    ->where('regions.level', 1)
                    ->orderBy('agencies.name')
                    ->get();

                foreach ($agencies as $agency) {
                    $agency = $agency->toArray();
                    $agency['state'] = 'closed';
                    $agency['action'] = view('agency.action', compact('agency'))
                        ->with('identifier', $this->identifier)
                        ->render();
                    array_push($results, $agency);
                }

            } else {
                // jika bukan pemda
                // yane mewakili wilayah tertentu
                // atau tanpa wilayah

                if ($parentAgency->region_id == null) {

                    $agencies = Agency::where('parent_id', $parentAgency->id)->get();


                    foreach ($agencies as $agency) {
                        $agency = $agency->toArray();
                        $agency['state'] = $this->hasChild($agency) ? 'closed' : 'open';
                        $agency['action'] = view('agency.action', compact('agency'))
                            ->with('identifier', $this->identifier)
                            ->render();
                        array_push($results, $agency);

                    }
                } else {


                    $regionParent = Region::find($parentAgency->region_id);

                    $regionChild = Region::whereParentId($regionParent->id)->pluck('id');

                    $agencies = Agency::whereIn('region_id', $regionChild)
                        ->where('parent_id', $parentAgency->id)
                        ->orderBy('agencies.name')
                        ->get();

                    foreach ($agencies as $agency) {
//                        $state = $this->hasChild($agency) ? 'closed' : 'open';
                        $agency = $agency->toArray();
                        $agency['state'] = 'open';
                        $agency['action'] = view('agency.action', compact('agency'))
                            ->with('identifier', $this->identifier)
                            ->render();
                        array_push($results, $agency);
                    }
                }
            }

        } else {
            $agencies = Agency::whereNull('parent_id')
                ->orderBy('name')
                ->get();

            foreach ($agencies as $agency) {
                $state = $this->hasChild($agency) ? 'closed' : 'open';
                $agency = $agency->toArray();
                $agency['state'] = $state;
                $agency['action'] = view('agency.action', compact('agency'))
                    ->with('identifier', $this->identifier)
                    ->render();
                array_push($results, $agency);
            }
        }

        return response()->json($results);

    }

    private function hasChild($agency)
    {
        $agency = !is_array($agency) ? $agency->toArray() : $agency;

        $count = Agency::whereParentId($agency['id'])->count();

        if ($count > 0) {
            return true;
        }

        return false;
    }

    public function select2Data(Request $request)
    {
        $q = strtolower($request->get('q'));

        return Agency::where(function ($query) use ($q) {
            $query->where(\DB::raw('LOWER(name)'), 'like', "%$q%");
        })->get();
    }

    public function comboTree(Request $request)
    {

        $id = $request->get('id');
        $results = [];

        if ($request->has('id')) {

            $parentAgency = Agency::find($id);

            if (strtolower($parentAgency->name) == strtolower('pemda')) {

                $agencies = Agency::select(['agencies.*'])
                    ->leftJoin('regions', 'regions.id', '=', 'agencies.region_id')
                    ->where('agencies.parent_id', $id)
                    ->where('regions.level', 1)
                    ->orderBy('agencies.name')
                    ->get();

                foreach ($agencies as $agency) {
                    $agency = $agency->toArray();
                    $agency['state'] = 'closed';
                    $agency['text'] = $agency['name'];

                    $agency['action'] = view('agency.action', compact('agency'))
                        ->with('identifier', $this->identifier)
                        ->render();
                    array_push($results, $agency);
                }

            } else {
                // jika bukan pemda
                if ($parentAgency->region_id == null) {
                    dd('x');
                } else {

                    $regionParent = Region::find($parentAgency->region_id);

                    $regionChild = Region::whereParentId($regionParent->id)->pluck('id');

                    $agencies = Agency::whereIn('region_id', $regionChild)
                        ->orderBy('agencies.name')
                        ->get();

                    foreach ($agencies as $agency) {
                        $agency = $agency->toArray();
                        $agency['state'] = 'open';
                        $agency['text'] = $agency['name'];

                        $agency['action'] = view('agency.action', compact('agency'))
                            ->with('identifier', $this->identifier)
                            ->render();
                        array_push($results, $agency);
                    }
                }
            }

        } else {
            $agencies = Agency::whereNull('parent_id')
                ->orderBy('name')
                ->get();

            foreach ($agencies as $agency) {
                $state = $this->hasChild($agency) ? 'closed' : 'open';
                $agency = $agency->toArray();
                $agency['state'] = $state;
                $agency['text'] = $agency['name'];
                $agency['action'] = view('agency.action', compact('agency'))
                    ->with('identifier', $this->identifier)
                    ->render();
                array_push($results, $agency);
            }
        }

        return response()->json($results);

    }


}
