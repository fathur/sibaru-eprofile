<?php

namespace App\Http\Controllers;

use App\Models\Tematic;
use App\Models\Unit;
use Illuminate\Http\Request;

//Daerah Tertinggal	   Edit    Delete
//Kawasan Ekonomi Khusus	   Edit    Delete
//Kawasan Industri	   Edit    Delete
//Kawasan Metropolitan	   Edit    Delete
//Kelautan / Perikanan	   Edit    Delete
//Mendorong Perekonomian (Rusun Pekerja / PNS)	   Edit    Delete
//Pariwisata	   Edit    Delete
//Pembangunan Pendidikan	   Edit    Delete
//Pembangunan Perumahan dan Pemukiman	   Edit    Delete
//Perbatasan

class TematicController extends Controller
{
    protected $identifier = 'tematic';

    public function index()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Tematik';

        return view('tematic.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'title'  => 'Tematik baru',
                'body'   => view('tematic.modal.create')
                    ->with([
                        'identifier' => $this->identifier,
                        'units' => Unit::whereNotIn('slug', ['dirjen'])->get()
                    ])
                    ->render(),
                'footer' => view('tematic.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('tematic.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            // ...
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return Tematic::create([
            'name'      => $request->get('name'),
            'unit_id'   => $request->get('unit')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param         $id
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function edit($id, Request $request)
    {
        $tematic = Tematic::find($id);

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit tematik {$tematic->name}",
                'body'   => view('tematic.modal.edit')
                    ->with([
                        'tematic' => $tematic,
                        'identifier' =>  $this->identifier

                    ])
                    ->render(),
                'footer' => view('tematic.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            $title = 'Edit tematik '. $tematic->name;
            return view('tematic.edit', compact('tematic', 'title'))
                ->with([
                    'identifier' => $this->identifier
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = validator($request->all(), [
            'name'     => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $tematic = Tematic::find($id);
        $tematic->name = $request->get('name');
        $tematic->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

//        return redirect()->route('tematics.edit', $id);
        return redirect()->route('tematics.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function destroy($id)
    {
        return Tematic::destroy($id);
    }

    public function data(Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }


        $builder = Tematic::select([
            'tematics.*',
            \DB::raw('units.name as unit_name'),
        ])->leftJoin('units','tematics.unit_id','=','units.id');



        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {
                return view('layout.action')
                    ->with([
                        'edit_link'   => route('tematics.edit', [$data->id]),
                        'delete_link' => route('tematics.destroy', [$data->id]),
                        'identifier'  => $this->identifier
                    ])
                    ->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function select2Data(Request $request)
    {
        $q = strtolower($request->get('q'));

        $unitId =  $request->get('unit');

        return Tematic::where(function ($query) use ($q, $unitId) {
            $query->where(\DB::raw('LOWER(name)'), 'like', "%$q%");
            $query->where('tematics.unit_id', $unitId);
        })->get();
    }
}