<?php

namespace App\Http\Controllers;

use App\Models\Surveyor;
use Illuminate\Http\Request;

class SurveyorController extends Controller
{
    protected $identifier = 'penyurvey';

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $proposalId = $request->get('proposal_id');

        return response()->json([
            'title'  => 'Tambah surveyor',
            'body'   => view('surveyor.create')
                ->with([
                    'identifier' => $this->identifier,
                    'proposalId' => $proposalId
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            // ...
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return Surveyor::create([
            'proposal_id' => $request->get('proposal_id'),
            'name'        => $request->get('name'),
            'nip'         => $request->get('nip'),
            'phone'       => $request->get('phone'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function edit($id)
    {
        $surveyor = Surveyor::with(['proposal'])->find($id);

        return response()->json([
            'title'  => "Edit surveyor {$surveyor->name}",
            'body'   => view('surveyor.edit')
                ->with([
                    'surveyor'   => $surveyor,
                    'identifier' => $this->identifier
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Surveyor     $surveyor
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = validator($request->all(), [
            // ...
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $surveyor = Surveyor::find($id);

        $surveyor->name = $request->get('name');
        $surveyor->nip = $request->get('nip');
        $surveyor->phone = $request->get('phone');
        $surveyor->save();

        return response()->json([]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        Surveyor::find($id)->delete();

        return response()->json([]);
    }

    public function data(Request $request)
    {
        $proposalId = $request->get('proposal_id');

        $builder = Surveyor::whereProposalId($proposalId);

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {
                return view('layout.action')
                    ->with([
                        'edit_link'   => route('proposals.surveyors.edit', [$data->id]),
                        'delete_link' => route('proposals.surveyors.destroy', [$data->id]),
                        'identifier'  => $this->identifier
                    ])
                    ->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
