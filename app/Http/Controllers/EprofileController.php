<?php

namespace App\Http\Controllers;

use App\DataTables\EprofileDataTable;
use App\Events\EprofileCreated;
use App\Models\Eprofile;
use App\Models\EprofileAsset;
use App\Models\Proposal;
use App\Models\Status;
use App\Models\Unit;
use App\Repositories\EprofileExport;
use Illuminate\Http\Request;

class EprofileController extends Controller
{
    protected $identifier = 'eprofile';

    public static $totalAssetImages = 3;

    protected $rules = [
        'program'       => 'required',
        'location'      => 'required',
        'recipient'     => 'required',
        'amount_unit'   => 'required',
        'total_budget'  => 'required',
//        'proposed_year' => 'required'
    ];

    protected $ruleMessages = [
        'program.required'       => 'Kegiatan tidak boleh kosong',
        'location.required'      => 'Lokasi tidak boleh kosong',
        'recipient.required'     => 'Penerima manfaat tidak boleh kosong',
        'amount_unit.required'   => 'Jumlah unit tidak boleh kosong',
        'total_budget.required'  => 'Anggaran tidak boleh kosong',
//        'proposed_year.required' => 'Tahun usulan pembangunan tidak boleh kosong'
    ];

    public function index(EprofileDataTable $dataTable)
    {
        $title = 'eProfile';

        return $dataTable->render('eprofile.index', [
            'title'      => $title,
            'identifier' => $this->identifier,
            'units'      => Unit::all(),
        ]);
    }

    public function create(Request $request)
    {
        $title = 'EProfile';

        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        if ($request->has('proposal')) {
            $proposalId = $request->get('proposal');

            $proposal = Proposal::with([
                'eprofile' => function ($q) {
                    $q->with('budgets');
                },

            ])->find($proposalId);

            return view('eprofile.create', compact('title', 'units', 'proposal'));
        }

        return view('eprofile.create-clean', compact('title', 'units'));
    }

    public function edit($id)
    {

//        $proposal = Proposal::with([
//            'eprofile' => function ($q) {
//                $q->with('budgets');
//            },
//
//        ])->find($id);

        $eprofile = Eprofile::with(['proposal', 'unit','assets'])->find($id);


        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        $title = 'EProfile';

        return view('eprofile.edit', compact('title', 'proposal', 'eprofile', 'units'));
    }

    public function update($id, Request $request)
    {

        $validator = validator($request->all(), $this->rules, $this->ruleMessages);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $dataGlobal = [
            'creator_id'      => \Auth::user()->id,
            'status_id'       => Status::whereSlug('proposal-passed')->first()->id,
            'program_id'      => $request->get('program'),
            'location_id'     => $request->get('location'),
            'recipient_id'    => $request->get('recipient'),
            'proposer_remark' => $request->get('proposer_remark'),
            'proposer_phone'  => $request->get('proposer_phone'),
            'proposer_email'  => $request->get('proposer_email'),
            'proposed_year'   => $request->get('proposed_year'),
            'submission_date' => $request->get('submission_date'),
            'address'         => $request->get('address'),
            'amount_unit'     => $request->get('amount_unit'),
            'total_budget'    => $request->get('total_budget'),
            'latitude'        => $request->get('latitude'),
            'longitude'       => $request->get('longitude'),
        ];

        if ($request->has('proposal')) {

            $proposalId = $request->get('proposal');
            $proposal = Proposal::find($proposalId);

            $dataSpecific = [
                'proposal_id' => $proposalId,
                'unit_id'     => $proposal->unit_id,
                'proposer_id' => $proposal->proposer_id,
            ];

        } else {
            $dataSpecific = [
                'unit_id'     => $request->get('unit'),
                'proposer_id' => $request->get('proposer'),
            ];

        }

        $data = array_merge($dataGlobal, $dataSpecific);


        Eprofile::where('id', '=', $id)->update($data);

        $eprofile = Eprofile::find($id);

        // Delete dulu semua gambarnya
        EprofileAsset::where('eprofile_id', '=', $eprofile->id)->delete();

        // Save image
        for ($i = 1; $i <= self::$totalAssetImages; $i++) {
            if ($request->has('location_asset_' . $i)) {
                $location{$i} = $request->get('location_asset_' . $i);
                $filename{$i} = $request->get('filename_asset_' . $i);

            } else {
                $location{$i} = '';
                $filename{$i} = '';
            }

            $asset = new EprofileAsset();
            $asset->eprofile_id = $eprofile->id;
            $asset->type = 'image';
            $asset->filename = $filename{$i};
            $asset->url = $location{$i};
            $asset->save();
        }



        return redirect()->route('eprofile.index');
    }

    public function store(Request $request)
    {
        $validator = validator($request->all(), $this->rules, $this->ruleMessages);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $dataGlobal = [
            'creator_id'      => \Auth::user()->id,
            'status_id'       => Status::whereSlug('proposal-passed')->first()->id,
            'program_id'      => $request->get('program'),
            'location_id'     => $request->get('location'),
            'recipient_id'    => $request->get('recipient'),
            'proposer_remark' => $request->get('proposer_remark'),
            'proposer_phone'  => $request->get('proposer_phone'),
            'proposer_email'  => $request->get('proposer_email'),
            'proposed_year'   => $request->get('proposed_year'),
            'latitude'        => $request->get('latitude'),
            'longitude'       => $request->get('longitude'),
            'submission_date' => $request->get('submission_date'),
            'address'         => $request->get('address'),
            'amount_unit'     => $request->get('amount_unit'),
            'total_budget'    => $request->get('total_budget')
        ];

        if ($request->has('proposal')) {

            $proposalId = $request->get('proposal');
            $proposal = Proposal::find($proposalId);

            $unitId = $proposal->unit_id;

            $dataSpecific = [
                'proposal_id' => $proposalId,
                'unit_id'     => $proposal->unit_id,
                'proposer_id' => $proposal->proposer_id,
            ];

        } else {

            $unitId = $request->get('unit');

            $dataSpecific = [
                'unit_id'     => $request->get('unit'),
                'proposer_id' => $request->get('proposer'),
            ];

        }

        $data = array_merge($dataGlobal, $dataSpecific);

        $eprofile = Eprofile::create($data);

        // Save image
        for ($i = 1; $i <= self::$totalAssetImages; $i++) {
            if($request->has('location_asset_' . $i)) {
                $location{$i} = $request->get('location_asset_' . $i);
                $filename{$i} = $request->get('filename_asset_' . $i);


            } else {
                $location{$i} = '';
                $filename{$i} = '';
            }

            $asset = new EprofileAsset();
            $asset->eprofile_id = $eprofile->id;
            $asset->type = 'image';
            $asset->filename = $filename{$i};
            $asset->url = $location{$i};
            $asset->save();
        }

//        event(new EprofileCreated());


        return redirect()->route('eprofile.index');
    }

    public function destroy($id)
    {
        $destroy = Eprofile::destroy($id);

//        event(new EprofileCreated());

        return $destroy;

    }

    public function data()
    {

        $builder = Eprofile::select([
            'eprofiles.*',
            \DB::raw('units.name as unit_name'),
            \DB::raw('agencies.name as proposer_name'),
            \DB::raw('programs.name as program_name')
        ])
            ->leftJoin('units', 'units.id', '=', 'eprofiles.unit_id')
            ->leftJoin('programs', 'programs.id', '=', 'eprofiles.program_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'eprofiles.proposer_id')
            ->orderBy('eprofiles.id', 'desc');

        // Searching
        $keySearchUnitId = request()->get('unit');
        $keySearchProposerName = request()->get('proposer_name');
        $keySearchProgramName = request()->get('program_name');
        $keySearchProposedYear = request()->get('proposed_year');
        $keySearchProposerRemark = request()->get('proposer_remark');

        if ((int)$keySearchUnitId !== 0) {
            $builder->where('eprofiles.unit_id', '=', $keySearchUnitId);
        }

        if (trim($keySearchProposerName) !== '') {
            $keySearchProposerName = strtolower($keySearchProposerName);
            $builder->where(\DB::raw('lower(agencies.name)'), 'like', "%{$keySearchProposerName}%");
        }

        if (trim($keySearchProgramName) !== '') {
            $keySearchProgramName = strtolower($keySearchProgramName);
            $builder->where(\DB::raw('lower(programs.name)'), 'like', "%{$keySearchProgramName}%");
        }

        if (trim($keySearchProposerRemark) !== '') {
            $keySearchProposerRemark = strtolower($keySearchProposerRemark);
            $builder->where(\DB::raw('lower(eprofiles.proposer_remark)'), 'like', "%{$keySearchProposerRemark}%");
        }

        if ((int)$keySearchProposedYear !== 0) {
            $builder->where('eprofiles.proposed_year', '=', $keySearchProposedYear);
        }

        return \Datatables::of($builder)
            ->editColumn('total_budget', function ($data) {
                return rupiah($data->total_budget);
            })
            ->addColumn('location', function ($data) {
                return cache('region-' . $data->location_id);
            })
            ->addColumn('action', function ($data) {
                return view('eprofile.action')
                    ->with([
                        'edit_link'   => route('eprofile.edit', [$data->id]),
                        'delete_link' => route('eprofile.destroy', [$data->id]),
                        'identifier'  => $this->identifier,
                        'data'        => $data
                    ])
                    ->render();
            })
            ->make(true);
    }

    public function showFormAdvancedExcel()
    {
        $years = Eprofile::select('proposed_year')
            ->orderBy('proposed_year', 'asc')
            ->distinct()->pluck('proposed_year');


        return view('eprofile.advanced-excel', compact('years'));
    }

    public function exportRincian()
    {
        return (new EprofileExport())
            ->rincian()
            ->region(\request('province'))
            ->year(\request('begin'), \request('end'))
            ->excel();
    }

    public function exportRekap()
    {
        return (new EprofileExport())
            ->rekap()
            ->region(\request('province'))
            ->year(\request('begin'), \request('end'))
            ->excel();
    }
}