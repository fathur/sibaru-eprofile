<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AssetController extends Controller
{
    public function upload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,bmp,xls,xlsx,zip'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        if ($request->hasFile('file')) {

            $file = $request->file('file');

            $path = $file->store('storage/upload');

            return response()->json([

                'location'  => $path,
                'file_name' => $file->getClientOriginalName(),
            ]);
        }

        abort(400);
    }

    public function view($location)
    {
//        dd(storage_path());
        $image = Image::make(file_get_contents(storage_path($location)));

        $response = \Response::make($image->encode('png'));

        // set content-type
        $response->header('Content-Type', 'image/png');

        return $response;

    }

    public function download()
    {

    }
}