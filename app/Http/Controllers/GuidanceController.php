<?php

namespace App\Http\Controllers;

use App\DataTables\GuidanceDataTable;
use App\Models\Document;
use Illuminate\Http\Request;

class GuidanceController extends Controller
{
    protected $identifier = 'pedoman';

    public function index(GuidanceDataTable $dataTable)
    {
        $title = 'Pedoman';

        return $dataTable->render('guidance.index', [
            'title'      => $title,
            'identifier' => $this->identifier,
        ]);
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'title'  => 'Pedoman baru',
                'body'   => view('guidance.modal.create')
                    ->with([
                        'identifier' => $this->identifier
                    ])
                    ->render(),
                'footer' => view('guidance.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('guidance.create');
        }
    }

    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file'  => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return Document::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'file' => $request->get('file'),
            'filename' => $request->get('file_name'),
        ]);
    }

    public function edit($id, Request $request)
    {

        $document = Document::find($id);

//        if ($request->ajax()) {
        return response()->json([
            'title'  => "Ubah pedoman",
            'body'   => view('guidance.modal.edit')
                ->with([
                    'guidance'   => $document,
                    'identifier' => $this->identifier
                ])
                ->render(),
            'footer' => view('guidance.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
//        } else {
//            return view('faq.edit');
//        }
    }

    public function update(Request $request, $id)
    {
        $document = Document::find($id);

        $validator = validator($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'file'  => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $document->title = $request->get('title');
        $document->description = $request->get('description');
        $document->file = $request->get('file');
        $document->filename = $request->get('file_name');
        $document->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('guidance.index');

    }

    public function destroy($id)
    {
        return Document::destroy($id);
    }
}