<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    protected $identifier = 'survey-result';

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $proposalId = $request->get('proposal_id');

        return response()->json([
            'title'  => 'Tambah hasil survey',
            'body'   => view('survey.create')
                ->with([
                    'identifier' => $this->identifier,
                    'proposalId' => $proposalId
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            // ...
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        // todo: upload dok

        return Survey::create([
            'proposal_id' => $request->get('proposal_id'),
            'date'        => $request->get('date'),
            'content'     => $request->get('content'),
            'attachment'  => $request->get('attachment'),
            'file_name'   => $request->get('file_name'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     * @internal param Survey $survey
     *
     */
    public function edit($id)
    {
        $survey = Survey::with(['proposal'])->find($id);

        return response()->json([
            'title'  => "Edit",
            'body'   => view('survey.edit')
                ->with([
                    'survey'     => $survey,
                    'identifier' => $this->identifier
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, $id)
    {
        $validator = validator($request->all(), [
            // ...
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $survey = Survey::find($id);

        $survey->date = $request->get('date');
        $survey->content = $request->get('content');
        $survey->attachment = $request->get('attachment');
        $survey->file_name = $request->get('file_name');
        $survey->save();

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param Survey $survey
     *
     */
    public function destroy($id)
    {
        Survey::find($id)->delete();

        return response()->json([]);
    }

    public function data(Request $request)
    {
        $proposalId = $request->get('proposal_id');

        $builder = Survey::whereProposalId($proposalId);

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {
                return view('layout.action')
                    ->with([
                        'edit_link'   => route('proposals.surveys.edit', [$data->id]),
                        'delete_link' => route('proposals.surveys.destroy', [$data->id]),
                        'identifier'  => $this->identifier
                    ])
                    ->render();
            })
            ->editColumn('attachment', function ($data) {
                return '<a href="' . url('surveys/download/' . $data->id) . '">' . $data->file_name . '</a>';
            })
            ->rawColumns(['action', 'attachment'])
            ->make(true);
    }

    public function download($id)
    {
        $survey = Survey::find($id);

//        dd($p->toArray());
        return response()->download(base_path($survey->attachment), $survey->file_name);
    }
}
