<?php

namespace App\Http\Controllers;

use App\Models\Criteria;
use App\Models\Unit;
use Illuminate\Http\Request;

class CriteriaController extends Controller
{
    protected $identifier = 'kriteria';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Kriteria';

        return view('criteria.index', compact('title'))->with('identifier', $this->identifier);
    }

    public function rusun()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Kriteria Rusun';

        return view('criteria.index', compact('title'))
            ->with('identifier', $this->identifier)
            ->with('unit', Unit::whereSlug('rusun')->first())
            ->with('type', 'rusun');
    }

    public function rusus()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Kriteria Rusus';

        return view('criteria.index', compact('title'))
            ->with('identifier', $this->identifier)
            ->with('unit', Unit::whereSlug('rusus')->first())
            ->with('type', 'rusus');
    }

    public function swadaya()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Kriteria Swadaya';

        return view('criteria.index', compact('title'))
            ->with('identifier', $this->identifier)
            ->with('unit', Unit::whereSlug('swadaya')->first())
            ->with('type', 'swadaya');
    }

    public function ruk()
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Kriteria RUK';

        return view('criteria.index', compact('title'))
            ->with('identifier', $this->identifier)
            ->with('unit', Unit::whereSlug('ruk')->first())
            ->with('type', 'ruk');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $type = $request->get('type');
        $unitSlug = $request->get('unit');
        $unit = Unit::whereSlug($unitSlug)->first();

        if ($request->ajax()) {
            return response()->json([
                'title'  => 'Kriteria baru',
                'body'   => view('criteria.modal.create')
                    ->with('identifier', $this->identifier . '-'. $type)
                    ->with('unit', $unit)
                    ->with('type', $type)
                    ->render(),
                'footer' => view('criteria.modal.footer')
                    ->with('identifier', $this->identifier . '-'. $type)
                    ->render()
            ]);
        } else {
            return view('criteria.create')
                ->with('type', $type);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $validator = validator($request->all(), [
            'order'     => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return Criteria::create([
            'unit_id'  => $request->get('unit_id'),
            'order'    => $request->get('order'),
            'type'     => $request->get('type'),
            'weight'   => $request->get('weight'),
            'content'  => $request->get('content'),
            'template' => $request->get('template'),
            'filename'  => $request->get('file_name')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function show(Criteria $criteria)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        return view('user.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function edit($id, Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $criteria = Criteria::find($id);
        $type = $request->get('type');

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit kriteria",
                'body'   => view('criteria.modal.edit')
                    ->with(['criteria' => $criteria])
                    ->with('identifier', $this->identifier . '-' . $type)
                    ->render(),
                'footer' => view('criteria.modal.footer')
                    ->with('identifier', $this->identifier . '-' . $type)
                    ->render()
            ]);
        } else {
            return view('user.edit');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $validator = validator($request->all(), [
            'order'     => 'required',
            'content' => 'required',
        ]);

        $criteria = Criteria::find($id);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $criteria->order = $request->get('order');
        $criteria->content = $request->get('content');
        $criteria->template = $request->get('template');
        $criteria->filename = $request->get('file_name');
        $criteria->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('criteria.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $criteria = Criteria::find($id);
        $criteria->delete();

        return response()->json([]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function data(Request $request)
    {
        if(\Gate::denies('do-master')) {
            abort(404);
        }

        $unitId = $request->get('unit');
        $type = $request->get('type');

        $builder = Criteria::whereUnitId($unitId)->whereType($type);

        return \Datatables::of($builder)
            ->editColumn('filename', function($data){
                return "<a href='".route('criteria.download', [$data->id])."'>{$data->filename}</a>";
            })
            ->addColumn('action', function ($data) use ($type) {
                return view('layout.action')
                    ->with([
                        'edit_link'   => route('criteria.edit', [$data->id]) . "?type={$type}",
                        'delete_link' => route('criteria.destroy', [$data->id]),
                        'identifier'     => $this->identifier . '-' .$type
                    ])
                    ->render();
            })
            ->rawColumns(['email', 'action', 'filename'])
            ->make(true);
    }

    public function download($id)
    {
        $criteria = Criteria::find($id);

        return response()->download(base_path($criteria->template), $criteria->filename);
    }
}
