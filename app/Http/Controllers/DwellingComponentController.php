<?php

namespace App\Http\Controllers;

use App\Models\DwellingComponent;
use Illuminate\Http\Request;

class DwellingComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DwellingComponent  $dwellingComponent
     * @return \Illuminate\Http\Response
     */
    public function show(DwellingComponent $dwellingComponent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DwellingComponent  $dwellingComponent
     * @return \Illuminate\Http\Response
     */
    public function edit(DwellingComponent $dwellingComponent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DwellingComponent  $dwellingComponent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DwellingComponent $dwellingComponent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DwellingComponent  $dwellingComponent
     * @return \Illuminate\Http\Response
     */
    public function destroy(DwellingComponent $dwellingComponent)
    {
        //
    }
}
