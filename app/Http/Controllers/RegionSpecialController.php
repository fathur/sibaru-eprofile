<?php

namespace App\Http\Controllers;


use App\DataTables\RegionSpecialDataTable;
use App\Models\Region;
use App\Models\RegionSpecial;
use Illuminate\Http\Request;

class RegionSpecialController extends Controller
{
    protected $identifier = '2010-2014';

    public function index($regionId, RegionSpecialDataTable $dataTable)
    {
        $region = Region::find($regionId);
        $title = 'Data ' . $region->name . ' 2010-2014';

        return $dataTable->render('region.special.index', [
            'title'      => $title,
            'identifier' => $this->identifier,
            'region'     => $region
        ]);

    }

    public function create($regionId, Request $request)
    {
        $region = Region::find($regionId);

        return response()->json([
            'title'  => 'Data baru',
            'body'   => view('region.special.modal.create')
                ->with([
                    'identifier' => $this->identifier,
                    'region'     => $region
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);

    }

    public function store($regionId, Request $request)
    {
        $validator = validator($request->all(), [
//            'unit_id' => 'required',
            'program' => 'required',
            'unit'    => 'required',
            'volume'  => 'required',
            'budget'  => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        return RegionSpecial::create([
            'region_id' => $regionId,
            'order'     => $request->get('order'),
            'unit_id'   => $request->get('unit_id'),
            'program'   => $request->get('program'),
            'unit'      => $request->get('unit'),
            'volume'    => $request->get('volume'),
            'budget'    => $request->get('budget'),
        ]);
    }

    public function edit($regionId, $id)
    {
        $special = RegionSpecial::find($id);
        $region = Region::find($regionId);

        return response()->json([
            'title'  => "Edit data",
            'body'   => view('region.special.modal.edit')
                ->with([
                    'data'       => $special,
                    'region'     => $region,
                    'identifier' => $this->identifier,
                ])
                ->render(),
            'footer' => view('layout.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);

    }

    public function update($regionId, $id, Request $request)
    {
        $special = RegionSpecial::find($id);

        $validator = validator($request->all(), [
//            'unit_id' => 'required',
            'program' => 'required',
            'unit'    => 'required',
            'volume'  => 'required',
            'budget'  => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $special->order = $request->get('order');
        $special->unit_id = $request->get('unit_id');
        $special->program = $request->get('program');
        $special->unit = $request->get('unit');
        $special->volume = $request->get('volume');
        $special->budget = $request->get('budget');
        $special->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('regions.specials.index');
    }

    public function destroy($regionId, $id)
    {
        return RegionSpecial::destroy($id);

    }
}