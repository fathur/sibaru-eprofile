<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Faq;
use App\Models\Post;
use App\Models\Proposal;
use App\Models\Region;
use App\Models\Shortlist;
use App\Models\Status;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        if (\Auth::check()) {
            return redirect()->route('dashboard');
        }

        $prologue = Post::whereSlug('pengantar')->first()->content;

        return view('public.home', compact('prologue'));
    }

    public function pedoman(Request $request)
    {

        $documents = Document::select('*');

        $q = null;

        if ($request->has('q')) {
            $q = $request->get('q');

            $documents->where(function ($query) use ($q) {

                // use ILIKE for case insensitive

                $query->where('title', 'ilike', "%$q%");
                $query->orWhere('description', 'ilike', "%$q%");
            });
        }

        $documents = $documents->paginate(20);


        return view('public.pedoman', compact('documents', 'q'));
    }

    public function faq(Request $request)
    {
        $faqs = Faq::select('*');

        $q = null;

        if ($request->has('q')) {
            $q = $request->get('q');
            $faqs->where(function ($query) use ($q) {
                $query->where('question', 'ilike', "%$q%");
                $query->orWhere('answer', 'ilike', "%$q%");
            });
        }

        $faqs = $faqs->get();

        return view('public.faq', compact('faqs', 'q'));

    }

    public function aboutUs()
    {
        return view('public.about');
    }

    public function program()
    {
        $regions = Region::where('level', 1)
            ->orderBy('order', 'asc')
            ->get();

        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        $year = Carbon::now()->year;

        $data = Proposal::select([
            'proposals.unit_id',
            \DB::raw('rb.id AS province_id'),
            \DB::raw('count(*) AS cnt')
        ])
//            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin(\DB::raw('regions AS ra'), 'ra.id', '=', 'agencies.region_id')
            ->leftJoin(\DB::raw('regions AS rb'), 'rb.id', '=', 'ra.parent_id')
//            ->where('proposals.proposed_year', '=', $year)
            ->whereRaw(\DB::raw('extract(year from submission_date) = ?'), [$year])
            ->whereNotIn('status_id', [
                Status::whereSlug('proposal-draft')->first()->id
            ])
            ->groupBy(['proposals.unit_id', 'rb.id'])
//            ->orderBy('rb.name', 'asc')
            ->get();

        $totals = Proposal::select([\DB::raw('rb.id AS province_id'), \DB::raw('count(*) AS cnt')])
//            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin(\DB::raw('regions AS ra'), 'ra.id', '=', 'agencies.region_id')
            ->leftJoin(\DB::raw('regions AS rb'), 'rb.id', '=', 'ra.parent_id')
//            ->where('proposals.proposed_year', '=', $year)
            ->whereRaw(\DB::raw('extract(year from submission_date) = ?'), [$year])

            ->whereNotIn('status_id', [
                Status::whereSlug('proposal-draft')->first()->id
            ])
            ->groupBy(['rb.id'])
            //            ->orderBy('rb.name', 'asc')
            ->get();

        $years = Proposal::select([\DB::raw('distinct extract(year from submission_date) as year')])
            ->whereNotNull('submission_date')
            ->orderBy('year')
            ->pluck('year');


//        dd($data->toArray());
        $provinces = Region::whereLevel(1)->orderBy('order', 'asc')->pluck('name');

        $charts = $this->generateChartsBoilerplate();

        return view('public.program', compact('regions', 'data', 'units', 'years',
            'totals', 'year', 'charts', 'provinces'));

    }

    public function programYear($year)
    {
        $regions = Region::where('level', 1)
            ->orderBy('order', 'asc')
            ->get();

        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        $data = Proposal::select([
            'proposals.unit_id',
            \DB::raw('rb.id AS province_id'),
            \DB::raw('count(*) AS cnt')
        ])
//            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin(\DB::raw('regions AS ra'), 'ra.id', '=', 'agencies.region_id')
            ->leftJoin(\DB::raw('regions AS rb'), 'rb.id', '=', 'ra.parent_id')
//            ->where('proposals.proposed_year', '=', $year)
            ->whereRaw(\DB::raw('extract(year from submission_date) = ?'), [$year])
            ->whereNotIn('status_id', [
                Status::whereSlug('proposal-draft')->first()->id
            ])
            ->groupBy(['proposals.unit_id', 'rb.id'])
//            ->orderBy('rb.name', 'asc')
            ->get();

        $totals = Proposal::select([\DB::raw('rb.id AS province_id'), \DB::raw('count(*) AS cnt')])
//            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin(\DB::raw('regions AS ra'), 'ra.id', '=', 'agencies.region_id')
            ->leftJoin(\DB::raw('regions AS rb'), 'rb.id', '=', 'ra.parent_id')
//            ->where('proposals.proposed_year', '=', $year)
            ->whereRaw(\DB::raw('extract(year from submission_date) = ?'), [$year])

            ->whereNotIn('status_id', [
                Status::whereSlug('proposal-draft')->first()->id
            ])
            ->groupBy(['rb.id'])
            //            ->orderBy('rb.name', 'asc')
            ->get();

        $years = Proposal::select([\DB::raw('distinct extract(year from submission_date) as year')])
            ->whereNotNull('submission_date')
            ->orderBy('year')
            ->pluck('year');


//        dd($data->toArray());
        $provinces = Region::whereLevel(1)->orderBy('order', 'asc')->pluck('name');

        $charts = $this->generateChartsBoilerplate();

        return view('public.program', compact('regions', 'data', 'units', 'years',
            'totals', 'year', 'charts', 'provinces'));

    }

    public function programDetail($id, $year)
    {
        $region = Region::with('agency')->find($id);

//        dd($region->toArray());

        $data = Shortlist::select([
            'proposals.*',
            \DB::raw('shortlists.proposed_year AS shortlist_year'),
            \DB::raw('programs.name AS program_name'),
            \DB::raw('units.name AS unit_name'),
            \DB::raw('recipients.name AS recipient_name'),
            \DB::raw('agencies.name AS agency_name'),
        ])
            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin(\DB::raw('regions AS ra'), 'ra.id', '=', 'agencies.region_id')
            ->leftJoin(\DB::raw('regions AS rb'), 'rb.id', '=', 'ra.parent_id')
            ->leftJoin('programs', 'programs.id', '=', 'proposals.program_id')
            ->leftJoin('units', 'units.id', '=', 'proposals.unit_id')
            ->leftJoin('recipients', 'recipients.id', '=', 'proposals.recipient_id')
            ->where('rb.id', '=', $region->id)
            ->where('shortlists.proposed_year', '=', $year)
            ->get();

//        dd($data->toArray());

        return view('public.program-detail', compact('year', 'data'));
    }

    public function programChartData()
    {
        $year = request('year');

        $data = [

            'rusun'   => $this->generateRusunChartData($year),
            'rusus'   => $this->generateRususChartData($year),
            'ruk'     => $this->generateRukChartData($year),
            'swadaya' => $this->generateSwadayaChartData($year),
        ];
        return response()->json($data);
    }

    public function downloadPedoman($id)
    {
        $document = Document::find($id);

        return response()->download(base_path($document->file), $document->filename);

    }

    protected function generateChartsBoilerplate()
    {
        $charts = [];

        $currentYear = Carbon::now()->year;
        $lastThreeYear = Carbon::now()->subYears(3)->year;

        for ($year = $currentYear; $year > 2016; $year--) {
            array_push($charts, (object)[
                'year' => $year,
            ]);
        }

        return $charts;
    }

    private function generateRusunChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUSUN['slug'])->first();

        return [
            'name'  => 'Rusun',
            'color' => '#00ccff',
            'data'  => (new Statistic())
                ->whenStatusesNotIn(['proposal-draft'])
                ->setYear($year)->regionProposalTotal($unit)
        ];
    }

    private function generateRususChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUSUS['slug'])->first();

        return [
            'name'  => 'Rusus',
            'color' => '#ccff00',
            'data'  => (new Statistic())
                ->whenStatusesNotIn(['proposal-draft'])
                ->setYear($year)->regionProposalTotal($unit)
        ];
    }

    private function generateSwadayaChartData($year)
    {
        $unit = Unit::whereSlug(Unit::SWADAYA['slug'])->first();

        return [
            'name'  => 'Swadaya',
            'color' => '#9bc200',
            'data'  => (new Statistic())
                ->whenStatusesNotIn(['proposal-draft'])
                ->setYear($year)->regionProposalTotal($unit)
        ];
    }

    private function generateRukChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUK['slug'])->first();

        return [
            'name'  => 'RUK',
            'color' => '#ffcc00',
            'data'  => (new Statistic())
                ->whenStatusesNotIn(['proposal-draft'])
                ->setYear($year)->regionProposalTotal($unit)
        ];
    }
}