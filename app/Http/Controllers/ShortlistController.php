<?php

namespace App\Http\Controllers;

use App\DataTables\ViewShortlistDataTable;
use App\Models\DwellingPlan;
use App\Models\Proposal;
use App\Models\Shortlist;
use App\Models\ShortlistBudget;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ShortlistController extends Controller
{
    protected $identifier = 'shortlist';

    public function index()
    {
//        dd(\Auth::user()->with('unit')->get()->toArray());
        $title = 'Shortlist';

        return view('shortlist.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    public function data()
    {
        $builder = Shortlist::select([
            \DB::raw('shortlists.proposal_id as proposal_id'),
            \DB::raw('units.name as unit_name'),
//            \DB::raw('statuses.name as status_name'),
//            \DB::raw('programs.name as program_name'),
//            'proposals.proposed_year',
//            'proposals.status_id',
//            'proposals.proposer_remark',
//            \DB::raw('proposals.proposer_id'),
//            \DB::raw('proposals.id as id'),
            \DB::raw('agencies.name as proposer_name'),

            \DB::raw('SUM(dwelling_plans.amount) as a_amount'),
            \DB::raw('SUM(dwelling_plans.budget_plan) as a_budget'),

            \DB::raw('SUM(shortlist_budgets.amount) as b_amount'),
            \DB::raw('SUM(shortlist_budgets.budget_plan) as b_budget'),
        ])
            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('dwelling_plans', 'dwelling_plans.proposal_id', '=', 'proposals.id')
            ->leftJoin('shortlist_budgets', 'shortlist_budgets.dwelling_plan_id', '=', 'dwelling_plans.id')
            ->join('units', 'units.id', '=', 'proposals.unit_id')
            ->join('programs', 'programs.id', '=', 'proposals.program_id')
//            ->join('recipients', 'recipients.id', '=', 'proposals.recipient_id')
            ->join('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->join('statuses', 'statuses.id', '=', 'proposals.status_id')
            ->whereIn('status_id', Status::whereIn('slug', [/*'technical-passed', */'proposal-passed'])->pluck('id'))
            ->groupBy(['shortlists.proposal_id','units.name','agencies.name']);

        if (\Gate::denies('do-admin')) {

            if (\Auth::user()->agency_id !== null) {
                $builder->where('units.id', '=', \Auth::user()->unit_id);
                $builder->whereProposerId(\Auth::user()->agency->id);

            }
        }



//        if(\Gate::allows('do-directorate')) {
//            $statusesId = Status::whereIn('slug', [
//                'proposal-draft'
//            ])->pluck('id');
//
//            $builder->whereNotIn('status_id', $statusesId);
//        }

//        dd($builder->get()->toArray());

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {

                return view('shortlist.action')
                    ->with([
                        'edit_link'   => route('shortlists.edit', [$data->proposal_id]),
                        'delete_link' => route('shortlists.destroy', [$data->proposal_id]),
                        'edit_status_link' => route('proposals.status.edit', [$data->id]),
                        'identifier'  => $this->identifier,
                        'proposal'    => $data
                    ])
                    ->render();
            })
            ->editColumn('proposed_date', function ($data) {
                return Carbon::parse($data->proposed_date)->year;
            })
            ->editColumn('a_budget', function($data) {
                return rupiah($data->a_budget);
            })
            ->editColumn('b_budget', function($data) {
                return rupiah($data->b_budget);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function create($id)
    {
        $proposal = Proposal::with([
            //'unit', 'program', 'recipient', 'location', 'proposer','status',
            'shortlist',
            'dwellingPlans' => function ($q) {
                $q->with('unitType');
                $q->with('unitSize');
                $q->with('shortlist');
            }
        ])->find($id);

        if (\Gate::denies('shortlists.create', $proposal)) {
            abort(404);
        }

        $title = 'Shortlist';

        // calculate sum
        $sumUnit = 0;
        $sumBudget = 0;
        foreach ($proposal->dwellingPlans as $dwellingPlan) {
            $sumUnit += $dwellingPlan->amount;
            $sumBudget += $dwellingPlan->budget_plan;
        }

        $sum = (object)[
            'unit'   => $sumUnit,
            'budget' => $sumBudget
        ];

        if($proposal->shortlist != null) {
            return view('shortlist.edit', compact('title', 'proposal', 'sum'));

        }

        return view('shortlist.create', compact('title', 'proposal', 'sum'));
    }

    /**
     * @param Request $request
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $newAmount = $request->get('new_amount');
        $newBudgetPlan = $request->get('new_budget_plan');

        $proposalId = $request->get('proposal_id');

        $proposal = Proposal::find($proposalId);

//        dd($request->get('code'));

        // step 1. simpan shortlist proposal


        if($request->method() == 'PUT') {
            $shortlist = Shortlist::whereProposalId($proposalId)->first();
            $shortlist->code = $request->get('code');
            $shortlist->score = $request->get('score');
            $shortlist->save();
        } else {
            $shortlist = $proposal->shortlist()->create([
                'proposed_year' => $request->get('proposed_year', $proposal->proposed_year),
                'code'  => $request->get('code'),
                'score' => $request->get('score')
            ]);
        }


        // step 2. simpan budget perumahan yang baru
        $proposal->dwellingPlans->each(function ($dwellingPlan) use ($newAmount, $newBudgetPlan, $shortlist) {

            foreach ($newAmount as $amountDwellingPlanId => $amount) {

                if ((int)$dwellingPlan->id == (int)$amountDwellingPlanId) {


                    foreach ($newBudgetPlan as $bpDwellingPlanId => $budgetPlan) {
                        if ($dwellingPlan->id == (int)$bpDwellingPlanId) {

                            $bp = (int)preg_replace('/[^0-9\,]+/', '', $budgetPlan);

                            $cntSb = ShortlistBudget::where('dwelling_plan_id', $bpDwellingPlanId)->count();

                            if($cntSb == 0) {
                                $dwellingPlan->shortlist()->updateOrCreate([
                                    'shortlist_id' => $shortlist->id,
                                    'amount'      => $amount,
                                    'budget_plan' => $bp
                                ]);
                            } else {
                                $fSb = ShortlistBudget::where('dwelling_plan_id', $bpDwellingPlanId)->first();
                                $fSb->amount = $amount;
                                $fSb->budget_plan = $bp;
                                $fSb->save();
                            }
                        }
                    }

                }
            }
        });

        // update sum amout and sum budget plan in shortlist table
        \Artisan::call('calculate:shortlist', [
            '--id' => $shortlist->id
        ]);

        if($proposal->status->slug == 'technical-passed') {
            \App\Repositories\Proposal::changeStatus($proposal->id);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function update(Request $request)
    {
        $newAmount = $request->get('new_amount');
        $newBudgetPlan = $request->get('new_budget_plan');

        $proposalId = $request->get('proposal_id');

        $proposal = Proposal::find($proposalId);

        // step 1. simpan shortlist proposal
//        $proposal->shortlist()->firstOrCreate([
//            'proposed_year' => $request->get('proposed_year', $proposal->proposed_year)
//        ]);
        $shortlist = $proposal->shortlist();
        $shortlist->proposed_year = 3;
        $shortlist->save();

        // step 2. simpan budget perumahan yang baru
        $proposal->dwellingPlans->each(function ($budget) use ($newAmount, $newBudgetPlan) {


            foreach ($newAmount as $amountDwellingPlanId => $amount) {

                if ($budget->id == $amountDwellingPlanId) {

                    foreach ($newBudgetPlan as $bpDwellingPlanId => $budgetPlan) {
                        if ($budget->id == $bpDwellingPlanId) {

                            $bp = (int)preg_replace('/[^0-9\,]+/', '', $budgetPlan[0]);

                            $budget->shortlist()->firstOrCreate([
                                'amount'      => $amount[0],
                                'budget_plan' => $bp
                            ]);
                        }
                    }

                }
            }
        });

        return redirect()->back();
    }

    public function edit($id)
    {
        return $this->create($id);
    }

    /**
     * View shortlist like proposal
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     *
     * @param ViewShortlistDataTable $dataTable
     *
     * @return
     */
    public function view(ViewShortlistDataTable $dataTable)
    {
        $title = 'Daftar Shortlist';

        return $dataTable->render('shortlist.view', [
            'title' => $title,
            'identifier' => $this->identifier
        ]);
    }
}