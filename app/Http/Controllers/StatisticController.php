<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 8/19/17
 * Time: 14:39
 */

namespace App\Http\Controllers;


use App\DataTables\StatisticTableDataTable;
use App\Models\Agency;
use App\Models\Eprofile;
use App\Models\Proposal;
use App\Models\Region;
use App\Models\Status;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    protected $identifier = 'proposal-statistic';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function proposalIndex()
    {
        $type = 'proposal';
        $statuses = Status::all();
        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        $years = Proposal::select([\DB::raw('distinct extract(year from submission_date) as year')])
            ->whereNotNull('submission_date')
            ->orderBy('year')
            ->pluck('year');

        return view("statistic.{$type}.index", compact('statuses', 'units', 'type'))
            ->with([
                'user' => \Auth::user(),
                'years' => $years
            ]);
    }

    public function eprofileIndex()
    {
        $type = 'eprofile';

        $statuses = Status::all();
        $units = Unit::whereNotIn('slug', ['dirjen'])->get();


        // Uniques years
        $years = Eprofile::select([
            \DB::raw("distinct proposed_year")
        ])->orderBy('proposed_year', 'desc')
            ->pluck('proposed_year');


        return view("statistic.{$type}.index", compact('statuses', 'units', 'type'))
            ->with([
                'years' => $years,
                'user' => \Auth::user()
            ]);
    }

    /**
     * Formatted data for statistic program
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function proposalUnits(Request $request)
    {
        $data = Proposal::select([
            'units.program',
            'units.color',
            \DB::raw('COUNT(*) as count')
        ])->leftJoin('units', 'units.id', '=', 'proposals.unit_id')
            ->groupBy(['units.program', 'units.color']);


        if ($request->has('year')) {
            $year = $request->get('year', 0);

            if ((int)$year !== 0) {
                $data->whereRaw(\DB::raw("extract(year from submission_date) = ?"), [$year]);
            }
        }

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('proposals.status_id', '=', $statusId);
            }
        } else {
            $data->whereNotIn('proposals.status_id', [Status::whereSlug('proposal-draft')->first()->id]);
        }

        if ($request->has('region')) {

            $regionId = $request->get('region');


            if ($regionId != 0) {

                $rids = Region::descendantsAndSelf($regionId)->pluck('id');

                $data->whereIn('location_id', $rids);
            }
        }

        $data = $data->get();

        $results = [];

        foreach ($data as $datum) {
            array_push($results, [
                'name' => $datum->program,
                'y' => $datum->count,
                'color' => $datum->color
            ]);
        }

        return response()->json($results);
    }

    /**
     * Formatted data for statistic program
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eprofileUnits(Request $request)
    {
        $data = Eprofile::select([
            'units.program',
            'units.color',
            \DB::raw('COUNT(*) as count')
        ])->leftJoin('units', 'units.id', '=', 'eprofiles.unit_id')
            ->groupBy(['units.program', 'units.color']);

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('eprofiles.status_id', '=', $statusId);
            }
        }

        if ($request->has('region')) {

            $regionId = $request->get('region');


            if ($regionId != 0) {

                $rids = Region::descendantsAndSelf($regionId)->pluck('id');

                $data->whereIn('location_id', $rids);
            }
        }

        // Filter by year
        if ($request->has('year')) {
            if ((int)$request->get('year') !== 0) {
                $data->where('proposed_year', '=', $request->get('year'));
            }
        }

        $data = $data->get();

        $results = [];

        foreach ($data as $datum) {
            array_push($results, [
                'name' => $datum->program,
                'y' => $datum->count,
                'color' => $datum->color
            ]);
        }

        return response()->json($results);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function proposalRecipients(Request $request)
    {
        $data = Proposal::select([
            'recipients.name',
            \DB::raw('COUNT(*) as count')
        ])->leftJoin('recipients', 'recipients.id', '=', 'proposals.recipient_id')
            ->groupBy(['recipients.name']);

        if ($request->has('year')) {
            $year = $request->get('year', 0);

            if ((int)$year !== 0) {
                $data->whereRaw(\DB::raw("extract(year from submission_date) = ?"), [$year]);
            }
        }

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('proposals.status_id', '=', $statusId);
            }
        }

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('proposals.status_id', '=', $statusId);
            }
        }

        if ($request->has('unit')) {

            $unitId = $request->get('unit');

            if ($unitId != 0) {
                $data->where('proposals.unit_id', '=', $unitId);
            }
        }

        if ($request->has('region')) {

            $regionId = $request->get('region');


            if ($regionId != 0) {

                $rids = Region::descendantsAndSelf($regionId)->pluck('id');

                $data->whereIn('location_id', $rids);
            }
        }

        $data = $data->get();

        $results = [];

        foreach ($data as $datum) {
            array_push($results, [
                'name' => $datum->name,
                'y' => $datum->count
            ]);
        }

        return response()->json($results);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eprofileRecipients(Request $request)
    {
        $data = Eprofile::select([
            'recipients.name',
            \DB::raw('COUNT(*) as count')
        ])->leftJoin('recipients', 'recipients.id', '=', 'eprofiles.recipient_id')
            ->groupBy(['recipients.name']);

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('eprofiles.status_id', '=', $statusId);
            }
        }

        if ($request->has('status')) {

            $statusId = $request->get('status');

            if ($statusId != 0) {
                $data->where('eprofiles.status_id', '=', $statusId);
            }
        }

        if ($request->has('unit')) {

            $unitId = $request->get('unit');

            if ($unitId != 0) {
                $data->where('eprofiles.unit_id', '=', $unitId);
            }
        }

        if ($request->has('region')) {

            $regionId = $request->get('region');


            if ($regionId != 0) {

                $rids = Region::descendantsAndSelf($regionId)->pluck('id');

                $data->whereIn('location_id', $rids);
            }
        }

        // Filter by year
        if ($request->has('year')) {
            if ((int)$request->get('year') !== 0) {
                $data->where('proposed_year', '=', $request->get('year'));
            }
        }

        $data = $data->get();

        $results = [];

        foreach ($data as $datum) {
            array_push($results, [
                'name' => $datum->name,
                'y' => $datum->count
            ]);
        }

        return response()->json($results);
    }

    public function proposalRegions(Request $request)
    {
        $statusId = $request->get('status');

        $year = $request->get('year');

        $results = [
            'categories' => $this->regionCategories(),
            'series' => [
                [
                    'name' => 'Rusun',
                    'color' => Unit::whereSlug('rusun')->first()->color,
                    'data' => $this->totalRusun('proposal', $statusId, null, $year)
                ],
                [
                    'name' => 'Rusus',
                    'color' => Unit::whereSlug('rusus')->first()->color,
                    'data' => $this->totalRusus('proposal', $statusId, null, $year)
                ],
                [
                    'name' => 'RUK',
                    'color' => Unit::whereSlug('ruk')->first()->color,
                    'data' => $this->totalRuk('proposal', $statusId, null, $year)
                ],
                [
                    'name' => 'Swadaya',
                    'color' => Unit::whereSlug('swadaya')->first()->color,
                    'data' => $this->totalSwadaya('proposal', $statusId, null, $year)
                ]
            ]
        ];


        return response()->json($results);

    }

    public function eprofileRegions(Request $request)
    {
        $statusId = $request->get('status');

        $year = $request->get('year');

        if (\Gate::allows('do-technical')) {
            $user = \Auth::user();
            $unit = Unit::find($user->unit_id);

            $totalFunction = 'total' . title_case($unit->slug);

            $series = [
                [
                    'name' => $unit->program,
                    'data' => $this->{$totalFunction}('eprofile', $statusId, null, $year)
                ]
            ];
        } else {
            $series = [
                [
                    'name' => 'Rusun',
                    'color' => Unit::whereSlug('rusun')->first()->color,
                    'data' => $this->totalRusun('eprofile', $statusId, null, $year)
                ],
                [
                    'name' => 'Rusus',
                    'color' => Unit::whereSlug('rusus')->first()->color,

                    'data' => $this->totalRusus('eprofile', $statusId, null, $year)
                ],
                [
                    'name' => 'RUK',
                    'color' => Unit::whereSlug('ruk')->first()->color,

                    'data' => $this->totalRuk('eprofile', $statusId, null, $year)
                ],
                [
                    'name' => 'Swadaya',
                    'color' => Unit::whereSlug('swadaya')->first()->color,
                    'data' => $this->totalSwadaya('eprofile', $statusId, null, $year)
                ]
            ];
        }


        $results = [
            'categories' => $this->regionCategories(),
            'series' => $series
        ];


        return response()->json($results);

    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function regionCategories()
    {
        $regions = Region::where('level', '=', 1)
            ->orderBy('order', 'asc')
            ->pluck('name');

        return $regions;
    }

    /**
     * @param $type
     * @param null|integer $status
     * @param null $region
     * @param null $year
     * @return array
     */
    private function totalRusun($type, $status = null, $region = null, $year = null)
    {
        $unit = Unit::whereSlug(Unit::RUSUN['slug'])->first();
        $func = 'region' . title_case($type) . 'Total';

        $statistic = new Statistic();


        if ($year !== null and (int)$year !== 0) {
            return $statistic->setYear($year)->{$func}($unit, $region, $status);
        } else {
            return $statistic->{$func}($unit, $region, $status);
        }
    }

    /**
     * @param $type
     * @param null|integer $status
     * @param null $region
     * @param null $year
     * @return array
     */
    private function totalRusus($type, $status = null, $region = null, $year = null)
    {
        $unit = Unit::whereSlug(Unit::RUSUS['slug'])->first();
        $func = 'region' . title_case($type) . 'Total';

        $statistic = new Statistic();

        if ($year !== null and (int)$year !== 0) {
            return $statistic->setYear($year)->{$func}($unit, $region, $status);
        } else {
            return $statistic->{$func}($unit, $region, $status);
        }
    }

    /**
     * @param $type
     * @param null|integer $status
     * @param null $region
     * @param null $year
     * @return array
     */
    private function totalRuk($type, $status = null, $region = null, $year = null)
    {
        $unit = Unit::whereSlug(Unit::RUK['slug'])->first();
        $func = 'region' . title_case($type) . 'Total';

        $statistic = new Statistic();

        if ($year !== null and (int)$year !== 0) {
            return $statistic->setYear($year)->{$func}($unit, $region, $status);
        } else {
            return $statistic->{$func}($unit, $region, $status);
        }
    }

    /**
     * @param $type
     * @param null|integer $status
     * @param null $region
     * @param null $year
     * @return array
     */
    private function totalSwadaya($type, $status = null, $region = null, $year = null)
    {
        $unit = Unit::whereSlug(Unit::SWADAYA['slug'])->first();
        $func = 'region' . title_case($type) . 'Total';

        $statistic = new Statistic();

        if ($year !== null and (int)$year !== 0) {
            return $statistic->setYear($year)->{$func}($unit, $region, $status);
        } else {
            return $statistic->{$func}($unit, $region, $status);
        }
    }

    public function proposalTable(StatisticTableDataTable $dataTable)
    {
        $roleSlug = \Auth::user()->role->slug;

        return $dataTable->render("statistic.proposal.table.{$roleSlug}", [
            'identifier' => $this->identifier
        ]);

    }


}