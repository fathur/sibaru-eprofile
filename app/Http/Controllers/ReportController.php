<?php

namespace App\Http\Controllers;

use App\Models\Region;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;

class ReportController extends Controller
{
    protected $identifier = 'report';

    public function index()
    {
        if (\Gate::denies('do-admin')) {
            return redirect()->route('proposals.index');
        }

        $title = 'Laporan <small></small>';

        $provinces = Region::whereLevel(1)->orderBy('order')->pluck('name');


        $charts = $this->generateChartsData();

//        dd($charts);

        return view('report.index', compact('title', 'provinces', 'charts'))
            ->with([
                'identifier' => $this->identifier
            ]);
    }

    private function generateChartsData()
    {

        $charts = \Cache::tags(['chart'])
            ->remember('generate-chart-data-index', Carbon::now()->addHours(12), function () {

            $charts = [];

            $currentYear = Carbon::now()->year;
            $lastThreeYear = Carbon::now()->subYears(3)->year;

            for ($year = $currentYear; $year > $lastThreeYear; $year--) {
                array_push($charts, (object)[
                    'year' => $year,
//                'rusun'   => $this->generateRusunChartData($year),
//                'rusus'   => $this->generateRususChartData($year),
//                'swadaya' => $this->generateSwadayaChartData($year),
//                'ruk'     => $this->generateRukChartData($year),
                ]);
            }

            return $charts;
        });

        return $charts;
    }

    private function generateRusunChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUSUN['slug'])->first();

        return [
            'name'  => 'Rusun',
            'color' => '#00ccff',
            'data'  => (new Statistic())->setYear($year)->regionEprofileTotal($unit)
        ];
    }

    private function generateRususChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUSUS['slug'])->first();

        return [
            'name'  => 'Rusus',
            'color' => '#ccff00',
            'data'  => (new Statistic())->setYear($year)->regionEprofileTotal($unit)
        ];
    }

    private function generateSwadayaChartData($year)
    {
        $unit = Unit::whereSlug(Unit::SWADAYA['slug'])->first();

        return [
            'name'  => 'BSPS',
            'color' => '#9bc200',
            'data'  => (new Statistic())->setYear($year)->regionEprofileTotal($unit)
        ];
    }

    private function generateRukChartData($year)
    {
        $unit = Unit::whereSlug(Unit::RUK['slug'])->first();

        return [
            'name'  => 'PSU',
            'color' => '#ffcc00',
            'data'  => (new Statistic())->setYear($year)->regionEprofileTotal($unit)
        ];
    }

    public function data()
    {
        $year = request('year');

        $data = \Cache::tags(['chart'])
            ->remember('dashboard-chart-' . $year, Carbon::now()->addHours(12), function () use ($year) {

            return [

                'rusun' => $this->generateRusunChartData($year),
                'rusus' => $this->generateRususChartData($year),
                'ruk' => $this->generateRukChartData($year),
                'swadaya' => $this->generateSwadayaChartData($year),
            ];
        });

        return response()->json($data);
    }
}