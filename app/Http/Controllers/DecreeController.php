<?php

namespace App\Http\Controllers;

use App\Models\Decree;
use App\Models\DecreeBudget;
use App\Models\Proposal;
use App\Models\Shortlist;
use App\Models\ShortlistBudget;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DecreeController extends Controller
{
    protected $identifier = 'sk';

    public function index()
    {
        $title = 'SK Penetapan';

        return view('decree.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    public function data()
    {
        $builder = Decree::select([
            \DB::raw('shortlists.proposal_id as proposal_id'),
            \DB::raw('units.name as unit_name'),

            \DB::raw('agencies.name as proposer_name'),

            \DB::raw('SUM(dwelling_plans.amount) as a_amount'),
            \DB::raw('SUM(dwelling_plans.budget_plan) as a_budget'),

            \DB::raw('SUM(shortlist_budgets.amount) as b_amount'),
            \DB::raw('SUM(shortlist_budgets.budget_plan) as b_budget'),

            \DB::raw('SUM(decree_budgets.amount) as c_amount'),
            \DB::raw('SUM(decree_budgets.budget) as c_budget'),
        ])
            ->leftJoin('shortlists', 'shortlists.id', '=', 'decrees.shortlist_id')
            ->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('dwelling_plans', 'dwelling_plans.proposal_id', '=', 'proposals.id')
            ->leftJoin('shortlist_budgets', 'shortlist_budgets.dwelling_plan_id', '=', 'dwelling_plans.id')
            ->leftJoin('decree_budgets', 'decree_budgets.shortlist_budget_id', '=', 'shortlist_budgets.id')
            ->join('units', 'units.id', '=', 'proposals.unit_id')
            ->join('programs', 'programs.id', '=', 'proposals.program_id')
            ->join('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->join('statuses', 'statuses.id', '=', 'proposals.status_id')
            ->whereIn('status_id', Status::whereIn('slug', [/*'technical-passed', */'proposal-passed'])->pluck('id'))
            ->groupBy(['shortlists.proposal_id','units.name','agencies.name']);

        if (\Gate::denies('do-admin')) {

            if (\Auth::user()->agency_id !== null) {
                $builder->where('units.id', '=', \Auth::user()->unit_id);
                $builder->whereProposerId(\Auth::user()->agency->id);
            }
        }

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {

                return view('shortlist.action')
                    ->with([
                        'edit_link'   => route('decrees.edit', [$data->proposal_id]),
                        'delete_link' => route('decrees.destroy', [$data->proposal_id]),
                        'identifier'  => $this->identifier,
                        'proposal'    => $data
                    ])
                    ->render();
            })
            ->editColumn('proposed_date', function ($data) {
                return Carbon::parse($data->proposed_date)->year;
            })
            ->editColumn('a_budget', function($data) {
                return rupiah($data->a_budget);
            })
            ->editColumn('b_budget', function($data) {
                return rupiah($data->b_budget);
            })
            ->editColumn('c_budget', function($data) {
                return rupiah($data->c_budget);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function create($id)
    {
        $proposal = Proposal::with(['dwellingPlans' => function($q){
            $q->with(['shortlist' => function($q) {
                $q->with('decreeBudget');
            }]);
            $q->with('unitType');
        }, 'shortlist' => function($q){
            $q->with('decree');
        }])->find($id);

//        dd($proposal->toArray());


        $title = 'SK Penetapan';

        // calculate sum
        $sumDwellingPlanUnit = 0;
        $sumShortlistUnit = 0;
        $sumDwellingPlanBudget = 0;
        $sumShortlistBudget = 0;
        foreach ($proposal->dwellingPlans as $dwellingPlan) {
            $sumDwellingPlanUnit += $dwellingPlan->amount;
            $sumDwellingPlanBudget += $dwellingPlan->budget_plan;

            $sumShortlistUnit += $dwellingPlan->shortlist->amount;
            $sumShortlistBudget += $dwellingPlan->shortlist->budget_plan;
        }

        $sumDwellingPlan = (object)[
            'unit'   => $sumDwellingPlanUnit,
            'budget' => $sumDwellingPlanBudget
        ];

        $sumShortlist = (object)[
            'unit'   => $sumShortlistUnit,
            'budget' => $sumShortlistBudget
        ];

        $sum = (object)[
            'dwellingPlan'  => $sumDwellingPlan,
            'shortlist' => $sumShortlist
        ];

        $shortlist = Shortlist::find($proposal->shortlist->id);

        if($proposal->shortlist != null and $shortlist->decree != null) {

            return view('decree.edit', compact('title', 'proposal', 'sum'));

        }

        return view('decree.create', compact('title', 'proposal', 'sum'));

    }

    public function store(Request $request)
    {
        $newAmount = $request->get('new_amount');
        $newBudgetPlan = $request->get('new_budget_plan');

        $proposalId = $request->get('proposal_id');

        $proposal = Proposal::with(['shortlist'])->find($proposalId);

        $shortlist = Shortlist::find($proposal->shortlist->id);

        if($request->method() == 'PUT') {
            $decree = Decree::whereShortlistId($shortlist->id)->first();
            $decree->attachment = $request->get('attachment');
            $decree->filename = $request->get('file_name');
            $decree->save();
        } else {
            $decree = $shortlist->decree()->updateOrCreate([
                'proposed_year' => $request->get('proposed_year', $proposal->proposed_year),
                'attachment'    => $request->get('attachment'),
                'filename'      => $request->get('file_name'),
            ]);
        }

        $proposal->dwellingPlans->each(function ($dwellingPlan) use ($newAmount, $newBudgetPlan, $decree) {
            foreach ($newAmount as $amountShortlistBudgetId => $amount) {
                if ((int)$dwellingPlan->shortlist->id == (int)$amountShortlistBudgetId) {

                    foreach ($newBudgetPlan as $bpShortlistBudgetId => $budgetPlan) {
                        if ($dwellingPlan->shortlist->id == (int)$bpShortlistBudgetId) {
                            $bp = (int)preg_replace('/[^0-9\,]+/', '', $budgetPlan);
                            $cntDb = DecreeBudget::where('shortlist_budget_id', $bpShortlistBudgetId)->count();

                            if($cntDb == 0) {
                                $shortlistBudget = ShortlistBudget::find($dwellingPlan->shortlist->id);
                                $shortlistBudget->decreeBudget()->updateOrCreate([
                                    'decree_id'     => $decree->id,
                                    'amount'      => $amount,
                                    'budget' => $bp
                                ]);
                            } else {
                                $fDb = DecreeBudget::where('shortlist_budget_id', $bpShortlistBudgetId)->first();
                                $fDb->amount = $amount;
                                $fDb->budget = $bp;
                                $fDb->save();
                            }
                        }

                    }
                }
            }
        });

        return redirect()->route('decrees.edit', $proposalId);

    }

    public function edit($id)
    {
        return $this->create($id);
    }

    public function download($proposalId)
    {
        $proposal = Proposal::with(['shortlist' => function($q) {
            $q->with('decree');
        }])->find($proposalId);

        return response()->download(base_path($proposal->shortlist->decree->attachment), $proposal->shortlist->decree->filename);

    }

    public function destroy($id)
    {
        $proposal = Proposal::find($id);

        $decreeId = $proposal->shortlist->decree->id;

        return Decree::destroy($decreeId);
    }

}