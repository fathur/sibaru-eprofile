<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\Unit;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    protected $identifier = 'program';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Program';

        return view('program.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $type = $request->get('type');

        if ($request->ajax()) {

            $typeTitle = title_case($type);

            return response()->json([
                'title'  => "Program {$typeTitle} Baru",
                'body'   => view('program.modal.create')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $type
                    ])
                    ->render(),
                'footer' => view('program.modal.footer')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $type
                    ])
                    ->render()
            ]);
        } else {
            return view('program.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $type = $request->get('type');

        return Program::create([
            'unit_id' => Unit::whereSlug($type)->first()->id,
            'name'    => $request->get('name')
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program $program
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     *
     */
    public function edit($id, Request $request)
    {
        $program = Program::with('unit')->find($id);

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit program kegiatan",
                'body'   => view('program.modal.edit')
                    ->with([
                        'program'    => $program,
                        'identifier' => $this->identifier,
                        'type'       => $program->unit->slug

                    ])
                    ->render(),
                'footer' => view('program.modal.footer')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $program->unit->slug
                    ])
                    ->render()
            ]);
        } else {
            return view('program.edit');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, $id)
    {
        $validator = validator($request->all(), [
            'name'     => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $program = Program::find($id);

        $program->name = $request->get('name');
        $program->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('programs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program $program
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Program::destroy($id);
    }

    public function select2Data(Request $request)
    {
        $q = strtolower($request->get('q'));
        $unit_id = $request->get('unit_id', 0);

        if ($unit_id == 0) {
            return Program::where(\DB::raw('LOWER(name)'), 'like', "%$q%")
                ->get();
        } else {

            return Program::where(\DB::raw('LOWER(name)'), 'like', "%$q%")
            ->whereUnitId($unit_id)->get();
        }
    }

    public function data(Request $request)
    {
        $type = $request->get('type');

        $unit = Unit::whereSlug($type)->first();

        $builder = Program::whereUnitId($unit->id);

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) use ($type) {
                return view('program.action')
                    ->with([
                        'edit_link'   => route('programs.edit', [$data->id]),
                        'delete_link' => route('programs.destroy', [$data->id]),
                        'identifier'  => $this->identifier,
                        'type'        => $type
                    ])
                    ->render();
            })
            ->make(true);
    }
}
