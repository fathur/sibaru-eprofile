<?php

namespace App\Http\Controllers;

use App\DataTables\ProposalsDataTable;
use App\Models\Agency;
use App\Models\Criteria;
use App\Models\Program;
use App\Models\Proposal;
use App\Models\Recipient;
use App\Models\Region;
use App\Models\Role;
use App\Models\Status;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use ZipArchive;

class ProposalController extends Controller
{
    protected $identifier = 'usulan';

    /**
     * Display a listing of the resource.
     *
     * @param ProposalsDataTable $datatables
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ProposalsDataTable $datatables)
    {
        $user = \Auth::user();
        $agency = Agency::with('region')->find($user->agency_id);


        $title = 'Daftar Usulan';

        $recipients = Recipient::select([
            'recipients.id',
            'recipients.name',
            \DB::raw('units.name as unit')
        ])->leftJoin('units', 'units.id', '=', 'recipients.unit_id')
            ->orderBy('recipients.unit_id', 'asc')
            ->orderBy('name', 'asc')
            ->get();

        $programs = Program::select([
            'programs.id',
            'programs.name',
            \DB::raw('units.name as unit')
        ])->leftJoin('units', 'units.id', '=', 'programs.unit_id')
            ->orderBy('programs.unit_id', 'asc')
            ->orderBy('name', 'asc')
            ->get();


        return $datatables->render('proposal.index', [
            'title' => $title,
            'identifier' => $this->identifier,
            'recipients' => $recipients,
            'programs' => $programs,
            'units' => Unit::all(),
            'statuses' => Status::all(),
            'user' => $user,
            'agency' => $agency
        ]);


//        return view('proposal.index', compact('title'))
//            ->with([
//                'identifier' => $this->identifier,
//                'units'      => Unit::all(),
//                'statuses'   => Status::all()
//            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createProposalRusun()
    {

        $user = (\Auth::user());
        $agency = Agency::with('region')->find($user->agency_id);

        $title = 'Tambah usulan rusun';

        $unit = Unit::whereSlug('rusun')->first();

        return view('proposal.rusun.create', compact('title', 'unit', 'agency'));
    }

    public function createProposalRusus()
    {
        $user = (\Auth::user());
        $agency = Agency::with('region')->find($user->agency_id);

        $title = 'Tambah usulan rusus';

        $unit = Unit::whereSlug('rusus')->first();

        return view('proposal.rusus.create', compact('title', 'unit', 'agency'));

    }

    public function createProposalSwadaya()
    {
        $user = (\Auth::user());
        $agency = Agency::with('region')->find($user->agency_id);

        $title = 'Tambah usulan BSPS';

        $unit = Unit::whereSlug('swadaya')->first();

        return view('proposal.swadaya.create', compact('title', 'unit','agency'));

    }

    public function createProposalRuk()
    {
        $user = (\Auth::user());
        $agency = Agency::with('region')->find($user->agency_id);

        $title = 'Tambah usulan PSU';

        $unit = Unit::whereSlug('ruk')->first();

        return view('proposal.ruk.create', compact('title', 'unit', 'agency'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unitId = $request->get('unit_id');
        $unit = Unit::find($unitId);

        // todo: validation
        $rules = [
            'unit_id'         => 'required',
            'program'         => 'required',
            'recipient'       => 'required',
            'proposer_remark' => 'required',
            'proposer_phone'  => 'required',
            'proposer_email'  => 'required',
//            'proposed_date'   => 'required'
        ];

        $messages =  [
            'unit_id.required'  => 'Unit tidak boleh kosong',
            'program.required'  => 'Kegiatan tidak boleh kosong',
            'recipient.required'=> 'Penerima manfaat tidak boleh kosong',
            'proposer_remark.required' => 'Identitas pengusul tidak boleh kosong',
            'proposer_phone.required' => 'No. telp pengusul tidak boleh kosong',
            'proposer_email.required' => 'Email pengusul tidak boleh kosong'
        ];

        if ($unit->slug !== 'swadaya') {
            $rules['address'] = 'required';
            $messages['address.required'] = 'Alamat tidak boleh kosong';

            $rules['location'] = 'required';
            $messages['location.required'] = 'Lokasi tidak boleh kosong';
        }

        if ($unit->slug == 'rusun') {
            # rules data specific rusun
            $rules['area_wide'] = 'integer';
            $rules['area_length'] = 'integer';
            $rules['area_width'] = 'integer';
            $rules['potential_residents'] = 'integer';

        }

        $validator = validator($request->all(), $rules, $messages);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // save data and get the id

        $status = Status::whereSlug('proposal-draft')->first();

        $proposal = new Proposal();

        $proposal->unit_id = $unitId;
        $proposal->creator_id = \Auth::user()->id;
        $proposal->status_id = $status->id;
        $proposal->submission_date = Carbon::parse($request->get('submission_date'));

        if (\Gate::denies('do-admin')) {
            $proposerId = \Auth::user()->agency->id;
        } else {
            $proposerId = $request->get('proposer');
        }

        $proposal->proposer_id = $proposerId;
        $proposal->program_id = $request->get('program');
        $proposal->recipient_id = $request->get('recipient');
        $proposal->location_id = $request->get('location');
        $proposal->address = $request->get('address');
        $proposal->proposer_remark = $request->get('proposer_remark');
        $proposal->proposer_phone = $request->get('proposer_phone');
        $proposal->proposer_email = $request->get('proposer_email');
        $proposal->proposed_year = $request->get('proposed_date');

        if ($unit->slug == 'rusun') {
            $proposal->area_wide = $request->get('area_wide');
            $proposal->area_length = $request->get('area_length');
            $proposal->area_width = $request->get('area_width');
            $proposal->potential_residents = $request->get('potential_residents');
        }

        if ($unit->slug !== 'swadaya') {

            $coo = $request->get('coordinate');
            $coo = trim($coo);

            if ($coo !== '') {

                $xCoordinate = explode(',', $request->get('coordinate'));
                $latitude = trim($xCoordinate[0]);
                $longitude = trim($xCoordinate[1]);

                $proposal->latitude = $latitude;
                $proposal->longitude = $longitude;
            }
        }

        // redirect to edit page for this id
        if ($proposal->save()) {
            return redirect()->route('proposals.edit', [$proposal->id]);
        }

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proposal $proposal
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Proposal $proposal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user();

        $proposal = Proposal::with([
            'unit', 'program', 'recipient', 'location', 'proposer', 'status', 'tematics',
            'dwellingPlans', 'criterias', 'comments' => function ($query) {
                $query->with('user');
                $query->orderBy('created_at', 'desc');
            }
        ])->find($id);

        $agency = Agency::with('region')->find($user->agency_id);

//        if (\Gate::denies('proposals.view', $proposal)) {
//            abort(404);
//        }

        $criteriaAdministrations = Criteria::whereUnitId($proposal->unit->id)
            ->administration()
            ->orderBy('order')
            ->get();

        $documentAdministrations = $this->extractDocuments($criteriaAdministrations, $proposal);

        $criteriaTechnicals = Criteria::whereUnitId($proposal->unit->id)
            ->technical()
            ->orderBy('order')
            ->get();

        $documentTechnicals = $this->extractDocuments($criteriaTechnicals, $proposal);

        $criteriaOthers = Criteria::whereUnitId($proposal->unit->id)
            ->others()
            ->orderBy('order')
            ->get();

        $documentOthers = $this->extractDocuments($criteriaOthers, $proposal);

        $title = 'Edit Usulan';

        return view('proposal.edit', compact(
            'proposal', 'title', 'agency',
            'documentAdministrations', 'documentTechnicals', 'documentOthers'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, $id)
    {

        if(\Gate::allows('do-watcher')) {


            $validator = validator($request->all(), [
                'tematics'  => 'required'
            ], [
                'tematics.required' => 'Tematik tidak boleh kosong'
            ]);

        } else {

            $rules = [
//                'unit_id'         => 'required',
                'program'         => 'required',
                'recipient'       => 'required',
                'proposer_remark' => 'required',
                'proposer_phone'  => 'required',
                'proposer_email'  => 'required',
//            'proposed_date'   => 'required'
            ];

            $messages =  [
//                'unit_id.required'  => 'Unit tidak boleh kosong',
                'program.required'  => 'Kegiatan tidak boleh kosong',
                'recipient.required'=> 'Penerima manfaat tidak boleh kosong',
                'proposer_remark.required' => 'Identitas pengusul tidak boleh kosong',
                'proposer_phone.required' => 'No. telp pengusul tidak boleh kosong',
                'proposer_email.required' => 'Email pengusul tidak boleh kosong'
            ];

            $validator = validator($request->all(), $rules, $messages);
        }

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }

        $proposal = Proposal::find($id);

        if ($request->has('proposer')) {
            $proposal->proposer_id = $request->get('proposer');
        }

        if ($request->has('program')) {
            $proposal->program_id = $request->get('program');
        }

        if ($request->has('recipient')) {
            $proposal->recipient_id = $request->get('recipient');
        }

        if ($request->has('submission_date')) {
            $proposal->submission_date = Carbon::parse($request->get('submission_date'));
        }

        if ($request->has('location')) {
            $proposal->location_id = $request->get('location');
        }

        if ($request->has('address')) {
            $proposal->address = $request->get('address');
        }

        if ($request->has('proposer_remark')) {
            $proposal->proposer_remark = $request->get('proposer_remark');
        }

        if ($request->has('proposer_phone')) {
            $proposal->proposer_phone = $request->get('proposer_phone');
        }

        if ($request->has('proposer_email')) {
            $proposal->proposer_email = $request->get('proposer_email');
        }

        if ($request->has('proposed_date')) {
            $proposal->proposed_year = $request->get('proposed_date');
        }

        if ($request->has('area_wide')) {
            $proposal->area_wide = $request->get('area_wide');
        }

        if ($request->has('area_length')) {
            $proposal->area_length = $request->get('area_length');
        }

        if ($request->has('area_width')) {
            $proposal->area_width = $request->get('area_width');
        }

        if ($request->has('potential_residents')) {
            $proposal->potential_residents = $request->get('potential_residents');
        }

        if ($request->has('coordinate')) {
            $xCoordinate = explode(',', $request->get('coordinate'));
            $latitude = trim($xCoordinate[0]);
            $longitude = trim($xCoordinate[1]);

            $proposal->latitude = $latitude;
            $proposal->longitude = $longitude;
        }

        $proposal->save();

        if ($request->has('tematics')) {

            // tematics
            $proposal->tematics()->sync($request->get('tematics'));

        }

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('proposals.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $proposal = Proposal::find($id);
        $proposal->delete();

        return response()->json([]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function data(Request $request)
    {
        $builder = Proposal::select([
            \DB::raw('proposals.created_at'),
            \DB::raw('units.name as unit_name'),
//            \DB::raw('recipients.name as recipient_name'),
//            \DB::raw('regions.name as region_name'),
            \DB::raw('statuses.name as status_name'),
            \DB::raw('statuses.slug as status_slug'),
            \DB::raw('programs.name as program_name'),
            'proposals.proposed_year',
            'proposals.status_id',
            'proposals.proposer_remark',
            \DB::raw('proposals.proposer_id'),
            \DB::raw('proposals.id as id'),
            \DB::raw('agencies.name as proposer_name'),
        ])
            ->join('units', 'units.id', '=', 'proposals.unit_id')
            ->join('programs', 'programs.id', '=', 'proposals.program_id')
            ->join('recipients', 'recipients.id', '=', 'proposals.recipient_id')
//        ->join('regions', 'regions.id', '=', 'proposals.location_id')
            ->join('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->join('statuses', 'statuses.id', '=', 'proposals.status_id');

        // jika sebagai admin pusat (bukan super admin)
        if (\Auth::user()->role_id == Role::whereSlug('admin-central')->first()->id) {

            // Permintaan admin pusat bisa melihat status draft, jadi di komen dulu
            #$builder->whereNotIn('status_id', Status::whereIn('slug', [
            #    'proposal-draft'
            #])->pluck('id'));
        }

        // Jika sebagai pemkab/pemkot
        if (\Gate::denies('do-admin')) {

            if (\Auth::user()->agency_id !== null) {
                if (\Gate::denies('do-watcher')) {
                    $builder->whereProposerId(\Auth::user()->agency->id);
                }
            }
        }

        // Jika sebagai diektorat rusun, rusus, ruk, swadaya
        if (\Gate::allows('do-technical')) {
            $statusesId = Status::whereIn('slug', [
                'proposal-draft',
                'proposal-new',
                'proposal-passed'
            ])->pluck('id');

            $builder->whereNotIn('status_id', $statusesId);
            $builder->where('proposals.unit_id', '=', Auth::user()->unit_id);
        }

        // Searching
        $keySearchUnitId = $request->get('unit');
        $keySearchProposerName = $request->get('proposer_name');
        $keySearchStatusId = $request->get('status');
        $keySearchProgramName = $request->get('program_name');
        $keySearchProposedYear = $request->get('proposed_year');
        $keySearchProposerRemark = $request->get('proposer_remark');

        if ((int)$keySearchUnitId !== 0) {
            $builder->where('proposals.unit_id', '=', $keySearchUnitId);
        }

        if (trim($keySearchProposerName) !== '') {
            $keySearchProposerName = strtolower($keySearchProposerName);
            $builder->where(\DB::raw('lower(agencies.name)'), 'like', "%{$keySearchProposerName}%");
        }

        if ((int)$keySearchStatusId !== 0) {
            $builder->where('statuses.id', '=', $keySearchStatusId);
        }

        if (trim($keySearchProgramName) !== '') {
            $keySearchProgramName = strtolower($keySearchProgramName);
            $builder->where(\DB::raw('lower(programs.name)'), 'like', "%{$keySearchProgramName}%");
        }

        if (trim($keySearchProposerRemark) !== '') {
            $keySearchProposerRemark = strtolower($keySearchProposerRemark);
            $builder->where(\DB::raw('lower(proposals.proposer_remark)'), 'like', "%{$keySearchProposerRemark}%");
        }

        if ((int)$keySearchProposedYear !== 0) {
            $builder->where('proposals.proposed_year', '=', $keySearchProposedYear);
        }

        // gate
        if (\Gate::allows('do-watcher')) {
            // melihat kabupaten dibawah provinsinya
            $regionId = Auth::user()->agency->region->id;
            $region = Region::find($regionId);
            $agencies = Agency::whereIn('region_id', $region->descendants()->where('level', 2)->pluck('id'))
                ->pluck('id');

            $builder->whereIn('proposals.proposer_id', $agencies);

            $statusesId = Status::whereIn('slug', [
                'proposal-draft',
            ])->pluck('id');

            $builder->whereNotIn('status_id', $statusesId);
        }

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) {

                return view('proposal.action')
                    ->with([
                        'edit_link'        => route('proposals.edit', [$data->id]),
                        'delete_link'      => route('proposals.destroy', [$data->id]),
                        'edit_status_link' => route('proposals.status.edit', [$data->id]),
                        'identifier'       => $this->identifier,
                        'proposal'         => $data
                    ])
                    ->render();
            })
            ->editColumn('proposed_date', function ($data) {
                return Carbon::parse($data->proposed_date)->year;
            })
            ->rawColumns(['action'])
            ->setRowClass(function ($data) {
//                dd($data->toArray());

                if ($data->status_slug == 'technical-passed' || $data->status_slug == 'proposal-passed' || $data->status_slug == 'technical-passed') {
                    return 'success';
                }

                if ($data->status_slug == 'administration-verifying' || $data->status_slug == 'technical-surveyor-verifying' || $data->status_slug == 'technical-survey-verifying') {
                    return 'warning';
                }

                if (
                    $data->status_slug == 'administration-rejected' || $data->status_slug == 'technical-rejected' ||
                    $data->status_slug == 'administration-revising'
                ) {
                    return 'danger';
                }

            })
            ->make(true);
    }

    /**
     * @param $criteriaTypes
     * @param $proposal
     *
     * @return mixed
     * @author   Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function extractDocuments($criteriaTypes, $proposal)
    {
        $documentTypes = [];
        foreach ($criteriaTypes as $criteriaType) {

            $documentUri = null;
            $documentName = null;
            $documentIsVerified = null;
            $documentDescription = null;

            foreach ($proposal->criterias as $criteria) {
                if ($criteria->id == $criteriaType->id) {
                    $documentUri = $criteria->pivot->attachment;
                    $documentName = $criteria->pivot->file_name;
                    $documentIsVerified = $criteria->pivot->is_verified;
                    $documentDescription = $criteria->pivot->description;
                }
            }

            array_push($documentTypes, [
                "id"                   => $criteriaType->id,
                "unit_id"              => $criteriaType->unit_id,
                "order"                => $criteriaType->order,
                "type"                 => $criteriaType->type,
                "weight"               => $criteriaType->weight,
                "content"              => $criteriaType->content,
                "template"             => $criteriaType->template,
                "filename"             => $criteriaType->filename,
                'criteria_id'          => $criteriaType->id,
                'proposal_id'          => $proposal->id,
                'document_url'         => $documentUri,
                'document_name'        => $documentName,
                'document_is_verified' => $documentIsVerified,
                'document_description' => $documentDescription,
            ]);
        }

        return $documentTypes;
    }

    public function document(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'proposal_id' => 'required',
            'criteria_id' => 'required',
            'document'    => 'required|mimes:pdf,doc,docx,jpg,jpeg,png,bmp,xls,xlsx,zip'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $proposalId = $request->get('proposal_id');
        $criteriaId = $request->get('criteria_id');

        if ($request->hasFile('document')) {

            $file = $request->file('document');
//            $path = $file->storeAs('storage/upload', Carbon::now()->toDateTimeString() . '_' . str_slug($file->getClientOriginalName()));
            $path = $file->store('storage/upload');

            $proposal = Proposal::find($proposalId);

            // check existing
            $cntPC = \DB::table('proposal_criteria')->where('proposal_id', $proposalId)
                ->where('criteria_id', $criteriaId)->count();

            if ($cntPC == 0) {

                $proposal->criterias()->attach($criteriaId, [
                    'attachment'   => $path,
                    'file_name'    => $file->getClientOriginalName(),
                    'is_verified'  => false,
                    'is_published' => true
                ]);
            } elseif ($cntPC == 1) {
                $proposal->criterias()->updateExistingPivot($criteriaId, [
                    'attachment'   => $path,
                    'file_name'    => $file->getClientOriginalName(),
                    'is_verified'  => false,
                    'is_published' => true
                ]);
            }

            return response()->json([
                'proposal_id'  => $proposalId,
                'criteria_id'  => $criteriaId,
                'attachment'   => $path,
                'file_name'    => $file->getClientOriginalName(),
                'is_verified'  => false,
                'is_published' => true
            ]);
        }

        abort(400);
    }

    /**
     * @param $proposalId
     * @param $criteriaId
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function download($proposalId, $criteriaId)
    {
        $proposal = Proposal::with(['criterias' => function ($query) use ($criteriaId) {
            $query->where('criteria_id', '=', $criteriaId);
        }])->find($proposalId);

        if (count($proposal->criterias) == 0) {
            abort(400, 'Kriteria not exists');
        }

        $document = $proposal->criterias[0]->pivot;

        return response()->download(base_path($document->attachment), $document->file_name);
    }

    /**
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     *
     * @param $proposalId
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadAll($proposalId)
    {
        $proposal = Proposal::with(['criterias'])->find($proposalId);

        $files = [];
        foreach ($proposal->criterias as $criteria) {
            if ($criteria->pivot->attachment !== null or $criteria->pivot->attachment != '') {
                $files[base_path($criteria->pivot->attachment)] = $criteria->pivot->file_name;
            }
        }

        $name = "Semua dokumen {$proposal->id} - " . Carbon::now()->timestamp;
        $destination = storage_path("upload/{$name}.zip");

        if ($this->zipFiles($files, $destination, true)) {
            return response()->download($destination, "{$name}.zip");
        }
    }

    public function updatePivotCriteria(Request $request)
    {

        $data = [];
        if ($request->get('is_verified') == 'true') {
            $data['is_verified'] = true;
        } elseif ($request->get('is_verified') == 'false') {
            $data['is_verified'] = false;
        }

        if ($request->has('description')) {
            $data['description'] = $request->get('description');
        }

        $proposalId = $request->get('proposal_id');
        $criteriaId = $request->get('criteria_id');

        $proposal = Proposal::find($proposalId);

        // check existing
        $cntPC = \DB::table('proposal_criteria')->where('proposal_id', $proposalId)
            ->where('criteria_id', $criteriaId)->count();

        if ($cntPC == 0) {
            $proposal->criterias()->attach($criteriaId, $data);
        } else {

            $proposal->criterias()->updateExistingPivot($criteriaId, $data);
        }
    }

    /**
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(Request $request)
    {
        $proposalId = $request->get('proposal_id');
        $force = $request->get('force');

        $proposal = \App\Repositories\Proposal::changeStatus($proposalId, $force);

        if ($request->ajax()) {
            return response()->json($proposal->toArray());
        }

        return redirect()->route('proposals.edit', [$proposalId]);
    }

    /**
     * @param array $files
     * @param string $destination
     * @param bool $overwrite
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     * @return bool
     */
    private function zipFiles($files = [], $destination = '', $overwrite = false)
    {

        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }

        //vars
        $valid_files = [];
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $file => $name) {
                //make sure the file exists
                if (file_exists($file)) {
                    $valid_files[$file] = $name;
                }
            }
        }

        //if we have good files...
        if (count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();

            if ($overwrite) {
                if (!file_exists($destination)) {
                    $overwrite = false;
                }
            }
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }

            //add the files
            foreach ($valid_files as $file => $name) {
                $zip->addFile($file, round(microtime(true) * 1000) . str_random(2) . '-' . $name);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    public function editStatus($id, Request $request)
    {
        $proposal = Proposal::find($id);
//        return view('proposal.status', compact('proposal'));

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit status",
                'body'   => view('proposal.status')
                    ->with([
                        'identifier' => $this->identifier,
                        'proposal'   => $proposal,
                        'statuses'   => Status::orderBy('id', 'asc')->get()
                    ])
                    ->render(),
                'footer' => view('proposal.status-footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('proposal.status');
        }
    }

    public function updateStatus($id, Request $request)
    {
        $proposal = Proposal::find($id);
        $proposal->status_id = $request->get('status');
        $proposal->save();

        return response()->json([]);
    }
}
