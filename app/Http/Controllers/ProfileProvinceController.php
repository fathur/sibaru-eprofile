<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 9/9/17
 * Time: 09:14
 */

namespace App\Http\Controllers;


use App\Models\Eprofile;
use App\Models\Region;
use App\Models\Unit;
use Illuminate\Http\Request;

class ProfileProvinceController extends Controller
{
    protected $identifier = 'profil-provinsi';

    public function index()
    {
        if (\Gate::denies('do-admin')) {
            abort(404);
        }

        $title = 'Profil Provinsi';

        return view('profile-province.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    public function data(Request $request)
    {
        if (\Gate::denies('do-admin')) {
            abort(404);
        }

        $results = [];

        $level = $request->get('level');

        if ($request->has('id')) {

            $id = $request->get('id');

            $parent = Region::find($id);

            $level = $parent->level + 1;

            $regions = Region::defaultOrder()
                ->descendantsOf($id)
                ->where('level', $level);
        } else {
            $regions = Region::where('level', 1)
                ->orderBy('name')
                ->get();
        }

        foreach ($regions as $region) {
            $region = $region->toArray();
            $region['state'] = \App\Repositories\Region::hasChild($region, $level) ? 'closed' : 'open';
            $region['level_name'] = \App\Repositories\Region::namedLevel($region['level']);
            $region['action'] = view('profile-province.action', compact('region'))
                ->with([
                    'identifier' => $this->identifier,
                    'year'       => $request->has('year') ? $request->get('year') : null,
                ])
                ->render();
            array_push($results, $region);
        }

        return $results;


    }

    public function map()
    {
        if (\Gate::denies('do-admin')) {
            abort(404);

        }

        $title = 'Peta';

        $years = Eprofile::select([\DB::raw('distinct(proposed_year) as year')])->get();
        $units = Unit::whereIn('slug', ['rusun', 'rusus'])->get();

        return view('eprofile.map', compact('title', 'years', 'units'))
            ->with('identifier', $this->identifier);

    }

    public function mapData(Request $request)
    {
        $type = $request->get('type');
        $year = $request->get('year');


        $builder = Eprofile::whereNotNull('latitude')
            ->whereNotNull('longitude');

        if((int)$year !== 0) {
            $builder->where('proposed_year', $year);
        }

        if(\Gate::allows('do-technical')) {
            $builder->where('unit_id', '=', \Auth::user()->unit_id);
        } else {
            $unit = Unit::whereSlug($type)->first();

            $builder->where('unit_id', $unit->id);
        }

        $data = $builder
            ->get();

        return response()->json($data->toArray());
    }
}