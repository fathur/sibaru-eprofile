<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    protected $identifier = 'wilayah';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Wilayah';

        return view('region.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'title'  => 'Wilayah baru',
                'body'   => view('region.modal.create')
                    ->with([
                        'identifier' => $this->identifier,
                        'provinces'  => Region::whereLevel(1)->orderBy('name')->get()
                    ])
                    ->render(),
                'footer' => view('region.modal.footer')
                    ->with('identifier', $this->identifier)
                    ->render()
            ]);
        } else {
            return view('region.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'name'  => 'required',
            'order' => 'required'
        ], [
            'order.required' => 'No tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $parentId = null;
        $level = 1; // default level

        if ($request->has('parent')) {
            $parentId = $request->get('parent');

            $parent = Region::find($parentId);
            $level = $parent->level + 1;
        }

        $region = Region::create([
            'parent_id' => $parentId,
            'order'     => $request->get('order'),
            'name'      => $request->get('name'),
            'level'     => $level
        ]);

        $value = \App\Repositories\Region::constructName($region);
        \Cache::forever('region-' . $region->id, $value);

        return $region;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        $region = Region::find($id);
        $parents = Region::whereNull('parent_id')->get();

        return response()->json([
            'title'  => "Ubah wilayah",
            'body'   => view('region.modal.edit')
                ->with([
                    'region'    => $region,
                    'provinces' => Region::whereLevel(1)->orderBy('name')->get(),
                    'parents'   => $parents,

                    'identifier' => $this->identifier
                ])
                ->render(),
            'footer' => view('region.modal.footer')
                ->with('identifier', $this->identifier)
                ->render()
        ]);

//        $region = Region::with('profiles')->find($id);

//        $title = $region->name;

//        return view('region.edit', compact('region', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = validator($request->all(), [
            'name'  => 'required',
            'order' => 'required'
        ], [
            'order.required' => 'No tidak boleh kososng'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $parent = (int)$request->get('parent') == 0 ? null : $request->get('parent');


        $region = Region::find($id);
        $region->parent_id = $parent;
        $region->order = $request->get('order');
        $region->name = $request->get('name');
        $region->save();


        $value = \App\Repositories\Region::constructName($region);
        \Cache::forever('region-' . $region->id, $value);

        return response()->json($region);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Region::find($id);
        
        Region::destroy($id);

        return response()->json($region->toArray());
    }

    public function select2Data(Request $request)
    {
        $q = strtolower($request->get('q'));

        $regions = Region::where(\DB::raw('LOWER(name)'), 'like', "%$q%");

        $user = \Auth::user();

        if ($request->has('agency')) {

            if (\Gate::allows('do-admin')) {
                $agency = Agency::find($request->get('agency'));
                $regions->descendantsOf($agency->region_id);
            } else {
                $regions->descendantsOf($user->agency->region->id);
            }
        }

        if ($user->role->slug == 'proposer') {
            $regions->descendantsOf($user->agency->region->id);
        }

        if ($request->has('maxLevel')) {
            $regions->where('level', '<=', $request->get('maxLevel'));
        }

        $regions = $regions->get();

        $regionFull = [];

        foreach ($regions as $region) {
            array_push($regionFull, [
                'id'   => $region->id,
                'name' => \Cache::get('region-' . $region->id)
            ]);
        }

        return collect($regionFull);
    }

    public function data(Request $request)
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $table = $request->get('table');

        if ($table == 'datatables') {
            return $this->renderDataTable($request);
        } elseif ($table == 'treegrid') {
            return $this->renderTreeGridTable($request);
        } else {
            return $this->renderTreeGridTable($request);
        }


    }

    public function comboTree(Request $request)
    {
        $level = $request->get('level');

        $results = [];

        if ($request->has('id')) {
            $regions = Region::whereParentId($request->get('id'))
                ->where('level', '<=', $level)
                ->orderBy('order')
                ->get();

        } else {

            $regions = Region::whereNull('parent_id')
                ->whereLevel(1)
                ->orderBy('order')
                ->get();

        }

        foreach ($regions as $region) {
            $region = $region->toArray();
            $region['state'] = \App\Repositories\Region::hasChild($region, $level) ? 'closed' : 'open';
            $region['text'] = $region['name'];
            array_push($results, $region);
        }

        return $results;

    }

    private function renderTreeGridTable($request)
    {
        $results = [];

        $level = $request->get('level');

        if ($request->has('id')) {

            $id = $request->get('id');

            $parent = Region::find($id);

            $level = $parent->level + 1;

            $regions = Region::defaultOrder()
                ->descendantsOf($id)
                ->where('level', $level);
        } else {
            $regions = Region::where('level', 1)
                ->orderBy('name')
                ->get();
        }

        foreach ($regions as $region) {
            $region = $region->toArray();
            $region['state'] = 'closed';//\App\Repositories\Region::hasChild($region, $level) ? 'closed' : 'open';
            $region['level'] = \App\Repositories\Region::namedLevel($region['level']);
            $region['action'] = view('region.action', compact('region'))
                ->with([
                    'identifier' => $this->identifier,
                    'year'       => $request->has('year') ? $request->get('year') : null,
                ])
                ->render();
            array_push($results, $region);
        }

        return $results;
    }

    private function renderDataTable($request)
    {

        $builder = Region::whereIn('level', [1, 2]);

        if ($request->has('parent')) {
            $builder = $builder->where('parent_id', $request->get('parent'));
        }

        return \Datatables::of($builder)
            ->editColumn('name', function ($data) {
                return \Cache::get('region-' . $data->id);
            })
            ->editColumn('level', function ($data) {
                switch ($data->level) {
                    case 1:
                        return 'Provinsi';
                        break;
                    case 2:
                        return 'Kota/Kabupaten';
                        break;
                    case 3:
                        return 'Kecamatan';
                        break;
                    case 4:
                        return 'Desa/Kelurahan';
                        break;
                }
            })
            ->addColumn('action', function ($data) use ($request) {
                return view('region.action')
                    ->with([
                        'data'         => $data,
                        'year'         => $request->has('year') ? $request->get('year') : null,
                        'profile_link' => route('regions.profiles.index', [$data->id]),
                        'delete_link'  => route('regions.destroy', [$data->id]),
                        'identifier'   => $this->identifier
                    ])
                    ->render();
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    public function dataProvinceForEHousing()
    {
        $prov = Region::whereLevel(1)
            ->orderBy('order', 'asc')
            ->get(['id', 'name','level','order']);

        return response()->json($prov);
    }
}