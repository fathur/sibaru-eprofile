<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function store(Request $request)
    {
        $comment = Comment::create([
            'user_id'     => \Auth::user()->id,
            'proposal_id' => $request->get('proposal_id'),
            'text'        => $request->get('message'),
            'status'      => ''
        ]);

        return Comment::with(['user'])->find($comment->id);
    }
}