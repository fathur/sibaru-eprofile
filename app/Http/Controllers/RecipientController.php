<?php

namespace App\Http\Controllers;

use App\Models\Recipient;
use App\Models\Unit;
use Illuminate\Http\Request;

class RecipientController extends Controller
{
    protected $identifier = 'penerima';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::denies('do-master')) {
            abort(404);
        }

        $title = 'Penerima Manfaat';

        return view('recipient.index', compact('title'))
            ->with('identifier', $this->identifier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $type = $request->get('type');

        if ($request->ajax()) {

            $typeTitle = title_case($type);

            return response()->json([
                'title'  => "Penerima Manfaat {$typeTitle} Baru",
                'body'   => view('recipient.modal.create')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $type
                    ])
                    ->render(),
                'footer' => view('recipient.modal.footer')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $type
                    ])
                    ->render()
            ]);
        } else {
            return view('recipient.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'name'         => 'required',
            'abbreviation' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $type = $request->get('type');

        return Recipient::create([
            'unit_id'      => Unit::whereSlug($type)->first()->id,
            'name'         => $request->get('name'),
            'abbreviation' => $request->get('abbreviation')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipient $recipient
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Recipient $recipient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     *
     */
    public function edit($id, Request $request)
    {
        $recipient = Recipient::with('unit')->find($id);

        if ($request->ajax()) {
            return response()->json([
                'title'  => "Edit penerima manfaat",
                'body'   => view('recipient.modal.edit', compact('recipient'))
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $recipient->unit->slug

                    ])
                    ->render(),
                'footer' => view('recipient.modal.footer')
                    ->with([
                        'identifier' => $this->identifier,
                        'type'       => $recipient->unit->slug
                    ])
                    ->render()
            ]);
        } else {
            return view('recipient.edit');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Recipient $recipient
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = validator($request->all(), [
            'name'         => 'required',
            'abbreviation' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 400);
            }
        }

        $recipient = Recipient::find($id);

        $recipient->name = $request->get('name');
        $recipient->abbreviation = $request->get('abbreviation');
        $recipient->save();

        if ($request->ajax()) {
            return response()->json([]);
        }

        return redirect()->route('recipients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipient $recipient
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Recipient::destroy($id);
    }

    public function select2Data(Request $request)
    {
        $q = strtolower($request->get('q'));
        $unit_id = $request->get('unit_id', 0);

        if ($unit_id == 0) {
            return Recipient::where(\DB::raw('LOWER(name)'), 'like', "$q%")
                ->get();
        } else {

            return Recipient::where(\DB::raw('LOWER(name)'), 'like', "%$q%")
                ->whereUnitId($unit_id)->get();
        }
    }

    public function data(Request $request)
    {
        $type = $request->get('type');

        $unit = Unit::whereSlug($type)->first();

        $builder = Recipient::whereUnitId($unit->id);

        return \Datatables::of($builder)
            ->addColumn('action', function ($data) use ($type) {
                return view('recipient.action')
                    ->with([
                        'edit_link'   => route('recipients.edit', [$data->id]),
                        'delete_link' => route('recipients.destroy', [$data->id]),
                        'identifier'  => $this->identifier,
                        'type'        => $type
                    ])
                    ->render();
            })
            ->make(true);
    }
}
