<?php

use Carbon\Carbon;

if (! function_exists('rupiah')) {

    function rupiah($number, $symbol = true)
    {
        $rp = $symbol ? 'Rp. ' : '';
        return $rp . number_format($number, 0, ',', '.');
    }
}

if (! function_exists('human_date')) {

    function human_date($date)
    {
//        Carbon::setLocale('id_ID');
        $carbon = Carbon::parse($date);
        setlocale(LC_TIME, NULL);
        setlocale(LC_TIME, 'id_ID');
//        return strftime( "%A, %d %B %Y %H:%M", time());
        return $carbon->formatLocalized('%A, %e %B %Y');
    }
}