<?php

namespace App\DataTables;

use App\Models\RegionSpecial;
use App\User;
use Yajra\Datatables\Services\DataTable;

class RegionSpecialDataTable extends DataTable
{
    protected $identifier = '2010-2014';

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('budget', function($data){
                return rupiah($data->budget);
            })
            ->rawColumns(['action'])
            ->addColumn('action', function ($data) {

                return view('layout.action')
                    ->with([
                        'edit_link'   => route('regions.specials.edit', [$data->region_id, $data->id]),
                        'delete_link' => route('regions.specials.destroy', [$data->region_id, $data->id]),
                        'identifier'  => $this->identifier,
                        'special'     => $data
                    ])
                    ->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = RegionSpecial::query();//->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->addAction(['width' => '130px'])
            ->parameters([
                'dom'        => 'Bfrtip',
                'order'      => [[0, 'desc']],
                'buttons'    => [
                ],
                'processing' => true,
                'serverSide' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'order', 'name' => 'order', 'title' => 'No'],
            ['data' => 'program', 'name' => 'program', 'title' => 'Kegiatan'],
            ['data' => 'volume', 'name' => 'volume', 'title' => 'Vol'],
            ['data' => 'unit', 'name' => 'unit', 'title' => 'Satuan'],
            ['data' => 'budget', 'name' => 'budget', 'title' => 'Anggaran'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return '2010-2014_' . time();
    }
}
