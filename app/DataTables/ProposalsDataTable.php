<?php

namespace App\DataTables;

use App\Models\DwellingPlan;
use App\Models\Proposal;
use App\Models\Role;
use App\Models\Agency;
use App\Models\Region;
use App\Models\Status;
use App\Models\Unit;
use Yajra\Datatables\Services\DataTable;

class ProposalsDataTable extends DataTable
{
    protected $identifier = 'usulan';

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        $swadaya = Unit::whereSlug('swadaya')->first();

        return $this->datatables
            ->eloquent($this->query())
            ->setRowClass(function ($data) {

                if ($data->status_slug == 'technical-passed' || $data->status_slug == 'proposal-passed' || $data->status_slug == 'technical-passed') {
                    return 'success';
                }

                if ($data->status_slug == 'administration-verifying' || $data->status_slug == 'technical-surveyor-verifying' || $data->status_slug == 'technical-survey-verifying') {
                    return 'warning';
                }

                if (
                    $data->status_slug == 'administration-rejected' || $data->status_slug == 'technical-rejected' ||
                    $data->status_slug == 'administration-revising'
                ) {
                    return 'danger';
                }

            })
            ->addColumn('location', function($data) use ($swadaya) {


                if ($data->unit_id == $swadaya->id) {
                    return '<span style="color: #ff592e; font-style: italic">[Swadaya banyak lokasi]</span>';
                }

                return \Cache::get('region-'.$data->location_id);
            })
            ->rawColumns(['action', 'location'])
            ->addColumn('action', function ($data) {

                return view('proposal.action')
                    ->with([
                        'edit_link'        => route('proposals.edit', [$data->id]),
                        'delete_link'      => route('proposals.destroy', [$data->id]),
                        'edit_status_link' => route('proposals.status.edit', [$data->id]),
                        'identifier'       => $this->identifier,
                        'proposal'         => $data
                    ])
                    ->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        //$query = User::query()->select($this->getColumns());

        $builder = Proposal::select([
            \DB::raw('proposals.created_at'),
            \DB::raw('proposals.unit_id'),
            \DB::raw('units.name as unit_name'),
//            \DB::raw('recipients.name as recipient_name'),
//            \DB::raw('regions.name as region_name'),
            \DB::raw('statuses.name as status_name'),
            \DB::raw('statuses.slug as status_slug'),
            \DB::raw('programs.name as program_name'),
            'proposals.proposed_year',
            'proposals.status_id',
            'proposals.proposer_remark',
            'proposals.location_id',
            \DB::raw('proposals.proposer_id'),
            \DB::raw('proposals.id as id'),
            \DB::raw('agencies.name as proposer_name'),
            \DB::raw('users.name as creator_name')
        ])
            ->join('units', 'units.id', '=', 'proposals.unit_id')
            ->join('programs', 'programs.id', '=', 'proposals.program_id')
            ->join('recipients', 'recipients.id', '=', 'proposals.recipient_id')
//        ->join('regions', 'regions.id', '=', 'proposals.location_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->join('users', 'users.id', '=', 'proposals.creator_id')
            ->join('statuses', 'statuses.id', '=', 'proposals.status_id');

        // jika sebagai admin pusat (bukan super admin)
        if (\Auth::user()->role_id == Role::whereSlug('admin-central')->first()->id) {


            // Permintaan admin pusat bisa melihat status draft, jadi di komen dulu
            #$builder->whereNotIn('status_id', Status::whereIn('slug', [
            #    'proposal-draft'
            #])->pluck('id'));


//            $builder->whereNull('proposer_id');
        }

        // Jika sebagai pemkab/pemkot
        if (\Gate::denies('do-admin')) {


            if (\Auth::user()->agency_id !== null) {
                if (\Gate::denies('do-watcher')) {
                    $builder->whereProposerId(\Auth::user()->agency->id);
                }
            }

        }

        // Jika sebagai diektorat rusun, rusus, ruk, swadaya
        if (\Gate::allows('do-technical')) {
            $statusesId = Status::whereIn('slug', [
                'proposal-draft',
                'proposal-new',
//                'proposal-passed',
            ])->pluck('id');

            $builder->whereNotIn('status_id', $statusesId);
            $builder->where('proposals.unit_id', '=', \Auth::user()->unit_id);
        }

        // Searching
        $keySearchUnitId = request()->get('unit');
        $keySearchProposerName = request()->get('proposer_name');
        $keySearchStatusId = request()->get('status');
        $keySearchProgramId = request()->get('program');
        $keySearchRecipientId = request()->get('recipient');
        $keySearchProgramName = request()->get('program_name');
        $keySearchProposedYear = request()->get('proposed_year');
        $keySearchProposerRemark = request()->get('proposer_remark');
        $keySearchRegion = request()->get('region');

        if ((int)$keySearchUnitId !== 0) {
            $builder->where('proposals.unit_id', '=', $keySearchUnitId);
        }

        if (trim($keySearchProposerName) !== '') {
            $keySearchProposerName = strtolower($keySearchProposerName);
            $builder->where(\DB::raw('lower(agencies.name)'), 'like', "%{$keySearchProposerName}%");
        }

        if ((int)$keySearchStatusId !== 0) {
            $builder->where('statuses.id', '=', $keySearchStatusId);
        }

        if((int)$keySearchRecipientId !== 0) {
            $builder->where('proposals.recipient_id', $keySearchRecipientId);
        }

        if((int)$keySearchProgramId !== 0) {
            $builder->where('proposals.program_id', $keySearchProgramId);
        }

        if (trim($keySearchProgramName) !== '') {
            $keySearchProgramName = strtolower($keySearchProgramName);
            $builder->where(\DB::raw('lower(programs.name)'), 'like', "%{$keySearchProgramName}%");
        }

        if (trim($keySearchProposerRemark) !== '') {
            $keySearchProposerRemark = strtolower($keySearchProposerRemark);
            $builder->where(\DB::raw('lower(proposals.proposer_remark)'), 'like', "%{$keySearchProposerRemark}%");
        }

        if ((int)$keySearchProposedYear !== 0) {
            $builder->where('proposals.proposed_year', '=', $keySearchProposedYear);
        }


        if ((int)$keySearchRegion !== 0) {
            $regions = Region::descendantsAndSelf($keySearchRegion)->pluck('id');

            if ((int)$keySearchUnitId == Unit::whereSlug('swadaya')->first()->id || (int)$keySearchUnitId == 0) {
                # Spesial buat swadaya
//                $builder->leftJoin('dwelling_plans', 'dwelling_plans.proposal_id', '=', 'proposals.id');
                # Cari dulu dwelling plans dengan region id tersebut dan proposal tersebut
                # Pluck id nya
                $proposalIds = DwellingPlan::whereIn('location_id', $regions)->pluck('proposal_id');
                $builder->whereIn('proposals.id', $proposalIds);
            } else {
                $builder->whereIn('location_id', $regions);
            }
        }


        // gate
        if (\Gate::allows('do-watcher')) {
            // melihat kabupaten dibawah provinsinya
            $regionId = \Auth::user()->agency->region->id;
            $region = Region::find($regionId);
            $agencies = Agency::whereIn('region_id', $region->descendants()->where('level', 2)->pluck('id'))
                ->pluck('id');

            $agencies->push(\Auth::user()->agency->id);

            $builder->whereIn('proposals.proposer_id', $agencies);

            $statusesId = Status::whereIn('slug', [
                'proposal-draft',
            ])->pluck('id');

            $builder->whereNotIn('status_id', $statusesId);
        }

        return $this->applyScopes($builder);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->ajax([
                        'url'   => '',
                        'data'  =>
                            'function(d) {
                                var $filter = $(\'#filter\');
                                d.unit = $filter.find(\'#unit\').val();
                                d.proposer_name = $filter.find(\'#proposer_name\').val();
                                d.status = $filter.find(\'#status\').val();
                                // d.program_name = $filter.find(\'#program_name\').val();
                                d.program = $filter.find(\'#program\').val();
                                d.recipient = $filter.find(\'#recipient\').val();
                                d.proposed_year = $filter.find(\'#proposed_year\').val();
                                d.proposer_remark = $filter.find(\'#proposer_remark\').val();
                                d.region = $filter.find(\'#region\').val();

                            }'
                    ])
                    ->addAction(['width' => '150px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
//                        'stateSave' => true,
                        'buttons' => [
                            'excel',
//                            'reload',
                        ],
                        'searching' => false,
//                        'pageLength' => 25,
//                        'lengthMenu' => [[25, 50, 75, 100], [25, 50, 75, 100]],
                        'processing' => true,
                        'serverSide' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'created_at', 'name' => 'proposals.created_at', 'title' => 'Tanggal pembuatan'],
            ['data' => 'unit_name', 'name' => 'units.name', 'title' => 'Direktorat'],
            ['data' => 'creator_name', 'name' => 'users.name', 'title' => 'User pengusul'],
            ['data' => 'status_name', 'name' => 'statuses.name', 'title' => 'Status'],
            ['data' => 'program_name', 'name' => 'programs.name', 'title' => 'Kegiatan'],
            ['data' => 'location', 'name' => 'proposals.location_id', 'title' => 'Lokasi', 'searchable' => false, 'orderable' => false, ],
//            ['data' => 'proposed_year', 'name' => 'proposed_year', 'title' => 'Tahun penanganan'],
            ['data' => 'proposer_remark', 'name' => 'proposer_remark', 'title' => 'Identitas pengusul'],
//            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'width' =>  '150px'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usulan_' . time();
    }
}
