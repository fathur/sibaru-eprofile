<?php

namespace App\DataTables;

use App\Models\Eprofile;
use App\Models\Region;
use App\User;
use Yajra\Datatables\Services\DataTable;

class EprofileDataTable extends DataTable
{
    protected $identifier = 'eprofile';

    protected $actions = ['excel', 'advancedExcel'];

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('total_budget', function ($data) {
                return rupiah($data->total_budget);
            })
            ->addColumn('location', function($data) {
                return cache('region-' . $data->location_id);
            })
            ->addColumn('action', function($data){
                return view('eprofile.action')
                    ->with([
                        'edit_link' => route('eprofile.edit', [$data->id]),
                        'delete_link' => route('eprofile.destroy', [$data->id]),
                        'identifier' => $this->identifier,
                        'data' => $data
                    ])
                    ->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $builder = Eprofile::select([
            'eprofiles.*',
            \DB::raw('units.name as unit_name'),
            \DB::raw('agencies.name as proposer_name'),
            \DB::raw('programs.name as program_name'),
            \DB::raw('recipients.name as recipient_name'),
        ])
            ->leftJoin('units', 'units.id', '=', 'eprofiles.unit_id')
            ->leftJoin('recipients', 'recipients.id', '=', 'eprofiles.recipient_id')
            ->leftJoin('programs', 'programs.id', '=', 'eprofiles.program_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'eprofiles.proposer_id');

        // Searching
        $keySearchUnitId = request()->get('unit');
        $keySearchProposerName = request()->get('proposer_name');
        $keySearchProgramName = request()->get('program_name');
        $keySearchProposedYear = request()->get('proposed_year');
        $keySearchProposerRemark = request()->get('proposer_remark');
        $keySearchRegion = request()->get('region');
        $keySearchRecipient = request()->get('recipient');


        if(\Gate::allows('do-technical')) {
            $builder->where('eprofiles.unit_id', '=', \Auth::user()->unit_id);
        }

        if ((int)$keySearchUnitId !== 0) {
            $builder->where('eprofiles.unit_id', '=', $keySearchUnitId);
        }

        if (trim($keySearchProposerName) !== '') {
            $keySearchProposerName = strtolower($keySearchProposerName);
            $builder->where(\DB::raw('lower(agencies.name)'), 'like', "%{$keySearchProposerName}%");
        }

        if (trim($keySearchProgramName) !== '') {
            $keySearchProgramName = strtolower($keySearchProgramName);
            $builder->where(\DB::raw('lower(programs.name)'), 'like', "%{$keySearchProgramName}%");
        }

        if (trim($keySearchProposerRemark) !== '') {
            $keySearchProposerRemark = strtolower($keySearchProposerRemark);
            $builder->where(\DB::raw('lower(eprofiles.proposer_remark)'), 'like', "%{$keySearchProposerRemark}%");
        }

        if ((int)$keySearchProposedYear !== 0) {
            $builder->where('eprofiles.proposed_year', '=', $keySearchProposedYear);
        }

        if((int)$keySearchRegion !== 0) {
            $regions = Region::descendantsAndSelf($keySearchRegion)->pluck('id');
            $builder->whereIn('location_id', $regions);
        }

        if((int)$keySearchRecipient !== 0) {
            $builder->where('recipient_id', '=', $keySearchRecipient);
        }


        return $this->applyScopes($builder);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->ajax([
                        'url'   => '',
                        'data'  =>
                            'function(d) {
                                var $filter = $(\'#filter\');
                                d.unit = $filter.find(\'#unit\').val();
                                d.proposer_name = $filter.find(\'#proposer_name\').val();
                                d.program_name = $filter.find(\'#program_name\').val();
                                d.proposed_year = $filter.find(\'#proposed_year\').val();
                                d.proposer_remark = $filter.find(\'#proposer_remark\').val();
                                d.region = $filter.find(\'#region\').val();
                                d.recipient = $filter.find(\'#recipient\').val();
                            }'
                    ])
                    ->addAction(['width' => '150px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'advancedExcel',
                            'excel',
//                            'reload',
                        ],
                        'searching' => false,
//                        'pageLength' => 25,
//                        'lengthMenu' => [[25, 50, 75, 100], [25, 50, 75, 100]],
                        'processing' => true,
                        'serverSide' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'proposed_year', 'name' => 'proposed_year', 'title' => 'Tahun'],
            ['data' => 'unit_name', 'name' => 'units.name', 'title' => 'Direktorat'],
            ['data' => 'location', 'name' => 'location', 'searchable' => false, 'orderable' => false, 'title' => 'Lokasi'],
            ['data' => 'recipient_name', 'name' => 'recipient_name', 'searchable' => false, 'orderable' => false, 'title' => 'Penerima Manfaat'],
            ['data' => 'program_name', 'name' => 'program_name', 'title' => 'Kegiatan'],
            ['data' => 'amount_unit', 'name' => 'amount_unit', 'searchable' => false, 'orderable' => false, 'title' => 'Jumlah Unit'],
            ['data' => 'total_budget', 'name' => 'total_budget', 'searchable' => false, 'orderable' => false, 'title' => 'Anggaran'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'eprofile_' . time();
    }

    public function advancedExcel()
    {
//        return 'a';
    }
}
