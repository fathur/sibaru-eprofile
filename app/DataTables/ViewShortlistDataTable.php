<?php

namespace App\DataTables;

use App\Models\Agency;
use App\Models\Shortlist;
use Yajra\Datatables\Services\DataTable;

class ViewShortlistDataTable extends DataTable
{
    protected $identifier = 'shortlist';

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('proposer_name', function($data) {


                return \Cache::tags(['region', 'parent', 'agency'])
                    ->rememberForever('agency-parent-' . $data->region_id, function() use ($data) {
                        if($data->region_id != null and $data->parent_id != null) {
                            $agency = Agency::find($data->parent_id);

                            if(is_null($agency->parent_id)) {
                                return $agency->name;
                            }
                        }

                        return $data->proposer_name;
                    });


            })
            ->editColumn('location', function($data) {
                return cache('region-' . $data->location_id);
            })
            ->editColumn('sum_budget_plan', function($data) {
                return rupiah($data->sum_budget_plan);
            })
            ->rawColumns(['action'])
            ->addColumn('action', function ($data) {

                return view('shortlist.action_view')
                    ->with([
                        'identifier' => $this->identifier,
                        'shortlist'  => $data
                    ])
                    ->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Shortlist::select([
            'score', 'proposer_remark', 'sum_amount', 'sum_budget_plan',
            'agencies.region_id', 'agencies.parent_id',
            \DB::raw('proposals.location_id as location_id'),
            \DB::raw('regions.name as location_name'),
            \DB::raw("units.name as unit_name"),
            \DB::raw("agencies.name as proposer_name"),
        ])->leftJoin('proposals', 'proposals.id', '=', 'shortlists.proposal_id')
            ->leftJoin('units', 'units.id', '=', 'proposals.unit_id')
            ->leftJoin('agencies', 'agencies.id', '=', 'proposals.proposer_id')
            ->leftJoin('regions', 'regions.id', '=', 'proposals.location_id');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
//            ->addAction([])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
                    'excel'
//                    'create',
//                    'export',
//                    'print',
//                    'reset',
//                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'score', 'name' => 'shortlists.score', 'title' => 'Score'],
            ['data' => 'unit_name', 'name' => 'units.name', 'title' => 'Direktorat'],
            ['data' => 'proposer_name', 'name' => 'agencies.name', 'title' => 'Instansi Pengusul'],
            ['data' => 'location', 'name' => 'proposals.location_name', 'title' => 'Lokasi'],
            ['data' => 'sum_amount', 'name' => 'shortlists.sum_amount', 'title' => 'Jumlah Unit'],
            ['data' => 'sum_budget_plan', 'name' => 'shortlists.sum_budget_plan', 'title' => 'Usulan Anggaran'],
//            ['data' => 'status_name', 'name' => 'statuses.name', 'title' => 'Status'],
//            ['data' => 'program_name', 'name' => 'programs.name', 'title' => 'Kegiatan'],
//            ['data' => 'proposed_year', 'name' => 'proposed_year', 'title' => 'Tahun penanganan'],
            ['data' => 'proposer_remark', 'name' => 'proposer_remark', 'title' => 'Identitas pengusul']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'shortlists_' . time();
    }
}
