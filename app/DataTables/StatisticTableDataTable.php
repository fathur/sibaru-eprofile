<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 05/08/18
 * Time: 17.24
 */

namespace App\DataTables;


use App\Models\Agency;
use App\Models\Proposal;
use App\Models\Region;
use App\Models\Status;
use App\Models\Unit;
use Carbon\Carbon;
use Yajra\Datatables\Services\DataTable;

class StatisticTableDataTable extends DataTable
{
    protected $identifier = 'proposal-statistic';

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $builder = Proposal::select();

        return $this->applyScopes($builder);
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->ajax([
                'url' => '',
            ])
            ->parameters([
                'dom' => 'Bfrtip',
//                        'stateSave' => true,
                'buttons' => [
//                    'excel',
////                            'reload',
                ],
                'searching' => false,
                'ordering' => false,
                'paging' => false,
                'processing' => true,
                'serverSide' => true,
            ]);
    }

    protected function getColumns()
    {
        $role = \Auth::user()->role->slug;

        if ($role == 'admin-central') {
            return [
                ['data' => 'province', 'name' => 'province', 'title' => 'Provinsi'],
                ['data' => 'rusun', 'name' => 'rusun', 'title' => 'Rusun'],
                ['data' => 'rusus', 'name' => 'rusus', 'title' => 'Rusus'],
                ['data' => 'ruk', 'name' => 'ruk', 'title' => 'Ruk'],
                ['data' => 'swadaya', 'name' => 'swadaya', 'title' => 'Swadaya'],
                ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
            ];
        } elseif ($role == 'admin-technical') {
            return [
                ['data' => 'province', 'name' => 'province', 'title' => 'Provinsi'],
                ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
            ];
        } elseif ($role == 'watcher') {
            return [
                ['data' => 'kabupaten', 'name' => 'province', 'title' => 'Kabupaten'],
                ['data' => 'rusun', 'name' => 'rusun', 'title' => 'Rusun'],
                ['data' => 'rusus', 'name' => 'rusus', 'title' => 'Rusus'],
                ['data' => 'ruk', 'name' => 'ruk', 'title' => 'Ruk'],
                ['data' => 'swadaya', 'name' => 'swadaya', 'title' => 'Swadaya'],
                ['data' => 'total', 'name' => 'total', 'title' => 'Total'],
            ];
        }
    }

    public function dataTable()
    {
        $role = \Auth::user()->role->slug;

        if ($role == 'admin-central') {
            $data = $this->collectProvinces();
        } elseif ($role == 'admin-technical') {
            $unit = Unit::find(\Auth::user()->unit_id);
            $data = $this->collectTechnical($unit);
        } elseif ($role == 'watcher') {
            $province = \Auth::user()->agency->region;
            $data = $this->collectKabupatens($province);

        }

        return $this->datatables->collection($data);
    }

    /**
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function collectProvinces()
    {
        # Get all provinces
        $provinces = Region::whereLevel(1)->orderBy('order')->get();
        $draft = Status::whereSlug('proposal-draft')->first();

        $dataProvinces = [];
        foreach ($provinces as $province) {

            # Mendapatkan kabupaten dari provinsi ini
            $agenciesId = \Cache::remember('statistic-table-province-agencies:' . $province->id, Carbon::now()->addDay(), function () use ($province) {
                $kabupatensId = $province->getDescendants()
                    ->pluck('id');


                $agenciesId = Agency::whereIn('region_id', $kabupatensId)->pluck('id');

                return $agenciesId;
            });

            $props = Proposal::select([\DB::raw("COUNT(*)"), 'unit_id'])
                ->groupBy(['unit_id'])
                ->whereIn('proposer_id', $agenciesId)
                ->whereNotIn('status_id', [$draft->id])
                ->get();

            $dataProvinces[$province->id] = $props;
        }

        $units = Unit::all();
        $all = [];
        foreach ($dataProvinces as $provId => $data) {

            $prov = [];
            foreach ($units as $unit) {

                foreach ($data as $datum) {
                    if ($unit->id == $datum->unit_id) {
                        $prov[$unit->slug] = $datum->count;
                    }
                }
            }

            $rusun = isset($prov['rusun']) ? $prov['rusun'] : 0;

            $rusus = isset($prov['rusus']) ? $prov['rusus'] : 0;
            $ruk = isset($prov['ruk']) ? $prov['ruk'] : 0;
            $swadaya = isset($prov['swadaya']) ? $prov['swadaya'] : 0;

            $renderText = [
                'province_id' => $provId,
                'province' => Region::find($provId)->name,
                'rusun' => $rusun,
                'rusus' => $rusus,
                'ruk' => $ruk,
                'swadaya' => $swadaya,
                'total' => $rusun + $rusus + $ruk + $swadaya
            ];

            array_push($all, $renderText);
        }

        return collect($all);
    }

    protected function collectTechnical(Unit $directorate)
    {
        # Get all provinces
        $provinces = Region::whereLevel(1)->orderBy('order')->get();
        $draft = Status::whereSlug('proposal-draft')->first();

        $dataProvinces = [];
        foreach ($provinces as $province) {

            # Mendapatkan kabupaten dari provinsi ini
            $agenciesId = \Cache::remember('statistic-table-province-agencies:' . $province->id, Carbon::now()->addDay(), function () use ($province) {
                $kabupatensId = $province->getDescendants()
                    ->pluck('id');


                $agenciesId = Agency::whereIn('region_id', $kabupatensId)->pluck('id');

                return $agenciesId;
            });

            $props = Proposal::select([\DB::raw("COUNT(*)"), 'unit_id'])
                ->groupBy(['unit_id'])
                ->whereIn('proposer_id', $agenciesId)
                ->where('unit_id', $directorate->id)
                ->whereNotIn('status_id', [$draft->id])
                ->get();

            $dataProvinces[$province->id] = $props;
        }

        $all = [];
        foreach ($dataProvinces as $provId => $data) {

            $prov = [];

            foreach ($data as $datum) {
                if ($directorate->id == $datum->unit_id) {
                    $prov[$directorate->slug] = $datum->count;
                }
            }

            $rusun = isset($prov['rusun']) ? $prov['rusun'] : 0;

            $rusus = isset($prov['rusus']) ? $prov['rusus'] : 0;
            $ruk = isset($prov['ruk']) ? $prov['ruk'] : 0;
            $swadaya = isset($prov['swadaya']) ? $prov['swadaya'] : 0;

            $renderText = [
                'province_id' => $provId,
                'province' => Region::find($provId)->name,
                'total' => $rusun + $rusus + $ruk + $swadaya
            ];

            array_push($all, $renderText);
        }

        return collect($all);

    }

    protected function collectKabupatens(Region $province)
    {
        $kabupatens = Region::whereLevel(2)
            ->where('parent_id', $province->id)
            ->orderBy('order')
            ->orderBy('name')
            ->get();

        $draft = Status::whereSlug('proposal-draft')->first();

        $dataKabs = [];
        foreach ($kabupatens as $kabupaten) {

            $props = Proposal::select([\DB::raw("COUNT(*)"), 'unit_id'])
                ->groupBy(['unit_id'])
                ->where('proposer_id', Agency::whereRegionId($kabupaten->id)->first()->id)
                ->whereNotIn('status_id', [$draft->id])
                ->get();

            $dataKabs[$kabupaten->id] = $props;
        }


        $units = Unit::all();
        $all = [];
        foreach ($dataKabs as $kabId => $data) {

            $kab = [];
            foreach ($units as $unit) {

                foreach ($data as $datum) {
                    if ($unit->id == $datum->unit_id) {
                        $kab[$unit->slug] = $datum->count;
                    }
                }
            }

            $rusun = isset($kab['rusun']) ? $kab['rusun'] : 0;

            $rusus = isset($kab['rusus']) ? $kab['rusus'] : 0;
            $ruk = isset($kab['ruk']) ? $kab['ruk'] : 0;
            $swadaya = isset($kab['swadaya']) ? $kab['swadaya'] : 0;

            $renderText = [
                'kabupaten_id' => $kabId,
                'kabupaten' => Region::find($kabId)->name,
                'rusun' => $rusun,
                'rusus' => $rusus,
                'ruk' => $ruk,
                'swadaya' => $swadaya,
                'total' => $rusun + $rusus + $ruk + $swadaya
            ];

            array_push($all, $renderText);
        }

        return collect($all);
    }
}