<?php

namespace App\DataTables;

use App\Models\Document;
use Yajra\Datatables\Services\DataTable;

class GuidanceDataTable extends DataTable
{
    protected $identifier = 'pedoman';

    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('file', function($data) {
                return '<a href="'.route('public.pedoman.download', $data->id).'">'.$data->filename.'</a>';
            })
            ->rawColumns(['action','file'])

            ->addColumn('action', function ($data) {

                return view('layout.action')
                    ->with([
                        'edit_link'        => route('guidance.edit', [$data->id]),
                        'delete_link'      => route('guidance.destroy', [$data->id]),
                        'identifier'       => $this->identifier,
                        'proposal'         => $data
                    ])
                    ->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Document::query();//->select($this->getColumns());

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->addAction(['width' => '130px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
//                            'create',
//                            'export',
//                            'print',
//                            'reset',
//                            'reload',
//                            'excel'
                ],
                'processing' => true,
                'serverSide' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'title', 'name' => 'title', 'title' => 'Judul'],
            ['data' => 'description', 'name' => 'description', 'title' => 'Keterangan'],
            ['data' => 'file', 'name' => 'file', 'title' => 'File', 'sortable' => false, 'orderable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pedoman_' . time();
    }
}
