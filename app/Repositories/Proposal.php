<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\Status;
use App\Models\Proposal as ProposalModel;
use App\Models\Unit;
use App\Models\User;
use App\Notifications\Proposal\AdministrationCompleteToTechnicalVerifying;
use App\Notifications\Proposal\AdministrationRevisingToRepaired;
use App\Notifications\Proposal\AdministrationVerifyingToComplete;
use App\Notifications\Proposal\AdministrationVerifyingToRejected;
use App\Notifications\Proposal\AdministrationVerifyingToRevising;
use App\Notifications\Proposal\ProposalDraftToNew;
use App\Notifications\Proposal\ProposalNewToAdministrationVerifying;
use App\Notifications\Proposal\TechnicalVerifyingToComplete;
use App\Notifications\Proposal\TechnicalVerifyingToRejected;

class Proposal
{
    public static function changeStatus($proposalId, $force = null)
    {

        $proposal = ProposalModel::find($proposalId);
//        dd($proposal->status->slug);

        switch ($proposal->status->slug) {
            case 'proposal-draft':
                $newStatus = Status::whereSlug('proposal-new')->first();

                // Pengusul: kabupaten. Send notification to: - admin pusat
                self::notificationProposalDraftToNew($proposal);

                break;

            case 'proposal-new':
                $newStatus = Status::whereSlug('administration-verifying')->first();

                self::notificationProposalNewToAdministrationVerifying($proposal);
                break;

            case 'administration-revising':
                $newStatus = Status::whereSlug('administration-repaired')->first();

                $role = Role::whereSlug('admin-technical')->first();
                $users = User::whereRoleId($role->id)->whereUnitId($proposal->unit_id)->get();
                \Notification::send($users, new AdministrationRevisingToRepaired($proposal));
                break;

            case 'administration-repaired':
                if ($force == 'reject') {
                    $newStatus = Status::whereSlug('administration-rejected')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToRejected($proposal));

                } elseif ($force == 'revise') {
                    $newStatus = Status::whereSlug('administration-revising')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToRevising($proposal));
                } else { // setujui
                    $newStatus = Status::whereSlug('technical-surveyor-verifying')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToComplete($proposal));
                }
                break;

            case 'administration-verifying':
                if ($force == 'reject') {
                    $newStatus = Status::whereSlug('administration-rejected')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToRejected($proposal));

                } elseif ($force == 'revise') {
                    $newStatus = Status::whereSlug('administration-revising')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToRevising($proposal));

                } else {
                    $newStatus = Status::whereSlug('technical-surveyor-verifying')->first(); # Administrasi lengkap

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new AdministrationVerifyingToComplete($proposal));
                }
                break;

            case 'technical-surveyor-verifying':
                $newStatus = Status::whereSlug('technical-survey-verifying')->first();

                $users = User::whereAgencyId($proposal->proposer_id)->get();
                \Notification::send($users, new AdministrationCompleteToTechnicalVerifying($proposal));
                break;

            case 'technical-survey-verifying':
                if ($force == 'reject') {
                    $newStatus = Status::whereSlug('technical-rejected')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new TechnicalVerifyingToRejected($proposal));

                } else {
                    $newStatus = Status::whereSlug('technical-passed')->first();

                    $users = User::whereAgencyId($proposal->proposer_id)->get();
                    \Notification::send($users, new TechnicalVerifyingToComplete($proposal));

                }
                break;

            case  'technical-passed':
                $newStatus = Status::whereSlug('proposal-passed')->first();
                break;

            default:

        }

        $proposal->status_id = $newStatus->id;
        $proposal->save();

        return $proposal;

    }

    /**
     * @param $proposal
     */
    private static function notificationProposalDraftToNew($proposal)
    {
        $role = Role::whereSlug('admin-central')->first();
        $users = User::where('role_id', '=', $role->id)->get();
        \Notification::send($users, new ProposalDraftToNew($proposal));
    }

    /**
     * @param $proposal
     */
    private static function notificationProposalNewToAdministrationVerifying($proposal)
    {
// Pengusul: kabupaten. Send notif to - admin teknis - pengusul kabupaten yang bersangkutan
        $role = Role::whereSlug('admin-technical')->first();
        $tech_users = User::whereRoleId($role->id)->whereUnitId($proposal->unit_id)->get();
        $kab_users = User::whereAgencyId($proposal->proposer_id)->get();
        $users = $tech_users->merge($kab_users);
        \Notification::send($users, new ProposalNewToAdministrationVerifying($proposal));
    }
}