<?php


namespace App\Repositories;


use App\Models\Eprofile;
use App\Models\Unit;
use Carbon\Carbon;

class DirectorateUpdate
{
    protected $today;

    protected $directorate;

    protected $inWeek;

    protected $firstTwoWeekDays = [];

    protected $secondTwoWeekDays = [];

    function __construct()
    {
        $this->today = Carbon::now();

        $this->firstTwoWeekDays = range(1, 15);

        $this->secondTwoWeekDays = range(16, 31);
    }

    public function rusun()
    {
        $this->directorate = 'rusun';

        return $this;
    }

    public function rusus()
    {
        $this->directorate = 'rusus';

        return $this;
    }

    public function ruk()
    {
        $this->directorate = 'ruk';

        return $this;
    }

    public function swadaya()
    {
        $this->directorate = 'swadaya';

        return $this;
    }

    public function firstTwoWeek()
    {
        $this->inWeek = 1;

        return $this;
    }

    public function secondTwoWeek()
    {
        $this->inWeek = 2;

        return $this;
    }

    public function isUpdate()
    {
        $count = \Cache::tags(['chart'])
            ->remember("progress-per-2-minggu@directorate:{$this->directorate}@week:{$this->inWeek}", Carbon::now()->addHour(), function () {
                $unit = Unit::whereSlug($this->directorate)->first();

                if ($this->inWeek == 1) {
                    $d1 = $this->firstTwoWeekDays[0];
                    $d2 = $this->firstTwoWeekDays[count($this->firstTwoWeekDays) - 1];

                } elseif ($this->inWeek == 2) {
                    $d1 = $this->secondTwoWeekDays[0];
                    $d2 = $this->secondTwoWeekDays[count($this->secondTwoWeekDays) - 1];
                }

                $m = $this->today->month;
                $y = $this->today->year;

                $format1 = "{$d1}-{$m}-{$y}";
                $format2 = "{$d2}-{$m}-{$y}";

                $firstDay = Carbon::parse($format1);
                $lastDay = Carbon::parse($format2);

                $count = Eprofile::where('unit_id', $unit->id)
                    ->whereBetween('updated_at', [$firstDay, $lastDay])
                    ->count();

                return $count;
            });



        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function lastUpdate()
    {
        $max = \Cache::tags(['chart'])
            ->remember("progress-per-2-minggu-last-update@directorate:{$this->directorate}", Carbon::now()->addHour(), function () {
                $unit = Unit::whereSlug($this->directorate)->first();

                $max = Eprofile::where('unit_id', $unit->id)
                    ->max('updated_at');

                return $max;
            });



        return 'Status tanggal ' . Carbon::parse($max)->format('j M Y');
    }
}
//
//$du = new DirectorateUpdate();
//$du->rusun()->firstTwoWeek()->isUpdate();