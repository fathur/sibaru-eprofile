<?php

namespace App\Repositories;


use App\Models\Eprofile;
use App\Models\RegionProfile;
use App\Models\RegionSpecial;
use App\Models\Unit;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

class RegionProfileExport
{
    protected $type;

    /**
     * @var RegionProfile
     */
    protected $regionProfile;

    /**
     * @var \App\Models\Region
     */
    protected $region;

    /**
     * @var integer
     */
    protected $year;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     * @throws \Exception
     */
    public function setType($type)
    {
        $possibleType = ['xls', 'xlsx', 'pdf'];

        if (in_array($type, $possibleType)) {

            $this->type = $type;

        } else {
            $this->type = 'xls';

        }

        return $this;


    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    public function whenYear($year)
    {
        return $this->setYear($year);
    }

    /**
     * @param RegionProfile $regionProfile
     * @return $this
     */
    public function setProfile(RegionProfile $regionProfile)
    {
        $this->regionProfile = $regionProfile;

        return $this;
    }

    public function getRegionProfile()
    {
        if($this->year !== null and $this->region !== null) {
            return RegionProfile::with('region')
                ->where('year', $this->year)
                ->where('region_id', $this->region->id)
                ->first();
        }

        return $this->regionProfile;
    }

    public function inRegion(\App\Models\Region $region)
    {
        $this->region = $region;

        return $this;
    }

    public function getRegencyProfiles()
    {
        if($this->year !== null and $this->region !== null) {
            $this->regionProfile = $this->getRegionProfile();
        }

        $regencyProfiles = RegionProfile::leftJoin('regions', 'regions.id', '=', 'region_profiles.region_id')
            ->select([
                'regions.name',
                'region_profiles.*'
            ])->where('regions.parent_id', $this->region->id)
            ->where('year', $this->regionProfile->year)
            ->where('regions.level', 2)
            ->orderBy('regions.name')
            ->get();

        return $regencyProfiles;
    }

    public function export()
    {
        return \Excel::create('Export profile ' . $this->regionProfile->id, function (LaravelExcelWriter $excel) {

            $regencyProfiles = $this->getRegencyProfiles();

            $this->sheetWilayah($excel, $regencyProfiles);
            $this->sheetRecapitulation($excel, $regencyProfiles);
            $this->sheetEprofile($excel, $regencyProfiles);
        })->download($this->type);
    }

    /**
     * @param LaravelExcelWriter $excel
     */
    protected function sheetWilayah(LaravelExcelWriter $excel, $regencyProfiles)
    {

        $excel->sheet('Wilayah', function (LaravelExcelWorksheet $sheet) use ($regencyProfiles) {


            $title = strtoupper("Profile Provinsi {$this->regionProfile->region->name}");
            $sheet->row(1, [$title]);


            $sheet->cell('A1', function (CellWriter $cell) {
                $cell->setFontSize(18);
                $cell->setFontWeight();
            });

            if ($this->regionProfile->head_photo) {
                $headDrawing = new \PHPExcel_Worksheet_Drawing();
                $headDrawing->setName($this->regionProfile->head_name);
                $headDrawing->setDescription($this->regionProfile->head_name);
                //Path to signature .jpg file
                $signature = base_path($this->regionProfile->head_photo);
                $headDrawing->setPath($signature);
//                $objDrawing->setOffsetX(600);                     //setOffsetX works properly
//                $objDrawing->setOffsetY(10);                     //setOffsetY works properly
                $headDrawing->setCoordinates('E1');             //set image to cell
//                $objDrawing->setWidth(100);
                $headDrawing->setHeight(130);                     //signature height
                $headDrawing->setWorksheet($sheet);
            }

            if ($this->regionProfile->deputy_photo) {
                $headDrawing = new \PHPExcel_Worksheet_Drawing();
                $headDrawing->setName($this->regionProfile->deputy_name);
                $headDrawing->setDescription($this->regionProfile->deputy_name);
                $signature = base_path($this->regionProfile->deputy_photo);
                $headDrawing->setPath($signature);
                $headDrawing->setCoordinates('F1');             //set image to cell
                $headDrawing->setHeight(130);                     //signature height
                $headDrawing->setWorksheet($sheet);
            }

            $sheet->cell('A1:F4', function (CellWriter $cell) {
                $cell->setBackground('#EFF3F8');
            });
            $sheet->cell('A5:F5', function (CellWriter $cell) {
                $cell->setBackground('#D7B966');
            });

            $sheet->row(8, [
                '1', 'Gubernur', ':', $this->regionProfile->head_name
            ]);
            $sheet->cell('D8', function (CellWriter $cell) {
                $cell->setFontWeight();
            });

            $sheet->rows([
                ['', '', '', $this->regionProfile->head_party],
                ['2', 'Wakil Gubernur', ':', 'sdf' . $this->regionProfile->deputy_name],
                ['', '', '', $this->regionProfile->deputy_party],

                ['3', 'Masa Bakti', ':', $this->regionProfile->masa_bakti],

                ['4', 'Kelembagaan Perkim', ':', $this->regionProfile->perkim_kelembagaan],

                ['5', 'Kontak personn', ':', $this->regionProfile->perkim_pic_name],
                ['', '(SKPD Dekosentrasi)', ':', 'Telp/Fax. ' . $this->regionProfile->perkim_phone . '/' . $this->regionProfile->perkim_fax],
                ['', '', ':', "Email: {$this->regionProfile->perkim_email}"],

                ['6', 'Letak Geografis', ':', $this->regionProfile->koordinat],

                ['7', 'Luas Wilayah'],
                ['', '- Daratan', ':', $this->regionProfile->land_large],
                ['', '- Lautan', ':', $this->regionProfile->sea_large],

                ['8', 'Basis Data A8:E22 (Backlog berdasarkan data BKKBN, RTLH berdasarkan)',],
                ['', 'Kabupaten/Kota', 'RTLH', '', 'Backlog Kepemilikan', 'Backlog Penghunian'],
            ]);

            $sheet->getStyle('D9')->getFont()->setItalic();
            $sheet->cell('D10', function (CellWriter $cell) {
                $cell->setFontWeight();
            });
            $sheet->getStyle('D11')->getFont()->setItalic();

            $sheet->cell('B22:F22', function (CellWriter $cell) {
                $cell->setBackground('#337AB7');
                $cell->setFontColor('#FFFFFF');
                $cell->setAlignment('center');
            });
            $sheet->mergeCells("C22:D22");

            $i = 23;

            foreach ($regencyProfiles as $regencyProfile) {


                $sheet->row($i, [
                    '', $regencyProfile->name, $regencyProfile->rtlh, '', $regencyProfile->backlog_kepemilikan, $regencyProfile->backlog_house
                ]);
                $sheet->mergeCells("C{$i}:D{$i}");

                $i++;
            }
            $sheet->setBorder('B22:F' . ($i - 1), 'thin');

            $sheet->rows([
                ['9', 'Informasi Umum PKP'],
                ['', 'a. Jumlah Penduduk', ':', 343],
                ['', 'b. Pertumbuhan Penduduk', ':', $this->regionProfile->population_growth],
                ['', 'c. Tingkat Kepadatan Penduduk', ':', $this->regionProfile->population_density],
                ['', 'd. Jumlah Penduduk Miskin Kota', ':', $this->regionProfile->total_city_poor_people],
                ['', 'e. Jumlah Penduduk Miskin Desa', ':', $this->regionProfile->total_village_poor_people],
            ]);


            // Styling
            $sheet->setWidth('A', 4);
            $sheet->setWidth('B', 30);
            $sheet->setWidth('C', 3);
            $sheet->setWidth('D', 10);
            $sheet->setWidth('E', 20);
            $sheet->setWidth('F', 20);

            $sheet->cell('A8:A1000', function (CellWriter $cell) {
                $cell->setAlignment('center');
            });
            $sheet->cell('C8:C1000', function (CellWriter $cell) {
                $cell->setAlignment('right');
            });
            $sheet->cell('D8:D1000', function (CellWriter $cell) {
                $cell->setAlignment('left');
            });

        });
    }

    protected function sheetEprofile(LaravelExcelWriter $excel, $regencyProfiles)
    {
        $excel->sheet('Data eProfile', function (LaravelExcelWorksheet $sheet) use ($regencyProfiles) {
            $units = Unit::whereNotIn('slug', ['dirjen'])->get();

            $title = "Rincian Kegiatan Kabupaten/Kota Tahun {$this->regionProfile->year} Di Provinsi {$this->regionProfile->region->name}";
            $title = strtoupper($title);
            $sheet->row(1, [$title]);

            $sheet->cell('A1', function (CellWriter $cell) {
                $cell->setFontSize(18);
                $cell->setFontWeight();
            });

            $sheet->rows([
                [''], // baris kosong
                ['NO', 'PROGRAM KEGIATAN', 'UNIT', 'ANGGARAN'],
                [''] // baris kosong
            ]);

            $sheet->setBorder('A3:D3', 'thin');

            $sheet->cell('A3:D3', function (CellWriter $cell) {
                $cell->setBackground('#5CB85C');
                $cell->setFontWeight();
            });

            // Loop daerah
            $indexProfile = 1;
            $rowNumber = $indexProfile + 3;
            foreach ($regencyProfiles as $regencyProfile) {

                if ($this->dataByRegencyExist($regencyProfile)) {
                    $sheet->rows([
                        [$indexProfile, $regencyProfile->name],
                    ]);
                    $rowNumber++;

                    $sheet->cell("A{$rowNumber}:D{$rowNumber}", function (CellWriter $cell) {
                        // $cell->setBackground('#5CB85C');

                        $cell->setFontWeight();
                    });

                    $indexUnit = 1;
                    foreach ($units as $unit) {

                        $programsWithRecipients = $this->getProgramsWithRecipients($regencyProfile, $unit);

                        if (count($programsWithRecipients) > 0) {


                            $sheet->rows([
                                [
                                    "{$indexProfile}.{$indexUnit}",
                                    $unit->name,
                                    $this->sumUnitInDirectorate($regencyProfile, $unit),
                                    $this->sumBudgetInDirectorate($regencyProfile, $unit)
                                ],
                            ]);
                            $sheet->mergeCells("B{$rowNumber}:D{$rowNumber}");
                            $rowNumber++;


                            $sheet->cell("A{$rowNumber}:D{$rowNumber}", function (CellWriter $cell) {
                                $cell->setBackground('#5CB85C');

                                $cell->setFontWeight();
                            });


                            foreach ($programsWithRecipients as $uniques) {
                                $sheet->rows([
                                    ['', $uniques['program']->name . ', ' . $uniques['recipient']->name, $uniques['sum_unit'], $uniques['sum_budget']]
                                ]);
                                $rowNumber++;

                                $sheet->cell("A{$rowNumber}:D{$rowNumber}", function (CellWriter $cell) {
                                    $cell->setBackground('#D9EDF7');
                                    $cell->setFontWeight();
                                });

                                foreach ($uniques['eprofiles'] as $eprofile) {
                                    $sheet->rows([
                                        ['', '- ' . $eprofile->proposer_remark, $eprofile->amount_unit, $eprofile->total_budget]
                                    ]);

                                    $sheet->getStyle("B{$rowNumber}")
                                        ->getAlignment()
                                        ->setWrapText(true);

                                    $rowNumber++;

                                }
                            }


                            $indexUnit++;
                        }
                    }

                    $indexProfile++;
                }
            }

            $sheet->setWidth('A', 4);
            $sheet->setWidth('B', 65);
            $sheet->setWidth('C', 10);
            $sheet->setWidth('D', 15);

            $sheet->setBorder('A5:D' . $rowNumber, 'thin');

        });
    }

    protected function sheetRecapitulation(LaravelExcelWriter $excel, $regencyProfiles)
    {
        $excel->sheet('Rekapitulasi', function (LaravelExcelWorksheet $sheet) use ($regencyProfiles) {

            $title = strtoupper('Rekapitulasi Kegiatan di Provinsi ' . $this->regionProfile->region->name);

            $sheet->rows([
                [$title],
                [''],
                [''],
                [''],
                [''],
                [''],
                ['Pembangungan Kemenpera 2010-2014'],
                ['No', 'Kegiatan', 'Volume', 'Satuan', 'Anggaran']
            ]);

            $sheet->cell('A1', function (CellWriter $cell) {
                $cell->setFontSize(18);
                $cell->setFontWeight();
            });

            $sheet->cell('A1:E4', function (CellWriter $cell) {
                $cell->setBackground('#EFF3F8');
            });
            $sheet->cell('A5:E5', function (CellWriter $cell) {
                $cell->setBackground('#D7B966');
            });

            $sheet->cell('A8:E8', function (CellWriter $cell) {
                $cell->setBackground('#337AB7');
                $cell->setFontColor('#FFFFFF');
                $cell->setAlignment('center');
            });

            $i = 1;
            foreach ($this->recapKemenpera($this->regionProfile) as $item) {
                $sheet->rows([
                    [$i, $item->program, $item->volume, $item->unit, $item->budget]
                ]);

                $i++;
            }

            $sheet->setBorder('A8:E' . ($i + 7), 'thin');


            $sheet->rows([
                ['']
            ]);

            $j = 0;
            $offset2prefix = 10;
            if (count($this->recapPreviousYears($this->regionProfile)) > 0) {
                $sheet->rows([
                    ['Pembangunan Kemenpupera 2015-' . ($this->regionProfile->year - 1)],
                    ['No', 'Program', 'Kegiatan', 'Unit', 'Anggaran']
                ]);
                $j = 1;
                foreach ($this->recapPreviousYears($this->regionProfile) as $unitId => $programs) {
                    foreach ($programs as $programId => $value) {
                        $sheet->rows([
                            [$j, $value['unit_name'], $value['program_name'], $value['sum_unit'], $value['sum_budget']]
                        ]);
                    }

                    $j++;
                }

                $sheet->rows([
                    ['']
                ]);

                $offset = 10 + $i;

                $sheet->cell("A{$offset}:E{$offset}", function (CellWriter $cell) {
                    $cell->setBackground('#337AB7');
                    $cell->setFontColor('#FFFFFF');
                    $cell->setAlignment('center');
                });

                $sheet->setBorder("A{$offset}:E" . ($offset + $j - 1), 'thin');
                $offset2prefix = 12;

            }

            $sheet->rows([
                ['Pembangunan Kemenpupera ' . $this->regionProfile->year],
                ['No', 'Unit', 'Kegiatan', 'Unit', 'Anggaran']
            ]);

            $k = 1;

            foreach ($this->recapCurrentYear($this->regionProfile) as $unitId => $programs) {
                foreach ($programs as $programId => $value) {
                    $sheet->rows([
                        [$k, $value['unit_name'], $value['program_name'], $value['sum_unit'], $value['sum_budget']]
                    ]);
                }

                $k++;
            }

            $offset2 = $offset2prefix + $i + $j;

            $sheet->cell("A{$offset2}:E{$offset2}", function (CellWriter $cell) {
                $cell->setBackground('#337AB7');
                $cell->setFontColor('#FFFFFF');
                $cell->setAlignment('center');
            });

            $sheet->setBorder("A{$offset2}:E" . ($offset2 + $k - 1), 'thin');


            // Styling
            $sheet->setWidth('A', 4);
            $sheet->setWidth('B', 20);
            $sheet->setWidth('C', 20);
            $sheet->setWidth('D', 20);
            $sheet->setWidth('E', 30);
//            $sheet->setWidth('E', 20);
        });
    }

    /**
     * @return $this
     */
    public function show()
    {
        return view('region.profile.show.index')
            ->with([
                'regionProfile'   => $this->regionProfile,
                'regencyProfiles' => $this->getRegencyProfiles(),
                'units'           => Unit::whereNotIn('slug', ['dirjen'])->get()
            ]);
    }

    /**
     * @param RegionProfile $regencyProfile
     * @param Unit $unit
     * @return array
     */
    public function getProgramsWithRecipients(RegionProfile $regencyProfile, Unit $unit)
    {

        $results = Eprofile::with(['program', 'recipient'])
            ->where('unit_id', $unit->id)
            ->where('location_id', $regencyProfile->region_id)
            ->where('proposed_year', $regencyProfile->year)
            ->get();


        $uniques = [];
        foreach ($results as $result) {


            if (!array_key_exists($result->program->id, $uniques)) {

                $query = Eprofile::with(['program', 'recipient'])
                    ->where('unit_id', $unit->id)
                    ->where('location_id', $regencyProfile->region_id)
                    ->where('proposed_year', $regencyProfile->year)
                    ->where('program_id', $result->program->id)
                    ->get();

                $sumUnit = $query->sum('amount_unit');
                $sumBudget = $query->sum('total_budget');

                $uniques[$result->program->id] = [
                    'program'    => $result->program,
                    'recipient'  => $result->recipient,
                    'sum_unit'   => $sumUnit,
                    'sum_budget' => $sumBudget,
                    'eprofiles'  => []
                ];
            }

            if ($uniques[$result->program->id]['eprofiles'] !== null) {

                array_push($uniques[$result->program->id]['eprofiles'], $result);
            }
        }

        return $uniques;
    }

    /**
     * @param RegionProfile $regencyProfile
     * @return bool
     */
    public function dataByRegencyExist(RegionProfile $regencyProfile)
    {
        $cnt = Eprofile::where('location_id', $regencyProfile->region_id)
            ->where('proposed_year', $regencyProfile->year)
            ->count();

        if ($cnt > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param RegionProfile $regencyProfile
     * @param Unit $unit
     * @return mixed
     */
    public function sumUnitInDirectorate(RegionProfile $regencyProfile, Unit $unit)
    {

        return Eprofile::where('location_id', $regencyProfile->region_id)
            ->where('unit_id', $unit->id)
            ->where('proposed_year', $regencyProfile->year)
            ->sum('amount_unit');
    }

    /**
     * @param RegionProfile $regencyProfile
     * @param Unit $unit
     * @return mixed
     */
    public function sumBudgetInDirectorate(RegionProfile $regencyProfile, Unit $unit)
    {
        return Eprofile::where('location_id', $regencyProfile->region_id)
            ->where('unit_id', $unit->id)
            ->where('proposed_year', $regencyProfile->year)
            ->sum('total_budget');
    }

    public function recapKemenpera($regionProfile)
    {
        return RegionSpecial::where('region_id', $regionProfile->region_id)->get();
    }

    /**
     * @param $regionProfile
     * @return array
     */
    public function recapPreviousYears($regionProfile)
    {
        return $this->recapitulationYear($regionProfile, 2015, $regionProfile->year - 1);

    }

    /**
     * @param $regionProfile
     * @return array
     */
    public function recapCurrentYear($regionProfile)
    {
        return $this->recapitulationYear($regionProfile, $regionProfile->year);
    }


    /**
     * @param $regionProfile
     * @param  $yearBegin
     * @param  $yearEnd
     * @return array
     */
    protected function recapitulationYear($regionProfile, $yearBegin, $yearEnd = null): array
    {
        $regions = \App\Models\Region::descendantsAndSelf($regionProfile->region_id)->pluck('id');

        $builder = Eprofile::with(['program', 'unit'])
            ->whereIn('location_id', $regions)
            ->orderBy('unit_id');

        if (is_null($yearEnd)) {
            $builder->where('proposed_year', $yearBegin);
        } else {
            $builder->whereBetween('proposed_year', [$yearBegin, $yearEnd]);

        }

        $eprofiles = $builder->get();

        $data = [];
        $sum_unit = [];
        $sum_budget = [];
        foreach ($eprofiles as $eprofile) {

            if (!isset($sum_unit[$eprofile->unit_id])) {
                $sum_unit[$eprofile->unit_id] = [];
                if (!isset($sum_unit[$eprofile->unit_id][$eprofile->program_id])) {
                    $sum_unit[$eprofile->unit_id][$eprofile->program_id] = 0;
                }
            }

            if(isset($sum_unit[$eprofile->unit_id][$eprofile->program_id])) {


                $sum_unit[$eprofile->unit_id][$eprofile->program_id] += $eprofile->amount_unit;

                if (!isset($sum_budget[$eprofile->unit_id])) {
                    $sum_budget[$eprofile->unit_id] = [];
                    if (!isset($sum_budget[$eprofile->unit_id][$eprofile->program_id])) {
                        $sum_budget[$eprofile->unit_id][$eprofile->program_id] = 0;
                    }
                }

                $sum_budget[$eprofile->unit_id][$eprofile->program_id] += $eprofile->total_budget;

                if (!isset($data[$eprofile->unit_id])) {
                    $data[$eprofile->unit_id] = [];
                    if (!isset($data[$eprofile->unit_id][$eprofile->program_id])) {
                        $data[$eprofile->unit_id][$eprofile->program_id] = [
                            'sum_unit'   => $sum_unit[$eprofile->unit_id][$eprofile->program_id],
                            'sum_budget' => $sum_unit[$eprofile->unit_id][$eprofile->program_id]
                        ];
                    }
                }

                $data[$eprofile->unit_id][$eprofile->program_id] = [
                    'unit_name'    => $eprofile->unit->name,
                    'program_name' => $eprofile->program->name,
                    'sum_unit'     => $sum_unit[$eprofile->unit_id][$eprofile->program_id],
                    'sum_budget'   => $sum_budget[$eprofile->unit_id][$eprofile->program_id]
                ];
            }
        }


        return $data;
    }


}
