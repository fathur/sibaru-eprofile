<?php


namespace App\Repositories;

use App\Models\Eprofile;
use App\Models\Unit;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;


class EprofileExport
{
    protected $type;

    protected $region;

    protected $yearBegin;

    protected $yearEnd;

    public function rincian()
    {
        $this->type = 'rincian';

        return $this;
    }

    public function rekap()
    {
        $this->type = 'rekap';

        return $this;
    }

    public function region($region)
    {
        $this->region = $region;

        return $this;
    }

    public function year(int $begin, $end = null)
    {
        $this->yearBegin = $begin;

        $this->yearEnd = $end;

        return $this;
    }

    public function excel()
    {
        if ($this->type == 'rincian') {
            return $this->generateRincian();
        } elseif ($this->type == 'rekap') {
            if (is_null($this->yearEnd)) {
                return $this->generateRekapSetahun();
            } else {
                return $this->generateRekapRentangTahun();
            }
        }
    }

    protected function generateRincian()
    {
        $region = \App\Models\Region::find($this->region);

        $title = "Rincian kegiatan Ditjen Penyediaan Perumahan " . $region->name;

        return \Excel::create($title, function (LaravelExcelWriter $excel) use ($title, $region) {
            $excel->sheet('Rincian', function (LaravelExcelWorksheet $sheet) use ($title, $region) {
                $sheet->rows([
                    [''],
                    ['', strtoupper("Rincian kegiatan Ditjen Penyediaan Perumahan")],
                    ['', strtoupper($region->name)],
                    ['']
                ]);

                $sheet->mergeCells("B2:G2");
                $sheet->mergeCells("B3:G3");
                $sheet->cell("B2:G3", function (CellWriter $cell) {
                    $cell->setFontWeight();
                    $cell->setAlignment('center');
                    $cell->setFontSize(20);
                });


                $eprofiles = Eprofile::with('unit', 'recipient')
                    ->where('location_id', $this->region)
                    ->whereBetween('proposed_year', [$this->yearBegin, $this->yearEnd])
                    ->orderBy('proposed_year')
                    ->orderBy('unit_id')->get();

                $no = 1;
                $offset = 3;
                foreach ($eprofiles as $eprofile) {

                    if (!is_null($eprofile->latitude) and !is_null($eprofile->longitude)) {
                        $coordinate = $eprofile->latitude . "," . $eprofile->longitude;
                    } else {
                        $coordinate = '-';
                    }

                    $sheet->rows([
                        ['', "TA {$eprofile->proposed_year}"],
                        ['', $eprofile->unit->name],
                        ['', 'NO', 'LOKASI', 'KOORDINAT', 'PERUNTUKAN', 'TIPE', 'ANGGARAN'],
                        ['', $no,
                            $eprofile->proposer_remark,
                            $coordinate,
                            $eprofile->recipient->name, '',
                            $eprofile->total_budget
                        ],
                        ['', 'VISUAL'],
                        ['', 'x', 'x'],
                        ['']
                    ]);

                    $sheet->mergeCells("B" . (($no * 7) + $offset - 5) . ":G" . (($no * 7) + $offset - 5));
                    $sheet->cell("B" . (($no * 7) + $offset - 5) . ":G" . (($no * 7) + $offset - 5), function (CellWriter $cell) {
                        $cell->setFontWeight();
                        $cell->setAlignment('right');
                    });

                    $sheet->mergeCells("B" . (($no * 7) + $offset - 4) . ":G" . (($no * 7) + $offset - 4));
                    $sheet->cell("B" . (($no * 7) + $offset - 4) . ":G" . (($no * 7) + $offset - 4), function (CellWriter $cell) {
                        $cell->setBackground("#EFF0F1");
                        $cell->setAlignment('center');
                        $cell->setFontWeight();
                    });

                    $sheet->cell("B" . (($no * 7) + $offset - 3) . ":G" . (($no * 7) + $offset - 3), function (CellWriter $cell) {
                        $cell->setBackground("#27ae60");
                        $cell->setFontColor('#FFFFFF');
                    });

                    $sheet->mergeCells("B" . (($no * 7) + $offset - 1) . ":G" . (($no * 7) + $offset - 1));
                    $sheet->cell("B" . (($no * 7) + $offset - 1) . ":G" . (($no * 7) + $offset - 1), function (CellWriter $cell) {
                        $cell->setBackground("#27ae60");
                        $cell->setFontColor('#FFFFFF');
                        $cell->setAlignment('center');
                    });


                    $sheet->setBorder('B' . (($no * 7) + $offset - 4) . ':G' . (($no * 7) + $offset));

                    $no++;
                }

            });
        })->download('xls');

    }

    protected function generateRekapRentangTahun()
    {
        $region = \App\Models\Region::find($this->region);

        $title = "Rekapitulasi Kegiatan Ditjen Perumahan " . $region->name;

        // Begin here
        $dataA = [];
        for ($year = $this->yearBegin; $year <= $this->yearEnd; $year++) {
            $dataA[$year] = Eprofile::selectRaw("eprofiles.unit_id, sum(eprofiles.amount_unit) as sum_unit, sum(eprofiles.total_budget) as sum_budget")
                ->where('proposed_year', $year)
                ->groupBy('unit_id')
                ->get()->toArray();

        }

        $dataB = [];
        $years = [];
        foreach ($dataA as $year => $data) {

            array_push($years, $year);

            foreach ($data as $item) {
                if (array_key_exists((int)$item['unit_id'], $dataB)) {
                    array_push($dataB[(int)$item['unit_id']], [
                        'year' => $year,
                        'sum_unit' => $item['sum_unit'],
                        'sum_budget' => $item['sum_budget']
                    ]);
                } else {
                    $dataB[(int)$item['unit_id']] = [];
                    array_push($dataB[(int)$item['unit_id']], [
                        'year' => $year,
                        'sum_unit' => $item['sum_unit'],
                        'sum_budget' => $item['sum_budget']
                    ]);
                }
            }
        }

        // Lets create the excel

        return \Excel::create($title, function (LaravelExcelWriter $excel) use ($title, $region, $dataB, $years) {
            $excel->sheet('Rekap', function (LaravelExcelWorksheet $sheet) use ($title, $region, $dataB, $years) {
                $sheet->rows([
                    [''],
                    ['', strtoupper("Rekapitulasi Kegiatan Ditjen Perumahan")],
                    ['', strtoupper("{$region->name} Tahun {$this->yearBegin}-{$this->yearEnd}")],
                    ['']
                ]);

                $sheet->mergeCells("B2:G2");
                $sheet->mergeCells("B3:G3");
                $sheet->cell("B2:G3", function (CellWriter $cell) {
                    $cell->setFontWeight();
                    $cell->setAlignment('center');
                    $cell->setFontSize(20);
                });


                $titleFirst = ['No', 'Program'];
//                $titleAndYears = array_merge($titleFirst, $yearsTitle);
                foreach ($years as $year) {
                    array_push($titleFirst, $year);
                    array_push($titleFirst, '');
                }

                array_push($titleFirst, 'Total');
                array_push($titleFirst, '');


                $titleSecond = ['', ''];
                foreach ($years as $year) {
                    array_push($titleSecond, 'Unit');
                    array_push($titleSecond, 'Anggaran');
                }

                array_push($titleSecond, 'Unit');
                array_push($titleSecond, 'Anggaran');


                $sheet->rows([
                    $titleFirst,
                    $titleSecond
                ]);

                // real data
                $num = 1;
                foreach ($dataB as $unitId => $data) {

                    $rowData = [$num, Unit::find($unitId)->name];
                    $totalUnit = 0;
                    $totalBudget = 0;
                    foreach ($data as $datum) {

//                        dd($datum);

                        array_push($rowData, $datum['sum_unit']);
                        array_push($rowData, $datum['sum_budget']);

                        $totalUnit += (int)$datum['sum_unit'];
                        $totalBudget += (int)$datum['sum_budget'];
                    }

                    array_push($rowData, $totalUnit);
                    array_push($rowData, $totalBudget);

                    $sheet->rows([$rowData]);

                    $num++;
                }

            });
        })->download('xls');

    }

    protected function generateRekapSetahun()
    {
        $mainRegion = \App\Models\Region::find($this->region);

        $childRegions = \App\Models\Region::where('parent_id', $this->region)->pluck('id');


        $eprofiles = Eprofile::whereIn('location_id', $childRegions)
            ->where('proposed_year', $this->yearBegin)->get();


        $dataA = [];
        foreach ($eprofiles as $eprofile) {

            if (array_key_exists($eprofile->location_id, $dataA)) {

                if (array_key_exists($eprofile->unit_id, $dataA[$eprofile->location_id])) {
                    array_push($dataA[$eprofile->location_id][$eprofile->unit_id]['unit'], $eprofile->amount_unit);
                    array_push($dataA[$eprofile->location_id][$eprofile->unit_id]['budget'], $eprofile->total_budget);
                } else {
                    $dataA[$eprofile->location_id][$eprofile->unit_id]['unit'] = [$eprofile->amount_unit];
                    $dataA[$eprofile->location_id][$eprofile->unit_id]['budget'] = [$eprofile->total_budget];
                }


            } else {
                $dataA[$eprofile->location_id][$eprofile->unit_id]['unit'] = [$eprofile->amount_unit];
                $dataA[$eprofile->location_id][$eprofile->unit_id]['budget'] = [$eprofile->total_budget];
            }

        }

//        dd($dataA);


        $dataB = [];
        $units = Unit::where('jenis', 'teknis')->orderBy('id')->get();

        foreach ($dataA as $regionId => $dataUnits) {

            foreach ($units as $unit) {
                $dataB[$regionId][$unit->id]['unit'] = 0;
                $dataB[$regionId][$unit->id]['budget'] = 0;

                foreach ($dataUnits as $unitId => $data) {

                    if($unitId == $unit->id) {

                        $dataB[$regionId][$unitId]['unit'] = array_sum($data['unit']);
                        $dataB[$regionId][$unitId]['budget'] = array_sum($data['budget']);
                    }

                }
            }


        }

//        dd($dataB);

        $title = "Rekapitulasi Kegiatan Ditjen Penyediaan Perumahan di Provinsi {$mainRegion->name} Tahun {$this->yearBegin}";

        return \Excel::create($title, function (LaravelExcelWriter $excel) use ($units, $title, $dataB) {
            $excel->sheet('Rekap', function (LaravelExcelWorksheet $sheet) use ($units, $title, $dataB) {
                $sheet->rows([
                    [''],
                    ['', strtoupper($title)],
                    [''],
                    ['']
                ]);

                $sheet->mergeCells("B2:G2");
                $sheet->mergeCells("B3:G3");
                $sheet->cell("B2:G3", function (CellWriter $cell) {
                    $cell->setFontWeight();
                    $cell->setAlignment('center');
                    $cell->setFontSize(20);
                });

                $titleFirst = ['No', 'Kabupaten/Kota'];


                foreach ($units as $unit) {
                    array_push($titleFirst, $unit->name);
                    array_push($titleFirst, '');
                }


                array_push($titleFirst, 'Total');
                array_push($titleFirst, '');

                $titleSecond = ['', ''];
                foreach ($units as $unit) {
                    array_push($titleSecond, 'Unit');
                    array_push($titleSecond, 'Anggaran');
                }

                array_push($titleSecond, 'Unit');
                array_push($titleSecond, 'Anggaran');

                $sheet->rows([
                    $titleFirst,
                    $titleSecond
                ]);

                $num = 1;
                foreach ($dataB as $regionId => $unitData) {
                    $rowData = [$num, \Cache::get("region-{$regionId}")];

                    foreach ($unitData as $unitId => $data) {
                        foreach ($units as $unit) {


                            if ((int)$unit->id === (int)$unitId) {

                                if((int)$data['unit'] === 0) {

                                    array_push($rowData, '-');
                                    array_push($rowData, '-');
                                } else {

                                array_push($rowData, $data['unit']);
                                array_push($rowData, $data['budget']);
                                }
                            }
                        }
                    }

                    $sheet->rows([$rowData]);

                    $num++;
                }

            });
        })->download('xls');
    }
}