<?php

namespace App\Repositories;


use App\Models\Status;
use Carbon\Carbon;

class Statistic
{
    private $results = [];

    protected $year;

    protected $statusesIn = [];

    protected $statusesNotIn = [];

    protected $cacheTime;

    protected $forceFlush = false;

    public function __construct()
    {
        $this->cacheTime = Carbon::now()->addDay();
    }

    public function forceFlush($force = false)
    {
        $this->forceFlush = $force;

        return $this;
    }

    /**
     * @param $unit
     * @param null $region
     * @param null $status
     * @return array
     */
    public function regionProposalTotal($unit, $region = null, $status = null): array
    {
        if (is_null($region)) {
            // JIka null maka anggap adalah parentnya provinsi
            \App\Models\Region::whereLevel(1)
                ->orderBy('name')
                ->chunk(5, function ($regions) use ($unit, $region, $status) {
                    if (is_null($status) || $status == 0) {
                        $statusKey = 'all';
                    } else {
                        $statusKey = $status;
                    }

                    $statusesIn = json_encode($this->statusesIn);
                    $statusesNotIn = json_encode($this->statusesNotIn);

                    foreach ($regions as $region) {

                        $count = \Cache::tags(['count', 'chart', 'region', 'proposal'])
                            ->remember("chart-proposal-unit:{$unit->id}-region:{$region->id}-year:{$this->year}-status:{$statusKey}|in:{$statusesIn}|not_in:{$statusesNotIn}", $this->cacheTime, function () use ($unit, $region, $status) {

                                $location = \App\Models\Region::descendantsAndSelf($region->id)->pluck('id');
                                $count = \App\Models\Proposal::whereUnitId($unit->id)
                                    ->whereIn('location_id', $location);

                                if (count($this->statusesIn) > 0) {

                                    $statuses = Status::whereIn('slug', $this->statusesIn)->pluck('id');

                                    $count->whereIn('status_id', $statuses);
                                }


                                if (count($this->statusesNotIn) > 0) {

                                    $statuses = Status::whereIn('slug', $this->statusesNotIn)->pluck('id');

                                    $count->whereNotIn('status_id', $statuses);
                                }

                                if (!is_null($status) and $status != 0) {
                                    $count->where('status_id', '=', $status);
                                }

                                if (!is_null($this->year) || (int)$this->year !== 0) {
                                    $count->whereRaw(\DB::raw('extract(year from submission_date) = ?'), [ $this->year ]);
                                }

                                $count = $count->count();

                                return $count;

                            });

                        array_push($this->results, (int)$count);
                    }
                });


        } else {

        }

        return $this->results;
    }

    /**
     * @param $unit
     * @param null $region
     * @param null $status
     * @return array
     */
    public function regionEprofileTotal($unit, $region = null, $status = null): array
    {
        if (is_null($region)) {
            // JIka null maka anggap adalah parentnya provinsi
            \App\Models\Region::whereLevel(1)
                ->orderBy('order', 'asc')
                ->chunk(5, function ($regions) use ($unit, $region, $status) {
                    if (is_null($status) || $status == 0) {
                        $statusKey = 'all';
                    } else {
                        $statusKey = $status;
                    }

                    foreach ($regions as $region) {

                        if($this->forceFlush) {

                            $location = \App\Models\Region::descendantsAndSelf($region->id)->pluck('id');
                            $count = \App\Models\Eprofile::whereUnitId($unit->id)
                                ->whereIn('location_id', $location);

                            if (!is_null($status) and $status != 0) {
                                $count->where('status_id', '=', $status);
                            }

                            if (!is_null($this->year) || (int)$this->year !== 0) {
                                $count->where('proposed_year', '=', $this->year);
                            }

                            $count = $count->count();

                            if(\Cache::tags(['count', 'chart', 'region', 'eprofile', $this->year])
                                ->has("chart-eprofile-unit:{$unit->id}-region:{$region->id}-status:{$statusKey}-year:{$this->year}")) {
                                \Cache::tags(['count', 'chart', 'region', 'eprofile', $this->year])
                                    ->forget("chart-eprofile-unit:{$unit->id}-region:{$region->id}-status:{$statusKey}-year:{$this->year}");
                            }

                            \Cache::tags(['count', 'chart', 'region', 'eprofile', $this->year])
                                ->put("chart-eprofile-unit:{$unit->id}-region:{$region->id}-status:{$statusKey}-year:{$this->year}", $count, $this->cacheTime);
                        } else {

                            $count = \Cache::tags(['count', 'chart', 'region', 'eprofile', $this->year])
                                ->remember("chart-eprofile-unit:{$unit->id}-region:{$region->id}-status:{$statusKey}-year:{$this->year}", $this->cacheTime, function () use ($unit, $region, $status) {

                                    $location = \App\Models\Region::descendantsAndSelf($region->id)->pluck('id');
                                    $count = \App\Models\Eprofile::whereUnitId($unit->id)
                                        ->whereIn('location_id', $location);

                                    if (!is_null($status) and $status != 0) {
                                        $count->where('status_id', '=', $status);
                                    }

                                    if (!is_null($this->year) || (int)$this->year !== 0) {
                                        $count->where('proposed_year', '=', $this->year);
                                    }

                                    $count = $count->count();

                                    return $count;

                                });
                        }

                        array_push($this->results, (int)$count);
                    }
                });


        } else {}

        return $this->results;
    }

    /**
     * @param $unit
     * @param null $region
     * @param null|integer $status
     * @return array
     */
    public function calculateTotal($unit, $region = null, $status = null): array
    {
        if (is_null($region)) {
            // JIka null maka anggap adalah parentnya provinsi
            $regions = \App\Models\Region::whereLevel(1)
                ->orderBy('name')
                ->chunk(5, function ($regions) use ($unit, $region, $status) {
                    if (is_null($status) || $status == 0) {
                        $statusKey = 'all';
                    } else {
                        $statusKey = $status;
                    }

                    foreach ($regions as $region) {

//                        $count = \Cache::tags(['count', 'chart', 'region'])
//                            ->remember("chart-unit:{$unit->id}-region:{$region->id}-status:{$statusKey}", $this->cacheTime, function () use ($unit, $region, $status) {

                                $location = \App\Models\Region::descendantsAndSelf($region->id)->pluck('id');
                                $count = \App\Models\Proposal::whereUnitId($unit->id)
                                    ->whereIn('location_id', $location);

                                if (!is_null($status) and $status != 0) {
                                    $count->where('status_id', '=', $status);
                                }

                                $count = $count->count();

//                                return $count;

//                            });

                        array_push($this->results, (int)$count);
                    }
                });


        } else {}

        return $this->results;
    }

    /**
     * @param $unit
     * @param $region
     * @param null|integer $status
     */
    public function removeTotal($unit, $region, $status = null)
    {
        if (is_null($status)) {
            $status = 'all';
        }

        \Cache::tags(['count', 'chart', 'region'])
            ->get("chart-unit:{$unit->id}-region:{$region->id}-status:{$status}")
            ->flush();
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    public function whenStatusesIn(array $statuses)
    {
        $this->statusesIn = $statuses;

        return $this;
    }

    public function whenStatusesNotIn(array $statuses)
    {
        $this->statusesNotIn = $statuses;

        return $this;
    }
}