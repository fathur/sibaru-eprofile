<?php

namespace App\Repositories;

use \App\Models\Region as RegionModel;

class Region
{
    public static function constructName($region)
    {
        $name = $region->name;

        if ($region->parent_id !== null) {

            $parentRegion = RegionModel::find($region->parent_id);

            $parentName = self::constructName($parentRegion);

            $name = "$name, $parentName";

        }

        return $name;
    }

    public static function namedLevel($level)
    {
        switch ($level) {
            case 1:
                return 'Provinsi';
                break;
            case 2:
                return 'Kota/Kabupaten';
                break;
            case 3:
                return 'Kecamatan';
                break;
            case 4:
                return 'Desa/Kelurahan';
                break;
        }
    }

    public static function hasChild($region, $level = 1)
    {
        if(is_array($region)) {
            $regionId = $region['id'];
        } else {
            $regionId = $region->id;
        }

        $count = RegionModel::whereParentId($regionId)
            ->where('level', '<=', $level)
            ->count();

        if ($count > 0) {
            return true;
        }

        return false;
    }
}