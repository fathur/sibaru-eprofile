<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DwellingTypes
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingTypes whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingTypes whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingTypes whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingTypes whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DwellingTypes extends Model
{
}
