<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Agency
 *
 * @mixin \Eloquent
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @property int $id
 * @property int $region_id
 * @property string $name
 * @property string $phone
 * @property string $fax
 * @property string $address
 * @property string $logo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereUpdatedAt($value)
 * @property-read \App\Models\Region $region
 * @property string $type
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agency whereType($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property int|null $parent_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agency whereParentId($value)
 */
class Agency extends Model
{
    protected $fillable = ['region_id', 'parent_id', 'name', 'phone', 'fax', 'address', 'logo', 'type'];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
