<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Recipient
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property int $unit_id
 * @property string $abbreviation
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereAbbreviation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Recipient whereUnitId($value)
 * @property-read \App\Models\Unit $unit
 */
class Recipient extends Model
{
    protected $fillable = ['unit_id', 'name', 'abbreviation'];

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
