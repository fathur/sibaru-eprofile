<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Decree
 *
 * @property int $id
 * @property int $shortlist_id
 * @property int $proposed_year
 * @property string|null $attachment
 * @property string|null $filename
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DecreeBudget[] $budgets
 * @property-read \App\Models\Shortlist $shortlist
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereProposedYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereShortlistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Decree extends Model
{
    protected $fillable = ['proposed_year', 'attachment', 'filename'];

    public function budgets()
    {
        return $this->hasMany(DecreeBudget::class);
    }

    public function shortlist()
    {
        return $this->belongsTo(Shortlist::class);
    }

//    public function eprofile()
//    {
//        return $this->hasOne(Eprofile::class);
//    }
}