<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Program
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property int $unit_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereUnitId($value)
 * @property-read \App\Models\Unit $unit
 */
class Program extends Model
{
    protected $fillable = ['unit_id', 'name'];

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
