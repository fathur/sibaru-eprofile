<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EprofileBudget
 *
 * @property int $id
 * @property int $eprofile_id
 * @property int $amount
 * @property int $budget
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Eprofile $eprofile
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereEprofileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileBudget whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EprofileBudget extends Model
{
    public function eprofile()
    {
        return $this->belongsTo(Eprofile::class);
    }

//    public function decreeBudget()
//    {
//        return $this->belongsTo(DecreeBudget::class);
//    }

}