<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UnitTypes
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitTypes whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitTypes whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitTypes whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitTypes whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $recipient_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitTypes whereRecipientId($value)
 */
class UnitTypes extends Model
{
    //
}
