<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Eprofile
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $proposal_id
 * @property int $unit_id
 * @property int $program_id
 * @property int|null $proposer_id
 * @property int $location_id
 * @property int $recipient_id
 * @property string|null $proposer_remark
 * @property string|null $address
 * @property int $amount_unit
 * @property int $total_budget
 * @property int|null $creator_id
 * @property int|null $status_id
 * @property string|null $proposer_phone
 * @property string|null $proposer_email
 * @property int|null $proposed_year
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string|null $submission_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EprofileAsset[] $assets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EprofileBudget[] $budgets
 * @property-read \App\Models\Program $program
 * @property-read \App\Models\Proposal|null $proposal
 * @property-read \App\Models\Recipient $recipient
 * @property-read \App\Models\Unit $unit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereAmountUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposedYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereProposerRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereSubmissionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereTotalBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Eprofile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Eprofile extends Model
{
    protected $fillable = [
        'proposal_id', 'unit_id', 'program_id', 'proposer_id', 'recipient_id',
        'proposer_remark', 'proposer_phone','proposer_email','proposed_year', 'location_id',
        'address', 'amount_unit', 'total_budget',
        'latitude', 'longitude', 'submission_date'
    ];

    public function budgets()
    {
        return $this->hasMany(EprofileBudget::class);
    }

//    public function decree()
//    {
//        return $this->belongsTo(Decree::class);
//
//    }

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function recipient()
    {
        return $this->belongsTo(Recipient::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function assets()
    {
        return $this->hasMany(EprofileAsset::class);
    }
}