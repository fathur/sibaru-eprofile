<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Proposal
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $unit_id
 * @property int $status_id
 * @property int $program_id
 * @property int $recipient_id
 * @property int $location_id
 * @property string $proposer_remark
 * @property string $proposer_phone
 * @property string $proposer_email
 * @property string $proposed_date
 * @property string $address
 * @property int $latitude
 * @property int $longitude
 * @property string $submission_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereLocationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProgramId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposerEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposerPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposerRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereRecipientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereSubmissionDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereUnitId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Criteria[] $criterias
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DwellingPlan[] $dwellingPlans
 * @property-read \App\Models\Program $program
 * @property-read \App\Models\Recipient $recipient
 * @property-read \App\Models\Status $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Surveyor[] $surveyors
 * @property-read \App\Models\Unit $unit
 * @property int $proposer_id
 * @property int $creator_id
 * @property int $proposed_year
 * @property-read \App\Models\Region $location
 * @property-read \App\Models\Agency $proposer
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereCreatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposedYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereProposerId($value)
 * @property int $area_wide
 * @property int $area_length
 * @property int $area_width
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereAreaLength($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereAreaWide($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proposal whereAreaWidth($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \App\Models\Proposal $shortlist
 * @property-read \App\Models\Eprofile $eprofile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tematic[] $tematics
 * @property int|null $potential_residents
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proposal wherePotentialResidents($value)
 */
class Proposal extends Model
{
    protected $fillable = [
        'unit_id',
        'proposer_id',
        'creator_id',
        'status_id',
        'program_id',
        'recipient_id',
        'location_id',
        'proposer_remark',
        'proposer_phone',
        'proposer_email',
        'proposed_date',
        'address',
        'latitude',
        'longitude',
        'submission_date',
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function recipient()
    {
        return $this->belongsTo(Recipient::class);
    }

    public function location()
    {
        return $this->belongsTo(Region::class, 'location_id');
    }

    public function proposer()
    {
        return $this->belongsTo(Agency::class, 'proposer_id');

    }

    public function criterias()
    {
        return $this->belongsToMany(Criteria::class, 'proposal_criteria', 'proposal_id', 'criteria_id')
            ->withPivot([
                'attachment', 'file_name', 'is_verified', 'is_published', 'point', 'description'
            ])->withTimestamps();
    }

    public function dwellingPlans()
    {
        return $this->hasMany(DwellingPlan::class);
    }

    public function surveyors()
    {
        return $this->belongsToMany(Surveyor::class, 'proposal_surveyor');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function shortlist()
    {
        return $this->hasOne(Shortlist::class);
    }

    public function tematics()
    {
        return $this->belongsToMany(Tematic::class)->withTimestamps();
    }

    public function eprofile()
    {
        return $this->hasOne(Eprofile::class);
    }
}
