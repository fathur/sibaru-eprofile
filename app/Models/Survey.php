<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Survey
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $proposal_id
 * @property string $date
 * @property string $attachment
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereAttachment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereProposalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereUpdatedAt($value)
 * @property string $file_name
 * @property-read \App\Models\Proposal $proposal
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereFileName($value)
 */
class Survey extends Model
{
    protected $fillable = ['proposal_id', 'date', 'content', 'attachment', 'file_name'];

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }
}
