<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Unit
 *
 * @mixin \Eloquent
 * @property int                                                                  $id
 * @property string                                                               $name
 * @property string                                                               $description
 * @property \Carbon\Carbon                                                       $created_at
 * @property \Carbon\Carbon                                                       $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Criteria[] $criterias
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property string                                                               $jenis
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereJenis($value)
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit whereSlug($value)
 * @property string|null $program
 * @property string|null $color
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereProgram($value)
 */
class Unit extends Model
{
    const RUSUN   = ['slug' => 'rusun', 'name' => 'Direktorat Rumah Susun', 'program' => 'Rusun'];
    const RUSUS   = ['slug' => 'rusus', 'name' => 'Direktorat Rumah Khusus', 'program' => 'Rusus'];
    const RUK     = ['slug' => 'swadaya', 'name' => 'Direktorat Rumah Swadaya', 'program' => 'PSU'];
    const SWADAYA = ['slug' => 'ruk', 'name' => 'Direktorat Rumah Umum dan Komersial', 'program' => 'BSPS'];

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function criterias()
    {
        return $this->hasMany(Criteria::class);
    }
}
