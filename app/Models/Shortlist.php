<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shortlist
 *
 * @property int $id
 * @property int $proposal_id
 * @property int $proposed_year
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Proposal $proposal
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shortlist whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shortlist whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shortlist whereProposalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shortlist whereProposedYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shortlist whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $score
 * @property string|null $code
 * @property int|null $sum_amount
 * @property int|null $sum_budget_plan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ShortlistBudget[] $budgets
 * @property-read \App\Models\Decree $decree
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shortlist whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shortlist whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shortlist whereSumAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shortlist whereSumBudgetPlan($value)
 */
class Shortlist extends Model
{
    protected $fillable = ['proposed_year','code','score'];

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function budgets()
    {
        return $this->hasMany(ShortlistBudget::class);
    }

    public function decree()
    {
        return $this->hasOne(Decree::class);
    }
}