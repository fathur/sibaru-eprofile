<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Models\Region
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereUpdatedAt($value)
 * @property-read \App\Models\Agency $agency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Region[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DwellingPlan[] $dwellingPlans
 * @property-read \App\Models\Region $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegionProfile[] $profiles
 * @property int $_lft
 * @property int $_rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region d()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereRgt($value)
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereOrder($value)
 */
class Region extends Model
{
    use NodeTrait;

    protected $primaryKey = 'id';

    protected $fillable = ['parent_id', 'name', 'level', 'order'];

    public function agency() {
        return $this->hasOne(Agency::class);
    }

    public function dwellingPlans()
    {
        return $this->hasMany(DwellingPlan::class);
    }

    public function profiles()
    {
        return $this->hasMany(RegionProfile::class);

    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
