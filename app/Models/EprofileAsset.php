<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 9/8/17
 * Time: 08:51
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EprofileAsset
 *
 * @property int $id
 * @property int $eprofile_id
 * @property string $url
 * @property string $filename
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Eprofile $eprofile
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereEprofileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EprofileAsset whereUrl($value)
 * @mixin \Eloquent
 */
class EprofileAsset extends Model
{
    protected $fillable = [
        'eprofile_id',
        'url', 'filename', 'type'
    ];

    public function eprofile()
    {
        return $this->belongsTo(Eprofile::class);
    }

}