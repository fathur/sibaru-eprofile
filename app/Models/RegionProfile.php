<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RegionProfile
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $region_id
 * @property int $year
 * @property string $head_name
 * @property string $deputy_name
 * @property string $head_photo
 * @property string $deputy_photo
 * @property string $head_party
 * @property string $deputy_party
 * @property int $period_start
 * @property int $period_end
 * @property string $perkim
 * @property string $perkim_pic_name
 * @property string $perkim_phone
 * @property string $perkim_fax
 * @property string $perkim_email
 * @property int $sea_large
 * @property int $land_large
 * @property int $latitude
 * @property int $longitude
 * @property int $total_man
 * @property int $total_woman
 * @property int $population_growth
 * @property int $population_density
 * @property int $total_city_poor_people
 * @property int $total_village_poor_people
 * @property int $total_district
 * @property int $total_city
 * @property int $apbd_amount
 * @property int $pad_pajak
 * @property int $pad_retribusi
 * @property int $pad_sda
 * @property int $pad_others
 * @property int $backlog_house
 * @property int $household
 * @property string $image
 * @property int $pupr_budget
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereApbdAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereBacklogHouse($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereDeputyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereDeputyParty($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereDeputyPhoto($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereHeadName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereHeadParty($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereHeadPhoto($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereHousehold($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereLandLarge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePadOthers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePadPajak($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePadRetribusi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePadSda($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePeriodEnd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePeriodStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePerkim($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePerkimEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePerkimFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePerkimPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePerkimPicName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePopulationDensity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePopulationGrowth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile wherePuprBudget($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereSeaLarge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalCityPoorPeople($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalDistrict($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalMan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalVillagePoorPeople($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereTotalWoman($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegionProfile whereYear($value)
 * @property-read \App\Models\Region $region
 * @property string|null $koordinat
 * @property string|null $masa_bakti
 * @property string|null $area_large
 * @property string|null $perkim_kelembagaan
 * @property float|null $rtlh
 * @property float|null $backlog_kepemilikan
 * @property string|null $total_people
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereAreaLarge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereBacklogKepemilikan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereKoordinat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereMasaBakti($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile wherePerkimKelembagaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereRtlh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionProfile whereTotalPeople($value)
 */
class RegionProfile extends Model
{
    protected $fillable = [
        'region_id',
        'year',
        'head_name',
        'deputy_name',
        'head_photo',
        'deputy_photo',
        'head_party',
        'deputy_party',
        'period_start',
        'period_end',
        'perkim',
        'perkim_pic_name',
        'perkim_phone',
        'perkim_fax',
        'perkim_email',
        'sea_large',
        'land_large',
        'latitude',
        'longitude',
        'total_man',
        'total_woman',
        'population_growth',
        'population_density',
        'total_city_poor_people',
        'total_village_poor_people',
        'total_district',
        'total_city',
        'apbd_amount',
        'pad_pajak',
        'pad_retribusi',
        'pad_sda',
        'pad_others',
        'backlog_house',
        'household',
        'image',
        'pupr_budget',
        'koordinat',
        'masa_bakti',
        'area_large',
        'perkim_kelembagaan',
        'rtlh',
        'backlog_kepemilikan',
        'total_people',
    ];

    public function region() {
        return $this->belongsTo(Region::class);
    }


}
