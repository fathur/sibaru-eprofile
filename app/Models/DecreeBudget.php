<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DecreeBudget
 *
 * @property int $id
 * @property int $decree_id
 * @property int $shortlist_budget_id
 * @property int $amount
 * @property int $budget
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Decree $decree
 * @property-read \App\Models\ShortlistBudget $shortlistBudget
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereShortlistBudgetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DecreeBudget whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DecreeBudget extends Model
{
    protected $fillable = [
        'decree_id',
        'shortlist_budget_id',
        'amount',
        'budget'
    ];

//    public function eprofileBudget()
//    {
//        return $this->hasOne(EprofileBudget::class);
//    }

    public function decree()
    {
        return $this->belongsTo(Decree::class);
    }

    public function shortlistBudget()
    {
        return $this->belongsTo(ShortlistBudget::class);
    }
}