<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DwellingComponent
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingComponent whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingComponent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingComponent whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingComponent whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DwellingPlan[] $plans
 */
class DwellingComponent extends Model
{
    public function plans()
    {
        return $this->hasMany(DwellingPlan::class);
    }
}
