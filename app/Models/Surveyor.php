<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Surveyor
 *
 * @mixin \Eloquent
 * @property int            $id
 * @property string         $nip
 * @property string         $name
 * @property string         $phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereNip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property int $proposal_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surveyor whereProposalId($value)
 * @property-read \App\Models\Proposal $proposal
 */
class Surveyor extends Model
{
    protected $fillable = ['proposal_id','name','nip','phone'];

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }
}
