<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Status
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereSlug($value)
 */
class Status extends Model
{
    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }
}
