<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tematic
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $unit_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tematic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tematic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tematic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tematic whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tematic whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tematic extends Model
{
    protected $fillable = ['name', 'unit_id'];

    public function proposals()
    {
        return $this->belongsToMany(Proposal::class);
    }
}
