<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @property string $username
 * @property string $phone
 * @property string $nip
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereNip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUsername($value)
 * @property int $role_id
 * @property int $agency_id
 * @property-read \App\Models\Agency $agency
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereAgencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRoleId($value)
 * @property-read \App\Models\Role $role
 * @property int $unit_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUnitId($value)
 * @property-read \App\Models\Unit|null $unit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User email($string)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User emailLike($string)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User username($string)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User usernameLike($string)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'agency_id', 'unit_id',
        'name', 'email', 'password', 'username', 'phone', 'nip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * ==============
     * RELATIONS AREA
     * ==============
     */
    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /*
     * ===========
     * SCOPE AREA
     * ===========
     */

    public function scopeUsername($query, $string)
    {
        $query->where('username', $string);
    }

    public function scopeUsernameLike($query, $string)
    {
        $query->whereRaw("username like '%{$string}%'");
    }

    public function scopeEmail($query, $string)
    {
        $query->where('email', $string);
    }

    public function scopeEmailLike($query, $string)
    {
        $query->whereRaw("email like '%{$string}%'");
    }
}
