<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UnitSize
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $type
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DwellingPlan[] $dwellingPlans
 * @property int $total_floor
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UnitSize whereTotalFloor($value)
 */
class UnitSize extends Model
{
    public function dwellingPlans()
    {
        return $this->hasMany(DwellingPlan::class);
    }
}
