<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ShortlistBudget
 *
 * @property int $id
 * @property int $dwelling_plan_id
 * @property int $amount
 * @property int $budget_plan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereBudgetPlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereDwellingPlanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShortlistBudget whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $shortlist_id
 * @property-read \App\Models\DecreeBudget $decreeBudget
 * @property-read \App\Models\DwellingPlan $dwellingPlan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShortlistBudget whereShortlistId($value)
 */
class ShortlistBudget extends Model
{
    protected $fillable = ['amount', 'budget_plan'];

    public function decreeBudget()
    {
        return $this->hasOne(DecreeBudget::class);
    }

    public function dwellingPlan()
    {
        return $this->belongsTo(DwellingPlan::class);
    }
}