<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DwellingPlan
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $proposal_id
 * @property int $location_id
 * @property int $component_id
 * @property int $dwelling_type_id
 * @property int $unit_size_id
 * @property int $amount
 * @property int $budget_plan
 * @property int $latitude
 * @property int $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereBudgetPlan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereComponentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereDwellingTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereLocationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereProposalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereUnitSizeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereUpdatedAt($value)
 * @property-read \App\Models\DwellingComponent $component
 * @property-read \App\Models\Proposal $proposal
 * @property-read \App\Models\Region $region
 * @property-read \App\Models\UnitSize $unitSize
 * @property int $unit_type_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DwellingPlan whereUnitTypeId($value)
 * @property-read \App\Models\UnitTypes $unitType
 * @property-read \App\Models\ShortlistBudget $shortlist
 * @property string|null $type
 * @property string|null $tower_block
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DwellingPlan whereTowerBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DwellingPlan whereType($value)
 */
class DwellingPlan extends Model
{
    protected $fillable = [
        'proposal_id', 'dwelling_type_id', 'unit_size_id', 'amount', 'budget_plan',
        'component_id', 'location_id', 'latitude', 'longitude', 'unit_type_id',
        'type', 'tower_block'
    ];

    public function component()
    {
        return $this->belongsTo(DwellingComponent::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'location_id');
    }

    public function unitSize()
    {
        return $this->belongsTo(UnitSize::class);
    }

    public function unitType()
    {
        return $this->belongsTo(UnitTypes::class);
    }

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function shortlist()
    {
        return $this->hasOne(ShortlistBudget::class);
    }
}
