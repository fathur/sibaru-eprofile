<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comment
 *
 * @property int                       $id
 * @property int                       $parent_id
 * @property int                       $proposal_id
 * @property int                       $user_id
 * @property string                    $text
 * @property string                    $status
 * @property \Carbon\Carbon            $created_at
 * @property \Carbon\Carbon            $updated_at
 * @property-read \App\Models\Proposal $proposal
 * @property-read \App\Models\User     $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereProposalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comment whereUserId($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{
    protected $fillable = ['user_id', 'proposal_id', 'text', 'status', 'parent_id'];

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}