<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Criteria
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $unit_id
 * @property int $order
 * @property string $type
 * @property int $weight
 * @property string $content
 * @property string $template
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereUnitId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria whereWeight($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proposal[] $proposals
 * @property-read \App\Models\Unit $unit
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria administration()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria others()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Criteria technical()
 * @property string|null $filename
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Criteria whereFilename($value)
 */
class Criteria extends Model
{
    protected $fillable = ['order','unit_id','type','content','weight','template', 'filename'];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function proposals()
    {
        return $this->belongsToMany(Proposal::class, 'proposal_criteria', 'criteria_id', 'proposal_id')
            ->withPivot([
                'attachment', 'is_verified', 'is_published', 'point', 'description'
            ]);
    }

    public function scopeAdministration($query)
    {
        return $query->where('type', '=', 'administration');
    }

    public function scopeTechnical($query)
    {
        return $query->where('type', '=', 'technical');
    }

    public function scopeOthers($query)
    {
        return $query->where('type', '=', 'others');
    }
}
