<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RegionSpecial
 *
 * @property int $id
 * @property int|null $unit_id
 * @property int $region_id
 * @property string $unit
 * @property float $volume
 * @property float $budget
 * @property string $program
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereProgram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionSpecial whereVolume($value)
 * @mixin \Eloquent
 */
class RegionSpecial extends Model
{
    protected $table = 'region_specials';

    protected $fillable = [
        'order',
        'unit_id',
        'region_id',
        'unit',
        'volume',
        'program',
        'budget'
    ];
}