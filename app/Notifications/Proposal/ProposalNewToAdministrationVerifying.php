<?php

namespace App\Notifications\Proposal;

use App\Models\Proposal;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProposalNewToAdministrationVerifying extends Notification
{
//    use Queueable;
    /**
     * @var Proposal
     */
    protected $proposal;

    /**
     * Create a new notification instance.
     *
     * @param Proposal $proposal
     */
    public function __construct(Proposal $proposal)
    {
        //
        $this->proposal = $proposal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {

//        dd(\Auth::user()->agency_id, $this->proposal->proposer_id);

        if ($notifiable->agency_id == $this->proposal->proposer_id) {
            // Untuk yang mengusulkan
            $message = 'Usulan Anda sedang dalam proses verifikasi administrasi.';

        } else {
            if (is_null($this->proposal->proposer)) {
                $message = "Admin pusat mendistribusikan usulan {$this->proposal->id}";
            } else {
                $message = "Admin pusat mendistribusikan usulan {$this->proposal->id} dari <strong>{$this->proposal->proposer['name']}</strong>.";
            }
        }

        return [
            'proposer' => $this->proposal->proposer,
            'proposal' => $this->proposal,
            'message' => $message
        ];
    }
}
