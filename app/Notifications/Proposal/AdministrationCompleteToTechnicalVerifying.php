<?php

namespace App\Notifications\Proposal;

use App\Models\Proposal;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdministrationCompleteToTechnicalVerifying extends Notification
{
//    use Queueable;

    /**
     * @var Proposal
     */
    private $proposal;

    /**
     * Create a new notification instance.
     *
     * @param Proposal $proposal
     */
    public function __construct(Proposal $proposal)
    {
        //
        $this->proposal = $proposal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        $message = "Usulan {$this->proposal->id} masuk verifikasi teknis.";

        return [
            'proposer' => $this->proposal->proposer,
            'proposal' => $this->proposal,
            'message' => $message
        ];
    }
}
