<?php

namespace App\Listeners;

use App\Events\EprofileCreated;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EprofileRukBarChart implements ShouldQueue
{
    use InteractsWithQueue;

    public $queue = 'bar-chart-ruk';

    protected $unit;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->unit = Unit::whereSlug(Unit::RUK['slug'])->first();

    }

    /**
     * Handle the event.
     *
     * @param  EprofileCreated $event
     * @return void
     */
    public function handle(EprofileCreated $event)
    {
//        \Cache::tags()->forget();

        $yearNow = Carbon::now()->year;
        $sixYearAgo = Carbon::now()->subYears(3)->year;

        for ($year = $sixYearAgo; $year <= $yearNow; $year++) {
            (new Statistic())->setYear($year)
                ->forceFlush(true)
                ->regionEprofileTotal($this->unit);
        }

//        \Cache::tags(['count', 'chart', 'region', 'eprofile', $event->year])
//            ->forget("chart-eprofile-unit:{$event->unitId}-region:{$event->locationId}-status:all-year:{$event->year}");
//
//        (new Statistic())->setYear($event->year)->regionEprofileTotal($this->unit);
    }
}
