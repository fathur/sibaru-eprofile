<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 10/1/17
 * Time: 17:25
 */

namespace App\Transformers\Recap;


use League\Fractal\TransformerAbstract;

class KemenperaTransformer extends TransformerAbstract
{
    public function transform($kemenpera)
    {
//        dd($kemenpera->toArray());
        return [
            'kegiatan' => $kemenpera->program,
            'volume'   => (double)$kemenpera->volume,
            'satuan'   => $kemenpera->unit,
        ];
    }
}