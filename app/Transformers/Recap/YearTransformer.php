<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 10/1/17
 * Time: 17:26
 */

namespace App\Transformers\Recap;


use League\Fractal\TransformerAbstract;

class YearTransformer extends TransformerAbstract
{
    public function transform($data)
    {
        //        dd($previous);
        return [
            'direktorat'  => $data['unit_name'],
            'kegiatan'    => $data['program_name'],
            'jumlah_unit' => $data['sum_unit'],
        ];
    }
}