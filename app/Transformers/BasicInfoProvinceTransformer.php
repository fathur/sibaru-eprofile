<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 10/1/17
 * Time: 15:30
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;

class BasicInfoProvinceTransformer extends TransformerAbstract
{
    public function transform($province)
    {
//        dd($province->toArray());
        return [
            'nama_gubernur'               => $province->head_name ?? "",
            'partai_gubernur'             => $province->head_party?? "",
            'nama_wakil_gubernur'         => $province->deputy_name?? "",
            'partai_wakil_gubernur'       => $province->deputy_party?? "",
            'masa_bakti'                  => $province->masa_bakti?? "",
            'kelembagaaan_perkim'         => $province->perkim ?? "",
            'kontak_person'               => $province->perkim_pic_name ?? "",
            'telepon'                     => $province->perkim_phone ?? "",
            'fax'                         => $province->perkim_fax ?? "",
            'email'                       => $province->perkim_email ?? "",
            'koordinat'                   => $province->koordinat ?? "",
            'luas_daratan'                => $province->land_large ? (double)$province->land_large : null,
            'luas_perairan'               => $province->sea_large ? (double)$province->sea_large : null,
            'pertumbuhan_penduduk'        => $province->population_growth ? (double)$province->population_growth : null,
            'kepadatan_penduduk'          => $province->population_density ? (double)$province->population_density : null,
//            'jumlah_penduduk'             => $province->,
            'jumlah_penduduk_miskin_kota' => $province->total_city_poor_people ? (int)$province->total_city_poor_people : null,
            'jumlah_penduduk_miskin_desa' => $province->total_village_poor_people ? (int)$province->total_village_poor_people : null,
        ];
    }
}