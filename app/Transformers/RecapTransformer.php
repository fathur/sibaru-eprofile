<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 10/1/17
 * Time: 17:24
 */

namespace App\Transformers;


use App\Transformers\Recap\YearTransformer;
use App\Transformers\Recap\KemenperaTransformer;
use League\Fractal\TransformerAbstract;

class RecapTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['kemenpera', 'previous', 'current'];

    public function transform($recapitulation)
    {


        return [];
    }

    public function includeKemenpera($recapitulation)
    {
        $kemenpera = $recapitulation->kemenpera;

        return $this->collection($kemenpera, new KemenperaTransformer());
    }


    public function includePrevious($recapitulation)
    {
        $previousYears = $recapitulation->previousYears;

        return $this->collection($previousYears, new YearTransformer());
    }


    public function includeCurrent($recapitulation)
    {
        $currentYears = $recapitulation->currentYear;

        return $this->collection($currentYears, new YearTransformer());
    }
}