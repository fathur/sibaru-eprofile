<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 10/1/17
 * Time: 17:53
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;

class BacklogTransformer extends TransformerAbstract
{
    public function transform($province)
    {
        return [
            'name'                => $province->name,
            'rtlh'                => $province->rtlh ? (int)$province->rtlh : null,
            'backlog_kepemilikan' => $province->backlog_kepemilikan ? (int)$province->backlog_kepemilikan : null,
            'backlog_penghunian'  => $province->backlog_house ? (int)$province->backlog_house : null,
        ];
    }
}