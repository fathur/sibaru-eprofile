<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EprofileCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $unitId;

    public $locationId;

    public $year;

    /**
     * Create a new event instance.
     *
     * @param null $unitId
     * @param null $locationId
     * @param null $year
     */
    public function __construct($unitId = null, $locationId = null, $year = null)
    {
        $this->unitId = $unitId;

        $this->locationId = $locationId;

        $this->year = $year;

//        $yearNow = Carbon::now()->year;
//        $sixYearAgo = Carbon::now()->subYears(6)->year;
//
//        for ($year = $sixYearAgo; $year <= $yearNow; $year++) {
//            \Cache::tags(['count', 'chart', 'region', 'eprofile', $year])->flush();
//        }
//
//        \Cache::tags(['count', 'chart', 'region'])->flush();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
