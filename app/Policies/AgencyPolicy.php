<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgencyPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    public function viewAgency(User $user)
    {
        return (
            $this->isSuperAdmin($user) ||
            $this->isAdmin($user)
        );
    }
}