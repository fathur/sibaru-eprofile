<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\User;
use App\Models\Criteria;
use Illuminate\Auth\Access\HandlesAuthorization;

class CriteriaPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the criteria.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Criteria $criteria
     *
     * @return mixed
     */
    public function view(User $user, Criteria $criteria)
    {
        //
    }

    /**
     * Determine whether the user can create criterias.
     *
     * @param  \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the criteria.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Criteria $criteria
     *
     * @return mixed
     */
    public function update(User $user, Criteria $criteria)
    {
        //
    }

    /**
     * Determine whether the user can delete the criteria.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Criteria $criteria
     *
     * @return mixed
     */
    public function delete(User $user, Criteria $criteria)
    {
        //
    }

//in_array($proposal->status->slug, [
//'proposal-draft', 'proposal-new', 'administration-verifying', 'administration-revising'
//])
    /**
     * @param User $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function viewCriteriaAdministration(User $user, Proposal $proposal)
    {

        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||

            ($this->isProposer($user) and $this->statuses($status, [
                    'proposal-draft', 'proposal-new', 'administration-verifying',
                    'administration-revising', 'technical-surveyor-verifying', 'technical-passed',
                    'administration-rejected', 'technical-rejected', 'technical-survey-verifying', 'administration-repaired'])) ||

            ($this->isAdmin($user) and $this->statuses($status, [
                    'proposal-draft', 'proposal-new', 'administration-verifying', 'administration-revising', 'technical-surveyor-verifying',
                    'administration-rejected', 'technical-rejected', 'technical-passed', 'technical-survey-verifying', 'administration-repaired'])) ||

            ($this->isTechnical($user) and $this->statuses($status, [
//                    'proposal-draft', // todo: make sure of this
                    'administration-verifying', 'administration-revising',
                    'technical-surveyor-verifying', 'technical-passed', 'administration-rejected', 'technical-rejected',
                    'administration-repaired', 'proposal-passed'])) ||

            ($this->isWatcher($user) and $this->statuses($status, [
                    'proposal-draft', 'proposal-new', 'administration-verifying',
                    'administration-revising', 'technical-surveyor-verifying', 'technical-passed',
                    'administration-rejected', 'technical-rejected', 'technical-survey-verifying', 'administration-repaired'
                ]));

    }

    /**
     * @param User $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function viewCriteriaTechnical(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||

            ($this->isProposer($user) and $this->statuses($status, [
                    'proposal-draft', 'proposal-new', 'administration-verifying', 'administration-revising',
                    'technical-surveyor-verifying', 'technical-passed', 'administration-rejected', 'technical-rejected',
                    'technical-survey-verifying', 'administration-repaired'])) ||

            ($this->isAdmin($user) and $this->statuses($status, [
                    'proposal-draft', 'proposal-new', 'administration-verifying', 'administration-revising', 'administration-rejected',
                    'technical-rejected', 'technical-passed', 'technical-surveyor-verifying', 'technical-survey-verifying', 'administration-repaired'])) ||

            ($this->isTechnical($user) and $this->statuses($status, [
                    'administration-revising', 'technical-surveyor-verifying', 'technical-survey-verifying',
                    'technical-passed', 'administration-rejected', 'technical-rejected', 'proposal-passed']));

    }

    public function storeCriteriaAdministration(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        if ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft'])) and ($proposal->creator_id == $user->id)) {
            return true;
        }

        return
            ($this->isSuperAdmin($user)) ||

            ($this->isAdmin($user) and $this->statuses($status, [
//                    'proposal-draft',
//                    'administration-revising'
                ])) ||

            ($this->isTechnical($user) and ($this->statuses($status, []))) ||

            ($user->role->slug == 'proposer' and in_array($proposal->status->slug, [
                    'proposal-draft', 'administration-revising'])) ||

            ($this->isWatcher($user) and $this->statuses($status, [
                    'proposal-draft', 'administration-revising'
                ]));
    }

    public function updateVerificationCriteriaAdministration(User $user, Proposal $proposal)
    {
        return
            ($this->isSuperAdmin($user)) ||

            ($user->role->slug == 'admin-technical' and in_array($proposal->status->slug, ['administration-verifying', 'administration-repaired']));
    }

    public function viewDescriptionCriteriaAdministration(User $user, Proposal $proposal)
    {
        return
            ($this->isSuperAdmin($user)) ||

            ($user->role->slug == 'proposer' and in_array($proposal->status->slug, ['administration-revising']));
    }

    public function viewVerificationStatusCriteriaAdministration(User $user, Proposal $proposal)
    {

    }

    public function storeCriteriaTechnical(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        if ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft'])) and ($proposal->creator_id == $user->id)) {
            return true;
        }
        
        return
            ($this->isSuperAdmin($user)) ||

            ($this->isAdmin($user) and $this->statuses($status, [
//                    'proposal-draft',
//                    'administration-revising'
                ])) ||

            ($user->role->slug == 'proposer' and in_array($proposal->status->slug, [
                    'proposal-draft', 'administration-revising']));
    }

    public function updateVerificationCriteriaTechnical(User $user, Proposal $proposal)
    {
        return
            ($this->isSuperAdmin($user)) ||

            ($user->role->slug == 'admin-technical' and in_array($proposal->status->slug, [
                    'administration-verifying', 'technical-survey-verifying', 'administration-repaired']));
    }

    public function viewDescriptionCriteriaTechnical(User $user, Proposal $proposal)
    {

        return
            ($this->isSuperAdmin($user)) ||

            ($user->role->slug == 'proposer' and in_array($proposal->status->slug, ['administration-revising']));
    }

    public function viewVerificationStatusCriteriaTechnical(User $user, Proposal $proposal)
    {

    }

    public function storeCriteriaOthers(User $user, Proposal $proposal)
    {

    }
}
