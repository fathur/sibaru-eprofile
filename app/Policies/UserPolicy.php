<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    public function viewUser(User $user)
    {
        return (
            $this->isSuperAdmin($user) ||
            $this->isAdmin($user)
        );
    }
}