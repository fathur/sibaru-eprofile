<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\User;
use App\Models\DwellingPlan;
use Illuminate\Auth\Access\HandlesAuthorization;

class DwellingPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the dwellingPlan.
     *
     * @param  \App\Models\DwellingPlan $dwellingPlan
     *
     * @return mixed
     */
    public function view(User $user, DwellingPlan $dwellingPlan)
    {
        //
    }

    /**
     * Determine whether the user can create dwellingPlans.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the dwellingPlan.
     *
     * @param  \App\User $user
     * @param  \App\Models\DwellingPlan $dwellingPlan
     * @return mixed
     */
    public function update(User $user, DwellingPlan $dwellingPlan)
    {
        //
    }

    /**
     * Determine whether the user can delete the dwellingPlan.
     *
     * @param  \App\User $user
     * @param  \App\Models\DwellingPlan $dwellingPlan
     * @return mixed
     */
    public function delete(User $user, DwellingPlan $dwellingPlan)
    {
        //
    }

    public function createDwelling(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        if ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft'])) and ($proposal->creator_id == $user->id)) {
            return true;
        }

        return
            ($this->isSuperAdmin($user) and $this->statuses($status, ['proposal-draft', 'administration-revising'])) ||
            ($this->isAdmin($user) and $this->statuses($status, [
//                    'proposal-draft',
//                    'administration-revising'
                ])) ||
            ($this->isProposer($user) and $this->statuses($status, ['proposal-draft', 'administration-revising'])) ||
            ($this->isWatcher($user) and $this->statuses($status, ['proposal-draft', 'administration-revising']));
    }
}
