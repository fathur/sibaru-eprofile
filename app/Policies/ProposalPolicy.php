<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Proposal;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProposalPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the proposal.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Proposal $proposal
     *
     * @return mixed
     */
    public function view(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||

//            ($this->isProposer($user) and $this->statuses($status, ['proposal-draft','proposal-new',
//                    'administration-verifying','administration-revising','administration-rejected','technical-surveyor-verifying']))||

            ($this->isProposer($user) and $this->statuses($status, ['*']) and ($user->agency_id == $proposal->proposer_id)) ||

            ($this->isTechnical($user) and $this->statuses($status, ['administration-verifying',
                    'administration-revising', 'administration-rejected', 'technical-surveyor-verifying',
                    'technical-survey-verifying', 'technical-rejected', 'technical-passed', 'administration-repaired',
                    'proposal-passed'])) ||

            ($this->isAdmin($user) and $this->statuses($status, ['proposal-new', 'administration-verifying',
                    'administration-revising', 'administration-rejected', 'technical-surveyor-verifying',
                    'technical-survey-verifying', 'technical-rejected', 'technical-passed', 'administration-repaired',
                    'proposal-passed', 'proposal-draft'])) ||

            ($this->isWatcher($user));
    }

    /**
     * Determine whether the user can create proposals.
     *
     * @param  \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the proposal.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Proposal $proposal
     *
     * @return mixed
     */
    public function update(User $user, Proposal $proposal)
    {
        //
    }

    /**
     * Determine whether the user can delete the proposal.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Proposal $proposal
     *
     * @return mixed
     */
    public function delete(User $user, Proposal $proposal)
    {
        //
    }

    public function editProposal(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        if ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft'])) and ($proposal->creator_id == $user->id)) {
            return true;
        }

        return
            ($this->isSuperAdmin($user) and ($this->statuses($status, ['proposal-draft', 'administration-revising']))) ||
//            ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft', 'administration-revising']))) ||
            ($this->isProposer($user) and $this->statuses($status, ['proposal-draft', 'administration-revising'])) ||
            ($this->isWatcher($user) and ($this->statuses($status, ['proposal-draft', 'administration-revising'])));
    }

    public function createProposal(User $user, Proposal $proposal)
    {

    }

    public function actionProposal(User $user, Proposal $proposal)
    {

    }

    /**
     * @param User $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function sendProposal(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        if ($this->isAdmin($user) and ($this->statuses($status, ['proposal-draft'])) and ($proposal->creator_id == $user->id)) {
            return true;
        }

        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user) and $this->statuses($status, [
//                    'proposal-draft',
//                    'administration-revising'
                ])) ||
            ($this->isProposer($user) and $this->statuses($status, ['proposal-draft', 'administration-revising'])) ||
            ($this->isTechnical($user) and $this->statuses($status, ['technical-surveyor-verifying'])) ||
            ($this->isWatcher($user) and ($this->statuses($status, ['proposal-draft', 'administration-revising'])));
    }

    public function distributeProposal(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-central' and in_array($proposal->status->slug, ['proposal-new']));

    }

    public function actionNew(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-technical' and in_array($proposal->status->slug, ['administration-verifying', 'administration-repaired']));
    }

    public function actionTechnical(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isTechnical($user) and $this->statuses($status, ['technical-survey-verifying']));
    }

    /**
     * @param User $user
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     *
     * Change status proposal
     */
    public function changeStatus(User $user)
    {
        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user));
    }
}
