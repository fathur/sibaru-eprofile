<?php

namespace App\Policies;


use App\Models\Comment;
use App\Models\Proposal;
use App\Models\Status;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the comment.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Comment  $comment
     * @return mixed
     */
    public function view(User $user, Comment $comment)
    {
        //
    }

    public function viewComments(User $user, Proposal $proposal)
    {

        if(self::disableProposalDraft($proposal)) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Comment  $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        //
    }

    /**
     * @param Proposal $proposal
     *
     *
     * @return bool
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public static function disableProposalDraft(Proposal $proposal)
    {

        // jika unit draft maka comment tidak bisa dilihat
        $status = Status::whereSlug('proposal-draft')->first();

        if ($status->id == $proposal->status_id) {
            return false;
        }

        return true;
    }
}
