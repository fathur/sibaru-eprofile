<?php

namespace App\Policies;

use App\Models\Status;
use App\Models\User;
use App\Models\Proposal;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProposalPolicyOld extends AdminPolicy
{
    use HandlesAuthorization;

    public function updateProposal(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-draft') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising');
    }

    public function storeAdministrationDocuments(User $user, Proposal $proposal)
    {
        return
            $this->isSuperAdmin($user) ||
            self::isBelongsToUserAndDraft($user, $proposal) ||
            self::isBelongsToUserAndRevising($user, $proposal);
    }

    public function storeTechnicalDocuments(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-draft');
    }

    public function updateAdministrationDocuments(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising');
    }

    public function updateTechnicalDocuments(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising');
    }

    /**
     * @param User     $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function updateStatus(User $user, Proposal $proposal)
    {
        $status = Status::whereSlug('proposal-draft')->first();

        return
            ($proposal->status_id == $status->id) ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising');
    }

    /**
     * Dapat di distribusikan ke admin teknis (rusun, rusus, swadaya, ruk)
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     *
     * @param User     $user
     * @param Proposal $proposal
     *
     * @return bool
     */
    public function distribute(User $user, Proposal $proposal)
    {
        // jika statusnya adalah usulan baru, dan dilihat oleh admin
        $status = Status::whereSlug('proposal-new')->first();

        if (
            ($this->isCentralAdmin($user) || $this->isSuperAdmin($user)) and
            ($proposal->status_id == $status->id)
        ) {
            return true;
        }

        return false;
    }

    public function viewAdministrationDocuments(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'proposal-new') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-verifying') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'proposal-passed') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-verifying') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'proposal-passed') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-draft') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-new') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-passed');
    }

    public function verifyAdministrationDocuments(User $user, Proposal $proposal)
    {
        // jika dia adalah admin rusun, rusus, ruk, swadaya
        // dan statusnya adalah administration verifying
        return
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-verifying');
    }

    public function verifyTechnicalDocuments(User $user, Proposal $proposal)
    {
        // jika statusnya technical verifying
        // dan sebagai admin tehnical
        return
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-verifying');

    }

    public function viewTechnicalDocuments(User $user, Proposal $proposal)
    {
        // jika statusnya technical verifying
        // dan sebagai admin tehnical
        return
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'proposal-new') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-verifying') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'proposal-passed') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'proposal-passed') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-draft') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-new') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-rejected') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-rejected') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-passed');
    }

    public function verifyAction(User $user, Proposal $proposal)
    {

        if (
            ($user->role->slug == 'admin-technical') and
            ($proposal->status->slug == 'administration-verifying' || $proposal->status->slug == 'technical-verifying')
        ) {
            return true;
        }

        return false;
    }

    public static function isBelongsToUserAndDraft(User $user, Proposal $proposal)
    {
        // jika user login adalah yang punya usulan dan statusnya draft
        $status = Status::whereSlug('proposal-draft')->first();

        if (
            ($user->agency_id == $proposal->proposer_id) and
            ($proposal->status_id == $status->id)
        ) {
            return true;
        }

        return false;
    }

    protected static function isBelongsToUserAndRevising(User $user, Proposal $proposal)
    {
        // jika user login adalah yang punya usulan dan statusnya draft
        $status = Status::whereSlug('administration-revising')->first();

        return
            ($user->agency_id == $proposal->proposer_id) and
            ($proposal->status_id == $status->id);
    }

    public function viewProposalSurvey(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-central' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-passed') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-passed');
    }

    public function createProposalSurvey(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'admin-technical' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'administration-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'proposal-draft');
    }

    public function viewStatusAdministrationVerification(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-verifying') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-revising') ||
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-passed');
    }

    public function viewStatusTechnicalVerification(User $user, Proposal $proposal)
    {
        return
            ($user->role->slug == 'proposer' and $proposal->status->slug == 'technical-passed');
    }
}
