<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Region;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegionPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the region.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Region  $region
     * @return mixed
     */
    public function view(User $user, Region $region)
    {
        //
    }

    /**
     * Determine whether the user can create regions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the region.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Region  $region
     * @return mixed
     */
    public function update(User $user, Region $region)
    {
        //
    }

    /**
     * Determine whether the user can delete the region.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Region  $region
     * @return mixed
     */
    public function delete(User $user, Region $region)
    {
        //
    }

    public function viewAllRegions(User $user)
    {
        // hanya jika punya level setara admin
        if(
            $this->isSuperAdmin($user) ||
            $this->isCentralAdmin($user)
        ) {
            return true;
        }

        return false;
    }
}
