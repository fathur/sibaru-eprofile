<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DecreePolicy extends AdminPolicy
{
    use HandlesAuthorization;

    public function createDecree(User $user, Proposal $proposal)
    {

        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user)) ||
            ($this->isTechnical($user) and $this->inShortlist($proposal) and $this->statuses($status, ['proposal-passed']));
    }

    private function inShortlist($proposal)
    {
        $status = $proposal->status->slug;


        return true;
    }

}