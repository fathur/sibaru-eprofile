<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\User;
use App\Models\Shortlist;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShortlistPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the shortlist.
     *
     * @param  \App\User      $user
     * @param  \App\Shortlist $shortlist
     *
     * @return mixed
     */
    public function view(User $user, Shortlist $shortlist)
    {
        //
    }

    /**
     * Determine whether the user can create shortlists.
     *
     * @param User     $user
     * @param Proposal $proposal
     *
     * @return mixed
     */
    public function create(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user) and $this->statuses($status, ['proposal-passed', 'technical-passed'])) ||
            ($this->isAdmin($user) and $this->statuses($status, ['proposal-passed', 'technical-passed'])) ||
            ($this->isTechnical($user) and $this->statuses($status, ['proposal-passed', 'technical-passed']));
    }

    /**
     * Determine whether the user can update the shortlist.
     *
     * @param  \App\User      $user
     * @param  \App\Shortlist $shortlist
     *
     * @return mixed
     */
    public function update(User $user, Shortlist $shortlist)
    {
        //
    }

    /**
     * Determine whether the user can delete the shortlist.
     *
     * @param  \App\User      $user
     * @param  \App\Shortlist $shortlist
     *
     * @return mixed
     */
    public function delete(User $user, Shortlist $shortlist)
    {
        //
    }

    public function editCode(User $user)
    {
        return $this->isAdmin($user) || $this->isSuperAdmin($user);
    }
}
