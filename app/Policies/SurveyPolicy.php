<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\Status;
use App\Models\User;
use App\Models\Survey;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the survey.
     *
     * @return mixed
     */
    public function view(User $user, Survey $survey)
    {
        //
    }

    /**
     * Determine whether the user can create surveys.
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    public function createSurvey(User $user, Proposal $proposal)
    {
//        // yang buat survey siapa?
//        // 1. ...
//
//        // statusnya apa?
//        // 1. statusnya: lolos administrasi
//        $gate = self::enableProposalPassAdministration($proposal);
//
//        return $gate;

        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user) and ($this->statuses($status, []))) ||
            ($this->isTechnical($user) and $this->statuses($status, ['technical-survey-verifying']));
    }

    /**
     * Determine whether the user can update the survey.
     *
     * @return mixed
     */
    public function update(User $user, Survey $survey)
    {
        //
    }

    /**
     * Determine whether the user can delete the survey.
     *
     * @return mixed
     */
    public function delete(User $user, Survey $survey)
    {
        //
    }

    /**
     * @param User     $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function viewSurvey(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||

//            ($this->isAdmin($user) and $this->statuses($status, ['technical-rejected','technical-passed'])) ||
            ($this->isAdmin($user)) ||

//            ($this->isProposer($user) and $this->statuses($status, ['technical-passed','technical-rejected'])) ||
            ($this->isProposer($user) and $this->statuses($status, [])) ||

            ($this->isTechnical($user) and $this->statuses($status, ['technical-survey-verifying','technical-passed',
                'technical-rejected','proposal-passed']));
    }

    public static function enableProposalPassAdministration($proposal, $gate = false)
    {
        // jika unit draft maka comment tidak bisa dilihat
        $status = Status::whereSlug('administration-passed')->first();

        if ($status->id == $proposal->status_id) {
            return true;
        }

        return $gate;
    }
}
