<?php

namespace App\Policies;

use App\Models\Proposal;
use App\Models\User;
use App\Models\Surveyor;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyorPolicy extends AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the surveyor.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Surveyor  $surveyor
     * @return mixed
     */
    public function view(User $user, Surveyor $surveyor)
    {
        //
    }

    /**
     * Determine whether the user can create surveyors.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the surveyor.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Surveyor  $surveyor
     * @return mixed
     */
    public function update(User $user, Surveyor $surveyor)
    {
        //
    }

    /**
     * Determine whether the user can delete the surveyor.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Surveyor  $surveyor
     * @return mixed
     */
    public function delete(User $user, Surveyor $surveyor)
    {
        //
    }

    /**
     * @param User     $user
     * @param Proposal $proposal
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function viewSurveyor(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;

        return
            ($this->isSuperAdmin($user)) ||

//            ($this->isAdmin($user) and $this->statuses($status, ['technical-survey-verifying','technical-rejected',
//                'technical-passed'])) ||

            ($this->isAdmin($user)) ||
/*
            ($this->isProposer($user) and $this->statuses($status, ['technical-passed','technical-survey-verifying',
                'technical-rejected'])) ||*/

            ($this->isProposer($user) and $this->statuses($status, [])) ||

            ($this->isTechnical($user) and $this->statuses($status, ['technical-surveyor-verifying',
                    'technical-survey-verifying','technical-passed','technical-rejected','proposal-passed']));
    }

    public function createSurveyor(User $user, Proposal $proposal)
    {
        $status = $proposal->status->slug;
        return
            ($this->isAdmin($user) and ($this->statuses($status, []))) ||

            ($this->isSuperAdmin($user)) ||

            ($this->isTechnical($user) and $this->statuses($status, ['technical-surveyor-verifying']));

    }
}
