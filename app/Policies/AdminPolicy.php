<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;

/**
 * Class AdminPolicy
 *
 * @package App\Policies
 */
class AdminPolicy
{
    protected function isSuperAdmin(User $user)
    {

        return $user->role->slug == 'administrator';
    }

    protected function isProposer(User $user)
    {
        return $user->role->slug == 'proposer';
    }

    protected function isAdmin(User $user)
    {
        return $user->role->slug == 'admin-central';
    }

    protected function isTechnical(User $user)
    {
        return $user->role->slug == 'admin-technical';
    }

    protected function isWatcher(User $user)
    {
        return $user->role->slug == 'watcher';
    }

    public function hasAdminPower(User $user)
    {
        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user)) ||
            ($this->isTechnical($user));
    }

    public function hasDirectorateTechnicalPower(User $user)
    {
        return $this->isTechnical($user);
    }

    public function hasWatcherPower(User $user)
    {
        return $this->isWatcher($user);
    }

    public function hasProposerPower(User $user)
    {
        return $this->isProposer($user);
    }

    public function hasMasterData(User $user)
    {
        return
            ($this->isSuperAdmin($user)) ||
            ($this->isAdmin($user));
    }

    /**
     * @param       $key
     * @param array $statuses
     *
     * @return bool
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function statuses($key, array $statuses = [])
    {
        // allow all
        if(in_array('*', $statuses)) {
            return true;
        }

        return in_array($key, $statuses);
    }
}