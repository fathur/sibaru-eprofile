<?php

namespace App\Providers;

use App\Policies\DecreePolicy;
use App\Policies\ProposalPolicy;
use App\Policies\ShortlistPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//        'App\Model' => 'App\Policies\ModelPolicy',
//        Comment::class => CommentPolicy::class,
//        Survey::class  => SurveyPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerProposalPolicies();

        $this->registerCriteriaPolicies();

        $this->registerSurveyorPolicies();

        $this->registerSurveyPolicies();

        $this->registerDwellingPolicies();

        $this->registerShortlistPolicies();

        $this->registerUserPolicies();

        $this->registerAgencyPolicies();

        $this->registerDecreePolicies();

        \Gate::define('do-master', 'App\Policies\AdminPolicy@hasMasterData');

        \Gate::define('do-admin', 'App\Policies\AdminPolicy@hasAdminPower');

        \Gate::define('do-technical', 'App\Policies\AdminPolicy@hasDirectorateTechnicalPower');

        \Gate::define('do-watcher', 'App\Policies\AdminPolicy@hasWatcherPower');

        \Gate::define('do-proposer', 'App\Policies\AdminPolicy@hasProposerPower');


    }

    /**
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function registerProposalPolicies()
    {
        // resource policy
        \Gate::resource('proposals', ProposalPolicy::class);

        // edit dan update proposal data
        \Gate::define(
            'edit-proposal',
            '\App\Policies\ProposalPolicy@editProposal'
        );

        // create new blank proposal
        \Gate::define(
            'create-proposal',
            '\App\Policies\ProposalPolicy@createProposal'
        );

        // menentukan apakah terdapat tab aksi atau tidak dalam proposal tersebut
        \Gate::define(
            'action-proposal',
            '\App\Policies\ProposalPolicy@actionProposal'
        );

        // Tombol kirim, berlaku untuk draft, revising, dan surveyor
        \Gate::define(
            'send-proposal',
            '\App\Policies\ProposalPolicy@sendProposal'
        );

        // dsitribusi ke admin pusat
        \Gate::define(
            'distribute-proposal',
            '\App\Policies\ProposalPolicy@distributeProposal'
        );

        // authorisasi ketika status proposal adalah new
        // tombol setujui, verifikasi, atau batal
        \Gate::define(
            'action-new-proposal',
            '\App\Policies\ProposalPolicy@actionNew'
        );

        // auth ketika proposal di status ver teknis, ketika input hasil survey
        // tombolnya adalah setujui dan tolak
        \Gate::define(
            'action-technical-proposal',
            '\App\Policies\ProposalPolicy@actionTechnical'
        );

        // auth untuk mengubah status
        \Gate::define('change-status', '\App\Policies\ProposalPolicy@changeStatus');
    }

    /**
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function registerCriteriaPolicies()
    {
        // view list administation in tab kriteria
        \Gate::define(
            'view-administration',
            '\App\Policies\CriteriaPolicy@viewCriteriaAdministration'
        );

        // upload dokumen kriteria adm
        \Gate::define(
            'store-administration',
            '\App\Policies\CriteriaPolicy@storeCriteriaAdministration'
        );

        // kasih centak verify dan notes
        \Gate::define(
            'update-verify-administration',
            '\App\Policies\CriteriaPolicy@updateVerificationCriteriaAdministration'
        );

        // lihat note revisi
        \Gate::define(
            'view-revise-administration',
            '\App\Policies\CriteriaPolicy@viewDescriptionCriteriaAdministration'
        );

        // lihat status sudah terferifikasi atau belum
        \Gate::define(
            'view-verify-administration',
            '\App\Policies\CriteriaPolicy@viewVerificationStatusCriteriaAdministration'
        );

        \Gate::define(
            'view-technical',
            '\App\Policies\CriteriaPolicy@viewCriteriaTechnical'
        );

        // upload dokumen kriteria teknis
        \Gate::define(
            'store-technical',
            '\App\Policies\CriteriaPolicy@storeCriteriaTechnical'
        );
        \Gate::define(
            'update-verify-technical',
            '\App\Policies\CriteriaPolicy@updateVerificationCriteriaTechnical'
        );
        \Gate::define(
            'view-revise-technical',
            '\App\Policies\CriteriaPolicy@viewDescriptionCriteriaTechnical'
        );
        \Gate::define(
            'view-verify-technical',
            '\App\Policies\CriteriaPolicy@viewVerificationStatusCriteriaTechnical'
        );

        // upload dokumen kriteria lain
        \Gate::define(
            'store-criteria-others',
            '\App\Policies\CriteriaPolicy@storeCriteriaOthers'
        );
    }

    protected function registerSurveyorPolicies()
    {
        // lihat tab surveyor
        \Gate::define(
            'view-surveyor',
            '\App\Policies\SurveyorPolicy@viewSurveyor'
        );

        // tombol tambah surveyor proposal
        \Gate::define(
            'create-surveyor',
            '\App\Policies\SurveyorPolicy@createSurveyor'
        );
    }

    protected function registerSurveyPolicies()
    {
        // lihat tab surveyor
        \Gate::define(
            'view-survey',
            '\App\Policies\SurveyPolicy@viewSurvey'
        );

        // tombol tambah hasil survey proposal
        \Gate::define(
            'create-survey',
            '\App\Policies\SurveyPolicy@createSurvey'
        );
    }

    protected function registerDwellingPolicies()
    {
// create new blank dwelling plan, also editing
        \Gate::define(
            'create-dwelling',
            '\App\Policies\DwellingPolicy@createDwelling'
        );
    }

    protected function registerShortlistPolicies()
    {
        \Gate::resource('shortlists', ShortlistPolicy::class);

        \Gate::define('edit-code', '\App\Policies\ShortlistPolicy@editCode');
    }

    private function registerUserPolicies()
    {
        \Gate::define('view-user', '\App\Policies\UserPolicy@viewUser');
    }

    private function registerAgencyPolicies()
    {
        \Gate::define('view-agency', '\App\Policies\AgencyPolicy@viewAgency');
    }

    /**
     * Policies untuk SK Penetapan
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    private function registerDecreePolicies()
    {
//        \Gate::resource('decrees', DecreePolicy::class);
        \Gate::define('create-decree', '\App\Policies\DecreePolicy@createDecree');

    }
}
