<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Region extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'region:json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Region to json file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $provinsi = \DB::table('wilayah')
            ->whereRaw(\DB::raw('character_length(wilayah_id) = 2'))
            ->get();

        foreach ($provinsi as $p) {

            $regions[title_case($p->wilayah_nama)] = [];

            $kotaKabupaten = \DB::table('wilayah')
                ->whereRaw(\DB::raw('character_length(wilayah_id) = 4'))
                ->where('wilayah_id', 'like', "{$p->wilayah_id}%")
                ->get();

            foreach ($kotaKabupaten as $k) {

                $regions[title_case($p->wilayah_nama)][title_case($k->wilayah_nama)] = [];

                $kecamatan = \DB::table('wilayah')
                    ->whereRaw(\DB::raw('character_length(wilayah_id) = 6'))
                    ->where('wilayah_id', 'like', "{$k->wilayah_id}%")
                    ->get();

                foreach ($kecamatan as $c) {

                    $regions[title_case($p->wilayah_nama)][title_case($k->wilayah_nama)][title_case($c->wilayah_nama)] = [];

                    $desaKelurahan = \DB::table('wilayah')
                        ->whereRaw(\DB::raw('character_length(wilayah_id) = 8'))
                        ->where('wilayah_id', 'like', "{$c->wilayah_id}%")
                        ->get();

                    foreach ($desaKelurahan as $d) {

                        array_push($regions[title_case($p->wilayah_nama)][title_case($k->wilayah_nama)][title_case($c->wilayah_nama)], title_case($d->wilayah_nama));

                    }

                }
            }

            \Storage::disk('local')->put("regions/provinsi-" . Str::slug(strtolower($p->wilayah_nama)) . ".json", json_encode($regions, JSON_PRETTY_PRINT));

            $this->info("Region " . title_case($p->wilayah_nama) . " written");
            $regions = [];

        }

    }
}
