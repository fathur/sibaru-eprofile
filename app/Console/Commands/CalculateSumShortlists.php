<?php

namespace App\Console\Commands;

use App\Models\Shortlist;
use App\Models\ShortlistBudget;
use Illuminate\Console\Command;

class CalculateSumShortlists extends Command
{
    protected $signature = 'calculate:shortlist {--id= : ID shortlist (not proposal or shortlist budget)}';

    protected $description = 'Caclulate total shortlists and insert into table shortlists';

    public function handle()
    {
        $id = $this->option('id');

        $this->setShortlistBudgetRelation();
        $this->sumShortlists($id);
    }

    /**
     * @param integer|null $id
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    protected function sumShortlists($id = null)
    {
        if (is_null($id)) {

            Shortlist::with(['proposal'])->chunk(10, function ($shortlists) {
                foreach ($shortlists as $shortlist) {

                    $this->warn('Calulating shortlist ' . $shortlist->id . '...');

                    // menghitung total shotlist budget berdasarkan shortlist ini
                    $sumAmount = ShortlistBudget::where('shortlist_id', $shortlist->id)->sum('amount');
                    $sumBudget = ShortlistBudget::where('shortlist_id', $shortlist->id)->sum('budget_plan');

                    $shortlist->sum_amount = $sumAmount;
                    $shortlist->sum_budget_plan = $sumBudget;
                    $shortlist->save();

                    $this->info('Shortlist ' . $shortlist->id . ' -> amount: ' . $sumAmount . ', budget: ' . $sumBudget);

                }
            });
        } else {
            $shortlist = Shortlist::with(['proposal'])->find($id);

            $this->warn('Calulating shortlist ' . $shortlist->id . '...');

            // menghitung total shotlist budget berdasarkan shortlist ini
            $sumAmount = ShortlistBudget::where('shortlist_id', $shortlist->id)->sum('amount');
            $sumBudget = ShortlistBudget::where('shortlist_id', $shortlist->id)->sum('budget_plan');

            $shortlist->sum_amount = $sumAmount;
            $shortlist->sum_budget_plan = $sumBudget;
            $shortlist->save();

            $this->info('Shortlist ' . $shortlist->id . ' -> amount: ' . $sumAmount . ', budget: ' . $sumBudget);

        }
    }

    /**
     * Set shortlist budget relation with shortlosts table.
     * In the beginning shortlist budget get ralation into shortlist table
     * using long ralation from proposals table,
     * but now I want to the relation direct into shortlists table.
     *
     * @author Fathur Rohman <hi.fathur.rohman@gmail.com>
     */
    private function setShortlistBudgetRelation()
    {
        ShortlistBudget::with(['dwellingPlan' => function ($q) {
            $q->with('proposal');
        }])
            ->chunk(10, function ($shortlistBudgets) {
                foreach ($shortlistBudgets as $shortlistBudget) {

                    // memastikan loopnya tidak boros jika datanya banyak
                    if (is_null($shortlistBudget->shortlist_id)) {

                        $proposalId = $shortlistBudget->dwellingPlan->proposal->id;
                        $shortlist = Shortlist::where('proposal_id', $proposalId)->first();

                        $shortlistBudget->shortlist_id = $shortlist->id;
                        $shortlistBudget->save();
                    }
                }
            });
    }
}