<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PrepareProduction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:production';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare application for production';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Migrate database
        $this->call('migrate');

        // Seed initial data
        $this->call('db:seed');

        // Cache configuration
        $this->call('config:cache');

        // Cache route
        $this->call('route:cache');

        // Cache regions
        $this->call('region:cache');
    }
}
