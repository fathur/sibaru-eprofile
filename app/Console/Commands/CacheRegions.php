<?php

namespace App\Console\Commands;

use App\Models\Region;
use Illuminate\Console\Command;

class CacheRegions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'region:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache region for faster load data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $regions = Region::all();

        foreach ($regions as $region) {

//            $value = $this->constructName($region);
            $value = \App\Repositories\Region::constructName($region);

            \Cache::forever('region-' . $region->id, $value);

            $this->info('Cached: ' . $value);

        }
    }

//    private function constructName($region)
//    {
//        $name = $region->name;
//
//        if ($region->parent_id !== null) {
//
//            $parentRegion = Region::find($region->parent_id);
//
//            $parentName = $this->constructName($parentRegion);
//
//            $name = "$name, $parentName";
//
//        }
//
//        return $name;
//    }
}
