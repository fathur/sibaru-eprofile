<?php
/**
 * Created by PhpStorm.
 * User: akung
 * Date: 9/25/17
 * Time: 21:19
 */

namespace App\Console\Commands;


use App\Models\Program;
use App\Models\Proposal;
use App\Models\Recipient;
use App\Models\Region;
use App\Models\Status;
use App\Models\Unit;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class ImportProposal extends Command
{
    protected $signature = 'import:proposal {type?}';

    protected $description = 'Import proposal';

    protected $cache = [];

    /**
     * Available columns
     *
     * no
     * direktorat
     * kegiatan
     * penerima_manfaat
     * lokasi
     * alamat
     * keterangan_pengusul
     * tgl_pengajuan
     * tahun_usulan
     * jml_unit
     */
    public function handle()
    {

        switch ($this->argument('type')) {
            case 'rusun':
                $this->importRusun();
                break;

            case 'rusus':
                $this->importRusus();
                break;

            case 'ruk':
                $this->importRuk();
                break;

            case 'swadaya':
                $this->importSwadaya();
                break;

            default:
                $this->importRusun();
                $this->importRusus();
                $this->importRuk();
                $this->importSwadaya();
        }

    }

    protected function importRusun()
    {
        $this->info('Importing rusun...');
        $this->importUnit('rusun');
    }

    protected function importSwadaya()
    {
        $this->info('Importing swadaya...');
        $this->importUnit('swadaya');
    }

    protected function importRusus()
    {
        $this->info('Importing rusus...');

        $this->importUnit('rusus');

    }

    protected function importRuk()
    {
        $this->info('Importing ruk...');

        $this->importUnit('ruk');

    }

    protected function guessRecipient($row)
    {
        $recipient = trim(strtolower($row->penerima_manfaat));

        $mappingUnits = $this->mapUnits();

        $unitId = $mappingUnits[trim($row->direktorat)];

        $id = \Cache::tags(['proposal', 'import'])->remember('import-proposal-unit-' . $unitId . '-' . $recipient, Carbon::now()->addHour(6), function () use ($unitId, $recipient) {

            if (trim($recipient) != '') {

                $data = Recipient::whereUnitId($unitId)
                    ->whereRaw("LOWER(name) = '{$recipient}'")
                    ->first();

                if ($data) {
                    return $data->id;
                }

                $data = Recipient::create([
                    'unit_id' => $unitId,
                    'name'    => ucfirst($recipient)
                ]);

                return $data->id;
            }

        });

        return $id != '' ? $id : false;
    }

    protected function guessLocation($row)
    {
        $location = trim(strtolower($row->lokasi));

//        $explode = explode(' ', $kabupaten);

//        $lowerEx0 = strtolower($explode[0]);

//        if ($lowerEx0 == 'kabupaten' || $lowerEx0 == 'kota') {
//
//            $regions = Region::whereRaw("LOWER(name) LIKE '{$kabupaten}'")
//                ->where('level', '=', 2)
//                ->get();
//
//
//        } else {

        $regions = Region::whereRaw("LOWER(name) ILIKE '{$location}'")
            ->whereNull('parent_id')
            ->where('level', 1)
            ->get();
//        }

        if (count($regions) > 1) {
            $this->error($location . ' ada ' . count($regions));
        }

        if (count($regions) == 0) {
            if (!in_array($location, $this->cache)) {
                # $this->error('arr: ' . $location);
            }

            array_push($this->cache, $location);

        }

        if (count($regions) == 1) {
            return $regions[0]->id;
        }

        return false;
    }

    protected function mapUnits()
    {
        $results = [];
        $map = [
            'rusun'   => 'Rumah Susun',
            'rusus'   => 'Rumah Khusus',
            'ruk'     => 'Rumah Umum dan Komersial',
            'swadaya' => 'Rumah Swadaya',
        ];

        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        foreach ($units as $unit) {

            $results[$map[$unit->slug]] = $unit->id;
        }

        return $results;
    }

    private function guessProgram($row)
    {

        $kegiatan = trim(strtolower($row->kegiatan));

        $mappingUnits = $this->mapUnits();

        $unitId = $mappingUnits[trim($row->direktorat)];

        $programId = \Cache::tags(['proposal', 'import'])->remember('import-proposal-unit-' . $unitId . '-' . $kegiatan, Carbon::now()->addHour(6), function () use ($unitId, $kegiatan) {

            $program = Program::whereUnitId($unitId)
                ->whereRaw("LOWER(name) = '{$kegiatan}'")
                ->first();

            if ($program) {
                return $program->id;
            }

            $program = Program::create([
                'unit_id' => $unitId,
                'name'    => ucfirst($kegiatan)
            ]);

            return $program->id;

        });

        return $programId;
    }

    protected function importUnit($type)
    {
        $path = resource_path("files/proposal/{$type}.csv");

        $file = \File::get($path);

        \Excel::load($path, function (LaravelExcelReader $reader) use ($file) {

            $mappingUnits = $this->mapUnits();

            $reader->chunk(10, function ($rows) use ($mappingUnits) {
                foreach ($rows as $row) {


                    if ($this->guessLocation($row) and $this->guessRecipient($row)) {


                        $proposal = new Proposal();

                        $proposal->unit_id = $mappingUnits[trim($row->direktorat)];
                        $proposal->program_id = $this->guessProgram($row);
                        $proposal->recipient_id = $this->guessRecipient($row);
                        $proposal->location_id = $this->guessLocation($row);
                        $proposal->proposer_id = User::whereUsername('admin_pusat')->first()->id;
                        $proposal->address = $row->alamat;
                        $proposal->proposer_remark = $row->keterangan_pengusul;
                        $proposal->proposed_year = $row->tahun_usulan ?? null;

                        try {
                            $proposal->submission_date = Carbon::parse($row->tgl_pengajuan);
                        } catch (\Exception $e) {
                            $this->error("{$row->no}, {$e->getMessage()}");
                        }

                        $proposal->creator_id = User::whereUsername('admin_pusat')->first()->id;
                        $proposal->status_id = Status::whereSlug('proposal-passed')->first()->id;
                        // ....

                        $proposal->save();

                        if ($row->jml_unit) {

                            $amount = $row->jml_unit;

                            if (trim($amount) == '-') {
                                $amount = 0;
                            }

                            $proposal->dwellingPlans()->create([
                                'budget_plan' => 0,
                                'amount'      => $amount
                            ]);
                        }
                    } else {

                        if($row->lokasi == '' || $row->lokasi == null) {
                            $this->error('Error: Lokasi tidak ada:');
                        }
                        elseif($row->penerima_manfaat == '' || $row->penerima_manfaat == null) {
                            $this->error('Error: Penerima manfaaat tidak ada:');
                        }
                        else {
                            $this->error('Error: ??');
                        }

                        $this->error("{$row->no},{$row->lokasi},{$row->penerima_manfaat},{$row->direktorat}," .
                            "{$row->kegiatan},{$row->penerima_manfaat},{$row->alamat},{$row->keterangan_pengusul}," .
                            "{$row->tgl_pengajuan},{$row->tahun_usulan},{$row->jml_unit}");

//                        $this->error('---');
//                        $this->error('lok: ' . $row->lokasi);
//                        $this->error('loc: ' . $this->guessLocation($row));
//                        $this->error('pm: ' . $row->penerima_manfaat);
//                        $this->error('rec: ' . $this->guessRecipient($row));
                    }
                }

            }, false);


        });
    }
}