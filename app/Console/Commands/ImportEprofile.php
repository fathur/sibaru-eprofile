<?php


namespace App\Console\Commands;

use App\Models\Eprofile;
use App\Models\Program;
use App\Models\Recipient;
use App\Models\Unit;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Finder\SplFileInfo;

class ImportEprofile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:eprofile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import eprofile';

    /**
     * Save non existing region
     * @var array
     */
    protected $cache = [];

    // Progress bar status
    private $bar;


    public function handle()
    {
        // Progress bar
        $this->bar = $this->output->createProgressBar(7044);

        \Cache::tags(['eprofile','import'])->flush();

        $files = \File::allFiles(resource_path('files/eprofile'));

        $files = $this->sortFiles($files);


        foreach ($files as $i => $file) {


            /** @var SplFileInfo $file */
            \Excel::load($file->getPathName(), function (\Maatwebsite\Excel\Readers\LaravelExcelReader $reader) use ($file) {

//                $this->info('Loaded file '.$file->getFileName());

                $mappingUnits = $this->mapUnits();

                $reader->chunk(100, function ($rows) use ($mappingUnits) {
                    foreach ($rows as $row) {


//                    $this->info('Saving ' . $row->program);

                        if ($this->guessLocation($row) and $this->guessRecipient($row)) {

                            $eprofile = new Eprofile();
                            $eprofile->unit_id = $mappingUnits[trim($row->program)];
                            $eprofile->total_budget = $this->calculateBudget($row); //(integer)$row->anggaran_000;
                            $eprofile->amount_unit = (integer)$row->unit;
                            $eprofile->proposed_year = (integer)$row->tahun;
                            $eprofile->recipient_id = $this->guessRecipient($row);
                            $eprofile->program_id = $this->guessProgram($row);
                            $eprofile->proposer_remark = $row->keterangan;
                            $eprofile->location_id = $this->guessLocation($row);
                            $eprofile->save();

//                            $this->info('Saved ' . $row->no);
                        } else {

                            $this->error("{$row->no},{$row->provinsi},{$row->no_kabkot},{$row->kabupaten_kota}," .
                                "{$row->peruntukan},{$row->program},{$row->kegiatan},\"{$row->penerima_manfaat}\",\"{$row->keterangan}\",{$row->unit},{$row->anggaran}," .
                                "{$row->tahun}");
                        }

                        $this->bar->advance();

                    }
                }, false);
            });


        }

        $this->bar->finish();
    }

    /**
     * @return array
     */
    private function mapUnits(): array
    {
        $results = [];
        $map = [
            'rusun' => 'RUMAH SUSUN',
            'rusus' => 'RUMAH KHUSUS',
            'ruk' => 'RUMAH UMUM DAN KOMERSIAL',
            'swadaya' => 'RUMAH SWADAYA',
        ];

        $units = Unit::whereNotIn('slug', ['dirjen'])->get();

        foreach ($units as $unit) {

            $results[$map[$unit->slug]] = $unit->id;
        }

        return $results;
    }

    private function guessProgram($row)
    {

        $kegiatan = trim(strtolower($row->kegiatan));

        $mappingUnits = $this->mapUnits();

        $unitId = $mappingUnits[trim($row->program)];

        $programId = \Cache::tags(['eprofile','import'])->remember('import-eprofile-unit-' . $unitId . '-' . $kegiatan, Carbon::now()->addHour(6), function () use ($unitId, $kegiatan) {

            $program = Program::whereUnitId($unitId)
                ->whereRaw("LOWER(name) = '{$kegiatan}'")
                ->first();

            if ($program) {
                return $program->id;
            }

            $program = Program::create([
                'unit_id' => $unitId,
                'name' => ucfirst($kegiatan)
            ]);

            return $program->id;

        });

        return $programId;
    }

    private function guessRecipient($row)
    {
        $recipient = trim(strtolower($row->penerima_manfaat));

        $mappingUnits = $this->mapUnits();

        $unitId = $mappingUnits[trim($row->program)];

        $id = \Cache::tags(['eprofile','import'])->remember('import-eprofile-unit-' . $unitId . '-' . $recipient, Carbon::now()->addHour(6), function () use ($unitId, $recipient) {

            if (trim($recipient) != '') {

                $data = Recipient::whereUnitId($unitId)
                    ->whereRaw("LOWER(name) = '{$recipient}'")
                    ->first();

                if ($data) {
                    return $data->id;
                }

                $data = Recipient::create([
                    'unit_id' => $unitId,
                    'name' => ucfirst($recipient)
                ]);

                return $data->id;
            }

        });

        return $id != '' ? $id : false;
    }

    private function guessLocation($row)
    {
        $kabupaten = trim(strtolower($row->kabupaten_kota));

        $explode = explode(' ', $kabupaten);

        $lowerEx0 = strtolower($explode[0]);

        if ($lowerEx0 == 'kabupaten' || $lowerEx0 == 'kota') {

            $regions = \App\Models\Region::whereRaw("LOWER(name) LIKE '{$kabupaten}'")
                ->where('level', '=', 2)
                ->get();


        } else {

            $regions = \App\Models\Region::whereRaw("LOWER(name) LIKE '{$kabupaten}%'")
                ->get();
        }

        if (count($regions) > 1) {
            $this->error($kabupaten . ' ada ' . count($regions));
        }

        if (count($regions) == 0) {
            if (!in_array(trim($row->kabupaten_kota) . ', ' . trim($row->provinsi), $this->cache)) {
                # $this->error(trim($row->kabupaten_kota) . ', ' . trim($row->provinsi));
            }

            array_push($this->cache, trim($row->kabupaten_kota) . ', ' . trim($row->provinsi));

        }

        if (count($regions) == 1) {
            return $regions[0]->id;
        }

        return false;
    }

    private function calculateBudget($row)
    {
        if ((int)$row->anggaran !== 0) {
            return (integer)$row->anggaran * 1000;
        }

        return (integer)$row->anggaran;
    }

    protected function regionNameNotMatch()
    {
        return [
            'KABUPATEN BANYUASIN, SUMATERA SELATAN'             => 'DB: BANYU ASIN -> BANYUASIN',
            'KABUPATEN SIAK, RIAU'                              => 'DB: S I A K -> SIAK',
            'KOTA DUMAI, RIAU'                                  => 'DB: D U M A I -> DUMAI',
            'KABUPATEN TULANG BAWANG, LAMPUNG'                  => 'DB: TULANGBAWANG -> TULANG BAWANG',
            'KABUPATEN FAK FAK, PAPUA BARAT'                    => 'FILE: FAK FAK -> FAKFAK',
            'KOTA BATAM, KEPULAUAN RIAU'                        => 'DB: B A T A M -> BATAM',
            'KOTA ADMINISTRASI JAKARTA BARAT, DKI JAKARTA'      => 'DB: KOTA JAKARTA BARAT -> KOTA ADMINISTRASI JAKARTA BARAT',
            'KOTA ADMINISTRASI JAKARTA SELATAN, DKI JAKARTA'    => 'DB: KOTA JAKARTA SELATAN -> KOTA ADMINISTRASI JAKARTA SELATAN',
            'KOTA ADMINISTRASI JAKARTA TIMUR, DKI JAKARTA'      => 'DB: KOTA JAKARTA TIMUR -> KOTA ADMINISTRASI JAKARTA TIMUR',
            'KOTA ADMINISTRASI JAKARTA UTARA, DKI JAKARTA'      => 'DB: KOTA JAKARTA UTARA -> KOTA ADMINISTRASI JAKARTA UTARA',
            'KOTA ADMINISTRASI JAKARTA PUSAT, DKI JAKARTA'      => 'DB: KOTA JAKARTA PUSAT -> KOTA ADMINISTRASI JAKARTA PUSAT',
            'KABUPATEN GUNUNG, KALIMANTAN TENGAH'               => '?',
            'KABUPATEN PURWODADI, JAWA TENGAH'                  => '?',
            'KABUPATEN SLAWI, JAWA TENGAH'                      => '?',
            'KABUPATEN GUNUNGKIDUL, DI YOGYAKARTA'              => '?',
            'KABUPATEN KARANGASEM, BALI'                        => 'DB: KARANG ASEM -> KARANGASEM',
            'KABUPATEN DENPASAR, BALI'                          => 'FILE: KABUPATEN DENPASAR -> KOTA DENPASAR',
            'KABUPATEN SUMBAWA BESAR, NUSA TENGGARA BARAT'      => '?',
            'KABUPATEN KUTOARJO, JAWA TENGAH'                   => '?',
            'KABUPATEN NGABANG, KALIMANTAN BARAT'               => '?',
            'KOTA PALANGKARAYA, KALIMANTAN TENGAH'              => 'FILE: PALANGKARAYA -> PALANGKA RAYA',
            'KOTA BANJARBARU, KALIMANTAN SELATAN'               => 'DB: BANJAR BARU -> BANJARBARU',
            'KABUPATEN KOTABARU, KALIMANTAN SELATAN'            => 'DB: KOTA BARU -> KOTABARU',
            'KABUPATEN KEPULAUAN SIAU TAGULANDANG BIARO, SULAWESI UTARA' => 'DB: KABUPATEN SIAU TAGULANDANG BIARO -> KABUPATEN KEPULAUAN SIAU TAGULANDANG BIARO',
            'KABUPATEN TOLI TOLI, SULAWESI TENGAH'              => 'FILE: TOLI TOLI -> TOLITOLI, DB: TOLI-TOLI -> TOLITOLI',
            'KOTA PARE PARE, SULAWESI SELATAN'                  => 'FILE: PARE PARE -> PAREPARE',
            'KABUPATEN MUKO MUKO, BENGKULU'                     => 'FILE: MUKO MUKO -> MUKOMUKO',
            'KABUPATEN PANGKAJENE KEPULAUAN, SULAWESI SELATAN'  => 'FILE: PANGKAJENE KEPULAUAN -> PANGKAJENE DAN KEPULAUAN',
            'KOTA BAU BAU, SULAWESI TENGGARA'                   => 'FILE: BAU BAU -> BAUBAU',
            'KABUPATEN PAHUWATO, GORONTALO'                     => 'FILE: PAHUWATO -> POHUWATO',
            'KOTA SERAM BAGIAN TIMUR, MALUKU'                   => 'FILE: KOTA SERAM BAGIAN TIMUR -> KABUPATEN SERAM BAGIAN TIMUR',
            'KOTA SOFIFI, MALUKU UTARA'                         => '?',
            'KABUPATEN BATANGHARI, JAMBI'                       => 'DB: BATANG HARI -> BATANGHARI',
            'KOTA SIDOARJO, JAWA TIMUR'                         => 'FILE: KOTA SIDOARJO -> KABUPATEN SIDOARJO',
            'KABUPATEN KOTAWARINGIN, KALIMANTAN TENGAH'         => '? ada dua barat atau timur?',
            'KABUPATEN SERAM BARAT, MALUKU'                     => 'FILE: SERAM BARAT -> SERAM BAGIAN BARAT',
            '(MENYEBAR), PAPUA'                                 => '?',
            'KABUPATEN LUBUK LINGGAU, SUMATERA SELATAN'         => 'FILE: KABUPATEN LUBUK LINGGAU -> KOTA LUBUKLINGGAU',
        ];
    }

    private function sortFiles($files)
    {
        $data = [];
        foreach ($files as $file) {
            $data[$file->getFileName()] = $file;
        }

        ksort($data, SORT_STRING);

        return $data;
    }
}