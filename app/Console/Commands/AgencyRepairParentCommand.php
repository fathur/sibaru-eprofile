<?php


namespace App\Console\Commands;


use App\Models\Agency;
use Illuminate\Console\Command;

class AgencyRepairParentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agency:repair';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Parent issue in agency';

    /**
     *
     */
    public function handle()
    {
        $pemda = Agency::whereName('Pemda')->first();

        $count = Agency::count();
        $bar = $this->output->createProgressBar($count);
        (new Agency)->chunk(100, function ($agencies) use ($pemda, $bar) {


            foreach ($agencies as $agency) {

                // Jika oemda parentnya di check ulang
                if ($agency->parent_id == $pemda->id) {

                    $region = (new \App\Models\Region)->find($agency->region_id);

                    if($region) {
                        $parentRegion = $region->parent;

                        if($parentRegion) {

                            $predictedAgencyParent = Agency::whereRegionId($parentRegion->id)->first();

                            if ($predictedAgencyParent) {
                                $newAgency = (new Agency)->find($agency->id);
                                $newAgency->parent_id = $predictedAgencyParent->id;
                                $newAgency->save();
                            }
                        }
                    }
                }

                $bar->advance();
            }
        });

        $bar->finish();
    }
}