<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class RefreshApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset and initialize for re-develop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::now();

        if (!app()->environment('production')) {

            $this->call('migrate:reset');
            $this->call('migrate');
            $this->call('db:seed');
            $this->call('cache:clear');
            $this->call('route:clear');
            $this->call('config:clear');
            $this->call('region:cache');
            $this->call('ide-helper:models', ['--write' => true]);
        }

        $end = Carbon::now();

        $diff = $end->diffInSeconds($start);

        $this->info('Selesai dalam ' . gmdate('i \m\e\n\i\t s \d\e\t\i\k', $diff));
    }

}