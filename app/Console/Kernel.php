<?php

namespace App\Console;

use App\Console\Commands\AgencyRepairParentCommand;
use App\Console\Commands\CacheRegions;
use App\Console\Commands\CalculateSumShortlists;
use App\Console\Commands\ImportEprofile;
use App\Console\Commands\ImportProposal;
use App\Console\Commands\PrepareProduction;
use App\Console\Commands\RefreshApp;
use App\Models\Status;
use App\Models\Unit;
use App\Repositories\Statistic;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // \App\Console\Commands\Region::class
        CacheRegions::class,
        PrepareProduction::class,
        RefreshApp::class,
        CalculateSumShortlists::class,

        ImportEprofile::class,
        ImportProposal::class,

        AgencyRepairParentCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $this->scheduleStatisticRegion($schedule, 'proposal');
        $this->scheduleStatisticRegion($schedule, 'eprofile');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }

    /**
     * @param Schedule $schedule
     * @param $type
     * @param bool $withYear
     */
    protected function scheduleStatisticRegion(Schedule $schedule, $type, $withYear = false)
    {
        $statuses = Status::all();
//        $func = 'calculate' . title_case($type) . 'Total';
        $func = 'calculateTotal';

        // Flush cache before calculate
        \Cache::tags(['chart'])->flush();

        $schedule->call(function () use ($statuses, $func, $withYear) {

            $this->calc($func, $statuses, Unit::whereSlug(Unit::RUSUN['slug'])->first());

            if ($withYear) {
                $yearNow = Carbon::now()->year;
                $threeYearsAgo = $yearNow - 3;

                for ($year = $threeYearsAgo; $year <= $yearNow; $year++) {
                    $this->calc(
                        $func,
                        $statuses,
                        Unit::whereSlug(Unit::RUSUN['slug'])->first(),
                        $year
                    );
                }
            }

        })->dailyAt('00:00')
            ->name('rusun-bar')
            ->withoutOverlapping();

        $schedule->call(function () use ($statuses, $func, $withYear) {

            $this->calc($func, $statuses, Unit::whereSlug(Unit::RUSUS['slug'])->first());

            if ($withYear) {
                $yearNow = Carbon::now()->year;
                $threeYearsAgo = $yearNow - 3;

                for ($year = $threeYearsAgo; $year <= $yearNow; $year++) {
                    $this->calc(
                        $func,
                        $statuses,
                        Unit::whereSlug(Unit::RUSUS['slug'])->first(),
                        $year
                    );
                }
            }

        })->dailyAt('00:00')
            ->name('rusus-bar')
            ->withoutOverlapping();

        $schedule->call(function () use ($statuses, $func, $withYear) {
            $this->calc($func, $statuses, Unit::whereSlug(Unit::RUK['slug'])->first());

            if ($withYear) {
                $yearNow = Carbon::now()->year;
                $threeYearsAgo = $yearNow - 3;

                for ($year = $threeYearsAgo; $year <= $yearNow; $year++) {
                    $this->calc(
                        $func,
                        $statuses,
                        Unit::whereSlug(Unit::RUK['slug'])->first(),
                        $year
                    );
                }
            }

        })->dailyAt('00:00')
            ->name('ruk-bar')
            ->withoutOverlapping();

        $schedule->call(function () use ($statuses, $func, $withYear) {

            $this->calc($func, $statuses, Unit::whereSlug(Unit::SWADAYA['slug'])->first());

            if ($withYear) {
                $yearNow = Carbon::now()->year;
                $threeYearsAgo = $yearNow - 3;

                for ($year = $threeYearsAgo; $year <= $yearNow; $year++) {
                    $this->calc(
                        $func,
                        $statuses,
                        Unit::whereSlug(Unit::SWADAYA['slug'])->first(),
                        $year
                    );
                }
            }

        })->dailyAt('00:00')
            ->name('swadaya-bar')
            ->withoutOverlapping();
    }

    /**
     * @param $func
     * @param $statuses
     * @param $unit
     * @param null $year
     */
    public function calc($func, $statuses, $unit, $year = null)
    {
        if (is_null($year)) {

            (new Statistic)->{$func}($unit);
        } else {
            (new Statistic)->setYear($year)
                ->{$func}($unit);
        }

        foreach ($statuses as $status) {

            if (is_null($year)) {

                (new Statistic)->{$func}($unit, null, $status->id);
            } else {
                (new Statistic)->setYear($year)
                    ->{$func}($unit, null, $status->id);

            }
        }
    }

}
