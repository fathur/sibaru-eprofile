<?php

use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = DB::table('regions')->whereIn('level', [1, 2])->get();

        foreach ($regions as $region) {
            DB::table('agencies')->insert([
                'region_id' => $region->id,
                'name'      => $region->name,
                'type'      => 'pemda'
            ]);
        }
    }
}
