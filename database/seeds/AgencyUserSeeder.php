<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class AgencyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agencies = DB::table('agencies')->get();

        foreach ($agencies as $agency) {

            $u = DB::table('users')->where('username', $agency->name)->first();

            $r = DB::table('regions')->where('id', $agency->region_id)->first();

            if ($r->level == 1) {
                $roleId = Role::whereSlug('watcher')->first()->id;
            } elseif ($r->level == 2) {
                $roleId = Role::whereSlug('proposer')->first()->id;
            }

            if (!$u) {
                DB::table('users')->insert([
                    'name'      => $agency->name,
                    'agency_id' => $agency->id,
                    'role_id'   => $roleId,
                    'username'  => snake_case($agency->name),
                    'email'     => str_random(15) . '@pu.go.id',
                    'password'  => bcrypt('a'),
                ]);
            }
        }
    }
}
