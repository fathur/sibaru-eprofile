<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            [
                'name'  => 'Direktorat Penyediaan Perumahan',
                'slug'  => 'dirjen',
                'jenis' => 'pusat',
                'color' => '#2c3e50'
            ],
            [
                'name'  => \App\Models\Unit::RUSUN['name'],
                'slug'  => \App\Models\Unit::RUSUN['slug'],
                'program' => \App\Models\Unit::RUSUN['program'],
                'jenis' => 'teknis',
                'color' => '#e74c3c'
            ],
            [
                'name'  => \App\Models\Unit::RUSUS['name'],
                'slug'  => \App\Models\Unit::RUSUS['slug'],
                'program' => \App\Models\Unit::RUSUS['program'],
                'jenis' => 'teknis',
                'color' => '#f1c40f'
            ],
            [
                'name'  => \App\Models\Unit::RUK['name'],
                'slug'  => \App\Models\Unit::RUK['slug'],
                'program' => \App\Models\Unit::RUK['program'],
                'jenis' => 'teknis',
                'color' => '#2ecc71'
            ],
            [
                'name'  => \App\Models\Unit::SWADAYA['name'],
                'slug'  => \App\Models\Unit::SWADAYA['slug'],
                'program' => \App\Models\Unit::SWADAYA['program'],
                'jenis' => 'teknis',
                'color' => '#3498db'
            ],
        ]);
    }
}
