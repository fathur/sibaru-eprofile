<?php

use Illuminate\Database\Seeder;

class UnitSizesTableSeeder extends Seeder
{
    protected $sizes = [21, 36, 45, 54, 60, 120, 'Barak'];
    protected $floors = [2, 3, 4, 5];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->rusus();

        $this->rusun();

    }

    private function rusus()
    {

        foreach ($this->sizes as $size) {

            DB::table('unit_sizes')->insert([
                ['type' => 'rusus', 'text' => $size]
            ]);
        }
    }

    private function rusun()
    {


        foreach ($this->sizes as $size) {

            foreach ($this->floors as $floor) {
                DB::table('unit_sizes')->insert([
                    ['type' => 'rusun', 'text' => $size, 'total_floor' => $floor]
                ]);
            }


        }
    }
}
