<?php

use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->rusun();
        $this->rusus();
        $this->ruk();
        $this->swadaya();
    }

    protected function rusun()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUSUN['slug'])->first();

        DB::table('programs')->insert([
            'unit_id' => $unit->id,
            'name'    => 'Pembangunan baru'
        ]);
    }

    private function rusus()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUSUS['slug'])->first();

        DB::table('programs')->insert([
            [
                'unit_id' => $unit->id,
                'name'    => 'Pembangunan baru'
            ],
            [
                'unit_id' => $unit->id,
                'name'    => 'Rehabilitasi pembangunan'
            ],
        ]);
    }

    private function ruk()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUK['slug'])->first();

        DB::table('programs')->insert([
            [
                'unit_id' => $unit->id,
                'name'    => 'Bantuan PSU untuk perumahan umum'
            ],

        ]);
    }

    private function swadaya()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::SWADAYA['slug'])->first();

        DB::table('programs')->insert([
            [
                'unit_id' => $unit->id,
                'name'    => 'Pembangunan baru'
            ],
            [
                'unit_id' => $unit->id,
                'name'    => 'Peningkatan kualitas rumah tidak layak huni (RTLH)'
            ],
            [
                'unit_id' => $unit->id,
                'name'    => 'Pembangunan PSU'
            ],
        ]);
    }
}
