<?php

use Illuminate\Database\Seeder;

class DwellingPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->rusun();
        $this->rusus();
        $this->ruk();
        $this->swadaya();
    }

    protected function rusun()
    {
        $unit = \App\Models\Unit::whereSlug(\App\Models\Unit::RUSUN['slug'])->first();
        $proposalRusun = \App\Models\Proposal::whereUnitId($unit->id)->get();

        foreach ($proposalRusun as $proposal) {
            $count = random_int(2, 20);

            for ($i = 1; $i <= $count; $i ++) {

                $unitType = \App\Models\UnitTypes::inRandomOrder()->first();
                $unitSize = \App\Models\UnitSize::inRandomOrder()
                    ->whereType('rusun')
                    ->first();

                DB::table('dwelling_plans')->insert([

                    'proposal_id'  => $proposal->id,
                    // tipe unit, ukuran unit, jumlah unit, rencana anggaran
                    'unit_type_id' => $unitType->id,
                    'unit_size_id' => $unitSize->id,
                    'amount'       => random_int(1, 10),
                    'budget_plan'  => random_int(10000000, 9999999999999)
                ]);
            }
        }
    }

    private function swadaya()
    {
        $unit = \App\Models\Unit::whereSlug(\App\Models\Unit::SWADAYA['slug'])->first();
        $proposalSwadaya = \App\Models\Proposal::whereUnitId($unit->id)->get();

        foreach ($proposalSwadaya as $proposal) {
            $count = random_int(2, 20);

            for ($i = 1; $i <= $count; $i ++) {

                $region = \App\Models\Region::inRandomOrder()->first();

                $faker = \Faker\Factory::create();

                DB::table('dwelling_plans')->insert([

                    'proposal_id' => $proposal->id,
                    'location_id' => $region->id,
                    'latitude'    => $faker->latitude,
                    'longitude'   => $faker->longitude,
                    'amount'      => $faker->numberBetween(1, 10),
                    'budget_plan' => $faker->numberBetween(10000000, 9999999999999)
                ]);
            }
        }
    }

    private function rusus()
    {
        $unit = \App\Models\Unit::whereSlug(\App\Models\Unit::RUSUS['slug'])->first();
        $proposalRusus = \App\Models\Proposal::whereUnitId($unit->id)->get();

        foreach ($proposalRusus as $proposal) {
            $count = random_int(2, 20);

            for ($i = 1; $i <= $count; $i ++) {

                $type = \App\Models\DwellingTypes::inRandomOrder()->first();
                $unitSize = \App\Models\UnitSize::inRandomOrder()
                    ->whereType('rusus')
                    ->first();

                DB::table('dwelling_plans')->insert([

                    'proposal_id'      => $proposal->id,
                    'dwelling_type_id' => $type->id,
                    'unit_size_id'     => $unitSize->id,
                    'amount'           => random_int(1, 10),
                    'budget_plan'      => random_int(10000000, 9999999999999)
                ]);
            }
        }
    }

    private function ruk()
    {
        $unit = \App\Models\Unit::whereSlug(\App\Models\Unit::RUK['slug'])->first();
        $proposalRuk = \App\Models\Proposal::whereUnitId($unit->id)->get();

        foreach ($proposalRuk as $proposal) {
            $count = random_int(2, 20);

            for ($i = 1; $i <= $count; $i ++) {

                $component = \App\Models\DwellingComponent::inRandomOrder()->first();

                DB::table('dwelling_plans')->insert([

                    'proposal_id'  => $proposal->id,
                    'component_id' => $component->id,
                    'amount'       => random_int(1, 10),
                    'budget_plan'  => random_int(10000000, 9999999999999)
                ]);
            }
        }
    }
}
