<?php

use Illuminate\Database\Seeder;

class ProposalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Proposal::class, 100)->create();

        $this->cleanUnrelatedUnit();
    }

    private function cleanUnrelatedUnit()
    {
        \App\Models\Proposal::whereProgramId(99999)->delete();
        \App\Models\Proposal::whereRecipientId(99999)->delete();
    }
}
