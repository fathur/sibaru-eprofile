<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->masterSeeder();

        $this->semiMasterSeeder();

        if (app()->environment(['local', 'development']))
            $this->fakeSeeder();

    }

    protected function masterSeeder()
    {
        if (DB::table('roles')->count() == 0)
            $this->call(RoleTableSeeder::class);

        if (DB::table('units')->count() == 0)
            $this->call(UnitTableSeeder::class);

        if (DB::table('regions')->count() == 0)
            $this->call(RegionTableSeeder::class);

        if (DB::table('agencies')->count() == 0)
            $this->call(AgencyTableSeeder::class);

        if (DB::table('statuses')->count() == 0)
            $this->call(StatusTableSeeder::class);

        if (DB::table('users')->count() == 0)
            $this->call(MainUserSeeder::class);


        if (DB::table('dwelling_types')->count() == 0)
            $this->call(DwellingTypesTableSeeder::class);

        if (DB::table('unit_sizes')->count() == 0)
            $this->call(UnitSizesTableSeeder::class);

        if (DB::table('dwelling_components')->count() == 0)
            $this->call(DwellingComponentTableSeeder::class);
    }

    /**
     *
     */
    protected function semiMasterSeeder()
    {
        if (DB::table('recipients')->count() == 0)
            $this->call(RecipientTableSeeder::class);

        if (DB::table('programs')->count() == 0)
            $this->call(ProgramsTableSeeder::class);

        $this->call(AgencyUserSeeder::class);
    }

    protected function fakeSeeder()
    {
        $this->call(ProposalTableSeeder::class);
        $this->call(UnitTypesTableSeeder::class);
        $this->call(DwellingPlansTableSeeder::class);
        $this->call(CriteriaTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
    }
}
