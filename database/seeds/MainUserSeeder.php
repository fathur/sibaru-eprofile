<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class MainUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'     => 'Super Administrator',
                'username' => 'administrator',
                'email'    => 'admin@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('administrator')->first()->id,
                'unit_id'  => null
            ],
            [
                'name'     => 'Admin Pusat',
                'username' => 'admin_pusat',
                'email'    => 'admin.pusat@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('admin-central')->first()->id,
                'unit_id'  => null
            ],
            [
                'name'     => 'Admin Rusun',
                'username' => 'admin_rusun',
                'email'    => 'admin.rusun@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('admin-technical')->first()->id,
                'unit_id'  => \App\Models\Unit::whereSlug('rusun')->first()->id
            ],
            [
                'name'     => 'Admin Rusus',
                'username' => 'admin_rusus',
                'email'    => 'admin.rusus@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('admin-technical')->first()->id,
                'unit_id'  => \App\Models\Unit::whereSlug('rusus')->first()->id
            ],
            [
                'name'     => 'Admin RUK',
                'username' => 'admin_ruk',
                'email'    => 'admin.ruk@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('admin-technical')->first()->id,
                'unit_id'  => \App\Models\Unit::whereSlug('ruk')->first()->id
            ],
            [
                'name'     => 'Admin Swadaya',
                'username' => 'admin_swadaya',
                'email'    => 'admin.swadaya@pu.go.id',
                'password' => bcrypt('sibaru123456'),
                'role_id'  => Role::whereSlug('admin-technical')->first()->id,
                'unit_id'  => \App\Models\Unit::whereSlug('swadaya')->first()->id
            ],
        ]);
    }
}
