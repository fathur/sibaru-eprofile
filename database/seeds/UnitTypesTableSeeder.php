<?php

use Illuminate\Database\Seeder;

class UnitTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rusun = \App\Models\Unit::whereSlug(\App\Models\Unit::RUSUN['slug'])->first();

        $recipients = \App\Models\Recipient::whereUnitId($rusun->id)->get();

        foreach ($recipients as $recipient) {

            $count = random_int(1, 3);

            for ($i = 1; $i <= $count; $i ++) {

                $faker = Faker\Factory::create();

                DB::table('unit_types')->insert([
                    'recipient_id' => $recipient->id,
                    'name'         => $faker->sentence
                ]);
            }
        }
    }
}
