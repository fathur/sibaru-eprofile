<?php

use Illuminate\Database\Seeder;

class ProposalCriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proposals = \App\Models\Proposal::all();

        foreach ($proposals as $proposal) {

            $count = random_int(5, 10);

            for ($i = 1; $i <= $count; $i ++) {

                $faker = \Faker\Factory::create();

                $criteria = \App\Models\Criteria::inRandomOrder()->first();

                $e = DB::table('proposal_criteria')
                    ->where('proposal_id', $proposal->id)
                    ->where('criteria_id', $criteria->id)
                    ->first();

                if (!$e) {

                    DB::table('proposal_criteria')->insert([
                        'proposal_id'  => $proposal->id,
                        'criteria_id'  => $criteria->id,
                        'attachment'   => $faker->file('storage/framework/cache', 'public/upload'),
                        'file_name'    => kebab_case($faker->name) . '.' . $faker->fileExtension,
                        'is_verified'  => false,
                        'is_published' => true
                    ]);
                }
            }
        }
    }
}
