<?php

use Illuminate\Database\Seeder;

class DwellingComponentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dwelling_components')->insert([
            ['name' => 'Jalan'],
            ['name' => 'Ruang terbuka non hijau'],
            ['name' => 'Sanitasi'],
            ['name' => 'Air minum'],
            ['name' => 'Rumah ibadah'],
            ['name' => 'Jaringan listrik'],
            ['name' => 'Penerangan jalan umum'],
        ]);
    }
}
