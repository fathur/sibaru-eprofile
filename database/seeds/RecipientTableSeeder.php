<?php

use Illuminate\Database\Seeder;

class RecipientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->rusun();
        $this->rusus();
        $this->ruk();
        $this->swadaya();
    }

    protected function rusun()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUSUN['slug'])->first();

        DB::table('recipients')->insert([
            ['unit_id' => $unit->id, 'name' => 'Lannjut Usia, Masyarakat sangat Miskin, Yatim Piatu, dan/atau Anak Terlantar', 'abbreviation' => 'LU'],
            ['unit_id' => $unit->id, 'name' => 'PNS, Pegawai Negeri di lingkungan TNI / POLRI', 'abbreviation' => 'POLRI'],
            ['unit_id' => $unit->id, 'name' => 'Petugas kawasan perbatasan, pekerja daerah tertinggal, atlet', 'abbreviation' => 'MH'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat Berpenghasilan Rendah (MBR)', 'abbreviation' => 'MBR'],
            ['unit_id' => $unit->id, 'name' => 'Mahasiswa / Siswa', 'abbreviation' => 'MH'],
            ['unit_id' => $unit->id, 'name' => 'Santri', 'abbreviation' => 'SA'],
            ['unit_id' => $unit->id, 'name' => 'Pekerja Paramedis', 'abbreviation' => 'PP'],
            ['unit_id' => $unit->id, 'name' => 'Peneliti , Tenaga Pendidik / Kependidikan', 'abbreviation' => 'PT'],
            ['unit_id' => $unit->id, 'name' => 'Atlet', 'abbreviation' => 'AT'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat Nelayan', 'abbreviation' => 'MN'],
            ['unit_id' => $unit->id, 'name' => 'Pekerja Industri', 'abbreviation' => 'PI'],

        ]);
    }

    private function swadaya()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::SWADAYA['slug'])->first();

        DB::table('recipients')->insert([

            ['unit_id' => $unit->id, 'name' => 'Masyarakat Berpenghasilan Rendah', 'abbreviation' => 'MBR'],
        ]);
    }

    private function ruk()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUK['slug'])->first();
        DB::table('recipients')->insert([

            ['unit_id' => $unit->id, 'name' => 'Masyarakat Berpenghasilan Rendah (MBR) yang menempati rumah umum berupa rumah tunggal, rumah deret dan rumah susun ', 'abbreviation' => 'MBR']
        ]);
    }

    private function rusus()
    {
        $unit = DB::table('units')->where('slug', \App\Models\Unit::RUSUS['slug'])->first();
        DB::table('recipients')->insert([

            ['unit_id' => $unit->id, 'name' => 'Masyarakat Nelayan', 'abbreviation' => 'MN'],
            ['unit_id' => $unit->id, 'name' => 'Prajurit dan/atau Petugas Negara', 'abbreviation' => 'TNI/POLRI'],
            ['unit_id' => $unit->id, 'name' => 'Korban Bencana', 'abbreviation' => 'MKB'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat yang bertempat tinggal di Pulau Terluar, Terpencil, atau Pedalaman', 'abbreviation' => 'MPT'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat yang terkena Program Pembangunan Pemerintah', 'abbreviation' => 'MPPP'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat dalam Wilayah Pengolah Sumberdaya Alam', 'abbreviation' => 'MS'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat yang memerlukan Penanganan Khusus', 'abbreviation' => 'MMPKL'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat dalam kawasan Cagar Budaya', 'abbreviation' => 'MCB'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat di Wilayah Perbatasan Negara', 'abbreviation' => 'MPN'],
            ['unit_id' => $unit->id, 'name' => 'Transmigran', 'abbreviation' => 'TR'],
            ['unit_id' => $unit->id, 'name' => 'Lanjut Usia, Miskin, Yatim Piatu dan/atau Anak Terlantar', 'abbreviation' => 'LU'],
            ['unit_id' => $unit->id, 'name' => 'Masyarakat di Lokasi Rawan Resiko Sosial', 'abbreviation' => 'MRS']
        ]);
    }
}
