<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        if (app()->environment(['production', 'staging'])) {
            $this->production();
//        } elseif (app()->environment(['local', 'development'])) {
//            $this->development();
//        }

        \App\Models\Region::fixTree();
    }

    protected function production()
    {
        $files = Storage::disk('local')->allFiles('resources/regions');

        foreach ($files as $file) {
            $data = Storage::disk('local')->get($file);
            $data = json_decode($data);

            foreach ($data as $provinsi => $districts) {

                // Insert provinsi
                DB::table('regions')->insert([
                    'name'       => $provinsi,
                    'level'      => 1, // provinsi
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]);

                $provinsiId = DB::table('regions')
                    ->select('id')
                    ->where('level', 1)
                    ->where('name', $provinsi)
                    ->first()->id;

                foreach ($districts as $kabupaten => $subDistricts) {

                    // Insert Kabupaten/Kota
                    DB::table('regions')->insert([
                        'name'       => $kabupaten,
                        'parent_id'  => $provinsiId,
                        'level'      => 2, // kota/kabupaten
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ]);

                    $kabupatenId = DB::table('regions')
                        ->select('id')
                        ->where('level', 2)
                        ->where('name', $kabupaten)
                        ->where('parent_id', $provinsiId)
                        ->first()->id;

                    foreach ($subDistricts as $kecamatan => $villages) {

                        // Insert Kecamatan
                        DB::table('regions')->insert([
                            'name'       => $kecamatan,
                            'parent_id'  => $kabupatenId,
                            'level'      => 3, // kecamatan
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now(),
                        ]);

                        $kecamatanId = DB::table('regions')
                            ->select('id')
                            ->where('level', 3)
                            ->where('name', $kecamatan)
                            ->where('parent_id', $kabupatenId)
                            ->first()->id;

                        foreach ($villages as $village) {

                            // Insert desa/kelurahan
                            DB::table('regions')->insert([
                                'name'       => $village,
                                'parent_id'  => $kecamatanId,
                                'level'      => 4, // desa/kelurahan
                                'created_at' => \Carbon\Carbon::now(),
                                'updated_at' => \Carbon\Carbon::now(),
                            ]);
                        }
                    }
                }
            }
        }
    }

    protected function development()
    {
        $files = Storage::disk('local')->allFiles('resources/regions');
        $file = $files[array_rand($files)];

        $data = Storage::disk('local')->get($file);
        $data = json_decode($data);

        foreach ($data as $provinsi => $districts) {

            // Insert provinsi
            DB::table('regions')->insert([
                'name'       => $provinsi,
                'level'      => 1, // provinsi
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);

            $provinsiId = DB::table('regions')
                ->select('id')
                ->where('level', 1)
                ->where('name', $provinsi)
                ->first()->id;

            foreach ($districts as $kabupaten => $subDistricts) {

                // Insert Kabupaten/Kota
                DB::table('regions')->insert([
                    'name'       => $kabupaten,
                    'parent_id'  => $provinsiId,
                    'level'      => 2, // kota/kabupaten
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]);

                $kabupatenId = DB::table('regions')
                    ->select('id')
                    ->where('level', 2)
                    ->where('name', $kabupaten)
                    ->where('parent_id', $provinsiId)
                    ->first()->id;

                foreach ($subDistricts as $kecamatan => $villages) {

                    // Insert Kecamatan
                    DB::table('regions')->insert([
                        'name'       => $kecamatan,
                        'parent_id'  => $kabupatenId,
                        'level'      => 3, // kecamatan
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ]);

                    $kecamatanId = DB::table('regions')
                        ->select('id')
                        ->where('level', 3)
                        ->where('name', $kecamatan)
                        ->where('parent_id', $kabupatenId)
                        ->first()->id;

                    foreach ($villages as $village) {

                        // Insert desa/kelurahan
                        DB::table('regions')->insert([
                            'name'       => $village,
                            'parent_id'  => $kecamatanId,
                            'level'      => 4, // desa/kelurahan
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now(),
                        ]);
                    }
                }
            }
        }
    }
}
