<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'slug' => 'proposal-draft',
                'name' => 'Draft',
                'description' => 'Menyimpan data usulan dan belum dikirim ke pusat'
            ],
            [
                'slug' => 'proposal-new',
                'name' => 'Usulan Baru',
                'description' => 'Mengirim data usulan'
            ],
            [
                'slug' => 'administration-verifying',
                'name' => 'Verifikasi Administrasi',
                'description' => 'Mendistribusikan usulan yang masuk'
            ],
            [
                'slug' => 'administration-revising',
                'name' => 'Revisi Administrasi',
                'description' => 'Merevisi data usulan'
            ],
            [
                'slug' => 'administration-repaired',
                'name' => 'Perbaikan',
                'description' => 'Administrasi telah diperbaiki'
            ],
//            [
//                'slug' => 'administration-passed',
//                'name' => 'Lolos Administrasi',
//                'description' => 'Menyetujui data usulan'
//            ],
            [
                'slug' => 'administration-rejected',
                'name' => 'Ditolak Administrasi',
                'description' => 'Menolak data usulan'
            ],
            [
                'slug' => 'technical-surveyor-verifying',
                'name' => 'Administrasi Lengkap',
                'description' => 'Menginput data surveyor'
            ],
            [
                'slug' => 'technical-survey-verifying',
                'name' => 'Verifikasi Teknis',
                'description' => 'Menginput data surveyor'
            ],
            [
                'slug' => 'technical-passed',
                'name' => 'Memenuhi Teknis',
                'description' => 'Menyetujui usulan yang sudah diinputkan hasil surveynya, masuk ke queue shortlist'
            ],
            [
                'slug' => 'technical-rejected',
                'name' => 'Usulan Ditolak',
                'description' => 'Menolak usulan yang sudah diinputkan hasil surveynya'
            ],
            [
                'slug' => 'proposal-passed',
                'name' => 'Usulan Diterima',
                'description' => 'Masuk shortlist'
            ],
//            [
//                'slug' => 'technical-revising',
//                'name' => 'Perbaikan',
//                'description' => 'Melakukan perbaikan dan mengirim lagi usulan'
//            ],
        ]);
    }
}
