<?php

use Illuminate\Database\Seeder;

class DwellingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dwelling_types')->insert([
            ['name' => 'Permanen'],
            ['name' => 'Panggung'],
        ]);
    }
}
