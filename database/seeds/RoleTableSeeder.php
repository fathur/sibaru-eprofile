<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'slug'  => 'administrator',
                'name'  => 'Administrator',
                'description' => 'Sistem admin, dapat melakukan apa saja'
            ],
            [
                'slug'  => 'admin-central',
                'name'  => 'Admin pusat',
                'description' => 'Admin pusat Dirjen PU'
            ],
            [
                'slug'  => 'admin-technical',
                'name'  => 'Admin teknis',
                'description' => 'Admin teknis Dirjen PU'
            ],
            [
                'slug'  => 'proposer',
                'name'  => 'Pengaju usulan',
                'description' => 'Pengaju usulan, biasanya kota/kabupaten/lembaga lainnya'
            ],
            [
                'slug'  => 'watcher',
                'name'  => 'Pengawas pengusul',
                'description' => 'Biasanya pemerintah provinsi, untuk mengawasi usulan dari kota/kabupaten.'
            ],
        ]);
    }
}
