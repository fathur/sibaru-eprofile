<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_criteria', function(Blueprint $table) {
            $table->unsignedInteger('proposal_id');
            $table->unsignedInteger('criteria_id');
            $table->string('attachment')->nullable();
            $table->string('file_name')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_published')->default(true);
            $table->integer('point')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();

            $table->unique(['proposal_id', 'criteria_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal_criteria');

    }
}
