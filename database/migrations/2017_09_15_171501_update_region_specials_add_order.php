<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionSpecialsAddOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_specials', function (Blueprint $table) {
            $table->integer('order')->nullable();
            $table->float('volume')->change();
            $table->float('budget')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_specials', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
