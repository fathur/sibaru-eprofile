<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDwellingPlansTableAddTypeAndTowerBlockFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dwelling_plans', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->string('tower_block')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dwelling_plans', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('tower_block');
        });
    }
}
