<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEprofilesTableRelateToProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->dropColumn('decree_id');
            $table->unsignedInteger('proposal_id')->nullable();

            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('program_id');

            $table->unsignedInteger('proposer_id'); // pengusul yang diiinginkan, hanya bisa diisi oleh admin, selebihnya dari session login
            $table->unsignedInteger('location_id');
            $table->unsignedInteger('recipient_id');
            $table->string('proposer_remark')->nullable();
            $table->text('address')->nullable();

            // nih yang baru
            $table->integer('amount_unit');
            $table->bigInteger('total_budget');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->dropColumn('proposal_id');
            $table->dropColumn('unit_id');
            $table->dropColumn('program_id');
            $table->dropColumn('proposer_id');
            $table->dropColumn('recipient_id');
            $table->dropColumn('proposer_remark');
            $table->dropColumn('address');
            $table->dropColumn('amount_unit');
            $table->dropColumn('total_budget');
        });
    }
}
