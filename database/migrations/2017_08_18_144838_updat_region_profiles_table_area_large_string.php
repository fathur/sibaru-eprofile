<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatRegionProfilesTableAreaLargeString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->string('area_large')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->double('area_large')->nullable()->change();

        });
    }
}
