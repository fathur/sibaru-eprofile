<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionProfilesTableChangeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->float('latitude')->change();
            $table->float('longitude')->change();
            $table->float('sea_large')->change();
            $table->float('land_large')->change();
            $table->float('population_growth')->change();
            $table->float('population_density')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->integer('latitude')->change();
            $table->integer('longitude')->change();
            $table->integer('sea_large')->change();
            $table->integer('land_large')->change();
            $table->integer('population_growth')->change();
            $table->integer('population_density')->change();
        });
    }
}
