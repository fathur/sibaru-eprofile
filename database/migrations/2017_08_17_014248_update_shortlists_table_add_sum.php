<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShortlistsTableAddSum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shortlists', function (Blueprint $table) {
            $table->integer('sum_amount')->nullable();
            $table->bigInteger('sum_budget_plan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shortlists', function (Blueprint $table) {
            $table->dropColumn('sum_amount');
            $table->dropColumn('sum_budget_plan');
        });
    }
}
