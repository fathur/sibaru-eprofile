<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShortlistBudgetsTableAddShortlistIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shortlist_budgets', function (Blueprint $table) {
            $table->unsignedInteger('shortlist_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shortlist_budgets', function (Blueprint $table) {
            $table->dropColumn('shortlist_id');
        });
    }
}
