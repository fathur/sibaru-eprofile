<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('proposer_id'); // pengusul yang diiinginkan, hanya bisa diisi oleh admin, selebihnya dari session login
            $table->unsignedInteger('creator_id'); // loged in user who created
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('recipient_id');
            $table->unsignedInteger('location_id')->nullable();
            $table->string('proposer_remark')->nullable();
            $table->string('proposer_phone')->nullable();
            $table->string('proposer_email')->nullable();
            $table->integer('proposed_year')->nullable();
            $table->text('address')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->date('submission_date')->nullable();
            $table->integer('area_wide')->nullable();
            $table->integer('area_length')->nullable();
            $table->integer('area_width')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
