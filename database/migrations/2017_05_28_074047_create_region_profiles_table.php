<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('region_id');
            $table->integer('year');
            $table->string('head_name');
            $table->string('deputy_name');
            $table->string('head_photo');
            $table->string('deputy_photo');
            $table->string('head_party');
            $table->string('deputy_party');
            $table->date('period_start')->nullable();
            $table->date('period_end')->nullable();
            $table->string('perkim');
            $table->string('perkim_pic_name');
            $table->string('perkim_phone');
            $table->string('perkim_fax');
            $table->string('perkim_email');
            $table->integer('sea_large');
            $table->integer('land_large');
            $table->integer('latitude');
            $table->integer('longitude');
            $table->integer('total_man');
            $table->integer('total_woman');
            $table->integer('population_growth');
            $table->integer('population_density');
            $table->integer('total_city_poor_people');
            $table->integer('total_village_poor_people');
            $table->integer('total_district')->nullable();
            $table->integer('total_city')->nullable();
            $table->integer('apbd_amount');
            $table->integer('pad_pajak');
            $table->integer('pad_retribusi');
            $table->integer('pad_sda');
            $table->integer('pad_others');
            $table->integer('backlog_house');
            $table->integer('household')->comment('rumah tangga');
            $table->string('image');
            $table->bigInteger('pupr_budget');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region_profiles');
    }
}
