<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionProfilesTableToFloat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->float('sea_large')->change();
            $table->float('land_large')->change();
            $table->float('area_large')->change();
            $table->float('latitude')->change();
            $table->float('longitude')->change();
            $table->float('total_man')->change();
            $table->float('total_woman')->change();
            $table->float('total_people')->change();
            $table->float('population_growth')->change();
            $table->float('population_density')->change();
            $table->float('total_city_poor_people')->change();
            $table->float('total_village_poor_people')->change();
            $table->float('total_district')->change();
            $table->float('total_city')->change();
            $table->float('backlog_house')->change();
            $table->float('household')->change();
            $table->float('rtlh')->change();
            $table->float('backlog_kepemilikan')->change();
            $table->string('perkim_kelembagaan')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            //
        });
    }
}
