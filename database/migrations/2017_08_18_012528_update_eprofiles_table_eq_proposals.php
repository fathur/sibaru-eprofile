<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEprofilesTableEqProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->unsignedInteger('creator_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();
            $table->string('proposer_phone')->nullable();
            $table->string('proposer_email')->nullable();
            $table->integer('proposed_year')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->date('submission_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->dropColumn('creator_id');
            $table->dropColumn('status_id');
            $table->dropColumn('proposer_phone');
            $table->dropColumn('proposer_email');
            $table->dropColumn('proposed_year');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('submission_date');
        });
    }
}
