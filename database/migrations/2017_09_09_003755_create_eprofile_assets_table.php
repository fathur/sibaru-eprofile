<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEprofileAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // drop tabel yang salah ini
        \Schema::dropIfExists('region_profile_assets');

        Schema::create('eprofile_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('eprofile_id');
            $table->text('url');
            $table->string('filename');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eprofile_assets');
    }
}
