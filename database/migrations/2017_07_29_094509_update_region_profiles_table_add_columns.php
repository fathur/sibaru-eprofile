<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionProfilesTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->string('koordinat')->nullable();
            $table->integer('masa_bakti')->nullable();
            $table->integer('area_large')->nullable();
            $table->integer('perkim_kelembagaan')->nullable();
            $table->integer('rtlh')->nullable();
            $table->integer('backlog_kepemilikan')->nullable();
            $table->integer('total_people')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->dropColumn('koordinat');
            $table->dropColumn('masa_bakti');
            $table->dropColumn('area_large');
            $table->dropColumn('perkim_kelembagaan');
            $table->dropColumn('rtlh');
            $table->dropColumn('backlog_kepemilikan');
            $table->dropColumn('total_people');
        });
    }
}
