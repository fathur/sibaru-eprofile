<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionProfilesTableChangeToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {

            // I will regret this :'(
            $table->string('population_growth')->nullable()->change();
            $table->string('sea_large')->nullable()->change();
            $table->string('land_large')->nullable()->change();
            $table->string('total_man')->nullable()->change();
            $table->string('total_woman')->nullable()->change();
            $table->string('population_density')->nullable()->change();
            $table->string('total_district')->nullable()->change();
            $table->string('total_city')->nullable()->change();
            $table->string('total_people')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->dropColumn('period');
        });
    }
}
