<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRegionProfilesTableSetNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            $table->string('head_name')->nullable()->change();
            $table->string('deputy_name')->nullable()->change();
            $table->string('head_photo')->nullable()->change();
            $table->string('deputy_photo')->nullable()->change();
            $table->string('head_party')->nullable()->change();
            $table->string('deputy_party')->nullable()->change();
//            $table->integer('period_start')->nullable()->change();
//            $table->integer('period_end')->nullable()->change();
            $table->string('perkim')->nullable()->change();
            $table->string('perkim_pic_name')->nullable()->change();
            $table->string('perkim_phone')->nullable()->change();
            $table->string('perkim_fax')->nullable()->change();
            $table->string('perkim_email')->nullable()->change();
            $table->integer('sea_large')->nullable()->change();
            $table->integer('land_large')->nullable()->change();
            $table->integer('latitude')->nullable()->change();
            $table->integer('longitude')->nullable()->change();
            $table->integer('total_man')->nullable()->change();
            $table->integer('total_woman')->nullable()->change();
            $table->integer('population_growth')->nullable()->change();
            $table->integer('population_density')->nullable()->change();
            $table->integer('total_city_poor_people')->nullable()->change();
            $table->integer('total_village_poor_people')->nullable()->change();
            $table->integer('total_district')->nullable()->nullable()->change();
            $table->integer('total_city')->nullable()->nullable()->change();
            $table->integer('apbd_amount')->nullable()->change();
            $table->integer('pad_pajak')->nullable()->change();
            $table->integer('pad_retribusi')->nullable()->change();
            $table->integer('pad_sda')->nullable()->change();
            $table->integer('pad_others')->nullable()->change();
            $table->integer('backlog_house')->nullable()->change();
            $table->integer('household')->comment('rumah tangga')->nullable()->change();
            $table->string('image')->nullable()->change();
            $table->bigInteger('pupr_budget')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region_profiles', function (Blueprint $table) {
            //
        });
    }
}
