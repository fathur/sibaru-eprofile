<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDwellingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dwelling_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proposal_id');
            $table->unsignedInteger('location_id')->nullable();
            $table->unsignedInteger('component_id')->nullable();
            $table->unsignedInteger('dwelling_type_id')->nullable();
            $table->unsignedInteger('unit_size_id')
                ->nullable()
                ->comment('both rusun and rusus');
            $table->unsignedInteger('unit_type_id')->nullable();
            $table->integer('amount')->nullable();
            $table->bigInteger('budget_plan');
            $table->double('latitude')
                ->nullable()
                ->comment('khusus untuk rumah swadaya');
            $table->double('longitude')
                ->nullable()
                ->comment('khusus untuk rumah swadaya');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dwelling_plans');
    }
}
