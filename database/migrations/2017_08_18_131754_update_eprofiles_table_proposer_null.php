<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEprofilesTableProposerNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void≤
     */
    public function up()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->unsignedInteger('proposer_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eprofiles', function (Blueprint $table) {
            $table->unsignedInteger('proposer_id')->change();

        });
    }
}
