<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\Proposal::class, function (\Faker\Generator $faker) {

    $unit = \App\Models\Unit::inRandomOrder()->first();

    $program = \App\Models\Program::whereUnitId($unit->id)->inRandomOrder()->first();

    $recipient = \App\Models\Recipient::whereUnitId($unit->id)->inRandomOrder()->first();

    $proposer = \App\Models\Agency::inRandomOrder()->first();

    return [
        'unit_id'         => $unit->id,
        'proposer_id'     => $proposer->id,
        'creator_id'      => \App\Models\User::whereUsername('administrator')->first()->id,
        'status_id'       => \App\Models\Status::inRandomOrder()->first()->id,
        'program_id'      => $program->id ?? 99999,
        'recipient_id'    => $recipient->id ?? 99999,
        'location_id'     => \App\Models\Region::inRandomOrder()->first()->id,
        'proposer_remark' => $faker->sentence,
        'proposer_phone'  => $faker->phoneNumber,
        'proposer_email'  => $faker->email,
        'proposed_year'   => $faker->dateTimeBetween('now', '+10 years')->format('Y'),
        'address'         => $faker->address,
        'latitude'        => $faker->latitude,
        'longitude'       => $faker->longitude,
        'submission_date' => $faker->dateTimeBetween('-5 years', 'now')
    ];

});

$factory->define(\App\Models\Criteria::class, function (\Faker\Generator $faker) {

    $unit = \App\Models\Unit::inRandomOrder()->first();

    return [
        'unit_id' => $unit->id,
        'type'    => $faker->randomElement(['administration', 'technical', 'others']),
        'weight'  => $faker->randomDigitNotNull,
        'content' => $faker->sentence(),
//        'template' => $faker->file(),
        'order'   => $faker->randomDigitNotNull
    ];
});

$factory->define(\App\Models\Comment::class, function (\Faker\Generator $faker) {

    $proposal = \App\Models\Proposal::inRandomOrder()->first();
    $user = \App\Models\User::inRandomOrder()->first();

    return [
        'text'        => $faker->paragraph,
        'proposal_id' => $proposal->id,
        'user_id'     => $user->id
    ];
});


$factory->define(\App\Models\Faq::class, function(\Faker\Generator $faker) {
    return [
        'question'  => $faker->sentence(10),
        'answer'    => $faker->paragraph()
    ];
});

$factory->define(\App\Models\Document::class, function(\Faker\Generator $faker) {
    return [
        'title'  => $faker->sentence(10),
        'description'    => $faker->paragraph(),
        'file'  => $faker->sentence
    ];
});