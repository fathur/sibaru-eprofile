function saveForm(dom) {

    var identifier = $(dom).data('identifier');

    var id = $(dom).data('id');

    var $identifier = $('#' + identifier);

    var url = $identifier.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: $identifier.serialize(), // serializes the form's elements.
        success: function (data) {
            // close modal
            $('#general-modal').modal('hide');

            // reload datatables\
            $('#' + identifier + '-datatables').DataTable().ajax.reload(null, false);

            // kasih centang gede
            swal({
                title: 'Good job!',
                text: 'You add one data!',
                timer: 2000,
                type: 'success'
            });


        },
        error: function (jqXHR, textStatus, errorThrown) {

            $identifier.find('.form-group').removeClass('has-error');
            $identifier.find('.form-group').find('.help-block').html('');

            var data = $.parseJSON(jqXHR.responseText);

            $.each(data, function (item, errors) {

                var html = '<ul class="list-unstyled">';

                $.each(errors, function (index, error) {

                    html += '<li>' + error + '</li>';
                });

                html += '</ul>';

                $('#help-block-' + item).html(html);
                $('#help-block-' + item).parent().addClass('has-error');

            });
        }
    });
}

function editForm(dom) {
    var modalId = 'general-modal';
    var $modalId = $('#' + modalId);

    $modalId.modal('show');

    var url = $(dom).data('form');

    $.get(url, function (response) {

        $modalId.find('.modal-header').find('.modal-title').html(response.title);
        $modalId.find('.modal-body').html(response.body);
        $modalId.find('.modal-footer').html(response.footer);
    });
}

function deleteForm(dom) {

    var url = $(dom).data('link');
    var identifier = $(dom).data('identifier');

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {

        $.ajax({
            type: "DELETE",
            url: url,
            data: {
                _token: $("meta[name=csrf-token]").attr('content')
            }, // serializes the form's elements.
            success: function (data) {

                // reload datatables\
                $('#' + identifier + '-datatables').DataTable().ajax.reload(null, false);

                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )

            }
        });
    })
}

function editPassword(dom) {
    var modalId = 'general-modal';
    var $modalId = $('#' + modalId);

    $modalId.modal('show');

    var url = $(dom).data('url');

    $.get(url, {
        userId: $(dom).data('userId')
    }, function (response) {

        $modalId.find('.modal-header').find('.modal-title').html(response.title);
        $modalId.find('.modal-body').html(response.body);
        $modalId.find('.modal-footer').html(response.footer);
    });
}

function showGeneralModal(url) {
    var modalId = 'general-modal';
    var $modalId = $('#' + modalId);

    $modalId.find('.modal-header').find('.modal-title').html('');
    $modalId.find('.modal-body').html('');
    $modalId.find('.modal-footer').html('');

    $.get(url, function (response) {

        $modalId.find('.modal-header').find('.modal-title').html(response.title);
        $modalId.find('.modal-body').html(response.body);
        $modalId.find('.modal-footer').html(response.footer);
    });
}