@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Kriteria Administrasi</div>
                    </div>
                    <div class="actions">

                        <button class="btn btn-primary {{$identifier}}-trigger-modal"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                data-type="administration">
                            <i class="fa fa-plus"></i>&nbsp; Tambah kriteria
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-condensed table-hover table-striped" id="{{$identifier}}-administration-datatables">
                        <thead>
                        <tr>
                            <th>Order</th>
                            <th>Kriteria</th>
                            <th>Template</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Kriteria Teknis</div>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary {{$identifier}}-trigger-modal"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                data-type="technical"">
                            <i class="fa fa-plus"></i>&nbsp; Tambah kriteria
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-condensed table-hover table-striped" id="{{$identifier}}-technical-datatables">
                        <thead>
                        <tr>
                            <th>Order</th>
                            <th>Kriteria</th>
                            <th>Template</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#{{$identifier}}-administration-datatables').dataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: "{{route('criteria.data')}}",
            data: function(d) {
                d.unit = {{ $unit->id }};
                d.type = 'administration';
            }
        },
        columns: [
            {data: 'order', name: 'order'},
            {data: 'content', name: 'content'},
            {data: 'filename', name: 'filename', searchable: false, orderable: false},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

    $('#{{$identifier}}-technical-datatables').dataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: "{{route('criteria.data')}}",
            data: function(d) {
                d.unit = {{ $unit->id }};
                d.type = 'technical';
            }
        },
        columns: [
            {data: 'order', name: 'order'},
            {data: 'content', name: 'content'},
            {data: 'filename', name: 'filename', searchable: false, orderable: false},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

    $('.{{$identifier}}-trigger-modal').click(function () {

        var $this = $(this);
        var type = $this.data('type');

        showGeneralModal('{{route('criteria.create')}}?unit={{ $unit->slug }}&type='+type);
    });
</script>
@endpush