<form action="{{route('criteria.update', [$criteria->id])}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="order" class="control-label">Nomor</label>
        <input type="text" id="order" class="form-control" name="order" placeholder="Nomor"
               aria-describedby="help-block-order" value="{{$criteria->order}}">
        <div class="help-block" id="help-block-order"></div>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Kriteria</label>
        <textarea id="content" class="form-control" name="content" placeholder="Kriteria"
               aria-describedby="help-block-content">{{$criteria->content}}</textarea>
        <div class="help-block" id="help-block-content"></div>
    </div>

    <div class="form-group">
        <label for="template" class="control-label">Template</label>
        <div class="col-progress col-download">
            <div class="wrapper margin-bottom-15">{{$criteria->filename}}</div>
        </div>
        <input type="file" id="template" name="" placeholder="Template" class="simple-upload">
        <input type="hidden" name="template" value="{{$criteria->template}}" id="location-criteria">
        <input type="hidden" name="file_name" value="{{$criteria->filename}}" id="filename-criteria">
        <div class="help-block" id="help-block-template"></div>

    </div>
</form>


<script>
    $('.simple-upload').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-criteria');
        var $attachmentNameField = $this.parent().find('#filename-criteria');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });
</script>