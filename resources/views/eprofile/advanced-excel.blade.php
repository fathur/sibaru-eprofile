<form action="#">
    <div class="form-group">
        <label for="provinsi">Provinsi</label>
        <select type="text" id="provinsi" class="form-control"></select>
    </div>


    <div class="form-group">
        <label for="year-begin">Tahun Awal</label>

        <select name="year-begin" id="year-begin" class="form-control">
            @foreach($years as $year)
                <option value="{{$year}}">{{$year}}</option>
            @endforeach
        </select>

        {{--<input type="text" id="year-begin" class="form-control">--}}
    </div>

    <div class="form-group">
        <label for="year-end">Tahun akhir</label>

        <select name="year-end" id="year-end" class="form-control">
            @foreach($years as $year)
                <option value="{{$year}}">{{$year}}</option>
            @endforeach
        </select>

    </div>
</form>

<div class="btn-group">
    <button data-url="{{url('eprofile/advanced-excel/rincian')}}" class="btn btn-success" id="advex-rincian">
        <i class="fa fa-file-excel-o"></i> Rincian
    </button>
    <button data-url="{{url('eprofile/advanced-excel/rekap')}}" class="btn btn-success" id="advex-rekap-years">
        <i class="fa fa-file-excel-o"></i> Rekapitulasi
    </button>
    <button data-url="{{url('eprofile/advanced-excel/rekap')}}" class="btn btn-success" id="advex-rekap-year">
        <i class="fa fa-file-excel-o"></i> Rakapitulasi setahun
    </button>
</div>

<script>

    $('#provinsi').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('regions.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
//                    unit_id: $('#unit').val(),
                    page: params.page,
                    maxLevel: 2
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#advex-rincian').click(function () {

        var $this = $(this);
        var url = $this.data('url');

        var province = $('#provinsi').select2('val');
        var begin = $('#year-begin').val();
        var end = $('#year-end').val();


        var hitUrl = url+"?province="+province+"&begin="+begin+"&end="+end;
        console.log(hitUrl);

        window.open(hitUrl, '_blank');

//        $.get(url, {
//            province: $('#provinsi').select2('val'),
//            begin: $('#year-begin').val(),
//            end: $('#year-end').val(),
//        }, function () {
//            window
//        });
    });

    $('#advex-rekap-years').click(function () {
        var $this = $(this);
        var url = $this.data('url');

        var province = $('#provinsi').select2('val');
        var begin = $('#year-begin').val();
        var end = $('#year-end').val();

        window.open(url+"?province="+province+"&begin="+begin+"&end="+end, '_blank');


//        $.get(url, {
//            province: $('#provinsi').select2('val'),
//            begin: $('#year-begin').val(),
//            end: $('#year-end').val(),
//        }, function () {
//
//        });
    });

    $('#advex-rekap-year').click(function () {
        var $this = $(this);
        var url = $this.data('url');

        var province = $('#provinsi').select2('val');
        var begin = $('#year-begin').val();
        var end = $('#year-end').val();

        window.open(url+"?province="+province+"&begin="+begin, '_blank');

    });


</script>