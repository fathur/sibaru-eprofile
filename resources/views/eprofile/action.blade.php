<div class="btn-group btn-group-xs pull-right">

    @if(!is_null($data->proposal_id))
    <a href="{{route('proposals.edit', $data->proposal_id)}}" class="btn btn-default">
        <i class="fa fa-eye"></i>
    </a>
    @endif

    <a class="btn btn-warning" href="{{$edit_link}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </a>
    <button type="button" class="btn btn-danger" onclick="deleteForm(this)"
            data-link="{{$delete_link}}"
            data-identifier="{{$identifier}}">

        <i class="fa fa-trash"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Delete</span>
    </button>
</div>