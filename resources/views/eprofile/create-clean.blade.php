@extends('layout.main')
{{--{{dd($proposal->toArray())}}--}}

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>
                </div>
                <div class="portlet-body">

                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('eprofile.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label for="proposed_year" class="control-label">Tahun</label>
                                    <select name="proposed_year" id="proposed_year" class="form-control">
                                        @for($year = (\Carbon\Carbon::now()->year + 20); $year >= (\Carbon\Carbon::now()->year - 5); $year--)
                                            <option value="{{trim($year)}}" @if($year == \Carbon\Carbon::now()->year) selected @endif>{{$year}}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">

                                    <label for="unit" class="control-label">Direktorat</label>
                                    <select type="text" id="unit" name="unit" aria-describedby="help-block-unit"
                                            class="form-control select2">
                                        @foreach($units as $unit)
                                            <option value="{{$unit->id}}">{{$unit->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block" id="help-block-unit"></div>

                                </div>
{{--
                                <div class="form-group">
                                    <label for="proposer" class="control-label">Pengusul Bantuan</label>
                                    <br>
                                    <input type="text" id="proposer" name="proposer" placeholder="Pengusul Bantuan"
                                           aria-describedby="help-block-proposer" style="width: 100%;">
                                    <div class="help-block" id="help-block-proposer"></div>

                                </div>--}}

                                <div class="form-group">
                                    <label for="location" class="control-label">Lokasi</label>
                                    <br>
                                    <input type="text" id="location" name="location" placeholder="Lokasi"
                                           aria-describedby="help-block-location" style="width: 100%;">
                                    <div class="help-block" id="help-block-location"></div>

                                </div>
                                <div class="form-group">
                                    <label for="program" class="control-label">Kegiatan</label>
                                    <select name="program" id="program" class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient" class="control-label">Penerima manfaat</label>
                                    <select name="recipient" id="recipient" class="form-control"></select>
                                </div>

                                <div class="form-group">
                                    <label for="proposer_remark" class="control-label">Alamat/Keterangan pengusul</label>
                                    <textarea id="proposer_remark" name="proposer_remark" class="form-control">{{old('proposer_remark')}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="amount_unit" class="control-label">Jumlah unit</label>
                                    <input id="amount_unit" name="amount_unit" class="form-control" value="{{old('amount_unit')}}">
                                </div>

                                <div class="form-group">
                                    <label for="total_budget" class="control-label">Anggaran</label>
                                    <input id="total_budget" name="total_budget" class="form-control" value="{{old('total_budget')}}">
                                </div>

                                <div class="form-group">
                                    <label for="latitude" class="control-label">Latitude</label>
                                    <input id="latitude" name="latitude" class="form-control" value="{{old('latitude')}}">
                                </div>

                                <div class="form-group">
                                    <label for="longitude" class="control-label">Longitude</label>
                                    <input id="longitude" name="longitude" class="form-control" value="{{old('longitude')}}">
                                </div>
                                <hr>

                                <h3>Info Visual</h3>

                                @for($i = 1; $i <= \App\Http\Controllers\EprofileController::$totalAssetImages; $i++)
                                    <div class="form-group">
                                        <label for="asset_{{$i}}" class="control-label">Gambar {{$i}}</label>
                                        <div class="col-progress col-download">
                                            <div class="wrapper margin-bottom-15"></div>
                                        </div>
                                        <input type="file" id="asset_{{$i}}" name="asset_{{$i}}" placeholder="File">
                                        <input type="hidden" name="location_asset_{{$i}}" value="" id="location-asset_{{$i}}">
                                        <input type="hidden" name="filename_asset_{{$i}}" value="" id="filename-asset_{{$i}}">
                                        <div class="help-block" id="help-block-asset_{{$i}}"></div>
                                    </div>
                                @endfor
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-info">
                                    <i class="fa fa-floppy-o"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.easyui')

@push('scripts')
    <script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
    <script>
        $('.select2').select2({
            theme: "bootstrap"
        });

        $('#program').select2({
            theme: "bootstrap",
            ajax: {
                url: "{{route('programs.select')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                        unit_id: $('#unit').val()
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
//        allowClear: true,
//        minimumInputLength: 1,
            templateResult: function (data) {
                return data.name || data.text;
            },
            templateSelection: function (data) {
                return data.name || data.text;
            }
        });

        $('#recipient').select2({
            theme: "bootstrap",
            ajax: {
                url: "{{route('recipients.select')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                        unit_id: $('#unit').val()

                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
//        allowClear: true,
//        minimumInputLength: 1,
            templateResult: function (data) {
                return data.name || data.text;
            },
            templateSelection: function (data) {
                return data.name || data.text;
            }
        });

        $('#location').combotree({
            url: '{{route('regions.combotree')}}?level=4',
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34

        });

        @for($i = 1; $i <= \App\Http\Controllers\EprofileController::$totalAssetImages; $i++)
        $('#asset_{{$i}}').change(function () {

            var $this = $(this);
            var $thisProgressStatus =   $this.parent().find('.col-progress');
            var $thisDownload =   $this.parent().find('.col-download');
            var $attachmentField = $this.parent().find('#location-asset_{{$i}}');
            var $attachmentNameField = $this.parent().find('#filename-asset_{{$i}}');


            $(this).simpleUpload('{{route('assets.upload')}}', {
                name: 'file',
                data: {
                    _token: '{{csrf_token()}}'
                },
                progress: function(progress) {
                    var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                    $thisProgressStatus.find('.wrapper').html(htmlProgress);
                },
                success: function(data) {

                            {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                    var htmlLink = data.file_name;

                    $thisDownload.find('.progress').fadeOut();
                    $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                    $thisDownload.find('.wrapper').fadeIn();

                    $attachmentField.val(data.location);
                    $attachmentNameField.val(data.file_name);
                },
                error: function(error) {
                    console.log(error);
                    $thisDownload.find('.progress').fadeOut();
                    $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                    $thisDownload.find('.wrapper').fadeIn();
                }
            });
        });
        @endfor

        /*
                $('#proposer').combotree({
                    url: '{{route('agencies.combotree')}}',
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34

        });*/
    </script>
@endpush
