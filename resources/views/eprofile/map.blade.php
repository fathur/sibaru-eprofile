@extends('layout.main')
{{--{{dd($proposal->toArray())}}--}}

@section('content')
    <div class="row">


        <div class="col-sm-2">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Filter</div>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="#">
                        <div class="form-group">

                            <label for="year">Year</label>
                            <select name="year" id="year" class="form-control input-sm">
                                <option value="0">Semua</option>
                                @foreach($years as $year)
                                    <option value="{{$year->year}}">{{$year->year}}</option>
                                @endforeach
                            </select>
                        </div>

                        @cannot('do-technical')
                        <div class="form-group">

                            <label for="unit">Unit</label>
                            <select name="unit" id="unit" class="form-control input-sm">
                                <option value="0">Semua</option>
                                @foreach($units as $unit)
                                    <option value="{{$unit->slug}}">{{$unit->program}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endcannot

                        <button type="button" class="btn btn-info btn-sm" id="btn-filter-map">Filter</button>
                    </form>
                </div>
            </div>

            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Legenda</div>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul class="list-unstyled">
                        <li><img src="{{asset('img/rumah-susun.png')}}" alt="Rumah Susun"> Rusun</li>
                        <li><img src="{{asset('img/rumah-khusus.png')}}" alt="Rumah Khusus"> Rusus</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-sm-10">
            <div class="portlet">
                <div class="portlet-body">
                    <div id="map-canvas" style="height: 500px"></div>

                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
            type="text/javascript"></script>
@endpush


@push('script')
    <script>

        var map, marker, markers = [], infowindow;

        function initMap() {
            var myLatLng = new google.maps.LatLng(-3.0189086,118.7926493);

            var mapOptions = {
                zoom: 5,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: true,
                // scrollwheel: false,
                /*styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "hue": "#ff4400"
                            },
                            {
                                "saturation": -68
                            },
                            {
                                "lightness": -4
                            },
                            {
                                "gamma": 0.72
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.icon"
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "hue": "#0077ff"
                            },
                            {
                                "gamma": 3.1
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {
                                "hue": "#00ccff"
                            },
                            {
                                "gamma": 0.44
                            },
                            {
                                "saturation": -33
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "stylers": [
                            {
                                "hue": "#44ff00"
                            },
                            {
                                "saturation": -23
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "hue": "#007fff"
                            },
                            {
                                "gamma": 0.77
                            },
                            {
                                "saturation": 65
                            },
                            {
                                "lightness": 99
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "gamma": 0.11
                            },
                            {
                                "weight": 5.6
                            },
                            {
                                "saturation": 99
                            },
                            {
                                "hue": "#0091ff"
                            },
                            {
                                "lightness": -86
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "lightness": -48
                            },
                            {
                                "hue": "#ff5e00"
                            },
                            {
                                "gamma": 1.2
                            },
                            {
                                "saturation": -23
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "saturation": -64
                            },
                            {
                                "hue": "#ff9100"
                            },
                            {
                                "lightness": 16
                            },
                            {
                                "gamma": 0.47
                            },
                            {
                                "weight": 2.7
                            }
                        ]
                    }
                ]*/
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            // current location
            var currentLatLng = new google.maps.LatLng(-3.0189086,118.7926493);
//            geo.mark(currentLatLng);


            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }

            infowindow = new google.maps.InfoWindow();

            showAll();
        }



        var geo = {

            mark: function (location, icon) {

                console.log(marker);

                if (marker) {
                    marker.setPosition(location);
                } else {
                    marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            }
        }

        function showAll(year) {
            showRusun(year);

            showRusus(year);
        }

        function showRusun(year) {


            $.get('{{route('profile.province.map.data')}}?type=rusun&year='+year, function (response) {

                var i = 1;
                response.forEach(function (item) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.latitude, item.longitude),
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: '{{asset('img/rumah-susun.png')}}'

                    });

                    markers.push(marker);

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(item.proposer_remark);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));

                    i++;
                });

            });

        }

        function showRusus(year) {
            $.get('{{route('profile.province.map.data')}}?type=rusus&year='+year, function (response) {
                var i = 1;
                response.forEach(function (item) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.latitude, item.longitude),
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: '{{asset('img/rumah-khusus.png')}}'
                    });

                    markers.push(marker);

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(item.proposer_remark);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));

                    i++;
                });
            });
        }

        $('#btn-filter-map').click(function(){

            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }

            var unit = $('#unit').val();
            var year = $('#year').val();

            console.log(unit, year);

            if(unit === 'rusun') {
                showRusun(year);
            } else if (unit === 'rusus') {
                showRusus(year);
            } else {
                showAll(year);
            }
        });
    </script>
@endpush