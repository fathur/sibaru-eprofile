@extends('layout.main')
{{--{{dd($proposal->toArray())}}--}}

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>
                </div>
                <div class="portlet-body">

                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('eprofile.store')}}" method="post">
                        <input type="hidden" value="{{$proposal->id}}" name="proposal">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="location" class="control-label">Lokasi</label>
                                    <select name="location" id="location" class="form-control">
                                        @if($proposal->location_id)
                                            <option value="{{$proposal->location_id}}"
                                                    selected>{{Cache::get('region-'.$proposal->location_id)}}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="program" class="control-label">Kegiatan</label>
                                    <select name="program" id="program" class="form-control">
                                        @if($proposal->program_id)
                                            <option value="{{$proposal->program_id}}"
                                                    selected>{{$proposal->program->name}}</option>

                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient" class="control-label">Penerima manfaat</label>
                                    <select name="recipient" id="recipient" class="form-control">
                                        @if($proposal->recipient_id)
                                            <option value="{{$proposal->recipient_id}}"
                                                    selected>{{$proposal->recipient->name}}</option>

                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="proposer_remark" class="control-label">Keterangan
                                        pengusul</label>
                <textarea id="proposer_remark" name="proposer_remark"
                          class="form-control">{{$proposal->proposer_remark}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="control-label">Alamat</label>
                                    <textarea id="address" name="address"
                                              class="form-control">{{$proposal->address}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="amount_unit" class="control-label">Jumlah unit</label>
                                    <input id="amount_unit" name="amount_unit"
                                              class="form-control" >
                                </div>

                                <div class="form-group">
                                    <label for="total_budget" class="control-label">Anggaran</label>
                                    <input id="total_budget" name="total_budget"
                                              class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-info"><i class="fa fa-floppy-o"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@push('scripts')
@endpush
@push('script')
<script>
    $('#location').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('regions.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    @can('do-admin')
                    agency: $('#proposer').val()
                    @endcan
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
        minimumInputLength: 2,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#program').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#recipient').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });
</script>
@endpush
