@extends('layout.main')

@push('style')
    <style>
        @media (min-width: 768px) {
            #filter .form-inline .form-group {
                display: block;
                /*margin-bottom: 5px;*/
                vertical-align: middle;

            }

            #filter .form-inline .form-group .control-label{
                padding-right: 0;
            }

            #filter .form-inline .form-control {
                width: 100%;

            }
        }

        #filter .form-group {
            margin-bottom: 0px;;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>

                    <div class="actions">

                        <a class="btn btn-primary" href="{{route('eprofile.create')}}">
                            <i class="fa fa-plus"></i>&nbsp; Tambah eProfile
                        </a>
                    </div>

                </div>
                <div class="portlet-body">

                    <div class="alert alert-info">
                        <div class="row" id="filter">
                            <div class="col-sm-6">

                                {{--<div class="row">--}}
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="unit" class="col-sm-4 control-label">Direktorat:</label>

                                        <div class="col-sm-8">
                                            <select name="unit" id="unit" class="form-control input-sm">
                                                <option value="0" selected>Semua</option>
                                                @foreach($units as $unit)
                                                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label for="program" class="col-sm-4 control-label">Kegiatan:</label>

                                        <div class="col-sm-8">
                                            <select name="program" id="program" class="form-control input-sm"></select>

                                            {{--<input type="text" id="program_name" name="program_name" class="form-control input-sm" onkeyup="if (event.keyCode === 13) filterEprofile()">--}}
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-top: 3px;">
                                        <label for="recipient" class="col-sm-4 control-label">Penerima Manfaat:</label>

                                        <div class="col-sm-8">
                                            <select name="recipient" id="recipient" class="form-control input-sm"></select>

                                        </div>
                                    </div>
                                </div>
                                {{--</div>--}}
                            </div>

                            <div class="col-sm-6">
                                {{--<div class="row">--}}
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label for="proposed_year" class="col-sm-4 control-label">Tahun:</label>

                                        <div class="col-sm-8">

                                            <select name="proposed_year" id="proposed_year"  class="form-control input-sm ">
                                                <option value="0" selected>Semua</option>
                                                @for($year = (\Carbon\Carbon::now()->year + 20); $year >= (\Carbon\Carbon::now()->year - 5); $year--)
                                                    <option value="{{trim($year)}}">{{$year}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-top: 5px;">
                                        <label for="region" class="col-sm-4 control-label">Lokasi:</label>

                                        <div class="col-sm-8">
                                            <input type="text" id="region" name="region" placeholder="Lokasi"
                                                   aria-describedby="help-block" style="width: 100%;">
                                        </div>
                                    </div>

                                    {{--</div>--}}
                                </div>
                                <button type="button" class="btn btn-info btn-sm pull-right margin-top-15" id="act-search"><i class="fa fa-search"></i> Cari</button>

                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        {!! $dataTable->table() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@include('_component.easyui')
@include('_component.select2')

@push('script')

{!! $dataTable->scripts() !!}

<script>
    $('#act-search').click(function(){
        window.LaravelDataTables["dataTableBuilder"].ajax.reload( null, false );

    });

    function filterEprofile() {
        window.LaravelDataTables["dataTableBuilder"].ajax.reload( null, false );
    }

    $('#region').combotree({
        url: '{{route('regions.combotree')}}?level=2',
        method: "GET",
        idField: "id",
        treeField: "name",
        height: 34,
    });

    $('#program').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    unit_id: $('#unit').val(),
                    page: params.page,
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#recipient').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    unit_id: $('#unit').val(),
                    page: params.page,
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

</script>
@endpush