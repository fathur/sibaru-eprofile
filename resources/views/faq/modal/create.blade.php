<form action="{{route('faqs.store')}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}

    <div class="form-group">
        <label for="question" class="control-label">Partanyaan</label>
        <textarea id="username" class="form-control" name="question" placeholder="Partanyaan" aria-describedby="help-block-question"></textarea>
        <div class="help-block" id="help-block-question"></div>
    </div>

    <div class="form-group">
        <label for="answer" class="control-label">Jawaban</label>
        <textarea id="answer" class="form-control" name="answer" placeholder="Jawaban" aria-describedby="help-block-answer"></textarea>

        <div class="help-block" id="help-block-answer"></div>
    </div>

</form>


<script>

</script>