<form action="{{route('faqs.update', $faq->id)}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field("PUT")}}
    <div class="form-group">
        <label for="question" class="control-label">Partanyaan</label>
        <textarea id="username" class="form-control" name="question" placeholder="Partanyaan"
                  aria-describedby="help-block-question">{{$faq->question}}</textarea>
        <div class="help-block" id="help-block-question"></div>
    </div>
    <div class="form-group">
        <label for="answer" class="control-label">Jawaban</label>
        <textarea id="answer" class="form-control" name="answer" placeholder="Jawaban"
                  aria-describedby="help-block-answer">{{$faq->answer}}</textarea>
        <div class="help-block" id="help-block-answer"></div>
    </div>
</form>
<script>
</script>