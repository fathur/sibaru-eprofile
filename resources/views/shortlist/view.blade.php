@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>
                    <div class="actions"></div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')

@push('script')
{!! $dataTable->scripts() !!}

<script>

</script>
@endpush