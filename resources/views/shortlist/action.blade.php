<div class="btn-group btn-group-xs pull-right">
    <a class="btn btn-warning" href="{{$edit_link}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp;Edit&nbsp;</span>
    </a>
    <button type="button" class="btn btn-danger" onclick="deleteForm(this)"
            data-link="{{$delete_link}}"
            data-identifier="{{$identifier}}">
        <i class="fa fa-trash"></i>
        <span class="hidden-xs hidden-sm">&nbsp;Delete</span>
    </button>
</div>


