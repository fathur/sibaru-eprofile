<div class="btn-group btn-group-xs pull-right">
   {{-- <a class="btn btn-warning" href="{{route('regions.profiles.index', [$region['id']])}}">
        <i class="fa fa-user"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Profile</span>
    </a>--}}

    <button type="button" class="btn btn-warning" onclick="editForm(this)"
            data-id="{{$region['id']}}"
            data-form="{{route('regions.edit', $region['id'])}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </button>

    <button type="button" class="btn btn-danger" onclick="deleteRegion(this)"
            data-link="{{route('regions.destroy', [$region['id']])}}"
            data-identifier="{{$identifier}}">
        <i class="fa fa-trash"></i>
    </button>
</div>

{{-- Ini buat profile provinsi --}}
@if(!is_null($year))
    <a class="btn btn-warning btn-xs pull-right" href="{{route('regions.profiles.index', $region['id'])}}?year={{$year}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </a>
@endif