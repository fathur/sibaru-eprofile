@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{Cache::get("region-{$region->id}")}}</div>
                    </div>
                    <div class="actions">

                        <div class="btn-group">

                            <a href="{{route('regions.specials.index', [$region->id])}}" class="btn btn-info">
                                <i class="fa fa-plus"></i>&nbsp; Data 2010 - 2014
                            </a>

                            <a class="btn btn-primary" href="{{route('regions.profiles.create', [$region->id])}}">
                                <i class="fa fa-plus"></i>&nbsp; Tambah profile
                            </a>

                            {{--<a href="#" class="btn btn-success">Excel</a>--}}
                        </div>
                    </div>
                </div>
                <div class="portlet-body">


                    <table class="table table-bordered table-condensed table-hover table-striped" id="{{$identifier}}-datatables">
                        <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Gubernur/Bupati</th>
                            <th>Wakil</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@include('_component.select2')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#{{$identifier}}-datatables').dataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: "{{route('regions.profiles.data')}}",
            data: {
                region: {{$region->id}}
            }
        },
        columns: [
            {data: 'year', name: 'year'},
            {data: 'head_name', name: 'head_name'},
            {data: 'deputy_name', name: 'deputy_name'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

    $('#{{$identifier}}-trigger-modal').click(function () {

        showGeneralModal('{{route('regions.create')}}');
    });
</script>
@endpush