@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{Cache::get('region-'.$region->id)}}</div>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('regions.profiles.store', $region->id)}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Informasi dasar</h3>

                                <div class="form-group">
                                    <label for="year">Tahun</label>
                                    <input type="text" name="year" id="year" class="form-control" placeholder="Tahun" value="{{old('year')}}">
                                </div>



                                {{--<div class="form-group">
                                    <label for="period_start">Awal</label>
                                    <input type="text" name="period_start" id="period_start" class="form-control" placeholder="Awal">
                                </div>

                                <div class="form-group">
                                    <label for="period_end">Akhir</label>
                                    <input type="text" name="period_end" id="period_end" class="form-control" placeholder="Akhir">
                                </div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="image">Logo</label>--}}
                                    {{--<input type="text" name="image" id="image" class="form-control" placeholder="Logo">--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="logo" class="control-label">Logo</label>
                                    <div class="col-progress col-download">
                                        <div class="wrapper margin-bottom-15"></div>
                                    </div>
                                    <input type="file" id="logo" name="logo" placeholder="File">
                                    <input type="hidden" name="logo" value="" id="logo-profile">
                                    <input type="hidden" name="file_name" value="" id="filename-profile">
                                    <div class="help-block" id="help-block-logo"></div>

                                </div>


                                <hr>

                                <h4>Kepala daerah</h4>


                                <div class="form-group">
                                    <label for="head_name">Kepala</label>
                                    <input type="text" name="head_name" id="head_name" class="form-control" placeholder="Kepala" value="{{old('head_name')}}">
                                </div>

                                <div class="form-group">
                                    <label for="head_photo" class="control-label">Foto Kepala</label>
                                    <div class="col-progress col-download">
                                        <div class="wrapper margin-bottom-15"></div>
                                    </div>
                                    <input type="file" id="head_photo" name="head_photo" placeholder="File">
                                    <input type="hidden" name="head_photo" value="" id="location-head_photo">
                                    <input type="hidden" name="file_name" value="" id="filename-head_photo">
                                    <div class="help-block" id="help-block-head_photo"></div>

                                </div>
                                {{--<div class="form-group">
                                    <label for="head_photo">Foto Kepala</label>
                                    <input type="text" name="head_photo" id="head_photo" class="form-control" placeholder="Foto Kepala">
                                </div>--}}

                                <div class="form-group">
                                    <label for="head_party">Partai</label>
                                    <input type="text" name="head_party" id="head_party" class="form-control" placeholder="Partai" value="{{old('head_party')}}">
                                </div>
                                <hr>

                                <h4>Wakil kepala daerah</h4>

                                <div class="form-group">
                                    <label for="deputy_name">Wakil</label>
                                    <input type="text" name="deputy_name" id="deputy_name" class="form-control" placeholder="Wakil" value="{{old('deputy_name')}}">
                                </div>

                                <div class="form-group">
                                    <label for="deputy_photo" class="control-label">Foto Wakil</label>
                                    <div class="col-progress col-download">
                                        <div class="wrapper margin-bottom-15"></div>
                                    </div>
                                    <input type="file" id="deputy_photo" name="deputy_photo" placeholder="File">
                                    <input type="hidden" name="deputy_photo" value="" id="location-deputy_photo">
                                    <input type="hidden" name="file_name" value="" id="filename-deputy_photo">
                                    <div class="help-block" id="help-block-deputy_photo"></div>

                                </div>

                               {{-- <div class="form-group">
                                    <label for="deputy_photo">Foto Wakil</label>
                                    <input type="text" name="deputy_photo" id="deputy_photo" class="form-control" placeholder="Foto Wakil">
                                </div>--}}

                                <div class="form-group">
                                    <label for="deputy_party">Partai</label>
                                    <input type="text" name="deputy_party" id="deputy_party" class="form-control" placeholder="Partai" value="{{old('deputy_party')}}">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="masa_bakti">Masa Bakti</label>
                                    <input type="text" name="masa_bakti" id="masa_bakti" class="form-control" placeholder="Masa Bakti" value="{{old('masa_bakti')}}">
                                </div>
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <h3>Geografis</h3>
                                <div class="form-group">
                                    <label for="koordinat">Letak geografis</label>
                                    <input type="text" name="koordinat" id="koordinat" class="form-control" placeholder="Letak geografis" value="{{old('koordinat')}}">
                                </div>

                               {{-- <div class="form-group">
                                    <label for="latitude">Latitude</label>
                                    <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitude">
                                </div>

                                <div class="form-group">
                                    <label for="longitude">Longitude</label>
                                    <input type="text" name="longitude" id="longitude" class="form-control" placeholder="Longitude">
                                </div>--}}

                                <div class="form-group">
                                    <label for="area_large">Luas wilayah</label>
                                    <input type="text" name="area_large" id="area_large" class="form-control" placeholder="Luas wilayah" value="{{old('area_large')}}">
                                </div>

                                <div class="form-group">
                                    <label for="sea_large">Luas lautan</label>
                                    <input type="text" name="sea_large" id="sea_large" class="form-control" placeholder="Luas laut" value="{{old('sea_large')}}">
                                </div>

                                <div class="form-group">
                                    <label for="land_large">Luas daratan</label>
                                    <input type="text" name="land_large" id="land_large" class="form-control" placeholder="Luas darat" value="{{old('land_large')}}">
                                </div>
                                <hr>

                                <h3>Perkim</h3>

                                <div class="form-group">
                                    <label for="perkim">Kelembagaan Perkim</label>
                                    <input type="text" name="perkim" id="perkim" class="form-control" placeholder="Perkim" value="{{old('perkim')}}">
                                </div>

                                <div class="form-group">
                                    <label for="perkim_pic_name">Kontak Person</label>
                                    <input type="text" name="perkim_pic_name" id="perkim_pic_name" class="form-control" placeholder="PIC" value="{{old('perkim_pic_name')}}">
                                </div>

                                <div class="form-group">
                                    <label for="perkim_phone">Phone</label>
                                    <input type="text" name="perkim_phone" id="perkim_phone" class="form-control" placeholder="Phone" value="{{old('perkim_phone')}}">
                                </div>

                                <div class="form-group">
                                    <label for="perkim_fax">Fax</label>
                                    <input type="text" name="perkim_fax" id="perkim_fax" class="form-control" placeholder="Fax" value="{{old('perkim_fax')}}">
                                </div>

                                <div class="form-group">
                                    <label for="perkim_email">Email</label>
                                    <input type="text" name="perkim_email" id="perkim_email" class="form-control" placeholder="Email" value="{{old('perkim_email')}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h3>Informasi Umum PKP</h3>

                                <div class="form-group">
                                    <label for="population_growth">Pertumbuhan penduduk</label>
                                    <input type="text" name="population_growth" id="population_growth" class="form-control" placeholder="Pertumbuhan penduduk" value="{{old('population_growth')}}">
                                </div>

                                <div class="form-group">
                                    <label for="population_density">Tingkat kepadatan penduduk</label>
                                    <input type="text" name="population_density" id="population_density" class="form-control" placeholder="Tingkat kepadatan penduduk" value="{{old('population_density')}}">
                                </div>

{{--

                                <div class="form-group">
                                    <label for="total_man">Jumlah pria</label>
                                    <input type="text" name="total_man" id="total_man" class="form-control" placeholder="Jumlah pria" value="{{old('total_man')}}">
                                </div>

                                <div class="form-group">
                                    <label for="total_woman">Jumlah wanita</label>
                                    <input type="text" name="total_woman" id="total_woman" class="form-control" placeholder="Jumlah wanita" value="{{old('total_woman')}}">
                                </div>
--}}

                                <div class="form-group">
                                    <label for="total_people">Jumlah penduduk</label>
                                    <input type="text" name="total_people" id="total_people" class="form-control" placeholder="Jumlah penduduk" value="{{old('total_people')}}">
                                </div>

                                {{--<div class="form-group">
                                    <label for="total_district">Jumlah kecamatan</label>
                                    <input type="text" name="total_district" id="total_district" class="form-control" placeholder="Jumlah kecamatan" value="{{old('total_district')}}">
                                </div>

                                <div class="form-group">
                                    <label for="total_city">Jumlah kota/kabupaten</label>
                                    <input type="text" name="total_city" id="total_city" class="form-control" placeholder="Jumlah kota/kabupaten" value="{{old('total_city')}}">
                                </div>--}}

                                <div class="form-group">
                                    <label for="total_city_poor_people">Jumlah penduduk miskin kota</label>
                                    <input type="text" name="total_city_poor_people" id="total_city_poor_people" class="form-control" placeholder="Jumlah penduduk miskin kota">
                                </div>

                                <div class="form-group">
                                    <label for="total_village_poor_people">Jumlah penduduk miskin desa</label>
                                    <input type="text" name="total_village_poor_people" id="total_village_poor_people" class="form-control" placeholder="Jumlah penduduk miskin desa">
                                </div>
                            </div>

                            <div class="col-md-6">

                                {{--<h3>APBD</h3>
                                <div class="form-group">
                                    <label for="apbd_amount">APBD</label>
                                    <input type="text" name="apbd_amount" id="apbd_amount" class="form-control" placeholder="APBD">
                                </div>

                                <div class="form-group">
                                    <label for="pupr_budget">Budget</label>
                                    <input type="text" name="pupr_budget" id="pupr_budget" class="form-control" placeholder="Budget">
                                </div>

                                <div class="form-group">
                                    <label for="pad_pajak">PAD Pajak</label>
                                    <input type="text" name="pad_pajak" id="pad_pajak" class="form-control" placeholder="PAD Pajak">
                                </div>

                                <div class="form-group">
                                    <label for="pad_retribusi">PAD Retribusi</label>
                                    <input type="text" name="pad_retribusi" id="pad_retribusi" class="form-control" placeholder="PAD Retribusi">
                                </div>

                                <div class="form-group">
                                    <label for="pad_sda">PAD SDA</label>
                                    <input type="text" name="pad_sda" id="pad_sda" class="form-control" placeholder="PAD SDA">
                                </div>

                                <div class="form-group">
                                    <label for="pad_others">PAD Lainnya</label>
                                    <input type="text" name="pad_others" id="pad_others" class="form-control" placeholder="PAD Lainnya">
                                </div>
                                <hr>
--}}
                                {{-- Sembunyikan jika provinsi--}}
                                @if($region->level != 1)
                                <h3>Basis data perumahan</h3>
                                <div class="form-group">
                                    <label for="backlog_house">Backlog Penghunian</label>
                                    <input type="text" name="backlog_house" id="backlog_house" class="form-control" placeholder="Backlog Rumah" value="{{old('backlog_house')}}">
                                </div>

                                <div class="form-group">
                                    <label for="backlog_kepemilikan">Backlog Kepemilikan</label>
                                    <input type="text" name="backlog_kepemilikan" id="backlog_kepemilikan" class="form-control" placeholder="Backlog Kepemilikan" value="{{old('backlog_kepemilikan')}}">
                                </div>

                                <div class="form-group">
                                    <label for="rtlh">RTLH</label>
                                    <input type="text" name="rtlh" id="rtlh" class="form-control" placeholder="RTLH" value="{{old('rtlh')}}">
                                </div>
                                @endif

                               {{-- <div class="form-group">
                                    <label for="household">Rumah Tangga</label>
                                    <input type="text" name="household" id="household" class="form-control" placeholder="Rumah Tangga">
                                </div>--}}

                                <h3>Info Visual</h3>

                                @for($i = 1; $i <= \App\Http\Controllers\EprofileController::$totalAssetImages; $i++)
                                <div class="form-group">
                                    <label for="asset_{{$i}}" class="control-label">Gambar {{$i}}</label>
                                    <div class="col-progress col-download">
                                        <div class="wrapper margin-bottom-15"></div>
                                    </div>
                                    <input type="file" id="asset_{{$i}}" name="asset_{{$i}}" placeholder="File">
                                    <input type="hidden" name="location_asset_{{$i}}" value="" id="location-asset_{{$i}}">
                                    <input type="hidden" name="filename_asset_{{$i}}" value="" id="filename-asset_{{$i}}">
                                    <div class="help-block" id="help-block-asset_{{$i}}"></div>
                                </div>
                                @endfor
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary pull-right btn-lg">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.bootstrap-datepicker')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#logo').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#logo-profile');
        var $attachmentNameField = $this.parent().find('#filename-profile');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });

    $('#head_photo').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-head_photo');
        var $attachmentNameField = $this.parent().find('#filename-head_photo');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });

    $('#deputy_photo').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-deputy_photo');
        var $attachmentNameField = $this.parent().find('#filename-deputy_photo');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });

    @for($i = 1; $i <= \App\Http\Controllers\EprofileController::$totalAssetImages; $i++)
    $('#asset_{{$i}}').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-asset_{{$i}}');
        var $attachmentNameField = $this.parent().find('#filename-asset_{{$i}}');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                    '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                    '<span class="">'+Math.round(progress)+'%</span> ' +
                    '</div> ' +
                    '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                    .html('<span class="text-danger">'+error.message+'!</span>')
                    .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });
    @endfor

    $('#period_start').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: 'linked',
        todayHighlight: true,
        autoclose: true,
        keyboardNavigation:true,
    });

    $('#period_end').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: 'linked',
        todayHighlight: true,
        autoclose: true,
        keyboardNavigation:true,
    });
</script>
@endpush