<div class="btn-group btn-group-xs pull-right">
    <a class="btn btn-warning" href="{{$edit_link}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </a>
    <button type="button" class="btn btn-danger" onclick="deleteForm(this)"
            data-link="{{$delete_link}}"
            data-identifier="{{$identifier}}">

        <i class="fa fa-trash"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Delete</span>
    </button>
</div>


<div class="pull-right btn-group btn-group-xs margin-right-10">
    @if($region->level == 1)
        <a href="{{ route('regions.profiles.show', [$data->region->id, $data->id]) }}" class="btn btn-default">
            <i class="fa fa-eye"></i>
            <span class="hidden-xs hidden-sm"> View</span>
        </a>
        <a href="{{ route('regions.profiles.export', [$data->region->id, $data->id]) }}?type=xls" class="btn btn-success">
            <i class="fa fa-file-excel-o"></i>
            <span class="hidden-xs hidden-sm"> Excel</span>
        </a>
    @endif

</div>