<div class="btn-group btn-group-xs pull-right">
    <a class="btn btn-warning" target="_blank" href="{{route('regions.profiles.edit', [$profile['region_id'], $profile['id']])}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </a>
</div>