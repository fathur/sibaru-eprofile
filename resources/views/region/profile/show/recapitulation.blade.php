@inject('export', '\App\Repositories\RegionProfileExport')

<div class="row">
    <div class="col-xs-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <div class="caption-subject">Rekapitulasi Kegiatan di Provinsi Aceh</div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Pembangunan Kemenpera 2010-2014</h4>
                        <table class="table table-condensed table-bordered">
                            <thead class="head-table">
                            <tr>
                                <th width="30">No</th>
                                <th>Kegiatan</th>
                                <th>Volume</th>
                                <th>Satuan</th>
                                <th class="text-right">Anggaran</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($export->recapKemenpera($regionProfile) as $item)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$item->program}}</td>
                                    <td>{{$item->volume}}</td>
                                    <td>{{$item->unit}}</td>
                                    <td class="text-right">{{rupiah($item->budget)}}</td>
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                @if(count($export->recapPreviousYears($regionProfile)) > 0)
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Pembangunan Kemenpupera 2015-{{$regionProfile->year - 1}}</h4>

                        <table class="table table-condensed table-bordered">
                            <thead class="head-table">
                            <tr>
                                <th width="30">No</th>
                                <th>Direktorat</th>
                                <th>Kegiatan</th>
                                <th>Unit</th>
                                <th class="text-right">Anggaran</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($export->recapPreviousYears($regionProfile) as $unitId => $programs)
                                @foreach($programs as $programId => $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{ $value['unit_name'] }}</td>
                                        <td>{{ $value['program_name'] }}</td>
                                        <td>{{ $value['sum_unit'] }}</td>
                                        <td class="text-right">{{ rupiah($value['sum_budget'] ) }}</td>
                                    </tr>
                                @endforeach

                                @php $i++; @endphp

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-sm-12">
                        <h4>Pembangunan Kemenpupera {{$regionProfile->year}}</h4>

                        <table class="table table-condensed table-bordered">
                            <thead class="head-table">
                            <tr>
                                <th width="30">No</th>
                                <th>Unit</th>
                                <th>Kegiatan</th>
                                <th>Unit</th>
                                <th class="text-right">Anggaran</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp

                            @foreach($export->recapCurrentYear($regionProfile) as $unitId => $programs)
                                @foreach($programs as $programId => $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{ $value['unit_name'] }}</td>
                                        <td>{{ $value['program_name'] }}</td>
                                        <td>{{ $value['sum_unit'] }}</td>
                                        <td class="text-right">{{ rupiah($value['sum_budget'] ) }}</td>
                                    </tr>
                                @endforeach
                                @php $i++; @endphp

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

