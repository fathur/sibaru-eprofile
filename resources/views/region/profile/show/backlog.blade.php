
@push('style')
    <style>
        #head-kabupaten {
            background-color: #337AB7;
            color: #FFFFFF;
        }

        @media print {
            #head-kabupaten {
                background-color: #337AB7;
                color: #FFFFFF;
                -webkit-print-color-adjust: exact;
            }
        }
    </style>
@endpush

<div class="row">
    <div class="col-xs-12">

        <div class="portlet light portlet-datatable">


            <div class="portlet-body">
                <table class="table table-condensed table-bordered">
                    <thead id="head-kabupaten">
                    <tr>
                        <th>Kabupaten/Kota</th>
                        <th width="200">RTLH</th>
                        <th width="200">Backlog Kepemilikan</th>
                        <th width="200">Backlog Penghunian</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($regencyProfiles as $regencyProfile)
                        <tr>
                            <td>{{$regencyProfile->name}}</td>
                            <td>{{$regencyProfile->rtlh}}</td>
                            <td>{{$regencyProfile->backlog_kepemilikan}}</td>
                            <td>{{$regencyProfile->backlog_house}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
