@inject('export', '\App\Repositories\RegionProfileExport')

@push('style')
    <style>
        #head-rincian {
            background-color: #337AB7;
            color: #FFFFFF;
        }

        .unit-row {
            background-color: #5CB85C;
            color: #FFF;
        }

    </style>
@endpush

<div class="row">
    <div class="col-xs-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <div class="caption-subject">Rincian Kegiatan Kabupaten/Kota Tahun {{$regionProfile->year}} di
                        Provinsi {{$regionProfile->region->name}}</div>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-condensed ">
                    <thead id="head-rincian">
                    <tr>
                        <th width="30">No</th>
                        <th>Program Kegiatan</th>
                        <th>Unit</th>
                        <th>Anggaran</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($regencyProfiles as $regencyProfile)

                        @if($export->dataByRegencyExist($regencyProfile))

                            <tr>
                                <td width="30"><strong>{{$i}}</strong></td>
                                <td colspan="3"><strong>{{$regencyProfile->name}}</strong></td>
                            </tr>

                            @php $j = 1; @endphp

                            @foreach($units as $unit)

                                @php
                                $programsWithRecipients = $export->getProgramsWithRecipients($regencyProfile, $unit);
                                @endphp

                                @if(count($programsWithRecipients) > 0)

                                    <tr class="unit-row">
                                        <td width="30"><strong>{{$i}}.{{$j}}</strong></td>
                                        <td><strong>{{$unit->name}}</strong></td>
                                        <td>
                                            <strong>
                                            {{$export->sumUnitInDirectorate($regencyProfile, $unit)}}
                                            </strong>
                                        </td>
                                        <td align="right">
                                            <strong>
                                            {{rupiah($export->sumBudgetInDirectorate($regencyProfile, $unit))}}
                                            </strong>
                                        </td>
                                    </tr>

                                    @foreach($programsWithRecipients as $uniques)
                                        <tr>
                                            <td class="info"></td>
                                            <td class="info"><strong>{{$uniques['program']->name}}
                                                    , {{$uniques['recipient']->name}}</strong></td>
                                            <td class="info"><strong>{{$uniques['sum_unit']}}</strong></td>
                                            <td class="info text-right">
                                                <strong>{{rupiah($uniques['sum_budget'])}}</strong></td>
                                        </tr>

                                        @foreach($uniques['eprofiles'] as $eprofile)
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <ul style="margin-bottom: 0; padding-left: 20px;">
                                                        <li>{{$eprofile->proposer_remark}}</li>
                                                    </ul>
                                                </td>
                                                <td>{{$eprofile->amount_unit}}</td>
                                                <td width="150" align="right">{{rupiah($eprofile->total_budget)}}</td>
                                            </tr>
                                        @endforeach

                                    @endforeach

                                    @php $j++; @endphp

                                @endif

                            @endforeach

                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>

                            @php $i++; @endphp

                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>