@extends('layout.main')

@push('style')
    <style>
        @media print {
            #btn-print {
                display: none !important;
            }
        }

        .head-table {
            background-color: #337AB7;
            color: #FFFFFF;
        }
    </style>
@endpush


@section('content')
    <div class="row">
        <div class="col-xs-8">
            <h1 class="">Profil Provinsi {{{$regionProfile->region->name}}}</h1>
        </div>
        <div class="col-xs-4">
            <button type="button" class="btn btn-danger pull-right" id="btn-print" onclick="window.print()">
                <i class="fa fa-print"></i> Print
            </button>
            {{--<img src="https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg" alt="" class="img-responsive">--}}
            {{--<img src="https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg" alt="" class="img-responsive">--}}
        </div>
    </div>


    @include('region.profile.show.basic-info')
    @include('region.profile.show.backlog')
    @include('region.profile.show.recapitulation')
    @include('region.profile.show.detail')
@stop
