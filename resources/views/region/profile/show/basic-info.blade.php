<div class="row">
    <div class="col-xs-12">

        <div class="portlet light portlet-datatable">


            <div class="portlet-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-xs-2 control-label">Gubernur</label>
                        <div class="col-xs-10">
                            <span class="form-control-static bold">{{ $regionProfile->head_name }}</span>
                            <br>
                            <span class="form-control-static"><em>{{ $regionProfile->head_party }}</em></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">Wakil Gubernur</label>
                        <div class="col-xs-10">
                            <span class="form-control-static bold">{{ $regionProfile->deputy_name }}</span>
                            <br>
                            <span class="form-control-static"><em>{{ $regionProfile->deputy_party }}</em></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">Masa Bakti</label>
                        <div class="col-xs-10">
                            <span class="form-control-static"><strong>{{$regionProfile->masa_bakti}}</strong></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">
                            Kelembagaan Perkim
                        </label>
                        <div class="col-xs-10">
                            <span class="form-control-static"><strong>{{ $regionProfile->perkim_kelembagaan }}</strong></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">Kontak person <br/> (SKPD Dekosentrasi)</label>
                        <div class="col-xs-10">
                            <span class="form-control-static"><strong>{{ $regionProfile->perkim_pic_name }}</strong></span>
                            <br>
                            <span class="form-control-static">Telp./Fax: <strong>{{ $regionProfile->perkim_phone }}
                                    /{{$regionProfile->perkim_fax}}</strong></span>
                            <br>
                            <span class="form-control-static">Email: <strong>{{ $regionProfile->perkim_email }}</strong></span>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">
                            Letak Geografis
                        </label>
                        <div class="col-xs-10">
                            <span class="form-control-static"><strong>{{ $regionProfile->koordinat }}</strong></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-xs-2 control-label">
                            Luas Wilayah
                        </label>
                        <div class="col-xs-10">
                            <span class="form-control-static">Daratan: <strong>{{ $regionProfile->land_large }}</strong></span>
                            <br/>
                            <span class="form-control-static">Perairan: <strong>{{ $regionProfile->sea_large }}</strong></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">
                            Informasi Umum PKP
                        </label>
                        <div class="col-xs-10">
                            <span class="form-control-static">Jumlah Penduduk: <strong>{{--{{  }}--}}</strong></span>
                            <br/>
                            <span class="form-control-static">Pertumbuhan Penduduk: <strong>{{ $regionProfile->population_growth }}</strong></span>
                            <br/>
                            <span class="form-control-static">Tingkat Kepadatan Penduduk: <strong>{{ $regionProfile->population_density }}</strong></span>

                            <br/>
                            <span class="form-control-static">Jumlah Penduduk Miskin Kota: <strong>{{ $regionProfile->total_city_poor_people }}</strong></span>

                            <br/>
                            <span class="form-control-static">Jumlah Penduduk Miskin Desa: <strong>{{ $regionProfile->total_village_poor_people }}</strong></span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

