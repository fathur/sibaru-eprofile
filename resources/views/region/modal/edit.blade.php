<form action="{{route('regions.update', $region->id)}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="parent" class="control-label">Parent</label>
        <select id="parent" class="form-control" name="parent"
                aria-describedby="help-block-parent">s

            <option value="0">-</option>

            @foreach($provinces as $province)
                <option value="{{$province->id}}" @if($province->id == $region->parent_id) selected @endif>{{$province->name}}</option>
            @endforeach

        </select>
        <div class="help-block" id="help-block-parent"></div>
    </div>

    <div class="form-group">
        <label for="order">No</label>
        <input type="number" class="form-control" name="order" id="order" placeholder="No"
               aria-describedby="help-block-order" value="{{$region->order}}">
        <div class="help-block" id="help-block-order"></div>

    </div>

    <div class="form-group">
        <label for="name" class="control-label">Nama</label>
        <input id="name" class="form-control" name="name" placeholder="Nama" value="{{$region->name}}"
               aria-describedby="help-block-name" />
        <div class="help-block" id="help-block-name"></div>
    </div>
</form>