<form action="{{route('regions.store')}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    <div class="form-group">
        <label for="parent" class="control-label">Parent</label>
        <select id="parent" class="form-control" name="parent"
                  aria-describedby="help-block-parent">

            {{--@foreach($provinces as $province)--}}
                {{--<option value="{{$province->id}}">{{$province->name}}</option>--}}
            {{--@endforeach--}}

        </select>
        <div class="help-block" id="help-block-parent"></div>
    </div>

    <div class="form-group">
        <label for="order">No</label>
        <input type="number" class="form-control" name="order" id="order" placeholder="No"
               aria-describedby="help-block-order">
        <div class="help-block" id="help-block-order"></div>

    </div>
    <div class="form-group">
        <label for="name" class="control-label">Nama</label>
        <input id="name" class="form-control" name="name" placeholder="Nama"
                aria-describedby="help-block-name" />
        <div class="help-block" id="help-block-name"></div>
    </div>
</form>

<script>
    $('#parent').combotree({
        url: "{{route('regions.combotree')}}?level=4",
        width: '98%',
        method: "GET",
        idField: "id",
        treeField: "name",
        height: 34
    });
</script>