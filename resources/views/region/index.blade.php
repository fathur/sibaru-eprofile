@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Wilayah</div>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                id="{{$identifier}}-trigger-modal">
                            <i class="fa fa-plus"></i>&nbsp; Tambah wilayah
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    {{--<div class="row form-inline margin-bottom-15" id="filter">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="unit">Provinsi:</label>
                                <select name="unit" id="unit" class="form-control input-sm ">
                                    <option value="0" selected>Semua</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="unit">Kabupaten:</label>
                                <select name="unit" id="unit" class="form-control input-sm ">
                                    <option value="0" selected>Semua</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="unit">Kecamatan:</label>
                                <select name="unit" id="unit" class="form-control input-sm ">
                                    <option value="0" selected>Semua</option>

                                </select>
                            </div>
                        </div>

                    </div>--}}

                    <table class="" id="{{$identifier}}-datatables">

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.easyui')

@push('styles')
<link rel="stylesheet" href="{{asset('components/sweetalert2/dist/sweetalert2.css')}}">
@endpush

@push('scripts')
<script src="{{asset('components/sweetalert2/dist/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#{{$identifier}}-datatables').treegrid({
        method: "GET",
        url: "{{route('regions.data')}}?level=4",
        idField: "id",
        treeField: "name",
        lines: true,
        striped: true,
        columns: [[
            {title:'Nama',field:'name'},
            {title:'Level',field:'level',width:280},
            {title:' ',field:'action'}
        ]]
    });

    $('#{{$identifier}}-trigger-modal').click(function () {

        showGeneralModal('{{route('regions.create')}}');

    });

    function saveRegion(dom) {

        var identifier = $(dom).data('identifier');

        var id = $(dom).data('id');

        var $identifier = $('#' + identifier);

        var url = $identifier.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: $identifier.serialize(), // serializes the form's elements.
            success: function (data) {

                console.log(data);

                // close modal
                $('#general-modal').modal('hide');

                // reload datatables\
                if(data.parent_id !== null) {
                    $('#' + identifier + '-datatables').treegrid('reload', data.parent_id);
                } else {
                    $('#' + identifier + '-datatables').treegrid('reload');
                }

                // kasih centang gede
                swal({
                    title: 'Good job!',
                    text: 'You add one data!',
                    timer: 2000,
                    type: 'success'
                });


            },
            error: function (jqXHR, textStatus, errorThrown) {

                $identifier.find('.form-group').removeClass('has-error');
                $identifier.find('.form-group').find('.help-block').html('');

                var data = $.parseJSON(jqXHR.responseText);

                $.each(data, function (item, errors) {

                    var html = '<ul class="list-unstyled">';

                    $.each(errors, function (index, error) {

                        html += '<li>' + error + '</li>';
                    });

                    html += '</ul>';

                    $('#help-block-' + item).html(html);
                    $('#help-block-' + item).parent().addClass('has-error');

                });
            }
        });
    }

    function deleteRegion(dom) {

        var url = $(dom).data('link');
        var identifier = $(dom).data('identifier');



        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {

            $.ajax({
                type: "DELETE",
                url: url,
                data: {
                    _token: $("meta[name=csrf-token]").attr('content')
                }, // serializes the form's elements.
                success: function (data) {

                    console.log(data)

                    // reload datatables\
                    if(data.parent_id !== null) {
                        $('#' + identifier + '-datatables').treegrid('reload', data.parent_id);
                    } else {
                        $('#' + identifier + '-datatables').treegrid('reload');
                    }

                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )

                }
            });
        })
    }
</script>
@endpush