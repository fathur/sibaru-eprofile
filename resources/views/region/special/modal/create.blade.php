<form action="{{route('regions.specials.store', $region->id)}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}

   {{-- <div class="form-group">
        <label for="unit">Unit</label>
        <input type="text" class="form-control" id="unit" name="unit">
    </div>--}}

    <div class="form-group">
        <label for="order">No</label>
        <input type="text" class="form-control" id="order" name="order">
        <div class="help-block" id="help-block-order"></div>

    </div>

    <div class="form-group">
        <label for="program">Kegiatan</label>
        <input type="text" class="form-control" id="program" name="program">
        <div class="help-block" id="help-block-program"></div>

    </div>

    <div class="form-group">
        <label for="volume">Volume</label>
        <input type="text" class="form-control" id="volume" name="volume">
        <div class="help-block" id="help-block-volume"></div>

    </div>

    <div class="form-group">
        <label for="unit">Satuan</label>
        <input type="text" class="form-control" id="unit" name="unit">
        <div class="help-block" id="help-block-unit"></div>

    </div>

    <div class="form-group">
        <label for="budget">Anggaran</label>
        <input type="text" class="form-control" id="budget" name="budget">
        <div class="help-block" id="help-block-budget"></div>

    </div>

</form>


<script>

</script>