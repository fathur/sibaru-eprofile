<form action="{{route('regions.specials.update', [$region->id, $data->id])}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field("PUT")}}

    <div class="form-group">
        <label for="order">No</label>
        <input type="text" class="form-control" id="order" name="order" value="{{$data->order}}">
        <div class="help-block" id="help-block-order"></div>

    </div>

    <div class="form-group">
        <label for="program">Kegiatan</label>
        <input type="text" class="form-control" id="program" name="program" value="{{$data->program}}">
        <div class="help-block" id="help-block-program"></div>

    </div>

    <div class="form-group">
        <label for="volume">Volume</label>
        <input type="text" class="form-control" id="volume" name="volume" value="{{$data->volume}}">
        <div class="help-block" id="help-block-volume"></div>

    </div>

    <div class="form-group">
        <label for="unit">Satuan</label>
        <input type="text" class="form-control" id="unit" name="unit" value="{{$data->unit}}">
        <div class="help-block" id="help-block-unit"></div>

    </div>

    <div class="form-group">
        <label for="budget">Anggaran</label>
        <input type="text" class="form-control" id="budget" name="budget" value="{{$data->budget}}">
        <div class="help-block" id="help-block-budget"></div>

    </div>
</form>
<script>
</script>