@extends('layout.main')

{{--{{dd($proposal->toArray())}}--}}

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{route('decrees.update')}}" method="post">
                        {{method_field('PUT')}}
                        <input type="hidden" value="{{$proposal->id}}" name="proposal_id">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="proposer" class="control-label bold">Instansi Pengusul</label> <br>
                                    <div class="form-control-static">{{$proposal->proposer->name}}</div>
                                </div>
                                <div class="form-group">
                                    <label for="program" class="control-label bold">Kegiatan</label> <br>
                                    <p class="form-control-static">{{$proposal->program->name}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="recipient" class="control-label bold">Penerima manfaat</label> <br>
                                    <p class="form-control-static">{{$proposal->recipient->name}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="location" class="control-label bold">Lokasi</label> <br>
                                    <p class="form-control-static">{{Cache::get('region-'.$proposal->location_id)}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="control-label bold">Alamat</label> <br>
                                    <p class="form-control-static">{{$proposal->address}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="coordinate" class="control-label bold">Koordinat</label> <br>
                                    <p class="form-control-static">{{$proposal->latitude}}, {{$proposal->longitude}}</p>
                                </div>

                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="proposer_remark" class="control-label bold">Identitas pengusul</label> <br>
                                    <p class="form-control-static">{{$proposal->proposer_remark}}</p>
                                </div>
                                <div class="form-group">
                                    <label for="submission_date" class="control-label bold">Tanggal pengajuan</label> <br>
                                    <p class="form-control-static">{{human_date($proposal->submission_date)}}</p>
                                </div>
                                {{--<div class="form-group">
                                    <label for="proposed_date" class="control-label bold">Tahun usulan pembangunan</label>
                                    <br>
                                    <p class="form-control-static">{{$proposal->proposed_year}}</p>
                                </div>--}}
                                <div class="form-group">
                                    <label for="proposer_phone" class="control-label bold">No. telp pengusul</label> <br>
                                    <p class="form-control-static">
                                        <a href="tel:{{$proposal->proposer_phone}}">{{$proposal->proposer_phone}}</a>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label for="proposer_email" class="control-label bold">Email pengusul</label> <br>
                                    <p class="form-control-static">
                                        <a href="mailto:{{$proposal->proposer_email}}">{{$proposal->proposer_email}}</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row margin-bottom-15">
                            <div class="col-sm-12">
                                <hr>

                                <label for="attachment" class="control-label bold">Lampiran dokumen</label>
                                <div class="col-progress col-download">
                                    <div class="wrapper margin-bottom-15">
                                        <a href="{{route('decrees.download', $proposal->id)}}">{{$proposal->shortlist->decree->filename}}</a>
                                    </div>
                                </div>
                                <input type="file" id="attachment" name="" placeholder="Lampiran" class="simple-upload">
                                <input type="hidden" name="attachment" value="{{$proposal->shortlist->decree->attachment}}" id="location-attachment">
                                <input type="hidden" name="file_name" value="{{$proposal->shortlist->decree->filename}}" id="filename-attachment">
                                <div class="help-block" id="help-block-attachment"></div>

                                <hr>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed table-striped" id="shortlist-dwelling">
                                        <thead>
                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Tipe unit</th>
                                            <th rowspan="2">Ukuran unit</th>
                                            <th colspan="2" class="text-center">Pengusul</th>
                                            <th colspan="2" class="text-center">Direktorat Teknis</th>
                                            <th colspan="2" class="text-center">SK Penetapan</th>


                                        </tr>
                                        <tr>
                                            <th class="text-center">Jumlah unit</th>
                                            <th class="text-center">Anggaran</th>
                                            <th class="text-center">Jumlah unit</th>
                                            <th class="text-center">Anggaran</th>
                                            <th class="text-center">Jumlah unit</th>
                                            <th class="text-center">Anggaran</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @php
                                        $no = 1;
                                        @endphp
                                        @foreach($proposal->dwellingPlans as $dwellingPlan)
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$dwellingPlan->unitType->name ?? ''}}</td>
                                                <td>T. {{$dwellingPlan->unitSize->text ?? ''}} / {{$dwellingPlan->unitSize->total_floor ?? ''}} lantai</td>

                                                <td align="center">{{$dwellingPlan->amount}}</td>
                                                <td align="right">Rp. {{number_format($dwellingPlan->budget_plan, 0, ',', '.')}}</td>

                                                <td align="center">{{$dwellingPlan->shortlist->amount}}</td>
                                                <td align="right">Rp. {{number_format($dwellingPlan->shortlist->budget_plan, 0, ',', '.')}}</td>

                                                <td align="right">
                                                    <input type="text" class="form-control input-sm new-unit text-right"
                                                           title="Jumlah unit baru" name="new_amount[{{$dwellingPlan->shortlist->id}}]"
                                                           value="{{$dwellingPlan->shortlist->decreeBudget->amount}}"
                                                           onkeyup="sumUnit()">
                                                </td>
                                                <td align="right">
                                                    <input type="text" class="form-control input-sm text-right new-budget"
                                                           title="Anggaran baru" name="new_budget_plan[{{$dwellingPlan->shortlist->id}}]"
                                                           value="{{$dwellingPlan->shortlist->decreeBudget->budget}}"
                                                           onkeyup="sumBudget()">
                                                </td>
                                            </tr>
                                            @php
                                            $no++;
                                            @endphp
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr class="info">
                                            <td colspan="3">Jumlah</td>
                                            <td align="right" class="danger">{{$sum->dwellingPlan->unit}}</td>
                                            <td align="right" class="danger">Rp. {{number_format($sum->dwellingPlan->budget, 0, ',', '.')}}</td>
                                            <td align="right" class="danger">{{$sum->shortlist->unit}}</td>
                                            <td align="right" class="danger">Rp. {{number_format($sum->shortlist->budget, 0, ',', '.')}}</td>
                                            <td align="right"><span id="sumNewUnit">0</span></td>
                                            <td align="right"><span id="sumNewBudget">Rp. 0</span></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-info"><i class="fa fa-floppy-o"></i> Simpan</button>
                                <a href="{{route('eprofile.create', $proposal->id)}}" class="btn btn-success pull-right">eProfile</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
{{--<script src="{{asset('components/inputmask/dist/inputmask/bindings/inputmask.binding.js')}}"></script>--}}
<script src="{{asset('components/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{asset('js/simpleUpload.min.js')}}"></script>

@endpush

@push('script')
<script>
    Number.prototype.formatMoney = function(c, d, t){
        var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    function sumUnit() {

        var $dataRows = $('#shortlist-dwelling tbody tr');
        var $sum = 0;
        $dataRows.each(function(){
            $(this).find('.new-unit').each(function(){
                var val;
                if($(this).val() == '') {
                    val = 0;
                } else {
                    val = $(this).val();
                }

                $sum += parseInt(val);
            });
        });

        $('#sumNewUnit').html($sum);

    }

    function sumBudget() {

        var $dataRows = $('#shortlist-dwelling tbody tr');
        var $sum = 0;
        $dataRows.each(function(){
            $(this).find('.new-budget').each(function(){
                var val;
                if($(this).val() == '') {
                    val = 0;
                } else {
                    // change formatted currency to integer
                    // khusus untuk rupiah
                    val = Number($(this).val().replace(/[^0-9\,]+/g,""));
                }

                $sum += parseInt(val);
            });
        });

        $('#sumNewBudget').html('Rp. ' + $sum.formatMoney(0, ',', '.'));

    }

    $('.simple-upload').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-attachment');
        var $attachmentNameField = $this.parent().find('#filename-attachment');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });

    $(document).ready(function() {
        sumBudget();
        sumUnit();

        $(".new-budget").inputmask('currency', {
            prefix: 'Rp. ',
            groupSeparator: ".",
            radixPoint: ",",
            digits: 0
        });

        $('#save-and-send').click(function(){
            $.ajax({
                method: 'POST',
                url: "{{route('proposals.statuses.update')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    proposal_id: {{$proposal->id}}
                },
                success: function($data, textStatus, jqXHR) {
                    console.log($data);
                }
            });
        });
    });
</script>
@endpush
