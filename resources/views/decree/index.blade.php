@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover table-striped"
                               id="{{$identifier}}-datatables">
                            <thead>
                            <tr>
                                <th rowspan="2">Direktorat</th>
                                <th rowspan="2">Instansi Pengusul</th>
                                <th colspan="2" class="text-center">Pengusul</th>
                                <th colspan="2" class="text-center">Direktorat Teknis</th>
                                <th colspan="2" class="text-center">SK Penetapan</th>
                                {{--<th rowspan="2">Keterangan pengusul</th>--}}
                                <th rowspan="2">&nbsp;</th>

                            </tr>
                            <tr>
                                <th>Jumlah unit</th>
                                <th>Rencana anggaran</th>
                                <th>Usulan jumlah unit</th>
                                <th>Usulan anggaran</th>
                                <th>Jumlah unit</th>
                                <th>Anggaran</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@push('script')
<script>
    $('#{{$identifier}}-datatables').DataTable({
        processing: true,
        serverSide: true,
//        stateSave: true,
        ajax: {
            url: "{{route('decrees.data')}}"
        },
        columns: [
            {data: 'unit_name', name: 'units.name'},
            {data: 'proposer_name', name: 'agencies.name'},
            {data: 'a_amount', name: 'a_amount', searchable: false, orderable: false},
            {data: 'a_budget', name: 'a_budget', searchable: false, orderable: false},
            {data: 'b_amount', name: 'b_amount', searchable: false, orderable: false},
            {data: 'b_budget', name: 'b_budget', searchable: false, orderable: false},
            {data: 'c_amount', name: 'c_amount', searchable: false, orderable: false},
            {data: 'c_budget', name: 'c_budget', searchable: false, orderable: false},
//            {data: 'proposer_remark', name: 'proposer_remark'},

            {data: 'action', name: 'action', searchable: false, orderable: false},
        ],
        pageLength: 25,
        lengthMenu: [[25, 50, 75, 100], [25, 50, 75, 100]]
    });
</script>
@endpush