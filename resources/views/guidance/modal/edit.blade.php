<form action="{{route('guidance.update', $guidance->id)}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field("PUT")}}
    <div class="form-group">
        <label for="title" class="control-label">Judul</label>
        <input type="text" id="title" class="form-control" name="title" placeholder="Judul" aria-describedby="help-block-title" value="{{$guidance->title}}" />
        <div class="help-block" id="help-block-title"></div>
    </div>

    <div class="form-group">
        <label for="description" class="control-label">Deskripsi</label>
        <textarea id="description" class="form-control" name="description" placeholder="Deskripsi" aria-describedby="help-block-description">{{$guidance->description}}</textarea>

        <div class="help-block" id="help-block-description"></div>
    </div>

    <div class="form-group">
        <label for="file" class="control-label">File</label>
        <div class="col-progress col-download">
            <div class="wrapper margin-bottom-15">{{$guidance->filename}}</div>
        </div>
        <input type="file" id="file" name="" placeholder="File" class="simple-upload">
        <input type="hidden" name="file" value="{{$guidance->file}}" id="location-file" >
        <input type="hidden" name="file_name" value="{{$guidance->filename}}" id="filename-file">
        <div class="help-block" id="help-block-file"></div>
    </div>
</form>
<script>
    $('.simple-upload').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#location-file');
        var $attachmentNameField = $this.parent().find('#filename-file');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });
</script>