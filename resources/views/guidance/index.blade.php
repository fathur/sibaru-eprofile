@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title ?? ''}}</div>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                id="{{$identifier}}-trigger-modal">
                            <i class="fa fa-plus"></i>&nbsp; Tambah Pedoman
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        {!! $dataTable->table(['id' => $identifier . '-datatables']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
{!! $dataTable->scripts() !!}

<script>
    $('#{{$identifier}}-trigger-modal').click(function () {

        showGeneralModal('{{route('guidance.create')}}');
    });


</script>
@endpush