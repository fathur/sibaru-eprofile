<div class="row">
    <div class="col-md-6">
            {{--<div class="form-group">
                <label for="proposer" class="control-label bold">Instansi Pengusul</label> <br/>
                <div class="form-control-static">{{$proposal->proposer->name}}</div>

            </div>--}}
            <div class="form-group">
                <label for="program" class="control-label bold">Kegiatan</label> <br/>
                <div class="form-control-static">{{$proposal->program->name}}</div>
            </div>
            <div class="form-group">
                <label for="recipient" class="control-label bold">Penerima manfaat</label> <br/>
                <div class="form-control-static">{{$proposal->recipient->name}}</div>

            </div>

            <div class="form-group">
                <label for="proposer_remark" class="control-label bold">Identitas pengusul</label> <br/>
                <div class="form-control-static">{{$proposal->proposer_remark}}</div>

            </div>
            <div class="form-group">
                <label for="submission_date" class="control-label bold">Tanggal pengajuan</label> <br/>
                <div class="form-control-static">{{$proposal->submission_date}}</div>

            </div>
           {{-- <div class="form-group">
                <label for="proposed_date" class="control-label bold">Tahun usulan pembangunan</label> <br/>
                <div class="form-control-static">{{$proposal->proposed_year}}</div>
            </div>--}}
            <div class="form-group">
                <label for="proposer_phone" class="control-label bold">No. telp pengusul</label> <br/>
                <div class="form-control-static">{{$proposal->proposer_phone}}</div>
            </div>
            <div class="form-group">
                <label for="proposer_email" class="control-label bold">Email pengusul</label> <br/>
                <div class="form-control-static">{{$proposal->proposer_email}}</div>
            </div>

    </div>
    <div class="col-md-6">
    {{--<div id="map-canvas" style="height: 500px"></div>--}}
        @can('do-watcher')
        <form action="{{route('proposals.update', [$proposal->id])}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">

                <label for="tematics" class="control-label bold">Tematik</label>
                <select name="tematics[]" id="tematics" multiple>
                    @foreach($proposal->tematics as $tematic)
                        <option value="{{$tematic->id}}" selected>{{$tematic->name}}</option>
                    @endforeach
                </select>

            </div>

            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
        @endcan

        @if(\Gate::allows('do-technical') or \Gate::allows('do-admin'))
            <div class="form-group">
                <label for="tematics" class="control-label bold">Tematik: </label> <br/>
                <div class="form-control-static">
                    @foreach($proposal->tematics as $tematic)
                        <span class="btn btn-info btn-sm">{{$tematic->name}}</span>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>

@push('script')
<script>
    $('#tematics').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('tematics.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    unit: {{  $proposal->unit->id }},
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });
</script>
@endpush