<div class="row">
    <div class="col-md-6">
        <form action="{{route('proposals.update', [$proposal->id])}}" id="form-proposal">
            {{csrf_field()}}
          {{--  <div class="form-group">
                <label for="proposer" class="control-label">Instansi Pengusul</label>
                <select name="proposer" id="proposer" class="form-control">
                    <option value="{{$proposal->proposer_id}}">{{$proposal->proposer->name}}</option>
                </select>
            </div>--}}
            <div class="form-group">
                <label for="program" class="control-label">Kegiatan<sup>*</sup></label>
                <select name="program" id="program" class="form-control">
                    <option value="{{$proposal->program_id}}">{{$proposal->program->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="recipient" class="control-label">Penerima manfaat<sup>*</sup></label>
                <select name="recipient" id="recipient" class="form-control">
                    <option value="{{$proposal->recipient_id}}">{{$proposal->recipient->name}}</option>
                </select>
            </div>

            <div class="form-group">
                <label for="proposer_remark" class="control-label">Identitas pengusul<sup>*</sup></label>
                <textarea id="proposer_remark" name="proposer_remark"
                          class="form-control">{{$proposal->proposer_remark}}</textarea>
            </div>
            <div class="form-group">
                <label for="submission_date" class="control-label">Tanggal pengajuan</label>
                <input type="text" id="submission_date" name="submission_date" class="form-control"
                       value="{{$proposal->submission_date}}"/>
            </div>
           {{-- <div class="form-group">
                <label for="proposed_date" class="control-label">Tahun usulan pembangunan</label>
                <input type="text" id="proposed_date" name="proposed_date" class="form-control"
                       value="{{$proposal->proposed_year}}"/>
            </div>--}}
            <div class="form-group">
                <label for="proposer_phone" class="control-label">No. telp pengusul<sup>*</sup></label>
                <input type="text" id="proposer_phone" name="proposer_phone" class="form-control"
                       value="{{$proposal->proposer_phone}}"/>
            </div>
            <div class="form-group">
                <label for="proposer_email" class="control-label">Email pengusul<sup>*</sup></label>
                <input type="text" id="proposer_email" name="proposer_email" class="form-control"
                       value="{{$proposal->proposer_email}}"/>
            </div>

            <button type="button" onclick="javascript:void(0)" class="btn btn-primary" id="save-proposal">Simpan</button>
        </form>
    </div>
    {{--<div class="col-md-6">--}}
        {{--<div id="map-canvas" style="height: 500px"></div>--}}
    {{--</div>--}}
</div>

@push('script')
<script>
    $(function(){
        $("[data-toggle=popover]").popover({
            html : true,
            trigger: 'hover',
            content: function() {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function() {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });
    });
    let programSelectData = {
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit_id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    };

    let recipientSelectData = {
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit_id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//         minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    }

    $('a[href="#usulan"]').on('shown.bs.tab', function (e) {

        $('#program').select2(programSelectData);
        $('#recipient').select2(recipientSelectData);

        $('#submission_date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
            keyboardNavigation:true,
        });
    });

    $('#proposer').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('agencies.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#program').select2(programSelectData);

    $('#recipient').select2(recipientSelectData);

</script>
@endpush