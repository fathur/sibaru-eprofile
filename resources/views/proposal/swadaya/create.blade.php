@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="note note-info">
                <h3>Swadaya</h3>
                {{--<canvas id="stepStatus"></canvas>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="proposal-wizard">
                <div class="portlet-title tabbable-line">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#usulan" data-toggle="tab">
                                <i class="fa fa-pencil"></i>&nbsp; <span class="hidden-xs">Identitas</span>
                            </a>
                        </li>
                    </ul>

                    @cannot('do-technical')

                    <div class="actions">


                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> Tambah usulan <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('proposals.create.rusun')}}">Rusun</a></li>
                                <li><a href="{{route('proposals.create.rusus')}}">Rusus</a></li>
                                <li><a href="{{route('proposals.create.swadaya')}}">BPSPS</a></li>
                                <li><a href="{{route('proposals.create.ruk')}}">PSU</a></li>
                            </ul>
                        </div>
                    </div>
                    @endcannot

                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="usulan">
                            <div class="row">
                                <div class="col-md-6">

                                    @if(count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <form action="{{route('proposals.store')}}" id="form-proposal" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="unit_id" value="{{$unit->id}}">

                                      {{--  @can('do-admin')
                                        <div class="form-group">
                                            <label for="proposer" class="control-label">Instansi Pengusul</label>
                                            <select name="proposer" id="proposer" class="form-control">
                                            </select>
                                        </div>
                                        @endcan--}}

                                        <div class="form-group">
                                            <label for="program" class="control-label">Kegiatan<sup>*</sup></label>
                                            <select name="program" id="program" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient" class="control-label">Penerima manfaat<sup>*</sup></label>
                                            <select name="recipient" id="recipient" class="form-control">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="proposer_remark" class="control-label">Identitas pengusul<sup>*</sup></label>
                <textarea id="proposer_remark" name="proposer_remark"
                          class="form-control">{{old('proposer_remark')}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="submission_date" class="control-label">Tanggal pengajuan</label>
                                            <input type="text" id="submission_date" name="submission_date"
                                                   class="form-control"
                                                   value="{{old('submission_date')}}"/>
                                        </div>
                                        {{--<div class="form-group">
                                            <label for="proposed_date" class="control-label">Tahun usulan
                                                pembangunan</label>

                                            <select name="proposed_date" id="proposed_date" class="form-control">
                                                @for($year = (\Carbon\Carbon::now()->year + 20); $year >= (\Carbon\Carbon::now()->year - 5); $year--)
                                                    <option value="{{trim($year)}}" @if($year == \Carbon\Carbon::now()->year) selected @endif>{{$year}}</option>
                                                @endfor
                                            </select>
                                        </div>--}}
                                        <div class="form-group">
                                            <label for="proposer_phone" class="control-label">No. telp pengusul<sup>*</sup></label>
                                            <input type="text" id="proposer_phone" name="proposer_phone"
                                                   class="form-control"
                                                   value="{{old('proposer_phone')}}"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="proposer_email" class="control-label">Email pengusul<sup>*</sup></label>
                                            <input type="text" id="proposer_email" name="proposer_email"
                                                   class="form-control"
                                                   value="{{old('proposer_email')}}"/>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Next</button>
                                    </form>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden" id="popover-coordinate">
        <div class="popover-heading">Koordinat</div>

        <div class="popover-body">
            <p>Koordinat digunakan untuk meletakkan titik pada peta. Koordinat
                tidak bisa digunakan untuk menentukan area.</p>

            <p>Koordinate dituliskan dalam bentuk desimal dengan urutan
                <i>latitude</i> dan <i>longitude</i> yang dipisahkan tanda koma.
                Contoh penulisan koordinat <code>-6.853, 108.127</code></p>
        </div>
    </div>

    <div class="hidden" id="popover-potential_residents">
        <div class="popover-heading">Jumlah calon penghuni</div>

        <div class="popover-body">
            <p>Jumlah calon penghuni dalam bilangan <b>bulat</b>. Contoh 230 orang ditulis dengan <code>230</code></p>
        </div>
    </div>

    <div class="hidden" id="popover-area_wide">
        <div class="popover-heading">Luas wilayah</div>

        <div class="popover-body">
            <p>Luas wilayah dalam satuan m<sup>2</sup>. Cukup isi dengan bilangan bulat.</p>
        </div>
    </div>

    <div class="hidden" id="popover-area_width">
        <div class="popover-heading">Lebar tanah</div>

        <div class="popover-body">
            <p>Lebar tanah dalam satuan meter. Cukup isi dengan bilangan bulat.</p>
        </div>
    </div>

    <div class="hidden" id="popover-area_length">
        <div class="popover-heading">Panjang tanah</div>

        <div class="popover-body">
            <p>Panjang tanah dalam satuan meter. Cukup isi dengan bilangan bulat.</p>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.gitgraph')
@include('_component.bootstrap-datepicker')



@push('script')
<script>
    $(function(){
        $("[data-toggle=popover]").popover({
            html : true,
            trigger: 'hover',
            content: function() {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function() {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });
    });

   /* $('#proposer').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('agencies.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });*/

    $('#program').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#recipient').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#submission_date').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: 'linked',
        todayHighlight: true,
        autoclose: true,
        keyboardNavigation:true,
    });

    $('#proposed_date').select2({
        theme: "bootstrap"
    });

</script>
@endpush