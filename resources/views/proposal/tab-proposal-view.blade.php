@if($proposal->unit->slug == 'rusun')
    @include('proposal.rusun.view')
@elseif($proposal->unit->slug == 'rusus')
    @include('proposal.rusus.view')
@elseif($proposal->unit->slug == 'ruk')
    @include('proposal.ruk.view')
@elseif($proposal->unit->slug == 'swadaya')
    @include('proposal.swadaya.view')
@endif