<div class="row">
    <div class="col-md-12">
        <a href="{{route('proposals.documents.download.all', $proposal->id)}}" class="btn btn-info pull-right"><i class="fa fa-download"></i> Unduh semua</a>

        @can('view-administration', $proposal)
        <h3>Dokumen Administrasi</h3>

        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th>#</th>
                <th>Persyaratan</th>
                <th>Template</th>
                <th>Dokumen</th>

                @can('update-verify-administration', $proposal)
                <th class="text-center">Verifikasi</th>
                <th>Keterangan</th>
                @endcan

                {{--@cannot('verify-administration', $proposal)--}}
                @can('view-verification-administration', $proposal)
                <th class="text-center">Verifikasi</th>
                @endcan

                @can('store-administration', $proposal)
                <th>&nbsp;</th>
                @endcan

                @can('view-revise-administration', $proposal)
                <th>Keterangan</th>
                @endcan

            </tr>
            </thead>
            <tbody>
            @foreach($documentAdministrations as $document)
{{--                {{dd($document)}}--}}
                <tr>
                    <td>{{$document['order']}}</td>
                    <td>{{$document['content']}}</td>
                    <td>
                        @if($document['template'])
                        <a href='{{route('criteria.download', [$document['id']])}}'>
                            <i class="fa fa-download"></i>
                        </a>
                        @endif
                    </td>
                    <td class="col-progress col-download">
                        <div class="wrapper">
                            @if($document['document_url'] != null)
                                <a href="{{route('proposals.documents.download', [$document['proposal_id'], $document['criteria_id']])}}">{{str_limit($document['document_name'], 50)}}</a>
                            @else
                                -
                            @endif
                        </div>
                    </td>

                    @can('update-verify-administration', $proposal)
                    <td class="text-center">
                        <div class="ckbox ckbox-theme circle">
                            <input type="checkbox" @if($document['document_is_verified']) checked @endif
                                   class="verify-check"
                                   id="{{$document['proposal_id']}}-{{$document['criteria_id']}}"
                                   name="is_verified"
                                   value="yes"
                                   title="Verifikasi"
                                   data-proposal="{{$document['proposal_id']}}"
                                   data-criteria="{{$document['criteria_id']}}"
                                    />
                            <label for="{{$document['proposal_id']}}-{{$document['criteria_id']}}"></label>
                        </div>
                    </td>
                    <td>
                        <textarea name="description" id="description"
                                  class="form-control doc-desc"
                                  data-proposal="{{$document['proposal_id']}}"
                                  data-criteria="{{$document['criteria_id']}}"
                                  title="Keterangan" style="resize: none;">{{$document['document_description']}}</textarea>
                    </td>
                    @endcan

                    @can('view-verification-administration', $proposal)
                    <td class="text-center">
                        @if($document['document_is_verified'])
                            <span class="text-success"><i class="fa fa-check"></i></span>
                        @else
                            <span class="text-danger"><i class="fa fa-times"></i></span>
                        @endif
                    </td>
                    @endcan

                    @can('store-administration', $proposal)
                    <td>
                        <input type="file" class="simple-upload pull-right"
                               data-criteria="{{$document['criteria_id']}}"
                               data-proposal="{{$proposal->id}}">
                    </td>
                    @endcan

                    @can('view-revise-administration', $proposal)
                    <td>
                        @if($document['document_is_verified'])
                            <span class="text-success"><i class="fa fa-check"></i></span>
                            {{$document['document_description']}}
                        @else
                            <span class="text-danger"><i class="fa fa-times"></i></span>
                            <strong><ins>{{$document['document_description']}}</ins></strong>
                        @endif
                    </td>
                    @endcan

                </tr>
            @endforeach
            </tbody>
        </table>
        @endcan

        @can('view-technical', $proposal)
        <h3>Dokumen Teknis</h3>

        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th>#</th>
                <th>Persyaratan</th>
                <th>Template</th>
                <th>Dokumen</th>

                @can('upverify-technical', $proposal)
                <th class="text-center">Verifikasi</th>
                <th>Keterangan</th>
                @endcan

                @can('view-verification-technical', $proposal)
                <th class="text-center">Verifikasi</th>
                @endcan

                @can('store-technical', $proposal)
                <th>&nbsp;</th>
                @endcan

                @can('view-revise-technical', $proposal)
                <th>Keterangan</th>
                @endcan

            </tr>
            </thead>
            <tbody>
            @foreach($documentTechnicals as $document)
                {{--{{dd($document)}}--}}
                <tr>
                    <td>{{$document['order']}}</td>
                    <td>{{$document['content']}}</td>
                    <td>
                        @if($document['template'])
                            <a href='{{route('criteria.download', [$document['id']])}}'>
                                <i class="fa fa-download"></i>
                            </a>
                        @endif
                    </td>
                    <td class="col-progress col-download">
                        <div class="wrapper">
                            @if($document['document_url'] != null)
                                <a href="{{route('proposals.documents.download', [$document['proposal_id'], $document['criteria_id']])}}">{{str_limit($document['document_name'], 50)}}</a>
                            @else
                                -
                            @endif
                        </div>
                    </td>

                    @can('update-verify-technical', $proposal)
                    <td class="text-center">
                        <div class="ckbox ckbox-theme circle">
                            <input type="checkbox" @if($document['document_is_verified']) checked @endif
                            class="verify-check"
                                   id="{{$document['proposal_id']}}-{{$document['criteria_id']}}"
                                   name="is_verified"
                                   value="yes"
                                   title="Verifikasi"
                                   data-proposal="{{$document['proposal_id']}}"
                                   data-criteria="{{$document['criteria_id']}}"
                            />
                            <label for="{{$document['proposal_id']}}-{{$document['criteria_id']}}"></label>
                        </div>
                    </td>
                    <td>
                        <textarea name="description" id="description"
                                  class="form-control doc-desc"
                                  data-proposal="{{$document['proposal_id']}}"
                                  data-criteria="{{$document['criteria_id']}}"
                                  title="Keterangan" style="resize: none;">{{$document['document_description']}}</textarea>
                    </td>
                    @endcan


                    @can('view-verification-technical', $proposal)
                    <td class="text-center">
                        @if($document['document_is_verified'])
                            <span class="text-success"><i class="fa fa-check"></i></span>
                        @else
                            <span class="text-danger"><i class="fa fa-times"></i></span>
                        @endif
                    </td>
                    @endcan

                    @can('store-technical', $proposal)
                    <td class="">
                        <input type="file" class="simple-upload pull-right"
                               data-criteria="{{$document['criteria_id']}}"
                               data-proposal="{{$proposal->id}}">
                    </td>
                    @endcan

                    @can('view-revise-technical', $proposal)
                    <td>
                        @if($document['document_is_verified'])
                            <span class="text-success"><i class="fa fa-check"></i></span>
                            {{$document['document_description']}}
                        @else
                            <span class="text-danger"><i class="fa fa-times"></i></span>
                            <strong><ins>{{$document['document_description']}}</ins></strong>
                        @endif
                    </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
        @endcan

        {{--
        @can('view-administration', $proposal)
        <h3>Dokumen lain</h3>

        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th>Order</th>
                <th>Kriteria</th>
                <th>Dokumen</th>

                @can('verify-administration', $proposal)
                <th class="text-center">Verifikasi</th>
                <th>Keterangan</th>
                @endcan

                @if(Gate::denies('verify-administration', $proposal) and Gate::denies('store-administration', $proposal))
                <th class="text-center">Verifikasi</th>
                @endif

                @can('store-administration', $proposal)
                <th>&nbsp;</th>
                @endcan

            </tr>
            </thead>
            <tbody>
            @foreach($documentOthers as $document)
                <tr>
                    <td>{{$document['order']}}</td>
                    <td>{{$document['content']}}</td>
                    <td class="col-progress col-download">
                        <div class="wrapper">
                        @if($document['document_url'] != null)
                            <a href="{{route('proposals.documents.download', [$document['proposal_id'], $document['criteria_id']])}}">{{str_limit($document['document_name'], 50)}}</a>
                        @else
                            -
                        @endif
                        </div>
                    </td>

                    @can('verify-administration', $proposal)
                    <td class="text-center">
                        <div class="ckbox ckbox-theme circle">
                            <input type="checkbox" @if($document['document_is_verified']) checked @endif
                            class="verify-check"
                                   id="{{$document['proposal_id']}}-{{$document['criteria_id']}}"
                                   name="is_verified"
                                   value="yes"
                                   title="Verifikasi"
                                   data-proposal="{{$document['proposal_id']}}"
                                   data-criteria="{{$document['criteria_id']}}"
                            />
                            <label for="{{$document['proposal_id']}}-{{$document['criteria_id']}}"></label>
                        </div>
                    </td>
                    <td>
                        <textarea name="description" id="description"
                                  class="form-control doc-desc"
                                  data-proposal="{{$document['proposal_id']}}"
                                  data-criteria="{{$document['criteria_id']}}"
                                  title="Keterangan" style="resize: none;">{{$document['document_description']}}</textarea>
                    </td>
                    @endcan



                    @if(Gate::denies('verify-administration', $proposal) and Gate::denies('store-administration', $proposal))
                    <td class="text-center">
                        @if($document['document_is_verified'])
                            <span class="text-success"><i class="fa fa-check"></i></span>
                        @else
                            <span class="text-danger"><i class="fa fa-times"></i></span>
                        @endif
                    </td>
                    @endif

                    @can('store-administration', $proposal)
                    <td>
                        <input type="file" class="simple-upload pull-right"
                               data-criteria="{{$document['criteria_id']}}"
                               data-proposal="{{$proposal->id}}">
                    </td>
                    @endcan

                </tr>
            @endforeach
            </tbody>
        </table>
        @endcan
        --}}
    </div>
</div>

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
<script src="{{asset('components/autosize/dist/autosize.min.js')}}"></script>
@endpush

@push('script')
<script>
    $(document).ready(function () {

        autosize(document.querySelectorAll('textarea'));

        $('.simple-upload').change(function () {

            var $this = $(this);
            var $thisProgressStatus =   $this.parent().parent().find('.col-progress');
            var $thisDownload =   $this.parent().parent().find('.col-download');

            var proposalId = $(this).data('proposal');
            var criteriaId = $(this).data('criteria');

            $(this).simpleUpload('{{route('proposals.documents')}}', {
                name: 'document',
                data: {
                    proposal_id: proposalId,
                    criteria_id: criteriaId,
                    _token: '{{csrf_token()}}'
                },
                progress: function(progress) {
                    var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                            '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                            '<span class="">'+Math.round(progress)+'%</span> ' +
                            '</div> ' +
                            '</div>';
                    $thisProgressStatus.find('.wrapper').html(htmlProgress);
                },
                success: function(data) {

                    var htmlLink = '<a href="{{url('usulan/download')}}/'+data.proposal_id+'/'+data.criteria_id+'">'+data.file_name+'</a>';

                    $thisDownload.find('.progress').fadeOut();
                    $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                    $thisDownload.find('.wrapper').fadeIn();

                },
                error: function(error) {
                    console.log(error);
                    $thisDownload.find('.progress').fadeOut();
                    $thisDownload.find('.wrapper')
                            .html('<span class="text-danger">'+error.message+'!</span>')
                            .css({display:'none'});
                    $thisDownload.find('.wrapper').fadeIn();
                }
            });
        });
    });

    $('.verify-check').click(function(){

        var $this = $(this);

        $.post('{{route('proposals.criterias')}}', {
            _token: '{{csrf_token()}}',
            proposal_id: $this.data('proposal') ,
            criteria_id: $this.data('criteria'),
            is_verified: $this[0].checked
        })
    });

    $('.doc-desc').blur(function(){
        var $this = $(this);

        $.post('{{route('proposals.criterias')}}', {
            _token: '{{csrf_token()}}',
            proposal_id: $this.data('proposal') ,
            criteria_id: $this.data('criteria'),
            description: $this.val()
        })
    });
</script>
@endpush