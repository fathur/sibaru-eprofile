

<div class="row">
    <div class="col-md-12">
        <h3>Hasil survey</h3>

        @can('create-survey', $proposal)
        <button type="button" class="btn btn-primary pull-right"
                data-toggle="modal"
                data-target="#general-modal"
                data-backdrop="static"
                data-keyboard="false"
                id="btn-add-survey-result">
            <i class="fa fa-plus"></i>&nbsp; Tambah
        </button>
        @endcan

        <table class="table table-condensed table-striped table-hover" id="survey-result-datatables">
            <thead>
            <tr>
                <th>Survey</th>
                <th>Tanggal</th>
                <th>Lampiran</th>

                @can('create-survey', $proposal)
                <th>&nbsp;</th>
                @endcan

            </tr>
            </thead>
        </table>
    </div>
</div>


@push('script')
<script>

    $('#survey-result-datatables').DataTable({
        processing: true,
        serverSide: true,
//        stateSave: true,
        paging: false,
        info: false,
        searching: false,
        ajax: {
            url: "{{route('proposals.surveys.data')}}",
            data: {
                proposal_id: {{$proposal->id}}
            }
        },
        columns: [
            {data: 'content', name: 'content'},
            {data: 'date', name: 'date'},
            {data: 'attachment', name: 'attachment'},

            @can('create-survey', $proposal)
            {data: 'action', name: 'action', searchable: false, orderable: false},
            @endcan
        ],
    });

    $('#btn-add-survey-result').click(function () {
        $.get('{{route('proposals.surveys.create')}}', {
            unit_slug: '{{$proposal->unit->slug}}',
            proposal_id: {{$proposal->id}}
        }, function (response) {
            var modalId = 'general-modal';
            var $modalId = $('#' + modalId);
            $modalId.find('.modal-body').html('');

            $modalId.find('.modal-header').find('.modal-title').html(response.title);
            $modalId.find('.modal-body').html(response.body);
            $modalId.find('.modal-footer').html(response.footer);
        });
    });
</script>
@endpush