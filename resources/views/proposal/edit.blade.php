@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="note note-info">
                <h3>{{title_case($proposal->unit->slug)}} ({{$proposal->status->name}})</h3>
                {{--<canvas id="stepStatus"></canvas>--}}
            </div>
        </div>
    </div>
    <div class="row">
        {{--<div class="@can('view-comments', $proposal) col-md-8 @else col-md-12 @endcan">--}}
        <div class="col-md-8">
            <div class="portlet light" id="proposal-wizard">
                <div class="portlet-title tabbable-line">
                    {{--<div class="caption">--}}
                    {{--<span class="caption-subject">Portlet Tabs</span>--}}
                    {{--</div>--}}
                    <ul class="nav nav-tabs pull-left">
                        <li class="">
                            <a href="#usulan" data-toggle="tab">
                                <i class="fa fa-pencil"></i>&nbsp; <span class="hidden-xs">Identitas</span>
                            </a>
                        </li>
                        <li class="@if($proposal->status->slug == 'proposal-draft') active @endif">
                            <a href="#dwelling" data-toggle="tab"> <i class="fa fa-home"></i>&nbsp; <span
                                        class="hidden-xs">Spesifikasi</span> </a>
                        </li>
                        <li class="">
                            <a href="#documents" data-toggle="tab"> <i class="fa fa-folder"></i>&nbsp; <span
                                        class="hidden-xs">Dokumen</span> </a>
                        </li>

                        @can('view-surveyor', $proposal)
                        <li class="">
                            <a href="#surveyor" data-toggle="tab">
                                <i class="fa fa-check-square-o"></i>&nbsp; <span class="hidden-xs">Surveyor</span>
                            </a>
                        </li>
                        @endcan

                        @can('view-survey', $proposal)
                        <li class="">
                            <a href="#survey" data-toggle="tab">
                                <span class="">
                                    <i class="fa fa-check-square-o"></i>&nbsp; <span class="hidden-xs">Survey</span>
                                </span>
                            </a>
                        </li>
                        @endcan



                    </ul>

                    <div class="action">

                        @can('distribute-proposal', $proposal)
                        <form action="{{route('proposals.statuses.update')}}" method="post" style="margin-top: -14px;">
                            {{csrf_field()}}
                            <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-exchange"></i> Distribusi</button>
                        </form>
                        @endcan

                        @can('send-proposal', $proposal)
                        <form action="{{route('proposals.statuses.update')}}" method="post" style="margin-top: -14px;">
                            {{csrf_field()}}
                            <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-send"></i> Kirim</button>
                        </form>
                        @endcan

                        @can('action-new-proposal', $proposal)
                        <div class="btn-group pull-right" role="group" >
                            <button type="button" class="btn btn-success" id="criteria-pass"><i class="fa fa-check"></i> Setujui</button>
                            <button type="button" class="btn btn-info" id="criteria-revise"><i class="fa fa-refresh"></i> Revisi</button>
                            <button type="button" class="btn btn-danger" id="criteria-fail"><i class="fa fa-times"></i> Tolak</button>
                        </div>
                        @endcan

                        @can('action-technical-proposal', $proposal)
                        <div class="btn-group pull-right" role="group" >
                            <button type="button" class="btn btn-success" id="criteria-pass"><i class="fa fa-check"></i> Setujui</button>
                            <button type="button" class="btn btn-danger" id="criteria-fail"><i class="fa fa-times"></i> Tolak</button>
                        </div>
                        @endcan

                        @can('shortlists.create', $proposal)
                        <a href="{{route('shortlists.create', [$proposal->id])}}" class="btn btn-success pull-right"><i class="fa fa-list-ol"></i> Masukkan shortlist</a>
                        @endcan
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">


                        <div class="tab-pane" id="usulan">
                            @can('edit-proposal', $proposal)
                                @include('proposal.tab-proposal-edit')
                            @else
                                @include('proposal.tab-proposal-view')
                            @endcan
                        </div>

                        <div class="tab-pane @if($proposal->status->slug == 'proposal-draft') active @endif" id="dwelling">
                            @include('proposal.tab-dwelling')
                        </div>

                        <div class="tab-pane" id="documents">
                            @include('proposal.tab-documents')
                        </div>

                        @can('view-surveyor', $proposal)
                        <div class="tab-pane" id="surveyor">
                            @include('proposal.tab-surveyor')
                        </div>
                        @endcan

                        @can('view-survey', $proposal)
                        <div class="tab-pane" id="survey">
                            @include('proposal.tab-survey')
                        </div>
                        @endcan

                        <ul class="pager wizard">
                            <li class="previous first" style="display:none;"><a href="javascript:void(0);">First</a>
                            </li>
                            <li class="previous"><a href="javascript:void(0);">Previous</a></li>
                            <li class="next last" style="display:none;"><a href="javascript:void(0);">Last</a></li>
                            <li class="next"><a href="javascript:void(0);">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        {{--@can('view-comments', $proposal)--}}
        <div class="col-md-4">
            @include('proposal.message')
        </div>
        {{--@endcan--}}
    </div>
@stop

@push('scripts')
<script src="{{asset('components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{asset('components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
@endpush
@include('_component.datatables')
@include('_component.select2')
@include('_component.gitgraph')
@include('_component.bootstrap-datepicker')
@include('_component.easyui')

@push('script')
<script>
    $(document).ready(function () {

        // Re adjust table size in tab content
        // https://github.com/DataTables/Responsive/issues/40
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).css('width', '100%');
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
        });

        $('#save-proposal').click(function() {

            $.ajax({
                type: "PUT",
                url: $(this).parents('form').attr('action'),
                data: $('#form-proposal').serialize(),
                success: function (data) {
                    // kasih centang gede
                    swal({
                        title: 'Good job!',
                        text: 'Usulan ini berhasil di update!',
                        timer: 2000,
                        type: 'success'
                    });

                }
            });
        });

        @can('update-proposal', $proposal)
        var $validator = $('#form-proposal').validate({
            rules: {
                proposer: {
                    required: true,
                },
                program: {
                    required: true,
                },
                recipient: {
                    required: true,
                }
            },
            highlight:function(element) {
                $(element).parents('.form-group').addClass('has-error');
                $(element).next('.error').addClass('help-block');
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').removeClass('has-error');

            },
            submitHandler: function() {
//                alert("submitted!");
            }
        });
        @endcan

        $('#proposal-wizard').bootstrapWizard({
            @can('update-proposal', $proposal)
            onNext: function (tab, navigation, index) {

                var $valid = $("#form-proposal").valid();
                if(!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            },
            onTabClick: function (tab, navigation, index) {
                console.log(tab);
                console.log(navigation);
                console.log(index);
                var $valid = $("#form-proposal").valid();
                if(!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }
            @endcan
        });
    });

    $('#criteria-pass').click(function(){

        swal({
            title: 'Apakah yakin ingin disetujui?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, setujui!'
        }).then(function () {

            $.post('{{route('proposals.statuses.update')}}', {
                _token: '{{csrf_token()}}',
                proposal_id: {{$proposal->id}}
            }, function(resp) {
                console.log(resp)

                swal({
                    title: 'Good job!',
                    text: 'Usulan ini berhasil di update!',
                    timer: 1000,
                    type: 'success'
                });

                location.reload();

            });
        });


    });

    $('#criteria-fail').click(function(){
        swal({
            title: 'Apakah yakin ingin ditolak?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, tolak!'
        }).then(function () {
            $.post('{{route('proposals.statuses.update')}}', {
                _token: '{{csrf_token()}}',
                proposal_id: {{$proposal->id}},
                force: 'reject'
            }, function (resp) {
                swal({
                    title: 'Good job!',
                    text: 'Usulan ini berhasil di update!',
                    timer: 2000,
                    type: 'success'
                });

                location.reload();
            });
        });
    });

    $('#criteria-revise').click(function(){
        swal({
            title: 'Apakah yakin ingin direvisi?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, revisi kembali!'
        }).then(function () {
            $.post('{{route('proposals.statuses.update')}}', {
                _token: '{{csrf_token()}}',
                proposal_id: {{$proposal->id}},
                force: 'revise'
            }, function (resp) {
                swal({
                    title: 'Good job!',
                    text: 'Usulan ini berhasil di update!',
                    timer: 2000,
                    type: 'success'
                });

                location.reload();
            });
        });
    });

    {{--var gitgraph = new GitGraph({--}}
        {{--elementId: "stepStatus",--}}
        {{--template: "metro",--}}
        {{--orientation: "horizontal",--}}
        {{--mode: "compact"--}}
    {{--});--}}

    {{--var master = gitgraph.branch("master");--}}
    {{--gitgraph.commit("This commit is mine").commit({--}}
        {{--dotColor: "white",--}}
        {{--dotSize: 10,--}}
        {{--dotStrokeWidth: 10,--}}
        {{--sha1: "666",--}}
        {{--message: "Pimp dat commit",--}}
        {{--author: "Jacky <prince@dutunning.com>",--}}
        {{--tag: "Diterima",--}}
    {{--}).commit();--}}

    {{--var develop = gitgraph.branch("develop");    // New branch from HEAD--}}
    {{--var myfeature = develop.branch("myfeature");--}}
</script>
@endpush