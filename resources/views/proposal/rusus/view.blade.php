<div class="row">
    <div class="col-md-6">
        {{--<div class="form-group">
            <label for="proposer" class="control-label bold">Instansi Pengusul</label> <br>
            <div class="form-control-static">{{$proposal->proposer->name}}</div>
        </div>--}}
        <div class="form-group">
            <label for="program" class="control-label bold">Kegiatan</label> <br>
            <p class="form-control-static">{{$proposal->program->name}}</p>
        </div>
        <div class="form-group">
            <label for="recipient" class="control-label bold">Penerima manfaat</label> <br>
            <p class="form-control-static">{{$proposal->recipient->name}}</p>
        </div>
        <div class="form-group">
            <label for="location" class="control-label bold">Lokasi</label> <br>
            <p class="form-control-static">{{Cache::get('region-'.$proposal->location_id)}}</p>
        </div>
        <div class="form-group">
            <label for="address" class="control-label bold">Alamat</label> <br>
            <p class="form-control-static">{{$proposal->address}}</p>
        </div>
        <div class="form-group">
            <label for="coordinate" class="control-label bold">Koordinat</label> <br>
            <p class="form-control-static">{{$proposal->latitude}}, {{$proposal->longitude}}</p>

        </div>
        <div class="form-group">
            <label for="proposer_remark" class="control-label bold">Identitas pengusul</label> <br>
            <p class="form-control-static">{{$proposal->proposer_remark}}</p>

        </div>
        <div class="form-group">
            <label for="submission_date" class="control-label bold">Tanggal pengajuan</label> <br>
            <p class="form-control-static">{{$proposal->submission_date}}</p>
        </div>
        {{--<div class="form-group">
            <label for="proposed_date" class="control-label bold">Tahun usulan pembangunan</label> <br>
            <p class="form-control-static">{{$proposal->proposed_year}}</p>
        </div>--}}
        <div class="form-group">
            <label for="proposer_phone" class="control-label bold">No. telp pengusul</label> <br>
            <p class="form-control-static">
                <a href="tel:{{$proposal->proposer_phone}}">{{$proposal->proposer_phone}}</a>
            </p>

        </div>
        <div class="form-group">
            <label for="proposer_email" class="control-label bold">Email pengusul</label> <br>
            <p class="form-control-static">
                <a href="mailto:{{$proposal->proposer_email}}">{{$proposal->proposer_email}}</a>
            </p>
        </div>
    </div>
    <div class="col-md-6">
        <div id="map-canvas" style="height: 500px" class="margin-bottom-15"></div>

        @can('do-watcher')
        <form action="{{route('proposals.update', [$proposal->id])}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">

                <label for="tematics" class="control-label bold">Tematik</label>
                <select name="tematics[]" id="tematics" multiple>
                    @foreach($proposal->tematics as $tematic)
                        <option value="{{$tematic->id}}" selected>{{$tematic->name}}</option>
                    @endforeach
                </select>

            </div>

            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
        @endcan

        @if(\Gate::allows('do-technical') or \Gate::allows('do-admin'))
            <div class="form-group">
                <label for="tematics" class="control-label bold">Tematik: </label> <br/>
                <div class="form-control-static">
                    @foreach($proposal->tematics as $tematic)
                        <span class="btn btn-info btn-sm">{{$tematic->name}}</span>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
@push('scripts')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
        type="text/javascript"></script>
@endpush
@push('script')
<script>

    var map, marker;
    function initMap() {
        var myLatLng = new google.maps.LatLng({{$proposal->latitude}},{{$proposal->longitude}});

        var mapOptions = {
            zoom: 4,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // scrollwheel: false,
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        // current location
        var currentLatLng = new google.maps.LatLng({{$proposal->latitude}},{{$proposal->longitude}});
        geo.mark(currentLatLng);
    }

    var geo = {

        mark: function (location) {
            if (marker) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
        }
    }

    $('#tematics').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('tematics.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    unit: {{  $proposal->unit->id }},
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });
</script>
@endpush