<div class="hidden" id="popover-coordinate">
    <div class="popover-heading">Koordinat</div>

    <div class="popover-body">
        <p>Koordinat digunakan untuk meletakkan titik pada peta. Koordinat
            tidak bisa digunakan untuk menentukan area.</p>

        <p>Koordinate dituliskan dalam bentuk desimal dengan urutan
            <i>latitude</i> dan <i>longitude</i> yang dipisahkan tanda koma.
            Contoh penulisan koordinat <code>-6.853, 108.127</code></p>
    </div>
</div>

<div class="hidden" id="popover-potential_residents">
    <div class="popover-heading">Jumlah calon penghuni</div>

    <div class="popover-body">
        <p>Jumlah calon penghuni dalam bilangan <b>bulat</b>. Contoh 230 orang ditulis dengan <code>230</code></p>
    </div>
</div>

<div class="hidden" id="popover-area_wide">
    <div class="popover-heading">Luas wilayah</div>

    <div class="popover-body">
        <p>Luas wilayah dalam satuan m<sup>2</sup>. Cukup isi dengan bilangan bulat.</p>
    </div>
</div>

<div class="hidden" id="popover-area_width">
    <div class="popover-heading">Lebar tanah</div>

    <div class="popover-body">
        <p>Lebar tanah dalam satuan meter. Cukup isi dengan bilangan bulat.</p>
    </div>
</div>

<div class="hidden" id="popover-area_length">
    <div class="popover-heading">Panjang tanah</div>

    <div class="popover-body">
        <p>Panjang tanah dalam satuan meter. Cukup isi dengan bilangan bulat.</p>
    </div>
</div>