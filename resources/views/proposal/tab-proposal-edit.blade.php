@if($proposal->unit->slug == 'rusun')
    @include('proposal.rusun.edit')
@elseif($proposal->unit->slug == 'rusus')
    @include('proposal.rusus.edit')
@elseif($proposal->unit->slug == 'ruk')
    @include('proposal.ruk.edit')
@elseif($proposal->unit->slug == 'swadaya')
    @include('proposal.swadaya.edit')
@endif