<div class="btn-group btn-group-xs pull-right">
    @can('distribute-to-directorate', $proposal)
    <button title="Distribusi" class="btn btn-success btn-xs">
        <i class="fa fa-exchange"></i> <span class="hidden-xs hidden-sm">&nbsp; Distribusi</span>
    </button>
    @endcan
    <a class="btn btn-warning" href="{{$edit_link}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp;Edit&nbsp;</span>
    </a>
    @if(Gate::denies('do-watcher'))
        <button type="button" class="btn btn-danger" onclick="deleteForm(this)"
                data-link="{{$delete_link}}"
                data-identifier="{{$identifier}}">
            <i class="fa fa-trash"></i>
            <span class="hidden-xs hidden-sm">&nbsp;Delete</span>
        </button>
    @endif
</div>
@can('change-status')
<button class="btn btn-xs btn-default pull-right margin-right-10"
        onclick="editForm(this)"
        data-form="{{$edit_status_link}}"
        data-identifier="{{$identifier}}"><i class="fa fa-pencil"></i></button>
@endcan

