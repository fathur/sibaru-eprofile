@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="note note-info">
                <h3>RUK</h3>
                {{--<canvas id="stepStatus"></canvas>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="proposal-wizard">
                <div class="portlet-title tabbable-line">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#usulan" data-toggle="tab">
                                <i class="fa fa-pencil"></i>&nbsp; <span class="hidden-xs">Identitas</span>
                            </a>
                        </li>
                    </ul>

                    @cannot('do-technical')
                    <div class="actions">

                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> Tambah usulan <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('proposals.create.rusun')}}">Rusun</a></li>
                                <li><a href="{{route('proposals.create.rusus')}}">Rusus</a></li>
                                <li><a href="{{route('proposals.create.swadaya')}}">BSPS</a></li>
                                <li><a href="{{route('proposals.create.ruk')}}">PSU</a></li>
                            </ul>
                        </div>
                    </div>
                    @endcannot
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="usulan">
                            <div class="row">
                                <div class="col-md-6">

                                    @if(count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <form action="{{route('proposals.store')}}" id="form-proposal" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="unit_id" value="{{$unit->id}}">

                                        {{--@can('do-admin')
                                        <div class="form-group">
                                            <label for="proposer" class="control-label">Instansi Pengusul</label>
                                            <select name="proposer" id="proposer" class="form-control">
                                            </select>
                                        </div>
                                        @endcan--}}

                                        <div class="form-group">
                                            <label for="program" class="control-label">Kegiatan<sup>*</sup></label>
                                            <select name="program" id="program" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient" class="control-label">Penerima manfaat<sup>*</sup></label>
                                            <select name="recipient" id="recipient" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="location" class="control-label">Lokasi<sup>*</sup></label>
                                            <input type="text" id="location" name="location" placeholder="Lokasi"
                                            aria-describedby="help-block-location" style="width: 100%;">
                                            {{--<select name="location" id="location" class="form-control"--}}
                                                    {{--onchange="geo.searchInLocation()">--}}
                                            {{--</select>--}}
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="control-label">Alamat<sup>*</sup></label>
                <textarea id="address" name="address" class="form-control"
                          onkeyup="geo.searchInAddress()">{{old('address')}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="coordinate" class="control-label">Koordinat
                                                <a tabindex="0" data-toggle="popover" data-popover-content="#popover-coordinate"><i class="fa fa-info-circle"></i></a>
                                            </label>
                                            <input type="text" id="coordinate" name="coordinate" class="form-control"
                                                   value="{{old('coordinate')}}" onkeyup="geo.addToMap(this)"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="proposer_remark" class="control-label">Identitas
                                                pengusul<sup>*</sup></label>
                <textarea id="proposer_remark" name="proposer_remark"
                          class="form-control">{{old('proposer_remark')}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="submission_date" class="control-label">Tanggal pengajuan</label>
                                            <input type="text" id="submission_date" name="submission_date"
                                                   class="form-control"
                                                   value="{{old('submission_date')}}"/>
                                        </div>
                                        {{--<div class="form-group">
                                            <label for="proposed_date" class="control-label">Tahun usulan
                                                pembangunan</label>

                                            <select name="proposed_date" id="proposed_date" class="form-control">
                                                @for($year = (\Carbon\Carbon::now()->year + 20); $year >= (\Carbon\Carbon::now()->year - 5); $year--)
                                                    <option value="{{trim($year)}}" @if($year == \Carbon\Carbon::now()->year) selected @endif>{{$year}}</option>
                                                @endfor
                                            </select>
                                        </div>--}}
                                        <div class="form-group">
                                            <label for="proposer_phone" class="control-label">No. telp pengusul<sup>*</sup></label>
                                            <input type="text" id="proposer_phone" name="proposer_phone"
                                                   class="form-control"
                                                   value="{{old('proposer_phone')}}"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="proposer_email" class="control-label">Email pengusul<sup>*</sup></label>
                                            <input type="text" id="proposer_email" name="proposer_email"
                                                   class="form-control"
                                                   value="{{old('proposer_email')}}"/>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Next</button>
                                    </form>
                                </div>

                                <div class="col-md-6">
                                    <div id="map-canvas" style="height: 500px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.gitgraph')
@include('_component.bootstrap-datepicker')
@include('_component.easyui')

@push('scripts')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
        type="text/javascript"></script>
@endpush

@push('script')
<script>
    $(function(){
        $("[data-toggle=popover]").popover({
            html : true,
            trigger: 'hover',
            content: function() {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function() {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });
    });

   /* $('#proposer').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('agencies.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });*/

    $('#program').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#recipient').select2({
        theme: "bootstrap",
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$unit->id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });


    $('#submission_date').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: 'linked',
        todayHighlight: true,
        autoclose: true,
        keyboardNavigation:true,
    });

    $('#proposed_date').select2({
        theme: "bootstrap"
    });

    var map, marker;
    function initMap() {
        var myLatLng = new google.maps.LatLng({{config('sibaru.main_coordinate.latitude')}},{{config('sibaru.main_coordinate.longitude')}});

        var mapOptions = {
            zoom: 4,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // scrollwheel: false,
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        google.maps.event.addListener(map, 'click', function (event) {
            geo.mark(event.latLng);
        });

    }

    var geo = {
        search: function (text) {

            if (text !== null) {

                var geoCoder = new google.maps.Geocoder();
                geoCoder.geocode({
                    'address': text
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        //console.log(results);
                        map.setCenter(results[0].geometry.location);

                        if (results[0].geometry.viewport)
                            map.fitBounds(results[0].geometry.viewport);
                    } else {

                        // alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        },
        searchInLocation: function (location) {

            var loc;
            if(location === undefined) {
                loc = $('#location').select2('data');
            } else {
                loc = location;
            }

            if (loc.length !== 0) {
                this.search(loc[0].name);
            }
        },
        searchInAddress: function () {
            var locationText = $('#location').combotree('getText');
            var text = $('#address').val() + ', ' + locationText + ', Indonesia';


            this.search(text);
        },
        mark: function (location) {
            if (marker) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }

            $('#coordinate').val(marker.getPosition().lat() + ', ' + marker.getPosition().lng());
        },
        addToMap: function (input) {
            var location = $(input).val();
            var locationSplit = location.split(',');

            if (locationSplit !== undefined) {

                if (locationSplit[1] !== undefined) {
                    var lat = locationSplit[0].trim();
                    lat = parseFloat(lat);
                    var lng = locationSplit[1].trim();
                    lng = parseFloat(lng);
                    map.setCenter({lat: lat, lng: lng});

                    if (marker) {
                        marker.setPosition({lat: lat, lng: lng});
                    } else {
                        marker = new google.maps.Marker({
                            position: {lat: lat, lng: lng},
                            map: map
                        });
                    }
                }
            }
        }
    };

   $('#location').combotree({
       @if(is_null($agency))
       url: "{{route('regions.combotree')}}?level=4",
       @else
       url: "{{route('regions.combotree')}}?level=4&id={{$agency->region->id}}",
       @endif
       method: "GET",
       idField: "id",
       treeField: "name",
       height: 34,
       onSelect: function(node) {
           geo.searchInLocation(node.name + ', Indonesia' );
       }
   });
</script>
@endpush