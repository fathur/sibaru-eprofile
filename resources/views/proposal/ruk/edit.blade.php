<div class="row">
    <div class="col-md-6">
        <form action="{{route('proposals.update', [$proposal->id])}}" id="form-proposal">
            {{csrf_field()}}
           {{-- <div class="form-group">
                <label for="proposer">Instansi Pengusul</label>
                <select name="proposer" id="proposer" class="form-control">
                    <option value="{{$proposal->proposer_id}}">{{$proposal->proposer->name}}</option>
                </select>
            </div>--}}
            <div class="form-group">
                <label for="program">Kegiatan<sup>*</sup></label>
                <select name="program" id="program" class="form-control">
                    <option value="{{$proposal->program_id}}">{{$proposal->program->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="recipient">Penerima manfaat<sup>*</sup></label>
                <select name="recipient" id="recipient" class="form-control">
                    <option value="{{$proposal->recipient_id}}">{{$proposal->recipient->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="location" class="control-label">Lokasi<sup>*</sup></label>
                <input type="text" id="location" name="location" placeholder="Lokasi"
                       aria-describedby="help-block-location" style="width: 100%;">
                {{--<select name="location" id="location" class="form-control" onchange="geo.searchInLocation()">--}}
                    {{--<option value="{{$proposal->location_id}}">{{Cache::get('region-'.$proposal->location_id)}}</option>--}}
                {{--</select>--}}
            </div>
            <div class="form-group">
                <label for="address">Alamat<sup>*</sup></label>
                <textarea id="address" name="address" class="form-control"
                          onkeyup="geo.searchInAddress()">{{$proposal->address}}</textarea>
            </div>
            <div class="form-group">
                <label for="coordinate">Koordinat</label>
                <input type="text" id="coordinate" name="coordinate" class="form-control"
                       value="{{$proposal->latitude}}, {{$proposal->longitude}}" onkeyup="geo.addToMap(this)"/>
            </div>
            <div class="form-group">
                <label for="proposer_remark">Identitas pengusul<sup>*</sup></label>
                <textarea id="proposer_remark" name="proposer_remark"
                          class="form-control">{{$proposal->proposer_remark}}</textarea>
            </div>
            <div class="form-group">
                <label for="submission_date">Tanggal pengajuan</label>
                <input type="text" id="submission_date" name="submission_date" class="form-control"
                       value="{{$proposal->submission_date}}"/>
            </div>
           {{-- <div class="form-group">
                <label for="proposed_date">Tahun usulan pembangunan</label>
                <input type="text" id="proposed_date" name="proposed_date" class="form-control"
                       value="{{$proposal->proposed_year}}"/>
            </div>--}}
            <div class="form-group">
                <label for="proposer_phone">No. telp pengusul<sup>*</sup></label>
                <input type="text" id="proposer_phone" name="proposer_phone" class="form-control"
                       value="{{$proposal->proposer_phone}}"/>
            </div>
            <div class="form-group">
                <label for="proposer_email">Email pengusul<sup>*</sup></label>
                <input type="text" id="proposer_email" name="proposer_email" class="form-control"
                       value="{{$proposal->proposer_email}}"/>
            </div>
            <button type="button" onclick="javascript:void(0)" class="btn btn-primary" id="save-proposal">Simpan</button>

        </form>
    </div>
    <div class="col-md-6">
        <div id="map-canvas" style="height: 500px"></div>
    </div>
</div>
@push('scripts')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
        type="text/javascript"></script>
@endpush
@push('script')
<script>
    $(function(){
        $("[data-toggle=popover]").popover({
            html : true,
            trigger: 'hover',
            content: function() {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function() {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });
    });

    let programSelectData = {
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('programs.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit_id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    };

    let recipientSelectData = {
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('recipients.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    unit_id: {{$proposal->unit_id}}
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
//         minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    }

    $('a[href="#usulan"]').on('shown.bs.tab', function (e) {

        $('#program').select2(programSelectData);
        $('#recipient').select2(recipientSelectData);
        $('#location').combotree({
            @if(is_null($agency))
            url: "{{route('regions.combotree')}}?level=4",
            @else
            url: "{{route('regions.combotree')}}?level=4&id={{$agency->region->id}}",
            @endif

            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34,
            width: '100%',
            onSelect: function(node) {
                geo.searchInLocation(node.name + ', Indonesia' );
            }
        });

        $('#location').combotree('setValue', {
            id: {{$proposal->location_id}},
            text: '{{$proposal->location->name}}'
        });

        $('#submission_date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
            keyboardNavigation:true,
        });
    });


    $('#proposer').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('agencies.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
//        allowClear: true,
        minimumInputLength: 1,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });

    $('#program').select2(programSelectData);

    $('#recipient').select2(recipientSelectData);

    $('#location').combotree({
        @if(is_null($agency))
        url: "{{route('regions.combotree')}}?level=4",
        @else
        url: "{{route('regions.combotree')}}?level=4&id={{$agency->region->id}}",
        @endif
        method: "GET",
        idField: "id",
        treeField: "name",
        height: 34,
        onSelect: function(node) {
            geo.searchInLocation(node.name + ', Indonesia' );
        }
    });

    var map, marker;
    function initMap() {
        var myLatLng = new google.maps.LatLng({{$proposal->latitude}},{{$proposal->longitude}});

        var mapOptions = {
            zoom: 4,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // scrollwheel: false,
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        google.maps.event.addListener(map, 'click', function (event) {
            geo.mark(event.latLng);
        });

        // current location
        var currentLatLng = new google.maps.LatLng({{$proposal->latitude}},{{$proposal->longitude}});
        geo.mark(currentLatLng);
    }

    var geo = {
        search: function (text) {

            if (text !== null) {

                var geoCoder = new google.maps.Geocoder();
                geoCoder.geocode({
                    'address': text
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        //console.log(results);
                        map.setCenter(results[0].geometry.location);

                        if (results[0].geometry.viewport)
                            map.fitBounds(results[0].geometry.viewport);
                    } else {

                        // alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        },
        searchInLocation: function () {
            // var loc = $('#location').select2('data');
            //
            // if (loc.length !== 0) {
            //     this.search(loc[0].name);
            // }
        },
        searchInAddress: function () {
            var locationText = $('#location').combotree('getText');
            var text = $('#address').val() + ', ' + locationText + ', Indonesia';



            this.search(text);
        },
        mark: function (location) {
            if (marker) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }

            $('#coordinate').val(marker.getPosition().lat() + ', ' + marker.getPosition().lng());
        },
        addToMap: function (input) {
            var location = $(input).val();
            var locationSplit = location.split(',');

            if (locationSplit !== undefined) {

                if (locationSplit[1] !== undefined) {
                    var lat = locationSplit[0].trim();
                    lat = parseFloat(lat);
                    var lng = locationSplit[1].trim();
                    lng = parseFloat(lng);
                    map.setCenter({lat: lat, lng: lng});

                    if (marker) {
                        marker.setPosition({lat: lat, lng: lng});
                    } else {
                        marker = new google.maps.Marker({
                            position: {lat: lat, lng: lng},
                            map: map
                        });
                    }
                }
            }
        }
    }
</script>
@endpush