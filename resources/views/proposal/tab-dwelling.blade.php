@if($proposal->unit->slug == 'rusun')
    @include('dwelling.rusun.index')
@elseif($proposal->unit->slug == 'rusus')
    @include('dwelling.rusus.index')
@elseif($proposal->unit->slug == 'ruk')
    @include('dwelling.ruk.index')
@elseif($proposal->unit->slug == 'swadaya')
    @include('dwelling.swadaya.index')
@endif