@extends('layout.main')

@push('style')
    <style>
        @media (min-width: 768px) {
            #filter .form-inline .form-group {
                display: block;
                /*margin-bottom: 5px;*/
                vertical-align: middle;

            }

            #filter .form-inline .form-group .control-label {
                padding-right: 0;
            }

            #filter .form-inline .form-control {
                width: 100%;

            }
        }

        #filter .form-group {
            margin-bottom: 0px;;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">{{$title}}</div>
                    </div>
                    <div class="actions">


                        <div class="btn-group">
                            @can('do-technical')
                               {{-- <a class="btn btn-primary" href="{{route('proposals.create.'.$user->unit->slug)}}">
                                    <i class="fa fa-plus"></i> Tambah Usulan
                                </a>--}}
                            @else
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-plus"></i> Tambah usulan<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('proposals.create.rusun')}}">Rusun</a></li>
                                    <li><a href="{{route('proposals.create.rusus')}}">Rusus</a></li>
                                    <li><a href="{{route('proposals.create.swadaya')}}">BSPS</a></li>
                                    <li><a href="{{route('proposals.create.ruk')}}">PSU</a></li>
                                </ul>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="alert alert-info">
                        <div class="row" id="filter">
                            <div class="col-sm-4">

                                {{--<div class="row">--}}
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="unit" class="col-sm-4 control-label">Direktorat:</label>

                                        <div class="col-sm-8">
                                            <select name="unit" id="unit" class="form-control input-sm">
                                                <option value="0" selected>Semua</option>
                                                @foreach($units as $unit)
                                                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="proposer_name" class="col-sm-4 control-label" style="font-size: 12px;">User pengusul:</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="proposer_name" name="proposer_name"
                                                   class="form-control input-sm"
                                                   onkeyup="if (event.keyCode === 13) filterProposal()">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient" class="col-sm-4 control-label" style="font-size: 12px;">Penerima Manfaat:</label>

                                        <div class="col-sm-8">
                                            <select name="recipient" id="recipient" class="form-control input-sm">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                {{--</div>--}}
                            </div>

                            <div class="col-sm-4">

                                {{--<div class="row">--}}
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label for="status" class="col-sm-4 control-label">Status:</label>
                                        <div class="col-sm-8">

                                            <select id="status" name="status" class="form-control input-sm ">
                                                <option value="0" selected>Semua</option>
                                                @foreach($statuses as $status)
                                                    <option value="{{$status->id}}">{{$status->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="program" class="col-sm-4 control-label">Kegiatan:</label>

                                        <div class="col-sm-8">

                                            <select name="program" id="program" class="form-control input-sm">
                                                <option></option>
                                            </select>


                                        </div>
                                    </div>

                                    {{--</div>--}}
                                </div>


                            </div>

                            <div class="col-sm-4">
                                {{--<div class="row">--}}
                                <div class="form-horizontal">



                                    <div class="form-group" style="margin-top: 5px;">
                                        <label for="region" class="col-sm-4 control-label">Lokasi:</label>

                                        <div class="col-sm-8">
                                            <input type="text" id="region" name="region" placeholder="Lokasi"
                                                   aria-describedby="help-block" style="width: 100%;">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="proposer_remark" class="col-sm-4 control-label" style="font-size: 12px;">Identitas
                                            pengusul:</label>

                                        <div class="col-sm-8">
                                            <input type="text" id="proposer_remark" name="proposer_remark"
                                                   class="form-control input-sm"
                                                   onkeyup="if (event.keyCode === 13) filterProposal()">
                                        </div>
                                    </div>

                                    {{--</div>--}}
                                </div>

                                <div class="btn-group pull-right btn-sm">
                                    <button type="button" class="btn btn-warning" id="act-reset"><i
                                                class="fa fa-eraser"></i> Reset
                                    </button>

                                    <button type="button" class="btn btn-info" id="act-search"><i
                                                class="fa fa-search"></i> Cari
                                    </button>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        {!! $dataTable->table([
                            'id'    => $identifier . '-datatables'
                        ]) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@include('_component.select2')
@include('_component.easyui')

@push('script')
    {!! $dataTable->scripts() !!}

    <script>

        $('.select2').select2({
            theme: "bootstrap",
        });

        $('#region').combotree({
            @if(is_null($agency))
            url: '{{route('regions.combotree')}}?level=3',
            @else
            url: '{{route('regions.combotree')}}?level=3&id={{$agency->region->id}}',
            @endif
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34,
        });

        $('#recipient').select2({
            theme: "bootstrap",
            placeholder: 'Pilih salah satu',
//            allowClear: true,
            ajax: {
                url: "{{route('recipients.select')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        unit_id: $('#unit').val(),
                        page: params.page,
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {
                return data.name || data.text;
            },
            templateSelection: function (data) {
                return data.name || data.text;
            }
        });

        $('#program').select2({
            theme: "bootstrap",
            placeholder: 'Pilih salah satu',
//            allowClear: true,
//            debug: true,
            ajax: {
                url: "{{route('programs.select')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        unit_id: $('#unit').val(),
                        page: params.page,
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {
                return data.name || data.text;
            },
            templateSelection: function (data) {
                return data.name || data.text;
            }
        });

        $('#act-search').click(function () {
            $('#{{$identifier}}-datatables').DataTable().ajax.reload(null, false);
            // window.LaravelDataTables["dataTableBuilder"].ajax.reload(null, false);
        });

        $('#act-reset').click(function () {
            $('#program').val(null).trigger('change');
            $('#recipient').val(null).trigger('change');
        });

        function filterProposal() {
            $('#{{$identifier}}-datatables').DataTable().ajax.reload(null, false);

            // window.LaravelDataTables["dataTableBuilder"].ajax.reload(null, false);
        }
    </script>
@endpush