<div class="row">
    <div class="col-md-12">
        <h3>Surveyor</h3>

        @can('create-surveyor', $proposal)
        <button type="button" class="btn btn-default btn-sm pull-right"
                data-toggle="modal"
                data-target="#general-modal"
                data-backdrop="static"
                data-keyboard="false"
                id="btn-add-surveyor">
            <i class="fa fa-plus"></i>&nbsp; Tambah
        </button>
        @endcan

        <table class="table table-condensed table-striped table-hover" id="penyurvey-datatables">
            <thead>
            <tr>
                <th>NIP</th>
                <th>Name</th>
                <th>Phone</th>

                @can('create-surveyor', $proposal)
                <th>&nbsp;</th>
                @endcan
            </tr>
            </thead>
        </table>
    </div>
</div>

@push('script')
<script>
    $('#penyurvey-datatables').DataTable({
        processing: true,
        serverSide: true,
//        stateSave: true,
        paging: false,
        info: false,
        searching: false,
        ajax: {
            url: "{{route('proposals.surveyors.data')}}",
            data: {
                proposal_id: {{$proposal->id}}
            }
        },
        columns: [
            {data: 'nip', name: 'nip'},
            {data: 'name', name: 'name'},
            {data: 'phone', name: 'phone'},

            @can('create-surveyor', $proposal)
            {data: 'action', name: 'action', searchable: false, orderable: false},
            @endcan
        ],
    });

    @can('create-surveyor', $proposal)
    $('#btn-add-surveyor').click(function () {
        $.get('{{route('proposals.surveyors.create')}}', {
            unit_slug: '{{$proposal->unit->slug}}',
            proposal_id: {{$proposal->id}}
        }, function (response) {
            var modalId = 'general-modal';
            var $modalId = $('#' + modalId);
            $modalId.find('.modal-body').html('');

            $modalId.find('.modal-header').find('.modal-title').html(response.title);
            $modalId.find('.modal-body').html(response.body);
            $modalId.find('.modal-footer').html(response.footer);
        });
    });
    @endcan

</script>
@endpush