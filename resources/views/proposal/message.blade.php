<div class="portlet light message">
    <div class="portlet-title tabbable-line">
        <div class="caption">
            <span class="caption-subject">Pesan</span>
        </div>
    </div>
    <div class="portlet-body">
        <form action="{{route('comments.store')}}" id="form-message">
            {{csrf_field()}}
            <input type="hidden" name="proposal_id" value="{{$proposal->id}}">

            <div class="form-group">
                <textarea name="message" id="message" class="form-control" title="Pesan"></textarea>
            </div>

            <button type="button" class="btn btn-success btn-sm" id="send-message">Kirim</button>
        </form>
        <hr>
        
        <ul class="list-group" id="list-messages">

            @foreach($proposal->comments as $comment)

                @php
                if($comment->user->id == \Auth::user()->id) {
                    $class = 'mine';
                } else {
                    $class = '';
                }
                @endphp

                <li class="list-group-item {{$class}}">
                    <h4>{{$comment->user->name}}</h4>
                    <small class="text-info">{{$comment->created_at}}</small>
                    <div class="margin-top-10">{{$comment->text}}</div>
                </li>
            @endforeach
        </ul>
    </div>
</div>

@push('scripts')
<script>
$('#send-message').click(function(){
    $.ajax({
        type: "POST",
        url: $('#form-message').attr('action'),
        data: $('#form-message').serialize(), // serializes the form's elements.
        success: function (data) {
           console.log(data);

            $('#message').val('');

            var htmlComment = '<li class="list-group-item mine">' +
                    '<h4>'+data.user.name+'</h4> <small class="text-info">'+data.created_at+'</small> ' +
                    '<div class="margin-top-10">'+data.text+'</div></li>';
            $('#list-messages').prepend(htmlComment);
        },
        error: function (jqXHR, textStatus, errorThrown) {


        }
    });
});
</script>
@endpush