<form action="{{route('proposals.status.update', [$proposal->id])}}" id="{{$identifier}}" method="post" class="form-horizontal">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="un" class=" col-sm-2 control-label">Unit</label>
        <div class="col-sm-10">
        <p class="form-control-static">{{$proposal->unit->name}}</p>
        </div>
    </div>

    <div class="form-group">
        <label for="pb" class=" col-sm-2 control-label">Instansi Pengusul</label>
        <div class="col-sm-10">
            <p class="form-control-static">{{$proposal->proposer->name}}</p>
        </div>
    </div>

    <div class="form-group">
        <label for="kp" class=" col-sm-2 control-label">Identitas pengusul</label>
        <div class="col-sm-10">
            <p class="form-control-static">{{$proposal->proposer_remark}}</p>
        </div>
    </div>

    <div class="form-group">
        <label for="status" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">

            <select id="status" class="form-control" name="status" aria-describedby="help-block-status">
                @foreach($statuses as $status)
                    <option value="{{$status->id}}" @if($proposal->status_id ==  $status->id) selected @endif>{{$status->name}}</option>
                @endforeach
            </select>
            <div class="help-block" id="help-block-status"></div>
        </div>
    </div>

</form>

<script>
    $('#agency').select2({
        theme: "bootstrap",
        width: '100%',
    });
</script>