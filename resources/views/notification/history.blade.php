@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Semua Notifikasi ({{$notifications->count()}})</div>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="list-group">

                        @foreach($notifications as $notification)
                            <a href="{{route('notifications.show', $notification->id)}}" class="list-group-item">
                                {!! $notification->data['message'] !!} <small>({{$notification->created_at->diffForHumans(null, false)}})</small>
                            </a>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection