<form action="{{route('proposals.surveys.update', [$survey->id])}}" id="{{$identifier}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <input type="hidden" name="proposal_id" value="{{$survey->proposal->id}}">

    <div class="form-group">
        <label for="date" class="control-label">Date</label>
        <input type="text" id="date" class="form-control" name="date" placeholder="Date" aria-describedby="help-block-date" value="{{$survey->date}}">
        <div class="help-block" id="help-block-date"></div>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Konten</label>
        <textarea id="content" class="form-control" name="content" placeholder="Konten" aria-describedby="help-block-content">{{$survey->content}}</textarea>
        <div class="help-block" id="help-block-content"></div>
    </div>

    <div class="form-group">
        <label for="attachment" class="control-label">File</label>
        <div class="col-progress col-download">
            <div class="wrapper margin-bottom-15">{{$survey->file_name}}</div>
        </div>
        <input type="file" id="attachment" class="simple-upload" name="attachment" placeholder="File">
        <input type="hidden" name="attachment" value="{{$survey->attachment}}" id="attachment-survey">
        <input type="hidden" name="file_name" value="{{$survey->file_name}}" id="filename-survey">
        <div class="help-block" id="help-block-attachment"></div>

    </div>

</form>


<script>
    $('.simple-upload').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#attachment-survey');
        var $attachmentNameField = $this.parent().find('#filename-survey');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });
</script>