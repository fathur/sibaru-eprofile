<form action="{{route('proposals.surveyors.update', [$surveyor->id])}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <input type="hidden" name="proposal_id" value="{{$surveyor->proposal->id}}">

    <div class="form-group">
        <label for="nip" class="control-label">NIP</label>
        <input type="text" id="nip" class="form-control" name="nip" placeholder="NIP" aria-describedby="help-block-nip" value="{{$surveyor->nip}}">
        <div class="help-block" id="help-block-nip"></div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Name</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Name" aria-describedby="help-block-name" value="{{$surveyor->name}}">
        <div class="help-block" id="help-block-name"></div>
    </div>

    <div class="form-group">
        <label for="phone" class="control-label">No. Telp</label>
        <input type="email" id="phone" class="form-control" name="phone" placeholder="No. Telp"  value="{{$surveyor->phone}}">
        <div class="help-block" id="help-block-phone"></div>

    </div>

</form>