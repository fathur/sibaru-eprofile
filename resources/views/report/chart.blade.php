<div class="row dashboard-chart">
    <div class="col-sm-12">

        @foreach($charts as $chart)
        <div class="row">
            <div class="col-sm-12">
                <div id="thn-{{$chart->year}}" class="item"></div>
            </div>
        </div>
        @endforeach

        {{--<div class="row margin-top-30">--}}
            {{--<div class="col-sm-12">--}}
                {{--<div id="thn-2016" class="item"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row margin-top-30">--}}
            {{--<div class="col-sm-12">--}}
                {{--<div id="thn-2015" class="item"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>

@push('scripts')
<script src="{{asset('components/highcharts/highcharts.js')}}"></script>
@endpush

@push('script')
<script>

    @foreach($charts as $chart)
    var report{{$chart->year}} = Highcharts.chart('thn-{{$chart->year}}', {
        chart: {
            type: 'column',
//            backgroundColor: '#333333'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: {!! $provinces !!},
            labels: {
                style: {
//                    "color": "#FFFFFF"
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '{{$chart->year}}',
                style: {
//                    "color": "#FFFFFF",
                    "font-size": "20px"
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            shadow: false,
            itemStyle: {
//                "color": "#FFFFFF",
                "font-weight": 100
            }
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: []

    });

    function getReport{{$chart->year}}() {
        $.ajax({
            url: '{{route('reports.data')}}',
            type: "GET",
            dataType: "json",
            data: {
                year: {{$chart->year}}
            },
            success: function (data) {

                console.log(data);

                for(var key in data) {
                    var item = data[key];

                    report{{$chart->year}}.addSeries({
                        name: item.name,
                        data: item.data,
                        color: item.color,
                        borderWidth: 0
                    }, false);
                }


                report{{$chart->year}}.redraw();

            },
            cache: false
        });
    }

    @endforeach

    $(document).ready(function () {

        @foreach($charts as $chart)
        getReport{{$chart->year}}();
        @endforeach
    });
</script>
@endpush