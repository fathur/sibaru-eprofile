{{--
SELECT eprofiles.location_id, regions.name, (
select regions.id from "regions" where ((select "_"."_rgt" from "regions" as "_" where "id" = eprofiles.location_id limit 1) between "_lft" and "_rgt") and level =1
) as province_id
from eprofiles
left JOIN regions on regions.id = eprofiles.location_id;
--}}


@extends('layout.main')

@section('content')
    {{--<div class="row">
        <div class="col-md-12">
            <p>E-PROFIL Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan. Penyediaan perumahan adalah sistem informasi yang menyajikan data terkait perumahan.</p>
        </div>
    </div>--}}

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Progress Update Data eProfile Per Dua Minggu</div>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    @include('report.progress')

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Grafik Unit Penyediaan Perumahan Per Tahun</div>
                    </div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                    @include('report.chart')
                </div>
            </div>
        </div>
    </div>
@stop
