@inject('update', '\App\Repositories\DirectorateUpdate')

<div class="row">
    <div class="col-md-3">
        <div class="row">

            <div class="col-sm-12 text-center jenis-rumah">
                <span class="">Rumah  Susun</span>
                <span class="status-check
                    @if($update->rusun()->firstTwoWeek()->isUpdate() and $update->rusun()->secondTwoWeek()->isUpdate())
                        full-updated
                    @elseif($update->rusun()->firstTwoWeek()->isUpdate() or $update->rusun()->secondTwoWeek()->isUpdate())
                        half-updated
                    @else
                        not-updated
                    @endif
                        ">
                    <i class="fa fa-check-circle"></i>
                </span>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row text-center status-title">
                            <div class="col-sm-6 left">
                                <h3>I</h3>
                            </div>
                            <div class="col-sm-6 right"><h3>II</h3></div>
                        </div>
                        <div class="row status-container">
                            <div class="col-sm-6 left">
                                @if($update->rusun()->firstTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusun()->firstTwoWeek()->lastUpdate()}}"> <i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusun()->firstTwoWeek()->lastUpdate()}}"> <i class="fa fa-times"></i></div>
                                @endif
                            </div>
                            <div class="col-sm-6 right">
                                @if($update->rusun()->secondTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusun()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusun()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="col-md-3">


        <div class="row">
            <div class="col-sm-12 text-center jenis-rumah">
                <span>Rumah Khusus</span>
                <span class=" status-check
                    @if($update->rusus()->firstTwoWeek()->isUpdate() and $update->rusus()->secondTwoWeek()->isUpdate())
                        full-updated
                    @elseif($update->rusus()->firstTwoWeek()->isUpdate() or $update->rusus()->secondTwoWeek()->isUpdate())
                        half-updated
                    @else
                        not-updated
                    @endif
                        ">
                    <i class="fa fa-check-circle"></i>
                </span>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row text-center status-title">
                            <div class="col-sm-6 left"><h3>I</h3></div>
                            <div class="col-sm-6 right"><h3>II</h3></div>
                        </div>
                        <div class="row status-container">
                            <div class="col-sm-6 left">
                                @if($update->rusus()->firstTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusus()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusus()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                            <div class="col-sm-6 right">
                                @if($update->rusus()->secondTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusus()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->rusus()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="col-md-3">


        <div class="row">
            <div class="col-sm-12 text-center jenis-rumah">
                <span>Rumah Swadaya</span>
                <span class="status-check
                    @if($update->swadaya()->firstTwoWeek()->isUpdate() and $update->swadaya()->secondTwoWeek()->isUpdate())
                        full-updated
                    @elseif($update->swadaya()->firstTwoWeek()->isUpdate() or $update->swadaya()->secondTwoWeek()->isUpdate())
                        half-updated
                    @else
                        not-updated
                    @endif
                        ">
                    <i class="fa fa-check-circle"></i>
                </span>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row text-center status-title">
                            <div class="col-sm-6 left"><h3>I</h3></div>
                            <div class="col-sm-6 right"><h3>II</h3></div>
                        </div>
                        <div class="row status-container">
                            <div class="col-sm-6 left">
                                @if($update->swadaya()->firstTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->swadaya()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->swadaya()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                            <div class="col-sm-6 right">
                                @if($update->swadaya()->secondTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->swadaya()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->swadaya()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="col-md-3">


        <div class="row ">
            <div class="col-sm-12 text-center jenis-rumah">
                <span>RUK</span>
                <span class="status-check
                    @if($update->ruk()->firstTwoWeek()->isUpdate() and $update->ruk()->secondTwoWeek()->isUpdate())
                        full-updated
                    @elseif($update->ruk()->firstTwoWeek()->isUpdate() or $update->ruk()->secondTwoWeek()->isUpdate())
                        half-updated
                    @else
                        not-updated
                    @endif">
                    <i class="fa fa-check-circle"></i>
                </span>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row text-center status-title">
                            <div class="col-sm-6 left"><h3>I</h3></div>
                            <div class="col-sm-6 right"><h3>II</h3></div>
                        </div>
                        <div class="row status-container">
                            <div class="col-sm-6 left">
                                @if($update->ruk()->firstTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->ruk()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->ruk()->firstTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                            <div class="col-sm-6 right">
                                @if($update->ruk()->secondTwoWeek()->isUpdate())
                                    <div class="update-status updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->ruk()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-check"></i></div>
                                @else
                                    <div class="update-status not-updated" data-toggle="tooltip" data-placement="bottom" title="{{$update->ruk()->secondTwoWeek()->lastUpdate()}}"><i class="fa fa-times"></i></div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


@push('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush