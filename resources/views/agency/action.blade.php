<div class="btn-group btn-group-xs pull-right">
    <button type="button" class="btn btn-warning" onclick="editForm(this)"
            data-id="{{$agency['id']}}"
            data-form="{{route('agencies.edit', $agency['id'])}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </button>
    <button type="button" class="btn btn-danger" onclick="deleteAgencyForm(this)"
            data-link="{{route('agencies.destroy', $agency['id'])}}"
            data-identifier="{{$identifier}}">
        <i class="fa fa-trash"></i>
        <span class="hidden-xs hidden-sm">&nbsp;Delete</span>
    </button>
</div>

