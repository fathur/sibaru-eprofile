<form action="{{route('agencies.store')}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}

    {{--<div class="form-group">
        <label for="type" class="control-label">Tipe</label>
        <select name="type" id="type" class="form-control" onchange="hideRegion()">
            <option value="pemda">Pemda</option>
            <option value="dirjen">Dirjen Perencanaan</option>
            <option value="tni-polri">TNI/POLRI</option>
            <option value="dpr">DPR</option>
            <option value="pt">Lembaga Pendidikan Tinggi</option>
            <option value="pt-agama">Lembaga Pendidikan Tinggi Agama</option>
            <option value="bumn">Badan Usaha Milik Negara</option>
            <option value="yayasan-kemanusiaan">Yayasan Bidang Kemanusiaan / Keagamaan</option>
        </select>
        <div class="help-block" id="help-block-type"></div>
    </div>--}}

    <div class="form-group">
        <label for="parent">Parent</label>
        <select name="parent" id="parent" class="form-control" onchange="hideRegion()">
            <option value="0">-</option>

            @foreach($parents as $parent)
                <option value="{{$parent->id}}">{{$parent->name}}</option>
            @endforeach
        </select>
    </div>

    <!-- Seharusnya hilang jika memilih selain pemda-->
    <div class="form-group" id="region-form-group" style="display: none;">
        <label for="region" class="control-label">Wilayah</label>
        <br>
        <small>Wilayah ini <span class="text-danger bold">bukan</span>lah parent dari instansi, tetapi merupakan relasi dari menu Wilayah di <strong>Data Master → Wilayah</strong>.
            Untuk struktur tree-nya akan menyesuiakan dengan struktur tree di Wilayah</small>
        <br>
        <input type="text" id="region" name="region" placeholder="Wilayah" aria-describedby="help-block-region">
        {{--<select name="region" id="region" class="form-control"></select>--}}
        <div class="help-block" id="help-block-region"></div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Nama</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama">
        <div class="help-block" id="help-block-name"></div>

    </div>

    <div class="form-group">
        <label for="phone" class="control-label">Telp</label>
        <input type="text" id="phone" class="form-control" name="phone" placeholder="Telp">
        <div class="help-block" id="help-block-phone"></div>

    </div>

    <div class="form-group">
        <label for="fax" class="control-label">Fax</label>
        <input type="text" id="fax" class="form-control" name="fax" placeholder="Fax">
        <div class="help-block" id="help-block-fax"></div>

    </div>

    <div class="form-group">
        <label for="address" class="control-label">Address</label>
        <textarea id="address" class="form-control" name="address" placeholder="Address"></textarea>
        <div class="help-block" id="help-block-address"></div>
    </div>

    {{--<div class="form-group">
        <label for="logo" class="control-label">Logo</label>
        <div class="col-progress col-download">
            <div class="wrapper margin-bottom-15"></div>
        </div>
        <input type="file" id="logo" name="logo" placeholder="Logo" class="simple-upload">
        <input type="hidden" name="logo_url" value="" id="logo-url">
        <input type="hidden" name="logo_filename" value="" id="logo-filename">
        <div class="help-block" id="help-block-logo"></div>

    </div>--}}
</form>

<script>
    $('.simple-upload').change(function () {

        var $this = $(this);
        var $thisProgressStatus =   $this.parent().find('.col-progress');
        var $thisDownload =   $this.parent().find('.col-download');
        var $attachmentField = $this.parent().find('#logo-url');
        var $attachmentNameField = $this.parent().find('#logo-filename');


        $(this).simpleUpload('{{route('assets.upload')}}', {
            name: 'file',
            data: {
                _token: '{{csrf_token()}}'
            },
            progress: function(progress) {
                var htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                $thisProgressStatus.find('.wrapper').html(htmlProgress);
            },
            success: function(data) {

                        {{--var htmlLink = '<a href="{{url('survey/download')}}/'+data.proposal_id+'">'+data.file_name+'</a>';--}}
                var htmlLink = data.file_name;

                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper').html(htmlLink).css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();

                $attachmentField.val(data.location);
                $attachmentNameField.val(data.file_name);
            },
            error: function(error) {
                console.log(error);
                $thisDownload.find('.progress').fadeOut();
                $thisDownload.find('.wrapper')
                        .html('<span class="text-danger">'+error.message+'!</span>')
                        .css({display:'none'});
                $thisDownload.find('.wrapper').fadeIn();
            }
        });
    });

    $('#region').combotree({
        url: '{{route('regions.combotree')}}?level=2',
        method: "GET",
        idField: "id",
        treeField: "name",
        width: 567,
        height: 34

    });

    /*
    $('#region').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{route('regions.select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    maxLevel: 2
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        allowClear: true,
        minimumInputLength: 2,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    }); */

    function hideRegion() {
        var type = $('#parent').val();

        if(type == '{{\App\Models\Agency::where('name', 'Pemda')->first()->id}}') {
            $('#region-form-group').show();
        } else {
            $('#region-form-group').hide();
        }
    }
</script>