@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Instansi</div>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                id="{{$identifier}}-trigger-modal">
                            <i class="fa fa-plus"></i>&nbsp; Tambah instansi
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    {{--<table class="table table-bordered table-condensed table-hover table-striped" id="{{$identifier}}-datatables">
                        <thead>
                        <tr>
                            <th>Tipe</th>
                            <th>Wilayah</th>
                            <th>Name</th>
                            <th>Telp</th>
                            <th>Fax</th>
                            <th>Alamat</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                    </table>--}}
                    <table id="{{$identifier}}-datatables"></table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@include('_component.select2')
@include('_component.easyui')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    /*$('#{{$identifier}}-datatables').dataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: "{{route('agencies.data')}}"
        },
        columns: [
            {data: 'type', name: 'type'},
            {data: 'region', name: 'region', 'searchable': false, orderable: false},
            {data: 'name', name: 'name'},
            {data: 'phone', name: 'phone'},
            {data: 'fax', name: 'fax'},
            {data: 'address', name: 'address'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });*/

    $('#{{$identifier}}-datatables').treegrid({
        method: "GET",
        url: "{{route('agencies.data')}}",
        idField: "id",
        treeField: "name",
        lines: true,
        striped: true,
        columns: [[
            {title:'Nama',field:'name'},
            {title:'Telepon',field:'phone',width:280},
            {title:'Fax',field:'fax',width:280},
            {title:'Alamat',field:'address'},
            {title:' ',field:'action'},
//            {field:'persons',title:'Persons',width:60,align:'right'},
//            {field:'begin',title:'Begin Date',width:80},
//            {field:'end',title:'End Date',width:80}
        ]],
//        onBeforeExpand: function(node) {
//            console.log(node);
//        }
    });

    $('#{{$identifier}}-trigger-modal').click(function () {

        showGeneralModal('{{route('agencies.create')}}');
    });

    function saveAgency(dom) {

        var identifier = $(dom).data('identifier');

        var id = $(dom).data('id');

        var $identifier = $('#' + identifier);

        var url = $identifier.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: $identifier.serialize(), // serializes the form's elements.
            success: function (data) {

                console.log(data);

                // close modal
                $('#general-modal').modal('hide');

                // reload datatables\
                if(data.parent_id !== null) {
                    $('#' + identifier + '-datatables').treegrid('reload', data.parent_id);
                } else {
                    $('#' + identifier + '-datatables').treegrid('reload');
                }

                // kasih centang gede
                swal({
                    title: 'Good job!',
                    text: 'You add one data!',
                    timer: 2000,
                    type: 'success'
                });


            },
            error: function (jqXHR, textStatus, errorThrown) {

                $identifier.find('.form-group').removeClass('has-error');
                $identifier.find('.form-group').find('.help-block').html('');

                var data = $.parseJSON(jqXHR.responseText);

                $.each(data, function (item, errors) {

                    var html = '<ul class="list-unstyled">';

                    $.each(errors, function (index, error) {

                        html += '<li>' + error + '</li>';
                    });

                    html += '</ul>';

                    $('#help-block-' + item).html(html);
                    $('#help-block-' + item).parent().addClass('has-error');

                });
            }
        });
    }

    function deleteAgencyForm(dom) {

        var url = $(dom).data('link');
        var identifier = $(dom).data('identifier');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {

            $.ajax({
                type: "DELETE",
                url: url,
                data: {
                    _token: $("meta[name=csrf-token]").attr('content')
                }, // serializes the form's elements.
                success: function (data) {

                    // reload datatables\
                    $('#' + identifier + '-datatables').treegrid('reload');

                    swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                    )

                }
            });
        })
    }
</script>
@endpush