<div class="row margin-bottom-15">
    <div class="col-sm-12">
        <button class="btn btn-primary pull-right"
                data-toggle="modal"
                data-target="#general-modal"
                data-backdrop="static"
                data-keyboard="false"
                id="{{$identifier . $type}}-trigger-modal">
            <i class="fa fa-plus"></i>&nbsp; Tambah Program {{title_case($type)}}
        </button>
    </div>
</div>
<table class="table table-striped table-bordered table-condensed" id="{{$identifier. $type}}-datatables">
    <thead>
    <tr>
        <th>Program Kegiatan</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
</table>

@push('script')
<script>
    $('#{{$identifier . $type}}-datatables').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{route('programs.data')}}",
            data: function(d) {
                d.type = '{{$type}}'
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ],
    });

    $('#{{$identifier . $type}}-trigger-modal').click(function () {

        showGeneralModal('{{route('programs.create')}}?type={{$type}}');
    });
</script>
@endpush

