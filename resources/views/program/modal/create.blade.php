<form action="{{route('programs.store')}}" id="{{$identifier . $type}}" method="post">
    {{csrf_field()}}
    <input type="hidden" value="{{$type}}" name="type">

    <div class="form-group">
        <label for="name" class="control-label">Program Kegiatan</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama Program Kegiatan" aria-describedby="help-block-name">
        <div class="help-block" id="help-block-name"></div>
    </div>
</form>


<script>
</script>