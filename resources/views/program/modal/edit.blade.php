<form action="{{route('programs.update', [$program->id])}}" id="{{$identifier . $type}}" method="post" onsubmit="event.preventDefault();">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="name" class="control-label">Program Kegiatan</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama Program Kegiatan" aria-describedby="help-block-name" value="{{$program->name}}">
        <div class="help-block" id="help-block-name"></div>
    </div>
</form>