@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Users</div>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    @include('recipient.modal.create')
                </div>
            </div>
        </div>
    </div>
@stop