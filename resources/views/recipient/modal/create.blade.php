<form action="{{route('recipients.store')}}" id="{{$identifier . $type}}" method="post">
    {{csrf_field()}}
    <input type="hidden" value="{{$type}}" name="type">

    <div class="form-group">
        <label for="name" class="control-label">Penerima Manfaat</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama Penerima Manfaat" aria-describedby="help-block-name">
        <div class="help-block" id="help-block-name"></div>
    </div>

    <div class="form-group">
        <label for="abbreviation" class="control-label">Singkatan</label>
        <input type="text" id="abbreviation" class="form-control" name="abbreviation" placeholder="Singkatan" aria-describedby="help-block-abbreviation">
        <div class="help-block" id="help-block-abbreviation"></div>
    </div>
</form>


<script>
</script>