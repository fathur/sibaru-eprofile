<form action="{{route('recipients.update', [$recipient->id])}}" id="{{$identifier . $type}}" method="post" onsubmit="event.preventDefault();">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="name" class="control-label">Penerima Manfaat</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama Penerima Manfaat" aria-describedby="help-block-name" value="{{$recipient->name}}">
        <div class="help-block" id="help-block-name"></div>
    </div>

    <div class="form-group">
        <label for="abbreviation" class="control-label">Singkatan</label>
        <input type="text" id="abbreviation" class="form-control" name="abbreviation" placeholder="Singkatan" aria-describedby="help-block-abbreviation" value="{{$recipient->abbreviation}}">
        <div class="help-block" id="help-block-abbreviation"></div>
    </div>
</form>