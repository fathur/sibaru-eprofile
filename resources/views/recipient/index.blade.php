@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title tabbable-line">
                    {{--<div class="caption">
                        <div class="caption-subject">Program Kegiatan</div>
                    </div>--}}

                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#rusun" data-toggle="tab">Rusun</a>
                        </li>

                        <li class="">
                            <a href="#rusus" data-toggle="tab">Rusus</a>
                        </li>

                        <li class="">
                            <a href="#ruk" data-toggle="tab">RUK</a>
                        </li>

                        <li class="">
                            <a href="#swadaya" data-toggle="tab">Swadaya</a>
                        </li>
                    </ul>

                   {{-- <div class="actions">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                id="{{$identifier}}-trigger-modal">
                            <i class="fa fa-plus"></i>&nbsp; Tambah Program
                        </button>
                    </div>--}}
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="rusun">
                            @include('recipient.tab-content', ['type' => 'rusun'])
                        </div>

                        <div class="tab-pane" id="rusus">
                            @include('recipient.tab-content', ['type' => 'rusus'])
                        </div>

                        <div class="tab-pane" id="ruk">
                            @include('recipient.tab-content', ['type' => 'ruk'])
                        </div>

                        <div class="tab-pane" id="swadaya">
                            @include('recipient.tab-content', ['type' => 'swadaya'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.datatables')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>

    $(document).ready(function(){
        // Re adjust table size in tab content
        // https://github.com/DataTables/Responsive/issues/40
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).css('width', '100%');
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
        });

    });


</script>
@endpush