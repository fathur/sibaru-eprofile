<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78444166-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-78444166-5');

        @if(Auth::check())
        gtag('set', {'user_id': '{{Auth::user()->id}}'}); // Set the user ID using signed-in user_id.
        @endif

    </script>

    <meta charset="utf-8"/>
    <title>SIBARU</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Sibaru " name="description"/>
    <meta content="" name="author"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css"/>

    @stack('styles')

    @stack('style')

    {{--
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES --> --}}
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}"/>
</head>
<body class="page-container-bg-solid">

<div class="page-wrapper">
    <div class="page-wrapper-row">
        @include('layout.header')
    </div>
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            <div class="page-container">

                <div class="page-content-wrapper">

{{--                    @include('layout.page-head')--}}

                    <div class="page-content">
                        <div class="container-fluid">
                            {!! Breadcrumbs::render() !!}

                            <div class="page-content-inner">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

{{--                @include('layout.quick-sidebar')--}}
            </div>
        </div>
    </div>
    <div class="page-wrapper-row">
       @include('layout.footer')
    </div>
</div>

{{-- Modal--}}
@include('layout.modal.modal')

{{--<!-- BEGIN QUICK NAV -->
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                <span>Purchase Metronic</span>
                <i class="icon-basket"></i>
            </a>
        </li>
        <li>
            <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                <span>Customer Reviews</span>
                <i class="icon-users"></i>
            </a>
        </li>
        <li>
            <a href="http://keenthemes.com/showcast/" target="_blank">
                <span>Showcase</span>
                <i class="icon-user"></i>
            </a>
        </li>
        <li>
            <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                <span>Changelog</span>
                <i class="icon-graph"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>--}}

<script src="{{asset('components/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('components/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{asset('components/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/app.js')}}?_={{\Carbon\Carbon::today()->timestamp}}" type="text/javascript"></script>

@stack('scripts')

@stack('script')

</body>
</html>