<div class="page-wrapper-top">
    <div class="page-header">
        <div class="page-header-top" style="background-image: url('{{$backgroundHeader ?? ''}}')">
            <div class="container-fluid">

                <div class="page-logo">
                    {{--<a href="{{url('/')}}"> Sistem Data Perumahan </a>--}}
                    <a href="{{url('/')}}"> SIBARU</a>
                </div>

                <a href="javascript:;" class="menu-toggler"></a>

                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                        <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">

                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-bell"></i> <span class="badge badge-default">{{Auth::user()->unreadNotifications->count()}}</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3><strong>{{Auth::user()->unreadNotifications->count()}}</strong> notifikasi tak terbaca</h3> <a href="{{url('notifications')}}">lihat semua</a>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">

                                        @foreach(Auth::user()->unreadNotifications as $notification)
                                            <li>
                                                <a href="{{route('notifications.show', $notification->id)}}">
                                                    <span class="time">{{$notification->created_at->diffForHumans(null, true)}}</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-primary">
                                                            <i class="fa fa-newspaper-o"></i>
                                                        </span> {!! $notification->data['message']  !!}

                                                    </span>
                                                </a>
                                            </li>
                                        @endforeach

                                       {{-- <li>
                                            <a href="javascript:;">
                                                <span class="time">just now</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-success">
                                                        <i class="fa fa-plus"></i>
                                                    </span> New user registered.
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 mins</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Server #12 overloaded.
                                                </span>
                                            </a>
                                        </li>--}}

                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown dropdown-user dropdown-dark">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                {{--<img alt="" class="img-circle" src="{{asset('img/avatar9.jpg')}}"> --}}
                                {{--<div>--}}
                                    <span class="username username-hide-mobile">{{Auth::user()->name}}</span>
                                    {{--<span class="rolename rolename-hide-mobile">{{Auth::user()->role->name}}</span>--}}
                                {{--</div>--}}
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <!--
                                <li>
                                    <a href="page_user_profile_1.html"> <i class="icon-user"></i> My Profile </a>
                                </li>
                                <li>
                                    <a href="app_calendar.html"> <i class="icon-calendar"></i> My Calendar </a>
                                </li>
                                <li>
                                    <a href="app_inbox.html"> <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger"> 3 </span> </a>
                                </li>
                                <li>
                                    <a href="app_todo_2.html"> <i class="icon-rocket"></i> My Tasks <span class="badge badge-success"> 7 </span> </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="page_user_lock_1.html"> <i class="icon-lock"></i> Lock Screen </a>
                                </li>
                                -->
                                <li>
                                    <a href="{{url('profile')}}"><i class="icon-user"></i>Profile</a>
                                </li>
                                <li>
                                    <a href="{{url('logout')}}"> <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        {{--<li class="dropdown dropdown-extended quick-sidebar-toggler"> <span class="sr-only">Toggle Quick Sidebar</span> <i class="icon-logout"></i> </li>--}}
                    </ul>
                </div>
            </div>
        </div>

        <div class="page-header-menu">
            <div class="container-fluid">
                {{--<form class="search-form" action="page_general_search.html" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="query"> <span class="input-group-btn"> <a href="javascript:;" class="btn submit"> <i class="icon-magnifier"></i> </a> </span> </div>
                </form>--}}
                <div class="hor-menu ">
                    <ul class="nav navbar-nav">
                        {{--<li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">--}}
                            {{--<a href={{url( 'dashboard')}}><i class="fa fa-tachometer"></i> Beranda</a>--}}
                        {{--</li>--}}

                        @if(Gate::allows('do-admin') || Gate::allows('do-technical'))
                        <li>
                            <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        @endif

                        @can('do-master')
                        <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">
                            <a href="javascript:;">
                                <i class="fa fa-cogs"></i> Data Master <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li aria-haspopup="true" class=" "> <a href="{{route('users.index')}}" class="nav-link "> User</a> </li>
                                <li aria-haspopup="true" class=" "> <a href="{{route('regions.index')}}" class="nav-link "> Wilayah</a> </li>

                                <li class="divider"></li>

                                <li aria-haspopup="true" class=" "> <a href="{{route('agencies.index')}}" class="nav-link "> Instansi</a> </li>

                                <li aria-haspopup="true" class="dropdown-submenu">
                                    <a href="javascript:;" class="nav-link nav-toggle"> Kriteria
                                        <span class="arrow"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('criteria.rusun.index')}}" class="nav-link "> Rusun</a>
                                        </li>
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('criteria.rusus.index')}}" class="nav-link "> Rusus</a>
                                        </li>
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('criteria.swadaya.index')}}" class="nav-link "> Swadaya</a>
                                        </li>
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('criteria.ruk.index')}}" class="nav-link "> RUK</a>
                                        </li>
                                    </ul>
                                </li>
                                <li aria-haspopup="true" class=" "> <a href="{{route('tematics.index')}}" class="nav-link "> Tematik</a> </li>

                                <li class="divider"></li>
                                <li aria-haspopup="true" class=" "> <a href="{{route('programs.index')}}" class="nav-link "> Program Kegiatan</a> </li>
                                <li aria-haspopup="true" class=" "> <a href="{{route('recipients.index')}}" class="nav-link "> Penerima Manfaat</a> </li>
                                {{--<li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Skema</a> </li>--}}
                                <li class="divider"></li>
                                

                                {{--<li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Komponen fitur</a> </li>--}}
                                {{--<li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Informasi publik</a> </li>--}}
                                <li aria-haspopup="true" class="dropdown-submenu">
                                    <a href="javascript:;" class="nav-link nav-toggle"> Informasi Publik
                                        <span class="arrow"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('guidance.index')}}" class="nav-link "> Pedoman</a>
                                        </li>
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('faqs.index')}}" class="nav-link "> FAQ</a>
                                        </li>
                                        <li aria-haspopup="true" class=" ">
                                            <a href="{{route('posts.edit', 'pengantar')}}" class="nav-link "> Pengantar</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @endcan

                        <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">
                            <a href="{{route('proposals.index')}}"><i class="fa fa-pencil"></i> Usulan</a>
                        </li>

                        @can('do-admin')

                        <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">

                            <a href="javascript:;">
                                <i class="fa fa-list"></i> Shortlist <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li aria-haspopup="true" class=" "> <a href="{{url('shortlist')}}"class="nav-link "> Edit Shortlist</a> </li>
                                <li aria-haspopup="true" class=" "> <a href={{route('shortlists.view')}} class="nav-link "> View Shortlist</a> </li>
                            </ul>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">

                            <a href="javascript:;">
                                <i class="fa fa-file-o"></i> SK Penetapan <i class="fa fa-angle-down"></i>
                            </a>


                            <ul class="dropdown-menu pull-left">
                                <li aria-haspopup="true" class=" "> <a href="{{url('sk-penetapan')}}"class="nav-link "> Edit SK Penetapan</a> </li>
                                <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> View SK Penetapan</a> </li>
                                <li aria-haspopup="true" class=" "> <a href="#" class="nav-link "> Dokumen SK Penetapan</a> </li>
                            </ul>
                        </li>
                        @endcan


                        <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">
                            @can('do-proposer')
                            <a href="{{route('statistics.proposal.index')}}"><i class="fa fa-bar-chart"></i> Grafik Usulan</a>
                            @else
                                <a href="javascript:;">
                                    <i class="fa fa-bar-chart"></i> Grafik <i class="fa fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu pull-left">
                                    <li aria-haspopup="true" class="">
                                        <a href="{{route('statistics.proposal.index')}}" class="nav-link">Grafik Usulan</a>
                                    </li>

                                    <li aria-haspopup="true" class="">
                                        <a href="{{route('statistics.proposal.table')}}" class="nav-link">Table Usulan</a>
                                    </li>
                                </ul>
                            @endcan
                        </li>

                        @can('do-admin')
                            <li class="menu-dropdown classic-menu-dropdown" aria-haspopup="true">


                                <a href="javascript:;">
                                    <i class="fa fa-building"></i> eProfile <i class="fa fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu pull-left">
                                    <li aria-haspopup="true" class=" "> <a href="{{url('eprofile')}}" class="nav-link "> Data</a> </li>
                                    <li aria-haspopup="true" class=" "> <a href="{{route('statistics.eprofile.index')}}" class="nav-link "> Grafik</a> </li>
                                    <li aria-haspopup="true" class=" "> <a href="{{route('profile.province.map')}}" class="nav-link "> Peta</a> </li>
                                    <li aria-haspopup="true" class=" "> <a href="{{route('profile.province.index')}}" class="nav-link "> Profil Provinsi</a> </li>
                                </ul>
                            </li>
                        @endcan
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
