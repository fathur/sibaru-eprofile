<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78444166-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-78444166-5');
        
    </script>

    <meta charset="utf-8"/>
    <title>Metronic Admin Theme #3 | Blank Page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #3 for " name="description"/>
    <meta content="" name="author"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css"/>

    @stack('styles')

    @stack('style')

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-container-bg-solid">

<div class="page-wrapper">
    <div class="page-wrapper-row">
        @include('layout.public.header')
    </div>
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            <div class="page-container">

                <div class="page-content-wrapper">

                    {{--@include('layout.page-head')--}}

                    <div class="page-content">
                        <div class="container">
                            {{--{!! Breadcrumbs::render() !!}--}}

                            <div class="page-content-inner">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

                @include('layout.quick-sidebar')
            </div>
        </div>
    </div>
    <div class="page-wrapper-row">
        @include('layout.public.footer')
    </div>
</div>

<script src="{{asset('components/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('components/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('components/blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{asset('components/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/app.js')}}" type="text/javascript"></script>

@stack('scripts')

@stack('script')

</body>
</html>