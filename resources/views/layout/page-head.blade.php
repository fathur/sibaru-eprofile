<div class="page-head">
    <div class="container-fluid">
        <div class="page-title">
            <h1>{!! $title ?? '' !!}</h1>
        </div>
    </div>
</div>