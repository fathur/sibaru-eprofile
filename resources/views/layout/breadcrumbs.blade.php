@if ($breadcrumbs)
<ul class="page-breadcrumb breadcrumb">
    @foreach ($breadcrumbs as $breadcrumb)
    @if (!$breadcrumb->last)
    <li>
        <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
        <i class="fa fa-chevron-right"></i>
    </li>
    @else
    <li class="active">{{ $breadcrumb->title }}</li>
    @endif
    @endforeach
</ul>
@endif