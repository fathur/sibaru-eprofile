@extends('layout.main')

@push('style')
<style>
    #error-code {
        float: left;
        font-size: 140px;
        font-weight: 600;
        color: crimson;
    }

    #error-text {
        float: left;
        display: block;
        font-size: 20px;
        font-weight: bold;
        color: crimson;
        margin: 45px 0 0 28px;
    }
</style>
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div id="error-code">
                404
            </div>

            <div id="error-text">
                Maaf, Halaman yang Anda buka tidak ditemukan 😭.
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            Coba cek kembali:

            <ul>
                <li>Apakah url yang anda ketikkan benar?</li>
                <li>Apakah Anda diizinkan untuk membuka halaman ini?</li>
            </ul>
        </div>
    </div>
@stop
