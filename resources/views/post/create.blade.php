@extends('layout.main')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">

                </div>

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{route('posts.store')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="title">Judul</label>
                                    <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}">
                                </div>

                                <div class="form-group">
                                    <label for="content">Konten</label>
                                    <textarea name="content" id="content" class="form-control">{{old('content')}}</textarea>
                                </div>

                                <button type="submit" class="btn btn-success">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



@push('styles')
    <link rel="stylesheet" href="{{asset('components/summernote/dist/summernote.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('components/summernote/dist/summernote.js')}}"></script>
@endpush

@push('script')
    <script>
        $('#content').summernote();
    </script>
@endpush