@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Tematik</div>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#general-modal"
                                data-backdrop="static"
                                data-keyboard="false"
                                id="{{$identifier}}-trigger-modal">
                            <i class="fa fa-plus"></i>&nbsp; Tambah tematik
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-condensed table-hover table-striped" id="{{$identifier}}-datatables">
                        <thead>
                        <tr>
                            <th>Unit</th>
                            <th>Name</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.datatables')
@include('_component.select2')

@push('scripts')
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#{{$identifier}}-datatables').dataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: "{{route('tematics.data')}}"
        },
        columns: [
            {data: 'unit_name', name: 'units.name'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

    $('#{{$identifier}}-trigger-modal').click(function () {
        showGeneralModal('{{route('tematics.create')}}');

    });
</script>
@endpush