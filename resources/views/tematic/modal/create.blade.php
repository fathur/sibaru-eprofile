<form action="{{route('tematics.store')}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}

    <div class="form-group">
        <label for="unit" class="control-label">Unit</label>
        <select name="unit" id="unit" class="form-control">
            @foreach($units as $unit)
            <option value="{{$unit->id}}">{{$unit->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit"></div>

    </div>
    
    <div class="form-group">
        <label for="name" class="control-label">Nama</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Nama">
        <div class="help-block" id="help-block-name"></div>

    </div>
</form>
