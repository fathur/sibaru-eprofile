<form action="{{route('tematics.update', [$tematic->id])}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="form-group">
        <label for="name" class="control-label">Name</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Name" aria-describedby="help-block-name" value="{{$tematic->name}}">
        <div class="help-block" id="help-block-name"></div>
    </div>

</form>
