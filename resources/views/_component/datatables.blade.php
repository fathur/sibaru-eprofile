@push('scripts')
<script src="{{asset('components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('components/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('components/sweetalert2/dist/sweetalert2.min.js')}}"></script>
<script src="{{asset('components/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js')}}"></script>
{{--<script src="{{asset('components/datatables-buttons/js/dataTables.buttons.js')}}"></script>--}}
{{--<script src="{{asset('components/datatables-buttons/js/buttons.bootstrap.js')}}"></script>--}}
<script src="{{asset('components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('components/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('components/datatables/media/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('components/sweetalert2/dist/sweetalert2.css')}}">
<link rel="stylesheet" href="{{asset('components/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css')}}">
<link rel="stylesheet" href="{{asset('components/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}">
@endpush