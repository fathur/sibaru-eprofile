@push('scripts')
<script src="{{asset('components/gitgraph.js/build/gitgraph.min.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('components/gitgraph.js/build/gitgraph.css')}}">
@endpush