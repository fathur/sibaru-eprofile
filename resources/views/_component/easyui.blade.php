@push('scripts')
<script src="{{asset('components/jQuery-EasyUI/jquery.easyui.min.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('components/jQuery-EasyUI/themes/metro/easyui.css')}}">
@endpush