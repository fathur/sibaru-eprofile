@push('scripts')
<script src="{{asset('components/select2/dist/js/select2.full.min.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('components/select2-bootstrap-theme/dist/select2-bootstrap.min.css')}}">
@endpush