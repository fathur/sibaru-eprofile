@push('scripts')
<script src="{{asset('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css')}}">
@endpush