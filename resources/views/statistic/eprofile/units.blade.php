<div class="alert alert-info">
    <div class="row" id="filter">
        <div class="col-sm-6">

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="region-unit" class="col-sm-4 control-label">Wilayah</label>

                    <div class="col-sm-8">
                        <input type="text" id="region-unit" name="region-unit" placeholder="Wilayah"
                               aria-describedby="help-block-region" style="width: 100%;">
                    </div>
                </div>


            </div>
        </div>

        <div class="col-sm-6">

            <div class="form-horizontal">

                <div class="form-group">
                    <label for="year-unit" class="col-sm-4 control-label">Tahun</label>

                    <div class="col-sm-8">
                        <select name="year-unit" id="year-unit" class="form-control">

                            <option value="0" selected>Semua</option>
                            @foreach($years as $year)
                                <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--<div class="form-group">
                    <label for="status" class="col-sm-4 control-label">Tahun:</label>
                    <div class="col-sm-8">

                        <input type="text" class="form-control">
                        <input type="text" class="form-control">
                    </div>
                </div>--}}

            </div>


        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <button type="button" id="filter-unit-btn" class="btn btn-info pull-right">Tampilkan</button>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <div id="unit-chart">

        </div>
    </div>
</div>

@push('script')
    <script>
        // Build the chart
        var units = Highcharts.chart('unit-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height: 600
            },
            title: {
                text: 'Program'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> ({point.y})'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        format: '{point.name} <br> {point.percentage:.1f}% ({point.y})'
                    },
                    showInLegend: true
                }
            },
            legend: {
//                align: 'right',
//                verticalAlign: 'top',
//                layout: 'vertical',
            },
            series: [{
                name: 'Program',
                colorByPoint: true,
                data: []
            }]
        });

        $('#region-unit').combotree({
            url: '{{route('regions.combotree')}}?level=4',
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34,
        });

        $('#filter-unit-btn').click(function(){
            getUnitsData();
        });

        function getUnitsData() {
            $.ajax({
                url: '{{route("statistics.{$type}.units")}}',
                type: "GET",
                dataType: "json",
                data: {
                    region: $('#region-unit').val(),
                    year: $('#year-unit').val()
                },
                success: function (data) {

                    console.log(data);

                    units.series[0].update({
                        data: data
                    });
                },
                cache: false
            });
        }

        $(document).ready(function () {
            getUnitsData();
        });
    </script>
@endpush