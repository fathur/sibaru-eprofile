@extends('layout.main')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title tabbable-line">
                    <ul class="nav nav-tabs pull-left">

                        @cannot('do-technical')
                        <li class="active">
                            <a href="#unit" data-toggle="tab">Program</a>
                        </li>
                        @endcannot

                        <li class="@can('do-technical') active @endcan">
                            <a href="#recipient" data-toggle="tab">Penerima manfaat</a>
                        </li>
                        <li class="">
                            <a href="#daerah" data-toggle="tab">Daerah</a>
                        </li>
                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">

                        @cannot('do-technical')
                        <div class="tab-pane active" id="unit">
                            @include("statistic.{$type}.units")
                        </div>
                        @endcannot

                        <div class="tab-pane  @can('do-technical') active @endcan" id="recipient">
                            @include("statistic.{$type}.recipients")
                        </div>

                        <div class="tab-pane" id="daerah">
                            @include('statistic.'.$type.'.regions')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.easyui')

@push('scripts')
    <script src="{{asset('components/highcharts/highcharts.js')}}"></script>
@endpush

