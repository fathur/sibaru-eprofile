@extends('layout.main')

@section('content')
    <div class="row">


        <div class="col-md-6">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Statistik Usulan</div>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        {!! $dataTable->table([
                            'id'    => $identifier . '-datatables',
                            'class' => 'table table-hover'

                        ]) !!}

                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection

@include('_component.datatables')

@push('script')
    {!! $dataTable->scripts() !!}
@endpush