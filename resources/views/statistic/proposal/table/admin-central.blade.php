@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Statistik Usulan</div>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                        {!! $dataTable->table([
                            'id'    => $identifier . '-datatables',
                            'class' => 'table table-hover'

                        ]) !!}

                    </div>
                </div>
            </div>


        </div>
{{--
        <div class="col-md-6">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Statistik Usulan: <strong>Jawa Barat</strong></div>
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Kabupaten</th>
                            <th>Rusun</th>
                            <th>Rusus</th>
                            <th>RUK</th>
                            <th>Swadaya</th>
                            <th>Total</th>
                        </tr>

                        </thead>


                    </table>
                </div>
            </div>


        </div>--}}
    </div>
@endsection

@include('_component.datatables')

@push('script')
    {!! $dataTable->scripts() !!}
@endpush
