<div class="alert alert-info">
    <div class="row" id="filter">
        {{--<div class="col-sm-6">

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="region-region" class="col-sm-4 control-label">Wilayah</label>

                    <div class="col-sm-8">
                        <input type="text" id="region-region" name="region-region" placeholder="Wilayah"
                               aria-describedby="help-block-region" style="width: 100%;">
                    </div>
                </div>


            </div>
        </div>--}}

        <div class="col-sm-6">

            <div class="form-horizontal">

                <div class="form-group">
                    {{--<label for="status-region" class="col-sm-4 control-label">Status</label>--}}

                   {{-- <div class="col-sm-8">
                        <select name="status-region" id="status-region" class="form-control">

                            <option value="0" selected>Semua</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
--}}
                    <label for="year-region" class="col-sm-4 control-label">Tahun</label>

                    <div class="col-sm-8">
                        <select name="year-region" id="year-region" class="form-control">

                            <option value="0" selected>Semua</option>
                            @foreach($years as $optionYear)
                                <option value="{{$optionYear}}">{{$optionYear}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button type="button" id="filter-region-btn" class="btn btn-info pull-right">Tampilkan</button>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <div id="region-chart"></div>
    </div>
</div>

@push('script')
    <script>
        // Build the chart
        var regions = Highcharts.chart('region-chart', {
            chart: {
                type: 'bar',
                height: 4000

            },
            title: {
                text: 'Jumlah Program Berdasarkan Daerah/Wilayah'
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'jumlah',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
//                valueSuffix: ' millions'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
//                x: -40,
//                y: 80,
                floating: false,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: []
        });

        $('#region-region').combotree({
            url: '{{route('regions.combotree')}}?level=4',
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34,
        });

        $('#filter-region-btn').click(function () {

            regions.series[3].remove();
            regions.series[2].remove();
            regions.series[1].remove();
            regions.series[0].remove();

            regions.showLoading();

            setTimeout(function(){

                getRegionsData();
            }, 1000);
        });

        function getRegionsData() {
            $.ajax({
                url: '{{route('statistics.'.$type.'.regions')}}',
                type: "GET",
                dataType: "json",
                data: {
                    region: $('#region-region').val(),
                    year: $('#year-region').val()
//                    status: $('#status-region').val()
                },
                success: function (data) {

                    regions.hideLoading();

                    regions.xAxis[0].update({
                        categories: data.categories
                    });

                    data.series.forEach(function(item) {
                        regions.addSeries({
                            name: item.name,
                            data: item.data,
                            color: item.color
                        }, false);
                    });

                    regions.redraw();

//                    regions.hideLoading();


                },
                cache: false
            });
        }

        $(document).ready(function () {
            getRegionsData();
        });
    </script>
@endpush