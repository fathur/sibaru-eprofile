<div class="alert alert-info">
    <div class="row" id="filter">
        <div class="@can('do-technical') col-sm-6 @else col-sm-4 @endcan">

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="region-recipient" class="col-sm-4 control-label">Wilayah</label>

                    <div class="col-sm-8">
                        <input type="text" id="region-recipient" name="region-recipient" placeholder="Wilayah"
                               aria-describedby="help-block-region">
                    </div>
                </div>


            </div>
        </div>

        @cannot('do-technical')
        <div class="col-sm-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="program-recipient" class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-8">
                        <select name="program-recipient" id="program-recipient" class="form-control">
                            <option value="0" selected>Semua</option>
                            @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->program}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        @endcannot

        <div class="@can('do-technical') col-sm-6 @else col-sm-4 @endcan">

            <div class="form-horizontal">

                <div class="form-group">
                   {{-- <label for="status-recipient" class="col-sm-4 control-label">Status</label>

                    <div class="col-sm-8">
                        <select name="status-recipient" id="status-recipient" class="form-control">

                            <option value="0" selected>Semua</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>--}}


                    <label for="year-recipient" class="col-sm-4 control-label">Tahun</label>

                    <div class="col-sm-8">
                        <select name="year-recipient" id="year-recipient" class="form-control">

                            <option value="0" selected>Semua</option>
                            @foreach($years as $optionYear)
                                <option value="{{$optionYear}}">{{$optionYear}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


            </div>


        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <button type="button" id="filter-recipient-btn" class="btn btn-info pull-right">Tampilkan</button>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <div id="recipient-chart">

        </div>
    </div>
</div>

@push('script')
    <script>
        // Build the chart
        var recipients = Highcharts.chart('recipient-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height: 600

            },
            title: {
                text: 'Perbandingan Usulan Berdasarkan Penerima Manfaat',
//                align: 'left'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> ({point.y})'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                        format: '{point.name} <br> {point.percentage:.1f}% ({point.y})'
                    },
                    showInLegend: true
                }
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                itemMarginTop: 5,
                labelFormat: '{name} ({y})',
                y: 30
//                labelFormatter: function() {
//                    return this.name + ' (' + this.y + ')';
//                }
            },
            series: [{
                name: 'Penerima Manfaat',
                colorByPoint: true,
                data: []
            }]
        });

        $('#region-recipient').combotree({
            url: '{{route('regions.combotree')}}?level=4',
            method: "GET",
            idField: "id",
            treeField: "name",
            height: 34,
            width: '100%'
        });

        $('#filter-recipient-btn').click(function(){
            getRecipientData();
        });

        function getRecipientData() {
            $.ajax({
                url: '{{route('statistics.'.$type.'.recipients')}}',
                type: "GET",
                dataType: "json",
                data: {
                    region: $('#region-recipient').val(),
//                    status: $('#status-recipient').val(),
                    year: $('#year-recipient').val(),
                    @can('do-technical')
                    unit: {{$user->unit_id}},
                    @else
                    unit: $('#program-recipient').val(),
                    @endcan
                },
                success: function (data) {
                    recipients.series[0].update({
                        data: data
                    });
                },
                cache: false
            });
        }

        $(document).ready(function () {
            getRecipientData();

            // Re adjust table size in tab content
            // https://github.com/DataTables/Responsive/issues/40
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#region-recipient').combotree({
                    width: '100%'
                })
            });
        });
    </script>
@endpush