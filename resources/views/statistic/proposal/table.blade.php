@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Tabel Usulan</div>
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Kabupaten</th>
                            <th>Jumlah Usulan</th>
                        </tr>

                        </thead>

                        <tbody>
                        @foreach($regions as $region)
                        <tr>
                            <td>{{$region->name}}</td>
                            <td>{{$region->totalProposal}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
@endsection