<form action="{{route('dwellings.plans.update', [$dwellingPlan->id])}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <input type="hidden" name="proposal_id" value="{{$dwellingPlan->proposal_id}}">

    <div class="form-group">
        <label for="dwelling_type" class="control-label">Jenis Hunian</label>
        <select id="dwelling_type" class="form-control" name="dwelling_type"  aria-describedby="help-block-dwelling_type">
            @foreach($dwellingTypes as $dwellingType)
                <option value="{{$dwellingType->id}}" @if($dwellingType->id == $dwellingPlan->dwelling_type_id) selected @endif >{{$dwellingType->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-dwelling_type"></div>
    </div>

   {{-- <div class="form-group">
        <label for="unit_size" class="control-label">Ukuran unit</label>
        <select id="unit_size" class="form-control" name="unit_size" aria-describedby="help-block-unit_size">
            @foreach($unitSizes as $unitSize)
                <option value="{{$unitSize->id}}"  @if($unitSize->id == $dwellingPlan->unit_size_id) selected @endif >{{$unitSize->text}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_size"></div>
    </div>--}}

    <div class="form-group">
        <label for="type" class="control-label">Tipe</label>
        <input type="text" id="type" class="form-control" name="type" placeholder="Tipe" value="{{$dwellingPlan->type}}">
        <div class="help-block" id="help-block-type"></div>
    </div>

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" value="{{$dwellingPlan->amount}}" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" value="{{$dwellingPlan->budget_plan}}" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>

</form>