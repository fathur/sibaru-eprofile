<form action="{{route('dwellings.plans.store')}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    <input type="hidden" name="proposal_id" value="{{$proposalId}}">

    <div class="form-group">
        <label for="dwelling_type" class="control-label">Jenis Hunian</label>
        <select id="dwelling_type" class="form-control" name="dwelling_type"  aria-describedby="help-block-dwelling_type">
            @foreach($dwellingTypes as $dwellingType)
                <option value="{{$dwellingType->id}}">{{$dwellingType->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-dwelling_type"></div>
    </div>

    {{--<div class="form-group">
        <label for="unit_size" class="control-label">Ukuran unit</label>
        <select id="unit_size" class="form-control" name="unit_size" aria-describedby="help-block-unit_size">
            @foreach($unitSizes as $unitSize)
                <option value="{{$unitSize->id}}">{{$unitSize->text}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_size"></div>
    </div>--}}

    <div class="form-group">
        <label for="type" class="control-label">Tipe</label>
        <input type="text" id="type" class="form-control" name="type" placeholder="Tipe">
        <div class="help-block" id="help-block-type"></div>
    </div>

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>
</form>