<form action="{{route('dwellings.plans.update', [$dwellingPlan->id])}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <input type="hidden" name="proposal_id" value="{{$dwellingPlan->proposal_id}}">

    {{--<div class="form-group">
        <label for="unit_type" class="control-label">Tipe unit</label>
        <select id="unit_type" class="form-control" name="unit_type"  aria-describedby="help-block-unit_type">
            @foreach($unitTypes as $unitType)
                <option value="{{$unitType->id}}" @if($unitType->id == $dwellingPlan->unit_type_id) selected @endif >{{$unitType->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_type"></div>
    </div>

    <div class="form-group">
        <label for="unit_size_rusun" class="control-label">Ukuran unit</label>
        <select id="unit_size_rusun" class="form-control" name="unit_size_rusun" aria-describedby="help-block-unit_size_rusun">
            @foreach($unitSizesRusun as $unitSizeRusun)
                <option value="{{$unitSizeRusun['id']}}" @if($unitSizeRusun['id'] == $dwellingPlan->unit_size_id) selected @endif>{{$unitSizeRusun['name']}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_size_rusun"></div>
    </div>--}}

    <div class="form-group">
        <label for="type" class="control-label">Tipe</label>
        <input type="text" id="type" class="form-control" name="type" placeholder="Tipe" value="{{$dwellingPlan->type}}">
        <div class="help-block" id="help-block-type"></div>
    </div>

    <div class="form-group">
        <label for="tower_block" class="control-label">Tower blok</label>
        <input type="text" id="tower_block" class="form-control" name="tower_block" placeholder="Tower blok" value="{{$dwellingPlan->tower_block}}">
        <div class="help-block" id="help-block-tower_block"></div>
    </div>

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" value="{{$dwellingPlan->amount}}" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" value="{{$dwellingPlan->budget_plan}}" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>


</form>