<form action="{{route('dwellings.plans.store')}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    <input type="hidden" name="proposal_id" value="{{$proposalId}}">

    {{--<div class="form-group">
        <label for="unit_type" class="control-label">Tipe unit</label>
        <select id="unit_type" class="form-control" name="unit_type"  aria-describedby="help-block-unit_type">
            @foreach($unitTypes as $unitType)
                <option value="{{$unitType->id}}">{{$unitType->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_type"></div>
    </div>

    <div class="form-group">
        <label for="unit_size_rusun" class="control-label">Ukuran unit</label>
        <select id="unit_size_rusun" class="form-control" name="unit_size_rusun" aria-describedby="help-block-unit_size_rusun">
            @foreach($unitSizesRusun as $unitSizeRusun)
                <option value="{{$unitSizeRusun['id']}}">{{$unitSizeRusun['name']}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit_size_rusun"></div>
    </div>--}}

    <div class="form-group">
        <label for="type" class="control-label">Tipe</label>
        <input type="text" id="type" class="form-control" name="type" placeholder="Tipe">
        <div class="help-block" id="help-block-type"></div>
    </div>

    <div class="form-group">
        <label for="tower_block" class="control-label">Tower blok</label>
        <input type="text" id="tower_block" class="form-control" name="tower_block" placeholder="Tower blok">
        <div class="help-block" id="help-block-tower_block"></div>
    </div>

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>
</form>