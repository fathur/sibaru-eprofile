<div class="row">
    <div class="col-md-12">

        @can('create-dwelling', $proposal)
        <button type="button" class="btn btn-primary pull-right"
                data-toggle="modal"
                data-target="#general-modal"
                data-backdrop="static"
                data-keyboard="false"
                id="btn-add-dwelling">
            <i class="fa fa-plus"></i>&nbsp; Tambah
        </button>
        @endcan

        <table class="table" id="dwelling-plan-datatables">
            <thead>
            <tr>{{--
                <th>Jenis Rusun</th>
                <th>Tipe/Lantai</th>--}}
                <th>Tipe</th>
                <th>Tower Blok</th>
                <th>Jumlah unit</th>
                <th>Rencana Anggaran</th>

                @can('create-dwelling', $proposal)
                <th width="105">&nbsp;</th>
                @endcan

            </tr>
            </thead>
        </table>
    </div>
</div>

@push('script')
<script>
    $('#dwelling-plan-datatables').DataTable({
        processing: true,
        serverSide: true,
//        stateSave: true,
        paging: false,
//        info: false,
        searching: false,

        ajax: {
            url: "{{route('dwellings.plans.data')}}",
            data: {
                proposal_id: {{$proposal->id}}
            }
        },
        columnDefs: [
            {className: "text-center", "targets": [2]},
            {className: "text-right", "targets": [3]},

        ],
        columns: [
           /* {data: 'unit_type_name', name: 'unit_types.name'},
            {data: 'rusun_unit_size', name: 'rusun_unit_size', searchable: false, orderable: false},*/
            {data: 'type', name: 'type'},
            {data: 'tower_block', name: 'tower_block'},
            {data: 'amount', name: 'amount'},
            {data: 'budget_plan', name: 'budget_plan'},

            @can('create-dwelling', $proposal)
            {data: 'action', name: 'action', searchable: false, orderable: false},
            @endcan
        ],
    });

    @can('create-dwelling', $proposal)
    $('#btn-add-dwelling').click(function () {
        $.get('{{route('dwellings.plans.create')}}', {
            unit_slug: '{{$proposal->unit->slug}}',
            proposal_id: {{$proposal->id}}
        }, function (response) {
            var modalId = 'general-modal';
            var $modalId = $('#' + modalId);
            $modalId.find('.modal-body').html('');

            $modalId.find('.modal-header').find('.modal-title').html(response.title);
            $modalId.find('.modal-body').html(response.body);
            $modalId.find('.modal-footer').html(response.footer);
        });
    });
    @endcan

</script>
@endpush