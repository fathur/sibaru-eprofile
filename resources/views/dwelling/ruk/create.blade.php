<form action="{{route('dwellings.plans.store')}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    <input type="hidden" name="proposal_id" value="{{$proposalId}}">

   {{-- <div class="form-group">
        <label for="component" class="control-label">Jenis komponen</label>
        <select id="component" class="form-control" name="component" aria-describedby="help-block-component">
            @foreach($components as $component)
                <option value="{{$component->id}}">{{$component->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-component"></div>
    </div>--}}

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>
</form>