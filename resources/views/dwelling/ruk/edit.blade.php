<form action="{{route('dwellings.plans.update', [$dwellingPlan->id])}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <input type="hidden" name="proposal_id" value="{{$dwellingPlan->proposal_id}}">

   {{-- <div class="form-group">
        <label for="component" class="control-label">Jenis komponen</label>
        <select id="component" class="form-control" name="component" aria-describedby="help-block-component">
            @foreach($components as $component)
                <option value="{{$component->id}}" @if($component->id == $dwellingPlan->component_id) selected @endif>{{$component->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-component"></div>
    </div>--}}

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" value="{{$dwellingPlan->amount}}" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" value="{{$dwellingPlan->budget_plan}}" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>

</form>