<form action="{{route('dwellings.plans.store')}}" id="dwelling-plan" method="post">
    {{csrf_field()}}
    <input type="hidden" name="proposal_id" value="{{$proposalId}}">

    <div class="form-group">
        <label for="location-swadaya" class="control-label">Lokasi</label>
        <input type="text" id="location-swadaya" name="location" placeholder="Lokasi"
               aria-describedby="help-block-location" style="width: 100%;">
        {{--<select id="location-swadaya" class="form-control" name="location"  aria-describedby="help-block-location">--}}

        {{--</select>--}}
        <div class="help-block" id="help-block-location"></div>
    </div>

    {{--<div class="form-group">
        <label for="coordinate" class="control-label">Koordinat</label>
        <input type="text" id="coordinate" class="form-control" name="coordinate" aria-describedby="help-block-coordinat" />
        <div class="help-block" id="help-block-coordinate"></div>
    </div>--}}

    <div class="form-group">
        <label for="amount" class="control-label">Jumlah unit</label>
        <input type="number" id="amount" class="form-control" name="amount" placeholder="Jumlah unit">
        <div class="help-block" id="help-block-amount"></div>

    </div>

    <div class="form-group">
        <label for="budget_plan" class="control-label">Rencana anggaran</label>
        <input type="text" id="budget_plan" class="form-control" name="budget_plan" placeholder="Rencana anggaran">
        <div class="help-block" id="help-block-budget_plan"></div>

    </div>
</form>

<script>
    $('#location-swadaya').combotree({
        @if(is_null($proposal->proposer))
        url: "{{route('regions.combotree')}}?level=4",
        @else
        url: "{{route('regions.combotree')}}?level=4&id={{$proposal->proposer->region->id}}",
        @endif

        method: "GET",
        idField: "id",
        treeField: "name",
        height: 34
    });

    {{--$('#location-swadaya').select2({--}}
        {{--theme: "bootstrap",--}}
        {{--width: '100%',--}}
        {{--ajax: {--}}
            {{--url: "{{route('regions.select')}}",--}}
            {{--dataType: 'json',--}}
            {{--delay: 250,--}}
            {{--data: function (params) {--}}
                {{--return {--}}
                    {{--q: params.term,--}}
                    {{--page: params.page,--}}
                {{--}--}}
            {{--},--}}
            {{--processResults: function (data, page) {--}}
                {{--return {--}}
                    {{--results: data--}}
                {{--}--}}
            {{--},--}}
            {{--cache: true--}}
        {{--},--}}
        {{--escapeMarkup: function (markup) {--}}
            {{--return markup;--}}
        {{--},--}}
{{--//        allowClear: true,--}}
        {{--minimumInputLength: 2,--}}
        {{--templateResult: function (data) {--}}
            {{--return data.name || data.text;--}}
        {{--},--}}
        {{--templateSelection: function (data) {--}}
            {{--return data.name || data.text;--}}
        {{--}--}}
    {{--});--}}
</script>