<div class="row">
    <div class="col-md-12">

        @can('create-dwelling', $proposal)
        <button type="button" class="btn btn-primary pull-right"
                data-toggle="modal"
                data-target="#general-modal"
                data-backdrop="static"
                data-keyboard="false"
                id="btn-add-dwelling">
            <i class="fa fa-plus"></i>&nbsp; Tambah
        </button>
        @endcan

        <table class="table" id="dwelling-plan-datatables">
            <thead>
            <tr>
                <th>Lokasi</th>
                {{--<th>Latitude</th>--}}
                {{--<th>Longitude</th>--}}
                <th>Jumlah unit</th>
                <th>Anggaran</th>

                @can('create-dwelling', $proposal)
                <th>&nbsp;</th>
                @endcan

            </tr>
            </thead>
        </table>
    </div>
    {{--<div class="col-md-6">--}}
        {{--map here soon--}}
    {{--</div>--}}
</div>
@push('script')
<script>
    $('#dwelling-plan-datatables').DataTable({
        processing: true,
        serverSide: true,
//        stateSave: true,
        paging: false,
//        info: false,
        searching: false,

        ajax: {
            url: "{{route('dwellings.plans.data')}}",
            data: {
                proposal_id: {{$proposal->id}}
            }
        },
        columnDefs: [

            {className: "text-center", "targets": [1]},
            {className: "text-right", "targets": [2]},
        ],
        columns: [

            {data: 'location', name: 'regions.name'},
            // {data: 'latitude', name: 'latitude'},
            // {data: 'longitude', name: 'longitude'},

            {data: 'amount', name: 'amount'},
            {data: 'budget_plan', name: 'budget_plan'},

            @can('create-dwelling', $proposal)
            {data: 'action', name: 'action', searchable: false, orderable: false},
            @endcan

        ],
    });

    @can('create-dwelling', $proposal)
    $('#btn-add-dwelling').click(function () {
        $.get('{{route('dwellings.plans.create')}}', {
            unit_slug: '{{$proposal->unit->slug}}',
            proposal_id: {{$proposal->id}}
        }, function (response) {
            var modalId = 'general-modal';
            var $modalId = $('#' + modalId);
            $modalId.find('.modal-body').html('');

            $modalId.find('.modal-header').find('.modal-title').html(response.title);
            $modalId.find('.modal-body').html(response.body);
            $modalId.find('.modal-footer').html(response.footer);
        });
    });
    @endcan

</script>
@endpush