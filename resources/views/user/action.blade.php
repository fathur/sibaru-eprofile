
<div class="btn-group btn-group-xs pull-right">
    <button type="button" class="btn btn-warning" onclick="editForm(this)"
            data-form="{{$edit_link}}">
        <i class="fa fa-pencil"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Edit</span>
    </button>
    <button type="button" class="btn btn-danger" onclick="deleteForm(this)"
            data-link="{{$delete_link}}"
            data-identifier="{{$identifier}}">

        <i class="fa fa-trash"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Delete</span>
    </button>
</div>

<button class="btn btn-success btn-xs pull-right"
        onclick="editPassword(this)"
        data-user-id="{{$user->id}}"
        data-url="{{route('users.password.edit')}}">
    <i class="fa fa-lock"></i> Password
</button>
