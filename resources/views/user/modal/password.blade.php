<form action="{{route('users.password.update', [$user->id])}}" id="change-passwd">

    {{csrf_field()}}
    <div class="form-group">
        <label for="password1" class="control-label">Password</label>
        <input type="password" class="form-control" id="password1" name="password1">
        <div class="help-block" id="help-block-password1"></div>

    </div>

    <div class="form-group">
        <label for="password2" class="control-label">Konfirmasi Password</label>
        <input type="password" class="form-control" id="password2" name="password2">
        <div class="help-block" id="help-block-password2"></div>
    </div>

    <button class="btn btn-info" type="submit">Simpan</button>
</form>
<script>
    $('#change-passwd').submit(function(e){
        e.preventDefault();
        var $this = $(this);

        $.ajax({
            type: "PUT",
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (data) {
                $('#general-modal').modal('hide');

            },
            error: function (jqXHR, textStatus, errorThrown) {

                var $identifier = $('#change-passwd');

                $identifier.find('.form-group').removeClass('has-error');
                $identifier.find('.form-group').find('.help-block').html('');

                var data = $.parseJSON(jqXHR.responseText);

                $.each(data, function (item, errors) {

                    var html = '<ul class="list-unstyled">';

                    $.each(errors, function (index, error) {

                        html += '<li>' + error + '</li>';
                    });

                    html += '</ul>';

                    $('#help-block-' + item).html(html);
                    $('#help-block-' + item).parent().addClass('has-error');

                });
            }
        });
    });
</script>