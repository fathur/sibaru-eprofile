<form action="{{route('users.store')}}" id="{{$identifier}}" method="post">
    {{csrf_field()}}

    <div class="form-group">
        <label for="username" class="control-label">Username</label>
        <input type="text" id="username" class="form-control" name="username" placeholder="Username" aria-describedby="help-block-username">
        <div class="help-block" id="help-block-username"></div>
    </div>

    <div class="form-group">
        <label for="nip" class="control-label">NIP</label>
        <input type="text" id="nip" class="form-control" name="nip" placeholder="NIP" aria-describedby="help-block-nip">
        <div class="help-block" id="help-block-nip"></div>
    </div>

    <div class="form-group">
        <label for="name" class="control-label">Name</label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Name" aria-describedby="help-block-name">
        <div class="help-block" id="help-block-name"></div>
    </div>

    <div class="form-group">
        <label for="email" class="control-label">Email</label>
        <input type="email" id="email" class="form-control" name="email" placeholder="Email">
        <div class="help-block" id="help-block-email"></div>

    </div>

    <div class="form-group">
        <label for="phone" class="control-label">No. Telp/HP</label>
        <input type="email" id="phone" class="form-control" name="phone" placeholder="No. Telp/HP">
        <div class="help-block" id="help-block-phone"></div>
    </div>

    <div class="form-group">
        <label for="role" class="control-label">Role</label>
        <select name="role" id="role" class="form-control">
            @foreach($roles as $role)
                <option value="{{$role->id}}">{{$role->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-role"></div>
    </div>

    <div class="form-group">
        <label for="agency" class="control-label">Instansi</label>
        <select name="agency" id="agency" class="form-control">

        </select>
        <div class="help-block" id="help-block-agency"></div>
    </div>

    <div class="form-group">
        <label for="unit" class="control-label">Unit</label>
        <select name="unit" id="unit" class="form-control">
            <option value=""> -Pilih satu- </option>
            @foreach($units as $unit)
                <option value="{{$unit->id}}">{{$unit->name}}</option>
            @endforeach
        </select>
        <div class="help-block" id="help-block-unit"></div>
    </div>

    <div class="divider"></div>

    <div class="form-group">
        <label for="password" class="control-label">Password</label>
        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
        <div class="help-block" id="help-block-password"></div>

    </div>
</form>


<script>
    // agencies/select'
    $('#agency').select2({
        theme: "bootstrap",
        width: '100%',
        ajax: {
            url: "{{url('agencies/select')}}",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    maxLevel: 2
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                }
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        allowClear: true,
        minimumInputLength: 2,
        templateResult: function (data) {
            return data.name || data.text;
        },
        templateSelection: function (data) {
            return data.name || data.text;
        }
    });
</script>