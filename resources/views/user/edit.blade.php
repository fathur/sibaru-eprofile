@extends('layout.main')

@section('styles')
@stop

@section('style')
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Edit user</div>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    @include('user.modal.edit')
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop

@section('script')
@stop