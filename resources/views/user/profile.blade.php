@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Profile</div>
                    </div>
                    <div class="action">
                        {{--no action--}}
                    </div>
                </div>

                <div class="portlet-body">

                    @if(session('status-profile'))
                        <div class="alert alert-success">
                            {{session('status-profile')}}
                        </div>
                    @endif



                    <form action="{{url('profile')}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="form-group">
                            <label for="username" class="control-label">Username
                                <a href=# data-toggle="tooltip"
                                   title="Username tidak dapat diedit, silahkan hubungin admin untuk mengubah username.">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <input type="text" id="username" class="form-control" aria-describedby="help-block-username"
                                   value="{{$user->username}}" disabled="disabled">
                            <div class="help-block" id="help-block-username"></div>
                        </div>

                        <div class="form-group">
                            <label for="nip" class="control-label">NIP
                                <a href=# data-toggle="tooltip" title="Nomor Induk Pegawai"><i
                                            class="fa fa-info-circle"></i></a>

                            </label>
                            <input type="text" id="nip" class="form-control" name="nip" placeholder="NIP"
                                   aria-describedby="help-block-nip" value="{{$user->nip}}">
                            <div class="help-block" id="help-block-nip"></div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Nama
                                <a href=# data-toggle="tooltip" title="Masukkan nama yang ingin Anda tampilkan."><i
                                            class="fa fa-info-circle"></i></a>

                            </label>
                            <input type="text" id="name" class="form-control" name="name" placeholder="Name"
                                   aria-describedby="help-block-name" value="{{$user->name}}">
                            <div class="help-block" id="help-block-name"></div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Email
                                <a href=# data-toggle="tooltip" title="Email valid untuk konfirmasi dan notifikasi."><i
                                            class="fa fa-info-circle"></i></a>

                            </label>
                            <input type="email" id="email" class="form-control" name="email" placeholder="Email"
                                   value="{{$user->email}}">
                            <div class="help-block" id="help-block-email"></div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">No. Telp/HP</label>
                            <input type="text" id="phone" class="form-control" name="phone" placeholder="No. Telp/HP"
                                   value="{{$user->phone}}">
                            <div class="help-block" id="help-block-phone"></div>
                        </div>

                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>

                    </form>


                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Password</div>
                    </div>
                </div>

                <div class="portlet-body">

                    @if(session('status-password'))
                        <div class="alert alert-success">
                            {{session('status-password')}}
                        </div>
                    @endif

                    @if(session('status-password-error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{session('status-password-error')}}</li>
                            </ul>
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{url('profile/password')}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="form-group">
                            <label for="password_old" class="control-label">Password lama
                                <a href=# data-toggle="tooltip"
                                   title="Password lama yang ingin Anda rubah. Jika Anda lupa password lama, silahkan menghubungi admin."><i
                                            class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <input type="password" id="password_old" name="password_old" class="form-control" placeholder="Password Lama" aria-describedby="help-block-username">
                            <div class="help-block" id="help-block-password_old"></div>
                        </div>

                        <div class="form-group">
                            <label for="password_new" class="control-label">Password baru
                                <a href=# data-toggle="tooltip"
                                   title="Password baru yang lebih kuat, pastikan password ini tidak sama dengan password lama."><i
                                            class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <input type="password" id="password_new" name="password_new" class="form-control" placeholder="Password Baru" aria-describedby="help-block-username">
                            <div class="help-block" id="help-block-password_new"></div>
                        </div>

                        <div class="form-group">
                            <label for="password_new_confirm" class="control-label">Konfirmasi password baru
                                <a href=# data-toggle="tooltip"
                                   title="Masukkan kembali password baru yang sama seperti field sebelumnya."><i
                                            class="fa fa-info-circle"></i></a>
                            </label>
                            <input type="password" id="password_new_confirm" name="password_new_confirm" class="form-control" placeholder="Konfirmasi Password Baru" aria-describedby="help-block-username">
                            <div class="help-block" id="help-block-password_new_confirm"></div>
                        </div>

                        <button type="submit" class="btn btn-danger"><i class="fa fa-key"></i> Ganti Password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                html: true
            });
        })
    </script>
@endpush