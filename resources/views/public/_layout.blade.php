
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('img/logo.png')}}">

    <title>SIBARU</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('components/font-awesome/css/font-awesome.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/login.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('styles')

    @stack('style')
</head>

<body>

<nav class="navbar navbar-sibaru-header">
    <div class="container">
        <div class="navbar-header hidden-xs">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{asset('img/logo.png')}}" alt="Kementerian PU" class="pull-left margin-right-10">
                <div class="pull-left">
                Direktorat Jenderal Penyediaan Perumahan<br/>
                Kementerian Pekerjaan Umum dan Perumahan Rakyat
                </div>

            </a>
        </div>

        <a href="{{route('login')}}" class="btn btn-success pull-right btn-login" id="login-button">Masuk</a>

    </div>
</nav>

<div class="line-image">
</div>

<nav class="navbar navbar-sibaru">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="centerer center-block">
                @include('public._menu')
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>

@yield('titles')

<div class="container" style="min-height: 650px;">

    @yield('content')

</div><!-- /.container -->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                Sibaru © 2017
            </div>
            {{--<div class="col-md-6 hidden-xs">--}}
                {{--<a href="http://perumahan.pu.go.id/" target="_blank" class="btn pull-right mr-5" style="background-color: #0D2D6C; color: #FFCA00">Ditjen Penyediaan Perumahan</a>--}}
                {{--<a href="http://pu.go.id/" target="_blank" class="btn pull-right mr-5" style="background-color: #FFCA00; color: #0D2D6C;">PU - Net</a>--}}
            {{--</div>--}}
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{url('components/jquery/jquery.min.js')}}"></script>
<script>window.jQuery || document.write('<script src="{{url('components/jquery/jquery.min.js')}}"><\/script>')</script>
<script src="{{url('components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

@stack('scripts')

@stack('script')

</body>
</html>
