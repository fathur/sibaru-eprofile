@extends('public._layout')

@push('style')
    <style>
        #title-container-fluid {
            margin-top: -20px;
            margin-bottom: 30px;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffca00+43,ffca00+43,0d2d6c+61 */
            background: #ffca00; /* Old browsers */
            background: -moz-linear-gradient(left, #ffca00 43%, #ffca00 43%, #0d2d6c 61%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left, #ffca00 43%,#ffca00 43%,#0d2d6c 61%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right, #ffca00 43%,#ffca00 43%,#0d2d6c 61%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffca00', endColorstr='#0d2d6c',GradientType=1 ); /* IE6-9 */
        }

        #title-container h3 {
            font-weight: 300;
        }
    </style>
@endpush

@section('titles')
    <div class="container-fluid" id="title-container-fluid">
        <div class="container" id="title-container">
            <div class="row">
                <div class="col-sm-12 text-center" style="background-color: #FFCA00; padding: 20px 5px 30px 5px;">
                    <h3>SIBARU - Sistem Bantuan Penyediaan Perumahan</h3>
                </div>
                {{--<div class="col-sm-6 text-center"--}}
                     {{--style="background-color: #0D2D6C; padding: 20px 5px 30px 5px; color: #FFF;">--}}
                    {{--<h3>eProfile Data Perumahan</h3>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

@section('content')
    {{--<div class="row mb-15">
        <div class="col-md-offset-1 col-md-11">
            <h1 class="welcome">Selamat Datang,</h1>
            <h4>SISTEM DATA PERUMAHAN</h4>
        </div>
    </div>--}}

    <div class="well well-lg">

        {!! $prologue !!}

    </div>

    <div class="row margin-bottom-15">
        <div class="col-sm-12">
            <a href="http://perumahan.pu.go.id/" target="_blank" class="btn pull-right mr-5"
               style="background-color: #0D2D6C; color: #FFCA00">Ditjen Penyediaan Perumahan</a>
            <a href="http://pu.go.id/" target="_blank" class="btn pull-right mr-5"
               style="background-color: #FFCA00; color: #0D2D6C;">PU - Net</a>
        </div>
    </div>
@stop

