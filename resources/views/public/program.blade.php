@extends('public._layout')


@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-sibaru">
                <div class="panel-body">
                    {{--<div id="map-canvas" style="height: 500px;"></div>--}}
                    <div class="row dashboard-chart">
                        <div class="col-sm-12">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="thn-{{$year}}" class="item"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-sibaru panel-table">
                <div class="panel-heading">
                    <h3 class="panel-title">Program</h3>
                </div>
                <div class="panel-body">

                    <div class="content">

                        <div class="form-group">
                            <label for="year" class="col-sm-2 control-label">Tahun</label>
                            <div class="col-sm-10">
                                <select name="year" id="year" class="form-control input-sm" onchange="reloadProgram(this)">


                                    @foreach($years as $optionYear)
                                            <option value="{{$optionYear}}" @if($optionYear == $year) selected @endif>{{$optionYear}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    {{--
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>Wilayah</th>
                            @foreach($units as $unit)
                            <th>{{$unit->slug}}</th>
                            @endforeach
                            <th>Total</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($regions as $region)
                        <tr>
                            <td>{{$region->name}}</td>
                            @foreach($units as $unit)
                                <td class="text-center">
                                    @php
                                    $count = 0
                                    @endphp

                                    @foreach($data as $item)
                                        @if($item->unit_id == $unit->id and $item->province_id == $region->id)
                                            @if($count == 0)
                                            @php
                                            $count = $item->cnt
                                            @endphp
                                            @endif
                                        @endif
                                    @endforeach

                                    {{$count}}

                                </td>
                            @endforeach
                            <td class="text-center">
                                @php
                                $count = 0
                                @endphp

                                @foreach($totals as $item)
                                    @if($item->province_id == $region->id)
                                        @if($count == 0)
                                            @php
                                            $count = $item->cnt
                                            @endphp
                                        @endif
                                    @endif
                                @endforeach

                                {{$count}}

                            </td>
                            <td>
                                <a href="{{url('program/' .  $region->id . '/tahun/' . $year)}}">
                                    <i class="fa fa-file-text-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    --}}
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
        type="text/javascript"></script>
<script src="{{asset('components/highcharts/highcharts.js')}}"></script>

@endpush

@push('script')
<script>
    function reloadProgram(dom) {
        var $this = $(dom);
        var year = $this.val();
        window.location = '{{url('/')}}' + '/rekap-usulan/' + year;
    }

    function initMap() {

        var mapOptions = {
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // scrollwheel: false,
        };

        map = new google.maps.Map(document.getElementById("map-canvas"));


    }


    /// Cahrt
            {{--@foreach($charts as $chart)--}}
    var report{{$year}} = Highcharts.chart('thn-{{$year}}', {
            chart: {
                type: 'column',
//            backgroundColor: '#333333'
            },
            title: {
                text: 'Jumlah Proposal Masuk',
            },
            credits: {
                enabled: false
            },
            loading: {
                hideDuration: 1000,
                showDuration: 1000
            },
            xAxis: {
                categories: {!! $provinces !!},
                labels: {
                    style: {
//                    "color": "#FFFFFF"
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '{{$year}}',
                    style: {
//                    "color": "#FFFFFF",
                        "font-size": "20px"
                    }
                }
            },
            legend: {
                align: 'center',
                shadow: false,
                itemStyle: {
//                "color": "#FFFFFF",
                    "font-weight": 100
                }
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },

            series: []

        });

    report{{$year}}.showLoading();

    function getReport{{$year}}() {
        $.ajax({
            url: '{{route('program.chart.data')}}',
            type: "GET",
            dataType: "json",
            data: {
                year: {{$year}}
            },
            success: function (data) {

                console.log(data);

                for(var key in data) {
                    var item = data[key];

                    report{{$year}}.addSeries({
                        name: item.name,
                        data: item.data,
                        color: item.color,
                        borderWidth: 0
                    }, false);
                }


                report{{$year}}.redraw();

                report{{$year}}.hideLoading();


            },
            cache: false
        });
    }

    {{--@endforeach--}}

    $(document).ready(function () {

        {{--@foreach($charts as $chart)--}}
        getReport{{$year}}();
        {{--@endforeach--}}
    });
</script>
@endpush

