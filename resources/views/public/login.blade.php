<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('img/logo.png')}}">

    <title>SIBARU</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{url('css/login.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('styles')

    @stack('style')
</head>

<body class="login">

<nav class="navbar navbar-sibaru-header">
    <div class="container">
        <div class="navbar-header hidden-xs">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{asset('img/logo.png')}}" alt="Kementerian PU" class="pull-left margin-right-10">
                <div class="pull-left">
                    Direktorat Jenderal Penyediaan Perumahan<br/>
                    Kementerian Pekerjaan Umum dan Perumahan Rakyat
                </div>
            </a>
        </div>


    </div>
</nav>

<nav class="navbar navbar-sibaru">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="centerer center-block">

                @include('public._menu')
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container" style="min-height: 800px;">

    <div class="login-wrapper">
        <div class="background"></div>
        <form action="{{url('login')}}" method="post">

            @if(count($errors))
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{csrf_field()}}
            <input type="text" id="username" class="form-control" name="username" placeholder="Username"
                   value="{{old('username')}}" required autofocus>
            <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
            <label class="check-remember">
                <input type="checkbox" name="remember" value="1"> Ingat saya
            </label>

                @if(config('captcha.enable'))
                    <p>{!! captcha_img('flat') !!}</p>

                    <input type="text" name="captcha" id="captcha" class="form-control" title="Captcha"
                           placeholder="Captcha">
                    {{--{!! app('captcha')->display(['add-js' => false]) !!}--}}
                @endif

                <button type="submit" class="btn btn-danger pull-right">Login</button>
        </form>
    </div>

</div><!-- /.container -->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                Sistem Data Perumahan © 2017
            </div>
            <div class="col-md-6 hidden-xs">
                <a href="http://perumahan.pu.go.id/" target="_blank" class="btn pull-right mr-5"
                   style="background-color: #0D2D6C; color: #FFCA00">Ditjen Penyediaan Perumahan</a>
                <a href="http://pu.go.id/" target="_blank" class="btn pull-right mr-5"
                   style="background-color: #FFCA00; color: #0D2D6C;">PU - Net</a>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{url('components/jquery/jquery.min.js')}}"></script>
<script>window.jQuery || document.write('<script src="{{url('components/jquery/jquery.min.js')}}"><\/script>')</script>
<script src="{{url('components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

{{--{!! app('captcha')->displayJs() !!}--}}
@stack('scripts')

@stack('script')

</body>
</html>
