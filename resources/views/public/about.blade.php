@extends('public._layout')


@section('content')
    <div class="row about-us">
        <div class="col-md-6 col-xs-12">
            <div id="map-canvas" style="height: 500px;"></div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="panel panel-sibaru">
                <div class="panel-heading">
                    <h3 class="panel-title">Kontak</h3>
                </div>
                <div class="panel-body">
                    <p>Kementerian Pekerjaan Umum dan Perumahan Rakyat - Direktorat Jenderal Penyediaan Perumahan</p>
                    <p>Jl. Pattimura 20, Kebayoran Baru - Jakarta Selatan Telp/Fax : (021) 72796124</p>
                    <p>E-mail : <a href="mailto:datinperumahan@gmail.com">datinperumahan@gmail.com</a></p>
                </div>
            </div>
            {{--<div class="panel panel-sibaru">--}}
            {{--<div class="panel-heading">--}}
            {{--<h3 class="panel-title">Customer Services</h3>--}}
            {{--</div>--}}
            {{--<div class="panel-body">--}}
            {{--<p>Senin - Jumat: 08:00 - 17:00</p>--}}
            {{--<p>Sabtu, Minggu, hari libur nasional: Libur</p>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@stop

@push('scripts')
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpXaYE9V0O3H5p71HfrVG3fYOEJZElv-s&callback=initMap"
        type="text/javascript"></script>
@endpush
@push('script')
<script>
    var map;
    function initMap() {
        var myLatLng = new google.maps.LatLng(-6.2369433,106.8003272);

        var mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        new google.maps.Marker({
            position: myLatLng,
            map: map
        });
    }
</script>
@endpush
