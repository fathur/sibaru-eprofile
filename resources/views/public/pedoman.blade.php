@extends('public._layout')


@section('content')
   <div class="row mb-15">
       <div class="col-md-12">
           <form action="{{url('pedoman')}}" method="get">
               <input type="text" class="form-control input-lg" placeholder="Cari" name="q" value="{{$q}}">
           </form>
       </div>
   </div>

   <div class="panel panel-default">
       <div class="panel-body">
           <ul class="list-group publications">

               @foreach($documents as $document)
               <li class="list-group-item">
                   <div class="content">
                       <div class="title"><h4>{{$document->title}}</h4></div>
                       <div class="description">{{$document->description}}</div>
                   </div>
                   <div class="download">
                       <a href="{{route('public.pedoman.download', $document->id)}}"><i class="fa fa-download fa-3x"></i></a>
                   </div>
               </li>
               @endforeach

               {{ $documents->appends(['q' => $q])->links() }}
           </ul>
       </div>
   </div>
@stop

