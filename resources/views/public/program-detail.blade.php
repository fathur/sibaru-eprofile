@extends('public._layout')


@section('content')
    <div class="row">


        <div class="col-md-12">
            <div class="panel panel-sibaru panel-table">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Shortlist Bantuan Program TA {{$year}}</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Unit</th>
                            <th>Kegiatan</th>
                            <th>Penerima Manfaat</th>
                            <th>Lokasi</th>
                            <th>Instansi Pengusul</th>
                            <th>Total Unit</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($data as $item)
                        <tr>
                            <td>{{$item->unit_name}}</td>
                            <td>{{$item->program_name}}</td>
                            <td>{{$item->recipient_name}}</td>
                            <td>{{Cache::get('region-'.$item->location_id)}}</td>
                            <td>{{$item->agency_name}}</td>
                            <td>-</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

