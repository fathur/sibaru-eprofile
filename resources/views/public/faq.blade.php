@extends('public._layout')


@section('content')
    <div class="row mb-15">
        <div class="col-md-12">
            <form action="{{url('faq')}}" method="get">
                <input type="text" class="form-control input-lg" placeholder="Cari" name="q" value="{{$q}}">
            </form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <ul class="list-group publications">

                @foreach($faqs as $faq)
                    <li class="list-group-item">
                        <div class="content">
                            <div class="title"><h4>{{$faq->question}}</h4></div>
                            <div class="description">{{$faq->answer}}</div>
                        </div>

                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop

