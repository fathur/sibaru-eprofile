<div class="btn-group btn-group-xs pull-right">


    <a class="btn btn-warning" href="{{route('regions.profiles.index', [$region['id']])}}">
        <i class="fa fa-user"></i>
        <span class="hidden-xs hidden-sm">&nbsp; Profile</span>
    </a>
</div>