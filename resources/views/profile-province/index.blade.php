@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light portlet-datatable">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption-subject">Profil Provinsi</div>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="" id="{{$identifier}}-datatables"></table>
                </div>
            </div>
        </div>
    </div>
@stop

@include('_component.select2')
@include('_component.easyui')

@push('styles')
<link rel="stylesheet" href="{{asset('components/sweetalert2/dist/sweetalert2.css')}}">
@endpush

@push('scripts')
<script src="{{asset('components/sweetalert2/dist/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/simpleUpload.min.js')}}"></script>
@endpush

@push('script')
<script>
    $('#{{$identifier}}-datatables').treegrid({
        method: "GET",
        url: "{{route('profile.province.data')}}?level=2",
        idField: "id",
        treeField: "name",
        lines: true,
        striped: true,
        columns: [[
            {title:'Nama',field:'name'},
            {title:'Level',field:'level_name',width:280},
            {title:' ',field:'action'}
        ]]
    });

    $('#{{$identifier}}-trigger-modal').click(function () {

        showGeneralModal('{{route('regions.create')}}');

    });
</script>
@endpush