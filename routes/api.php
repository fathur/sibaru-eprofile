<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::get('laporan/data', 'ReportController@data');

Route::get('provinsi/data', 'RegionController@dataProvinceForEHousing');

Route::get('profil-provinsi/{provinsi}/tahun/{year}/basic-info', 'RegionProfileController@apiBasicInfo');
Route::get('profil-provinsi/{provinsi}/tahun/{year}/backlog', 'RegionProfileController@apiBacklog');
Route::get('profil-provinsi/{provinsi}/tahun/{year}/recapitulation', 'RegionProfileController@apiRecapitulation');
Route::get('profil-provinsi/{provinsi}/tahun/{year}/detail', 'RegionProfileController@apiDetail');

