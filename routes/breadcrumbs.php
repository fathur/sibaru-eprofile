<?php

Breadcrumbs::register('dashboard', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->push('Dashboard', route('dashboard'));
});

Breadcrumbs::register('users.index', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Users', route('users.index'));
});

Breadcrumbs::register('criteria.index', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Kriteria', route('criteria.index'));
});

Breadcrumbs::register('proposals.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Usulan', route('proposals.index'));
});

Breadcrumbs::register('shortlists.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Shortlist', route('shortlists.index'));
});

Breadcrumbs::register('shortlists.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('shortlists.index');
    $breadcrumb->push('Shortlist ' . $id, route('shortlists.edit', [$id]));
});

Breadcrumbs::register('shortlists.view', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Shortlist', route('shortlists.view'));
});

Breadcrumbs::register('proposals.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id){
    $breadcrumb->parent('proposals.index');
    $breadcrumb->push($id, route('proposals.edit', [$id]));
});

Breadcrumbs::register('proposals.create.rusun', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('proposals.index');
    $breadcrumb->push('Tambah usulan', route('proposals.create.rusun'));
});

Breadcrumbs::register('proposals.create.rusus', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('proposals.index');
    $breadcrumb->push('Tambah usulan', route('proposals.create.rusus'));
});

Breadcrumbs::register('proposals.create.swadaya', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('proposals.index');
    $breadcrumb->push('Tambah usulan', route('proposals.create.swadaya'));
});

Breadcrumbs::register('proposals.create.ruk', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb){
    $breadcrumb->parent('proposals.index');
    $breadcrumb->push('Tambah usulan', route('proposals.create.ruk'));
});

Breadcrumbs::register('shortlists.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('proposals.edit', $id);
    $breadcrumb->push('Buat shortlist', route('shortlists.create', [$id]));
});

Breadcrumbs::register('decrees.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('SK Penetapan', route('decrees.index'));
});

Breadcrumbs::register('decrees.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('shortlists.edit', $id);
    $breadcrumb->push('Buat SK Penetapan', route('decrees.create', [$id]));
});

Breadcrumbs::register('decrees.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('decrees.index');
    $breadcrumb->push('SK Penetapan ' .  $id, route('decrees.edit', [$id]));
});

Breadcrumbs::register('agencies.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Instansi', route('agencies.index'));
});

Breadcrumbs::register('criteria.rusun.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Kriteria Rusun', route('criteria.rusun.index'));
});

Breadcrumbs::register('criteria.rusus.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Kriteria Rusus', route('criteria.rusus.index'));
});

Breadcrumbs::register('criteria.swadaya.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Kriteria Swadaya', route('criteria.swadaya.index'));
});

Breadcrumbs::register('criteria.ruk.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Kriteria RUK', route('criteria.ruk.index'));
});

Breadcrumbs::register('tematics.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Tematik', route('tematics.index'));
});

Breadcrumbs::register('tematics.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('tematics.index');
    $breadcrumb->push('Edit tematik', route('tematics.edit', $id));
});

Breadcrumbs::register('regions.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Wilayah', route('regions.index'));
});

Breadcrumbs::register('regions.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('regions.index');

    $region = \App\Models\Region::find($id);
    $breadcrumb->push($region->name, route('regions.edit', $id));
});




Breadcrumbs::register('reports.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Laporan', route('reports.index'));
});

Breadcrumbs::register('statistics.proposal.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Grafik Usulan', route('statistics.proposal.index'));
});

Breadcrumbs::register('statistics.proposal.table', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('statistics.proposal.index');
    $breadcrumb->push('Tabel Usulan', route('statistics.proposal.table'));
});

Breadcrumbs::register('statistics.eprofile.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('eprofile.index');
    $breadcrumb->push('Grafik', route('statistics.eprofile.index'));
});

Breadcrumbs::register('faqs.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('FAQ', route('faqs.index'));
});

Breadcrumbs::register('faqs.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('faqs.index');
    $breadcrumb->push('FAQ', route('faqs.edit', $id));
});

Breadcrumbs::register('guidance.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Pedoman', route('guidance.index'));
});

Breadcrumbs::register('guidance.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('guidance.index');
    $breadcrumb->push('Pedoman', route('guidance.edit', $id));
});

Breadcrumbs::register('eprofile.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Buat eprofile', route('eprofile.create'));
});

Breadcrumbs::register('eprofile.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('eprofile.index');
    $breadcrumb->push('Edit ' . $id, route('eprofile.edit', $id));
});

Breadcrumbs::register('programs.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Program', route('programs.index'));
});

Breadcrumbs::register('recipients.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Penerima Manfaat', route('recipients.index'));
});

Breadcrumbs::register('eprofile.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('eProfile', route('eprofile.index'));
});

Breadcrumbs::register('posts.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Post', route('posts.create'));
});

Breadcrumbs::register('posts.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Edit Post', route('posts.edit', $id));
});

Breadcrumbs::register('profile.province.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('eprofile.index');
    $breadcrumb->push('Profil Provinsi', route('profile.province.index'));
});

Breadcrumbs::register('regions.profiles.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('profile.province.index');
    $region = \App\Models\Region::find($id);
    $breadcrumb->push($region->name, route('regions.profiles.index', $id));
});

Breadcrumbs::register('regions.profiles.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('regions.profiles.index', $id);
//    $region = \App\Models\Region::find($id);
    $breadcrumb->push('Baru', route('regions.profiles.create', $id));
});

Breadcrumbs::register('regions.profiles.edit', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $regionId, $id) {

    $year = \App\Models\RegionProfile::find($id)->year;

    $breadcrumb->parent('regions.profiles.index', $regionId, $id);
//    $region = \App\Models\Region::find($id);
    $breadcrumb->push($year, route('regions.profiles.edit', [$regionId, $id]));
});

Breadcrumbs::register('regions.profiles.show', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $regionId, $profileId) {
    $breadcrumb->parent('regions.profiles.index', $regionId);
//    $region = \App\Models\Region::find($id);
    $breadcrumb->push('Show', route('regions.profiles.show', [$regionId,$profileId]));
});

Breadcrumbs::register('profile.province.map', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('eprofile.index');
    $breadcrumb->push('Peta', route('profile.province.map'));
});



Breadcrumbs::register('regions.specials.index', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('regions.profiles.index', $id);
    $breadcrumb->push('Data 2010 - 2014', route('regions.specials.index', $id));
});

Breadcrumbs::register('regions.specials.create', function(\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('regions.specials.index', $id);
    $breadcrumb->push('Tambah', route('regions.specials.create', $id));
});

Breadcrumbs::register('profile.index', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Profile', route('profile.index'));
});

Breadcrumbs::register('notifications.index', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Notifikasi', route('notifications.index'));
});

Breadcrumbs::register('notifications.show', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb, $id) {
    $breadcrumb->parent('notifications.index');
    $breadcrumb->push($id, route('notifications.show',  $id));
});

Breadcrumbs::register('notifications.history', function (\DaveJamesMiller\Breadcrumbs\Generator $breadcrumb) {
    $breadcrumb->parent('notifications.index');
    $breadcrumb->push('History', route('notifications.history'));
});