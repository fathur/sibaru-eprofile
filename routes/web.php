<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PublicController@index')->name('home');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout')->middleware('auth');

# Profile
Route::get('profile', 'UserController@showProfile')->name('profile.index')->middleware('auth');
Route::put('profile', 'UserController@updateProfile')->name('profile.update')->middleware('auth');
Route::put('profile/password', 'UserController@updateProfilePassword')->name('profile.pass')->middleware('auth');

#
Route::get('pedoman', 'PublicController@pedoman')->name('public.pedoman');
Route::get('pedoman/{id}/unduh', 'PublicController@downloadPedoman')->name('public.pedoman.download');
Route::get('faq', 'PublicController@faq')->name('public.faq');
Route::get('kontak', 'PublicController@aboutUs')->name('public.about');
Route::get('program', 'PublicController@program');
Route::get('rekap-usulan', 'PublicController@program');
Route::get('rekap-usulan/chart/data', 'PublicController@programChartData')->name('program.chart.data');
Route::get('rekap-usulan/{year}', 'PublicController@programYear')->name('public.program.year');
Route::get('rekap-usulan/{id}/tahun/{year}', 'PublicController@programDetail');

// Registration Routes...
# Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
# Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group([
    'middleware'    => 'auth'
], function(){

//    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/dashboard', 'ReportController@index')->name('dashboard');

    Route::resource('guidance', 'GuidanceController');

    Route::resource('faqs', 'FaqController');

    Route::post('assets', 'AssetController@upload')->name('assets.upload');
    Route::get('assets/{path}', 'AssetController@view')->name('assets.view');

    Route::get('users/data', 'UserController@data')->name('users.data');
    Route::get('users/password', 'UserController@getFormPassword')->name('users.password.edit');
    Route::put('users/password/{userId}', 'UserController@updatePassword')->name('users.password.update');
    Route::resource('users', 'UserController');

    Route::resource('posts', 'PostController');

    Route::get('criteria/data', 'CriteriaController@data')->name('criteria.data');
    Route::get('criteria/rusun', 'CriteriaController@rusun')->name('criteria.rusun.index');
    Route::get('criteria/rusus', 'CriteriaController@rusus')->name('criteria.rusus.index');
    Route::get('criteria/swadaya', 'CriteriaController@swadaya')->name('criteria.swadaya.index');
    Route::get('criteria/ruk', 'CriteriaController@ruk')->name('criteria.ruk.index');
    Route::get('criteria/create', 'CriteriaController@create')->name('criteria.create');
    Route::get('criteria/{id}/download', 'CriteriaController@download')->name('criteria.download');

    Route::put('criteria/{id}', 'CriteriaController@update')->name('criteria.update');
    Route::post('criteria', 'CriteriaController@store')->name('criteria.store');
    Route::get('criteria/{id}/edit', 'CriteriaController@edit')->name('criteria.edit');
    Route::delete('criteria/{id}', 'CriteriaController@destroy')->name('criteria.destroy');
//    Route::resource('criteria', 'CriteriaController');

    Route::get('agencies/data', 'AgencyController@data')->name('agencies.data');
    Route::get('agencies/select', 'AgencyController@select2Data')->name('agencies.select');
    Route::get('agencies/combo-tree', 'AgencyController@comboTree')->name('agencies.combotree');
    Route::resource('agencies', 'AgencyController');

    Route::get('tematics/data', 'TematicController@data')->name('tematics.data');
    Route::get('tematics/select', 'TematicController@select2Data')->name('tematics.select');
    Route::resource('tematics', 'TematicController');

//    Route::get('programs/data', 'ProgramController@data')->name('programs.data');
    Route::get('programs/select', 'ProgramController@select2Data')->name('programs.select');
//    Route::resource('programs', 'ProgramController');

//    Route::get('recipients/data', 'RecipientController@data')->name('recipients.data');
    Route::get('recipients/select', 'RecipientController@select2Data')->name('recipients.select');
//    Route::resource('recipients', 'RecipientController');

    // Program Kegiatan
    Route::get('programs/data', 'ProgramController@data')->name('programs.data');
    Route::resource('programs', 'ProgramController');

    // Penerima Manfaat
    Route::get('recipients/data', 'RecipientController@data')->name('recipients.data');
    Route::resource('recipients', 'RecipientController');

    Route::get('regions/data', 'RegionController@data')->name('regions.data');
    Route::get('regions/select', 'RegionController@select2Data')->name('regions.select');
    Route::get('regions/combo-tree', 'RegionController@comboTree')->name('regions.combotree');
    Route::resource('regions', 'RegionController');


    Route::get('dwelling-plans/data', 'DwellingPlanController@data')->name('dwellings.plans.data');
    Route::post('dwelling-plans', 'DwellingPlanController@store')->name('dwellings.plans.store');
    Route::get('dwelling-plans/create', 'DwellingPlanController@create')->name('dwellings.plans.create');
    Route::delete('dwelling-plans/{dwelling}', 'DwellingPlanController@destroy')->name('dwellings.plans.destroy');
    Route::put('dwelling-plans/{dwelling}', 'DwellingPlanController@update')->name('dwellings.plans.update');
    Route::get('dwelling-plans/{dwelling}/edit', 'DwellingPlanController@edit')->name('dwellings.plans.edit');

    Route::get('surveyors/data', 'SurveyorController@data')->name('proposals.surveyors.data');
    Route::get('surveyors/create', 'SurveyorController@create')->name('proposals.surveyors.create');
    Route::post('surveyors', 'SurveyorController@store')->name('proposals.surveyors.store');
    Route::get('surveyors/{surveyors}/edit', 'SurveyorController@edit')->name('proposals.surveyors.edit');
    Route::put('surveyors/{surveyors}', 'SurveyorController@update')->name('proposals.surveyors.update');
    Route::delete('surveyors/{surveyors}', 'SurveyorController@destroy')->name('proposals.surveyors.destroy');

    Route::get('surveys/data', 'SurveyController@data')->name('proposals.surveys.data');
    Route::get('surveys/create', 'SurveyController@create')->name('proposals.surveys.create');
    Route::post('surveys', 'SurveyController@store')->name('proposals.surveys.store');
    Route::get('surveys/download/{proposal}', 'SurveyController@download');//->name('proposals.surveys.documents');
    Route::get('surveys/{surveys}/edit', 'SurveyController@edit')->name('proposals.surveys.edit');
    Route::put('surveys/{surveys}', 'SurveyController@update')->name('proposals.surveys.update');
    Route::delete('surveys/{surveys}', 'SurveyController@destroy')->name('proposals.surveys.destroy');

    Route::get('usulan/rusun/create', 'ProposalController@createProposalRusun')->name('proposals.create.rusun');
    Route::get('usulan/rusus/create', 'ProposalController@createProposalRusus')->name('proposals.create.rusus');
    Route::get('usulan/swadaya/create', 'ProposalController@createProposalSwadaya')->name('proposals.create.swadaya');
    Route::get('usulan/ruk/create', 'ProposalController@createProposalRuk')->name('proposals.create.ruk');
    Route::post('usulan', 'ProposalController@store')->name('proposals.store');

    Route::post('usulan/status', 'ProposalController@changeStatus')->name('proposals.statuses.update');

    Route::post('usulan/documents', 'ProposalController@document')->name('proposals.documents');
    Route::post('usulan/criterias', 'ProposalController@updatePivotCriteria')->name('proposals.criterias');
    Route::get('usulan/download/{proposal}/all', 'ProposalController@downloadAll')->name('proposals.documents.download.all');
    Route::get('usulan/download/{proposal}/{criteria}', 'ProposalController@download')->name('proposals.documents.download');
    Route::get('usulan/data', 'ProposalController@data')->name('proposals.data');
    Route::get('usulan', ['uses' => 'ProposalController@index', 'as' => 'proposals.index']);
    Route::put('usulan/{usulan}', 'ProposalController@update')->name('proposals.update');
    Route::get('usulan/{usulan}/edit', ['uses' => 'ProposalController@edit', 'as' => 'proposals.edit']);
    Route::delete('usulan/{usulan}', ['uses' => 'ProposalController@destroy', 'as' => 'proposals.destroy']);
    Route::get('usulan/{usulan}/status/edit', ['uses' => 'ProposalController@editStatus'])->name('proposals.status.edit');
    Route::put('usulan/{usulan}/status', ['uses' => 'ProposalController@updateStatus'])->name('proposals.status.update');

    Route::post('comments', 'CommentController@store')->name('comments.store');

    // Shortlist
    Route::get('usulan/{id}/shortlist', 'ShortlistController@create')->name('shortlists.create');
    Route::get('shortlist/data', 'ShortlistController@data')->name('shortlists.data');
    Route::get('shortlist', 'ShortlistController@index')->name('shortlists.index');
    Route::post('shortlist', 'ShortlistController@store')->name('shortlists.store');
    Route::get('shortlist/{proposal}/edit', 'ShortlistController@edit')->name('shortlists.edit');
    Route::put('shortlist', 'ShortlistController@store')->name('shortlists.update');
    Route::get('shortlist/view', 'ShortlistController@view')->name('shortlists.view');
    Route::delete('shortlist', 'ShortlistController@destroy')->name('shortlists.destroy');

    // SK Penetapan
    Route::get('sk-penetapan', 'DecreeController@index')->name('decrees.index');
    Route::get('usulan/{id}/sk-penetapan', 'DecreeController@create')->name('decrees.create');
    Route::get('sk-penetapan/data', 'DecreeController@data')->name('decrees.data');
    Route::post('sk-penetapan', 'DecreeController@store')->name('decrees.store');
    Route::get('sk-penetapan/{proposal}/edit', 'DecreeController@edit')->name('decrees.edit');
    Route::put('sk-penetapan', 'DecreeController@store')->name('decrees.update');
    Route::delete('sk-penetapan/{proposal}', 'DecreeController@destroy')->name('decrees.destroy');
    Route::get('sk-penetapan/{proposal}/download', 'DecreeController@download')->name('decrees.download');

    // eprofile
     /* Route::get('usulan/{id}/eprofile', 'EprofileController@create')->name('eprofile.create');
    Route::get('eprofile', 'EprofileController@index')->name('eprofile.index');
    Route::post('eprofile', 'EprofileController@store')->name('eprofile.store');
    Route::get('eprofile/{id}/edit', 'EprofileController@edit')->name('eprofile.edit');
    Route::put('eprofile', 'EprofileController@store')->name('eprofile.update');
    Route::delete('eprofile', 'EprofileController@destroy')->name('eprofile.destroy');
    */
    Route::get('eprofile/data', 'EprofileController@data')->name('eprofile.data');
    Route::get('eprofile/advanced-excel', 'EprofileController@showFormAdvancedExcel');
    Route::get('eprofile/advanced-excel/rincian', 'EprofileController@exportRincian');
    Route::get('eprofile/advanced-excel/rekap', 'EprofileController@exportRekap');
    Route::resource('eprofile', 'EprofileController');

    Route::get('laporan', 'ReportController@index')->name('reports.index');
    Route::get('laporan/data', 'ReportController@data')->name('reports.data');

    Route::get('statistics/usulan', 'StatisticController@proposalIndex')->name('statistics.proposal.index');
    Route::get('statistics/usulan/units', 'StatisticController@proposalUnits')->name('statistics.proposal.units');
    Route::get('statistics/usulan/recipients', 'StatisticController@proposalRecipients')->name('statistics.proposal.recipients');
    Route::get('statistics/usulan/regions', 'StatisticController@proposalRegions')->name('statistics.proposal.regions');
    Route::get('statistics/usulan/table', 'StatisticController@proposalTable')->name('statistics.proposal.table');


    Route::get('statistics/eprofile', 'StatisticController@eprofileIndex')->name('statistics.eprofile.index');
    Route::get('statistics/eprofile/units', 'StatisticController@eprofileUnits')->name('statistics.eprofile.units');
    Route::get('statistics/eprofile/recipients', 'StatisticController@eprofileRecipients')->name('statistics.eprofile.recipients');
    Route::get('statistics/eprofile/regions', 'StatisticController@eprofileRegions')->name('statistics.eprofile.regions');

    # Notifications
    Route::get('notifications', 'NotificationController@index')->name('notifications.index');
    Route::get('notifications/history', 'NotificationController@history')->name('notifications.history');
    Route::get('notification/{notification}', 'NotificationController@show')->name('notifications.show');

    Route::group([
        'prefix'    => 'profil-provinsi'
    ], function() {
        Route::get('/', 'ProfileProvinceController@index')->name('profile.province.index');
        Route::get('data', 'ProfileProvinceController@data')->name('profile.province.data');
        Route::get('map', 'ProfileProvinceController@map')->name('profile.province.map');
        Route::get('map/data', 'ProfileProvinceController@mapData')->name('profile.province.map.data');


        Route::get('regions/profiles/data', 'RegionProfileController@data')->name('regions.profiles.data');
        Route::get('regions/profiles/{id}/asset', 'RegionProfileController@viewAsset')->name('regions.profiles.asset');
        Route::get('regions/{regions}/profile/{profile}/export', 'RegionProfileController@export')->name('regions.profiles.export');
        Route::resource('regions.profiles', 'RegionProfileController');
        Route::resource('regions.specials', 'RegionSpecialController');

    });

});

