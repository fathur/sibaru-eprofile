const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
        // 'resources/assets/js/metronic/app.js',
        'resources/assets/js/metronic/layout.js',
        'resources/assets/js/metronic/quick-sidebar.js',
        //'resources/assets/js/metronic/quick-nav.js'
    ], 'public/js/app.js');
